import {Id, IDbContext} from '../shared/db-context';
import Nedb from '@seald-io/nedb';
import {StringUtils} from '../shared/string-utils';

export class TestDbService implements IDbContext {

  private _dbs: { [name: string]: Nedb } = {};

  constructor() {
  }

  loadData<T>(dbOld: string): Promise<T[]> {
    throw new Error('Method not implemented.');
  }

  existsDbFile(commandsDbName: string): Promise<boolean> {
    throw new Error('Method not implemented.');
  }

  getDbDirectoryPath(): Promise<string> {
    throw new Error('Method not implemented.');
  }

  private existsDb(db: string) {
    return !!this._dbs[StringUtils.toFileName(db)];
  }


  public async createDb(db: string): Promise<boolean> {
    const dbName = StringUtils.toFileName(db);
    if (this._dbs[dbName]) {
      return;
    }
    const builder = window.require('@seald-io/nedb');
    let datastore: Nedb = new builder({inMemory: true});
    try {
      await datastore.loadDatabaseAsync();
      this._dbs[dbName] = datastore;
      return true;
    } catch (e) {
      console.error('could not load db', e);
      return false;
    }
  }

  public init(db: string): Promise<boolean> {
    if (!this.existsDb(db)) {
      return this.createDb(db);
    }
    return Promise.resolve(true);
  }

  public findOne<T>(query: any, db: string): Promise<T> {
    const dbName = StringUtils.toFileName(db);
    return this._dbs[dbName].findOneAsync(query);
  }

  public find<T>(query: any, db: string): Promise<T[]> {
    const dbName = StringUtils.toFileName(db);
    return this._dbs[dbName].findAsync(query);
  }

  public add<T>(element: T, db: string): Promise<T> {
    const dbName = StringUtils.toFileName(db);
    return this._dbs[dbName].insertAsync(element);
  }

  public addAll<T>(elements: T[], db: string): Promise<T[]> {
    const dbName = StringUtils.toFileName(db);
    return this._dbs[dbName].insertAsync(elements);
  }


  public async update<T extends Id>(element: T, db: string): Promise<void> {
    const dbName = StringUtils.toFileName(db);
    await this._dbs[dbName].updateAsync({_id: element._id}, element);
  }

  public async remove(id: string, db: string): Promise<void> {
    const dbName = StringUtils.toFileName(db);
    await this._dbs[dbName].remove({_id: id}, {});
  }

  public async exists(id: string, db: string): Promise<boolean> {
    const dbName = StringUtils.toFileName(db);
    const elem = await this.findOne({_id: id}, dbName);
    return !!elem;
  }

  public async upsert<T extends Id>(element: T, db: string): Promise<void> {
    const dbName = StringUtils.toFileName(db);
    await this._dbs[dbName].updateAsync({_id: element._id}, element, {upsert: true});
  }

  public async getAll<T extends Id>(dbName: string): Promise<T[]> {
    return this.find<T>({}, dbName);
  }

  public async reload(): Promise<void> {
    for (const dbName in this._dbs) {
      await this._dbs[dbName].loadDatabaseAsync();
    }
  }

}
