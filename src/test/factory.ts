import {ElectronService} from '../renderer/app/+shared/services/electron/electron.service';
import {UpdateService} from '../renderer/app/+shared/services/update/update.service';
import {DbService} from '../renderer/app/+shared/services/db/db.service';
import {SettingsService} from '../renderer/app/+shared/services/settings/settings.service';
import {TestHelper} from './helper';
import {ColorService} from '../renderer/app/+shared/services/color/color.service';
import {TabFactory} from '../renderer/app/+shared/abstract-components/tab/+state/tab.factory';
import {AutocompleteService} from '../renderer/app/autocomplete/+state/autocomplete.service';
import {Renderer} from '../renderer/app/terminal/+state/renderer/renderer';
import {GlobalMenuService} from '../renderer/app/+shared/abstract-components/menu/+state/global-menu.service';
import {GridService} from '../renderer/app/global/grid/+state/grid.service';
import {KeyboardService} from '../renderer/app/+shared/services/keyboard/keyboard.service';
import {ShellsLoaderService} from '../renderer/app/+shared/services/shells/shells-loader.service';
import {GeneralSettingsService} from '../renderer/app/settings/general/+state/general.settings.service';
import {SettingsTabService} from '../renderer/app/settings/+state/settings-tab.service';
import {ThemesSettingsService} from '../renderer/app/settings/themes/+state/themes.settings.service';
import {ThemesLoaderService} from '../renderer/app/settings/themes/+state/themes.loader.service';
import {FontsLoaderService} from '../renderer/app/+shared/services/fonts/fonts-loader.service';
import {NotificationService} from '../renderer/app/+shared/notification/notification.service';
import {WorkspacesService} from '../renderer/app/workspaces-menu/+state/workspaces.service';
import {NgZone, Renderer2, RendererFactory2, RendererType2} from '@angular/core';
import {PluginFileManager} from '../renderer/app/+shared/services/plugins/plugin.file.manager';
import {PluginsSettingsService} from '../renderer/app/settings/plugins/+state/plugins.settings.service';
import {PluginsLoader} from '../renderer/app/settings/plugins/+state/plugins.loader';
import {PluginSuggesterManager} from '../renderer/app/autocomplete/+state/suggester/plugins/plugin-suggester.manager';
import {HistorySuggesterManager} from '../renderer/app/autocomplete/+state/suggester/history/history-suggester.manager';
import {WindowService} from '../renderer/app/+shared/services/window/window.service';
import {OnboardingService} from '../renderer/app/onboarding/+state/onboarding.service';
import {rootStore} from '../renderer/app/common/store/store';
import {TerminalService} from '../renderer/app/terminal/+state/terminal.service';
import {PasteService} from '../renderer/app/global/paste-history-menu/+state/paste.service';
import {Shell} from '../renderer/app/terminal/+state/shell/shell';
import {DecorationHandler} from '../renderer/app/terminal/+state/handler/decoration.handler';
import {SelectionHandler} from '../renderer/app/terminal/+state/handler/selection.handler';
import {KeytipService} from '../renderer/app/+shared/services/keyboard/keytip.service';
import {Terminal} from '@xterm/xterm';
import {TerminalStatusObserver} from '../renderer/app/terminal/+state/osc/terminal-status.observer';
import {SettingsHandler} from '../main/settings.handler';
import getWindowSettings = SettingsHandler.getWindowSettings;
import {TitlebarMenuService} from '../renderer/app/global/titlebar-menu/+state/titlebar-menu.service';
import {TabContextMenuService} from '../renderer/app/global/tab-context-menu/+state/tab-context-menu.service';
import {InitService} from '../renderer/app/+shared/services/init/init.service';
import {TelemetryService} from '../renderer/app/+shared/services/analytics/telemetry.service';
import {WindowButtonsService} from '../renderer/app/global/window-buttons/+state/window-buttons.service';
import {TestDbService} from './test-db';
import {IDbContext} from '../shared/db-context';

let electronService: ElectronService = null;
let updateService: UpdateService = null;
let settingsService: SettingsService = null;
let initService: InitService = null;
let telemetryService: TelemetryService = null;
let windowButtonService: WindowButtonsService = null;
let generalSettingsService: GeneralSettingsService = null;
let titlbarMenuService: TitlebarMenuService = null;
let tabContextMenuService: TabContextMenuService = null;
let themesSettingsService: ThemesSettingsService = null;
let colorService: ColorService = null;
let tabFactory: TabFactory = null;
let keyboardService: KeyboardService = null;
let keytipService: KeytipService = null;
let globalMenuService: GlobalMenuService = null;
let gridService: GridService = null;
let dbAdapter: DbService = null;
let historySuggesterManager: HistorySuggesterManager = null;
let autocompleterService: AutocompleteService = null;
let shellsLoaderService: ShellsLoaderService = null;
let themesLoaderService: ThemesLoaderService = null;
let fontsLoaderService: FontsLoaderService = null;
let settingsTabService: SettingsTabService = null;
let pluginsSettingsService: PluginsSettingsService = null;
let notificationService: NotificationService = null;
let workspaceService: WorkspacesService = null;
let pluginSuggesterManager: PluginSuggesterManager = null;
let pluginsLoader: PluginsLoader = null;
let pluginFileManager: PluginFileManager = null;
let renderer: Renderer = null;
let windowService: WindowService = null;
let onboardingService: OnboardingService = null;
let terminalService: TerminalService = null;
let pasteService: PasteService = null;
let shell: Shell = null;
let decorationHandler: DecorationHandler = null;
let selectionHandler: SelectionHandler = null;
let terminalStatusObserver: TerminalStatusObserver = null;
let ngzone: NgZone = null;

export function clear() {
  electronService = null;
  settingsService = null;
  generalSettingsService = null;
  themesSettingsService = null;
  tabFactory = null;
  keyboardService = null;
  keytipService = null;
  globalMenuService = null;
  gridService = null;
  dbAdapter = null;
  historySuggesterManager = null;
  autocompleterService = null;
  shellsLoaderService = null;
  themesLoaderService = null;
  fontsLoaderService = null;
  settingsTabService = null;
  notificationService = null;
  workspaceService = null;
  pluginFileManager = null;
  pluginsSettingsService = null;
  pluginSuggesterManager = null;
  pluginsLoader = null;
  windowService = null;
  renderer = null;
  ngzone = null;
  onboardingService = null;
  terminalService = null;
  pasteService = null;
  decorationHandler = null;
  selectionHandler = null;
  terminalStatusObserver = null;
  initService = null;
  telemetryService = null;
  windowButtonService = null;
  jest.clearAllMocks();
  rootStore.clearAllStores();
}

export function getTerminalService(): TerminalService {
  if (!terminalService) {
    terminalService = new TerminalService(
      getGridService(),
      getAutocompleterService(),
      getSettingsService(),
      getWindowService(),
      getPasteService(),
      getNotificationService(),
      getElectronService(),
      getShell(),
      getRenderer(),
      getSelectionHandler(),
      getTerminalStatusObserver()
    );
  }
  return terminalService;
}

export function getDecorationHandler(): DecorationHandler {
  if (!decorationHandler) {
    decorationHandler = new DecorationHandler();
  }
  return decorationHandler;
}

export function getSelectionHandler(): SelectionHandler {
  if (!selectionHandler) {
    selectionHandler = new SelectionHandler(getRenderer());
  }
  return selectionHandler;
}

export function getTerminalStatusObserver(): TerminalStatusObserver {
  if (!terminalStatusObserver) {
    terminalStatusObserver = new TerminalStatusObserver(getNgZone());
  }
  return terminalStatusObserver;
}

export function getShell(): Shell {
  if (!shell) {
    shell = new Shell(electronService);
  }
  return shell;
}

export function getPasteService() {
  if (!pasteService) {
    pasteService = new PasteService(getDbAdapter(), getGlobalMenuService());
  }
  return pasteService;
}

export function getOnboardingService(): OnboardingService {
  if(!onboardingService) {
    onboardingService = new OnboardingService(getShellsLoaderService(), getFontsLoaderService(), getElectronService(), getSettingsService());
  }
  return onboardingService;
}

export function getElectronService(): ElectronService {
  if(!electronService){
    electronService = new ElectronService();
  }
  return electronService;
}

export function getPluginFileManager(): PluginFileManager {
  if(!pluginFileManager){
    pluginFileManager = new PluginFileManager(getElectronService());
  }
  return pluginFileManager;
}
export function getPluginSuggesterManager(): PluginSuggesterManager {
  if(!pluginSuggesterManager){
    pluginSuggesterManager = new PluginSuggesterManager(getPluginFileManager(), getSettingsService());
  }
  return pluginSuggesterManager;
}

export function getNotificationService(): NotificationService {
  if(!notificationService){
    notificationService = new NotificationService();
  }
  return notificationService;
}

export function getWorkspaceService(): WorkspacesService {
  if(!workspaceService) {
    workspaceService = new WorkspacesService(getGridService(), getDbAdapter(), getGlobalMenuService(), getTabFactory(), getElectronService(), getNgZone());
  }
  return workspaceService;
}
export function getShellsLoaderService(): ShellsLoaderService {
  if(!shellsLoaderService){
    shellsLoaderService = new ShellsLoaderService(getElectronService());
  }
  return shellsLoaderService;
}

export function getThemesLoaderService(): ThemesLoaderService {
  if(!themesLoaderService){
    themesLoaderService = new ThemesLoaderService(getElectronService());
  }
  return themesLoaderService;
}

export function getFontsLoaderService(): FontsLoaderService {
  if(!fontsLoaderService){
    fontsLoaderService = new FontsLoaderService(getElectronService());
  }
  return fontsLoaderService;
}

export function getUpdateService(): UpdateService {
  if (!updateService) {
    updateService = new UpdateService(getElectronService());
  }
  return updateService;
}

export function getDbAdapter(): DbService {
  if(!dbAdapter){
    dbAdapter = new TestDbService() as any as DbService;
  }
  return dbAdapter;
}

export function getNgZone(): NgZone {
  if(!ngzone){
    ngzone = {
      run(methode: () => void){methode();}
    } as NgZone;
  }
  return ngzone;
}

export function getInitService(): InitService {
  if(!initService) {
    initService = new InitService(getElectronService(), getSettingsService(), getTelemetryService(), getPasteService(), getWindowButtonService(), getNgZone());
  }
  return initService;
}

export function getTelemetryService(): TelemetryService {
  if(!telemetryService) {
    telemetryService = new TelemetryService(getElectronService());
  }
  return telemetryService;
}

export function getWindowButtonService(): WindowButtonsService {
  if(!windowButtonService) {
    windowButtonService = new WindowButtonsService(getElectronService(), getSettingsService(), getUpdateService(), getNotificationService(), getWorkspaceService(), getGridService());
  }
  return windowButtonService;
}

export function getSettingsService(): SettingsService {
  if(!settingsService){
    settingsService = new SettingsService();
    settingsService.update(TestHelper.createSettingsResult());
  }
  return settingsService;
}

export function getGeneralSettingsService(): GeneralSettingsService {
  if(!generalSettingsService){
    generalSettingsService = new GeneralSettingsService(getElectronService(), getSettingsService(), getShellsLoaderService(), getSettingsTabService());
  }
  return generalSettingsService;
}

export function getTitlebarMenuService(): TitlebarMenuService {
  if(!titlbarMenuService) {
    titlbarMenuService = new TitlebarMenuService(getGlobalMenuService(), getElectronService(), getSettingsService(), getGridService());
  }
  return titlbarMenuService;
}

export function getTabContextMenuService(): TabContextMenuService {
  if(!tabContextMenuService) {
    tabContextMenuService = new TabContextMenuService(getGlobalMenuService(), getGridService());
  }
  return tabContextMenuService;
}

export function getThemesSettingsService(): ThemesSettingsService {
  if(!themesSettingsService){
    themesSettingsService = new ThemesSettingsService(getElectronService(), getThemesLoaderService(), getFontsLoaderService(), getSettingsService(), getSettingsTabService(), getNotificationService(), getWindowService());
  }
  return themesSettingsService;
}

export function getPluginsLoader(): PluginsLoader {
  if(!pluginsLoader){
    pluginsLoader = new PluginsLoader(getElectronService());
  }
  return pluginsLoader;
}

export function getPluginsSettingsService(): PluginsSettingsService {
  if(!pluginsSettingsService){
    pluginsSettingsService = new PluginsSettingsService(
      getSettingsTabService(),
      getElectronService(),
      getPluginsLoader(),
      getPluginFileManager(),
      getPluginSuggesterManager()
    );
  }
  return pluginsSettingsService;
}
export function getSettingsTabService(): SettingsTabService {
  if(!settingsTabService){
    settingsTabService = new SettingsTabService(getGridService(), getElectronService(), getKeyboardService());
  }
  return settingsTabService;
}

export function getColorService(): ColorService {
  if(!colorService){
    colorService = new ColorService(getSettingsService());
  }
  return colorService;
}
export function getTabFactory(): TabFactory {
  if(!tabFactory){
    tabFactory = new TabFactory(getSettingsService());
  }
  return tabFactory;
}

export function getKeyboardService(): KeyboardService {
  if(!keyboardService){
    keyboardService = new KeyboardService(getSettingsService(), getNotificationService());
  }
  return keyboardService;
}

export function getKeytipService(): KeytipService {
  if(!keytipService){
    keytipService = new KeytipService(getKeyboardService());
  }
  return keytipService;
}

export function getAutocompleterService(): AutocompleteService {
  if(!autocompleterService){
    autocompleterService = new AutocompleteService(getHistorySuggesterManager(), getPluginSuggesterManager(), getGridService(), getNotificationService());
  }
  return autocompleterService;
}

export function getRenderer(): Renderer {
  if(!renderer){
    renderer = new Renderer(getSettingsService());
  }
  return renderer;
}

export function getGlobalMenuService(): GlobalMenuService {
  if(!globalMenuService){
    globalMenuService = new GlobalMenuService();
  }
  return globalMenuService;
}

export function getGridService(): GridService {
  if(!gridService){
    gridService = new GridService(getSettingsService(), getTabFactory(), getKeyboardService(), getGlobalMenuService(), getWindowService(), getUpdateService());
  }
  return gridService;
}

export function getHistorySuggesterManager(): HistorySuggesterManager {
  if(!historySuggesterManager){
    historySuggesterManager = new HistorySuggesterManager(getDbAdapter(), getSettingsService());
  }
  return historySuggesterManager;
}

export function getWindowService(): WindowService {
  if(!windowService){
    windowService = new WindowService(getElectronService());
  }
  return windowService;
}
