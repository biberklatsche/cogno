import {
  clear,
  getGlobalMenuService,
  getSettingsService,
  getKeyboardService, getKeytipService
} from '../../../test/factory';
import {ActionsComponent} from "./actions.component";
import {ActionsService} from "./+state/actions.service";
import {Key} from "../common/key";
import {ElementRef} from "@angular/core";
import {GlobalMenuService} from "../+shared/abstract-components/menu/+state/global-menu.service";
import {KeyboardService} from "../+shared/services/keyboard/keyboard.service";
import {SettingsService} from "../+shared/services/settings/settings.service";
import {KeytipService} from '../+shared/services/keyboard/keytip.service';

describe('ActionsComponent', () => {
  let component: ActionsComponent;
  let service: ActionsService;
  let nativeElement: any;
  let menuService: GlobalMenuService;
  let keyboardService: KeyboardService;
  let keytipService: KeytipService;
  let settingsService: SettingsService;

  beforeEach(() => {
    clear();
    settingsService = getSettingsService();
    keyboardService = getKeyboardService();
    keytipService = getKeytipService();
    menuService = getGlobalMenuService();
    nativeElement = {blur: () => {}};
    const elemRef: ElementRef = {nativeElement: nativeElement}

    service = new ActionsService(settingsService, keyboardService, menuService);
    component = new ActionsComponent(elemRef, keyboardService, keytipService, service);
  });

  it('should handle input change - select Shortcut', (done) => {
    component.inputChanged({key: Key.ArrowDown} as KeyboardEvent);
    service.selectSelectedShortcut().subscribe(s => {
      expect(s).toEqual(settingsService.getShortcutsAsArray()[0]);
      done();
    });
  });

  it('should handle input change - execute Shortcut Alias', (done) => {
    jest.spyOn(nativeElement, 'blur').mockImplementation(() => {});
    keyboardService.onAlias().subscribe(s => {
      expect(s).toEqual(settingsService.getShortcutsAsArray()[0]);
      done();
    });

    component.inputChanged({key: Key.ArrowDown} as KeyboardEvent);
    component.inputChanged({key: Key.Enter} as KeyboardEvent);
    expect(nativeElement.blur).toBeCalled();
    expect(menuService.getCurrentActiveMenu()).toEqual('AllClosed');
  });

  it('should handle input change - execute Shortcut', (done) => {
    jest.spyOn(nativeElement, 'blur').mockImplementation(() => {});
    keyboardService.onShortcut().subscribe(s => {
      expect(s).toEqual(settingsService.getShortcutsAsArray()[1]);
    });
    component.inputChanged({key: Key.ArrowDown} as KeyboardEvent);
    component.inputChanged({key: Key.ArrowDown} as KeyboardEvent);
    component.inputChanged({key: Key.ArrowDown} as KeyboardEvent);
    component.inputChanged({key: Key.Enter} as KeyboardEvent);
    expect(nativeElement.blur).toBeCalled();
    expect(menuService.getCurrentActiveMenu()).toEqual('AllClosed');
    done();
  });
});
