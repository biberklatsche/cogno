import {Component, ElementRef, ViewChild} from '@angular/core';
import {combineLatest, Observable} from 'rxjs';
import {Shortcut} from '../../../shared/models/shortcuts';
import {map, tap} from 'rxjs/operators';
import {MenuComponent} from '../+shared/abstract-components/menu/menu.component';
import {Key} from '../common/key';
import {ActionsService} from './+state/actions.service';
import {KeyboardService} from '../+shared/services/keyboard/keyboard.service';
import {CommonModule} from '@angular/common';
import {ShortcutPipe} from '../+shared/pipes/shortcut/shortcut.pipe';
import {KeytipService} from '../+shared/services/keyboard/keytip.service';

@Component({
    selector: 'app-actions',
    templateUrl: './actions.component.html',
    styleUrls: ['./actions.component.scss'],
    imports: [
        CommonModule,
        ShortcutPipe
    ]
})
export class ActionsComponent extends MenuComponent {

  @ViewChild('inputElem')
  private inputElement: ElementRef;

  @ViewChild('shortcutsElem')
  private shortcutsElement: ElementRef;

  public search: string;
  public shortcuts: Observable<Shortcut[]>;
  public selectedShortcut: Observable<Shortcut | null>;
  public isActive: Observable<boolean>;
  constructor(protected elementRef: ElementRef,
              protected keyboardService: KeyboardService,
              protected keytipService: KeytipService,
              protected actionService: ActionsService) {
    super(elementRef, actionService, keyboardService, keytipService);
    super.openOnShortcut('showActions');
    this.shortcuts = actionService.selectFilteredShortcuts();
    this.selectedShortcut = actionService.selectSelectedShortcut();
    this.subscriptions.push(combineLatest([this.shortcuts, this.selectedShortcut]).pipe(
      map(([shortcuts, selectedShortcut]) => shortcuts.findIndex(s => selectedShortcut?.key === s.key)),
      tap((index) => {
        this.scrollToIndex(index);
      })
    ).subscribe());
    this.isActive = this.actionService.selectIsActive().pipe(
      tap((isActive) => {
        if(isActive) {
          setTimeout(() => {
            this.inputElement.nativeElement.value = '';
            this.inputElement.nativeElement.focus();
          });
        }
      })
    );
  }

  inputChanged(event: KeyboardEvent): void {
    if (event.key === Key.Enter) {
      this.closeMenu();
      this.actionService.executeSelectedAction();
    }
    this.actionService.updateFilter(event);
  }

  shortcutClicked(shortcut: Shortcut) {
    this.closeMenu();
    this.actionService.executeShortcut(shortcut);
  }

  scrollToIndex(index: number) {
    if (!this.shortcutsElement || index === -1) {
      return;
    }
    const list = this.shortcutsElement.nativeElement as HTMLUListElement;
    if (list.children.length <= index) {
      return;
    }
    const targetLi = list.children.item(index);
    targetLi.scrollIntoView(false);
  }
}
