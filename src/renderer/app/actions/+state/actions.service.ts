import {Injectable, OnDestroy} from '@angular/core';
import {Observable, Subscription} from 'rxjs';
import {Key} from '../../common/key';
import {GlobalMenuService} from '../../+shared/abstract-components/menu/+state/global-menu.service';
import {Shortcut} from '../../../../shared/models/shortcuts';
import {createStore} from '../../common/store/store';
import {SettingsService} from '../../+shared/services/settings/settings.service';
import {MenuService} from '../../+shared/abstract-components/menu/menu.service';
import {KeyboardService} from '../../+shared/services/keyboard/keyboard.service';

export interface ActionsState {
  allShortcuts: Shortcut[];
  filteredShortcuts: Shortcut[];
  selectedShortcut: Shortcut;
};

const initialState: ActionsState = {
  allShortcuts: [],
  filteredShortcuts: [],
  selectedShortcut: null
};

@Injectable({
  providedIn: 'root'
})
export class ActionsService extends MenuService implements OnDestroy {
  private store = createStore('actions', initialState);
  private subscriptions: Subscription[] = [];

  constructor(readonly settingsService: SettingsService,
              private readonly KeyboardService: KeyboardService,
              menuService: GlobalMenuService) {
    super('Actions', menuService);
    this.subscriptions.push(
      settingsService.selectShortcutsAsArray().subscribe((shortcuts) => {
        this.store.update({allShortcuts: shortcuts || [], filteredShortcuts: [...shortcuts || []], selectedShortcut: null});
      })
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  executeSelectedAction() {
    this.KeyboardService.executeShortcut(this.store.get(s => s.selectedShortcut));
  }

  public updateFilter(event: KeyboardEvent) {
    switch (event.key) {
      case Key.Escape:
      case Key.Enter: {
        this.store.update({selectedShortcut: null, filteredShortcuts: [...this.store.get(s => s.allShortcuts)]});
        break;
      }
      case Key.ArrowDown: {
        const shortcuts = this.store.get(s => s.filteredShortcuts);
        let index = shortcuts.findIndex(s => s.key === this.store.get(s => s.selectedShortcut)?.key);
        index = ++index < 0 || shortcuts.length === 0 ? shortcuts.length - 1 : index % shortcuts.length;
        this.store.update({selectedShortcut: shortcuts[index]});
        break;
      }
      case Key.ArrowUp: {
        const shortcuts = this.store.get(s => s.filteredShortcuts);
        let index = shortcuts.findIndex(s => s.key === this.store.get(s => s.selectedShortcut)?.key);
        index = --index < 0 || shortcuts.length === 0 ? shortcuts.length - 1 : index % shortcuts.length;
        this.store.update({selectedShortcut: shortcuts[index]});
        break;
      }
      default: {
        const filter = (event.target as HTMLInputElement).value;
        const filteredShortcuts = this.store.get(s => s.allShortcuts).filter(s => s.label.toLowerCase().indexOf(filter.toLowerCase()) !== -1);
        const selectedShortcut = filteredShortcuts.find(s => s.key === this.store.get(s => s.selectedShortcut)?.key) || filteredShortcuts[0];
        this.store.update({filteredShortcuts, selectedShortcut});
      }
    }
  }

  selectFilteredShortcuts(): Observable<Shortcut[]> {
    return this.store.select(s => s.filteredShortcuts);
  }

  selectSelectedShortcut(): Observable<Shortcut> {
    return this.store.select(s => s.selectedShortcut);
  }

  executeShortcut(shortcut: Shortcut) {
    this.KeyboardService.executeShortcut(shortcut);
  }
}
