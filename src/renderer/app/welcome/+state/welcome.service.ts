import {Injectable, OnDestroy} from '@angular/core';
import {TabType} from '../../../../shared/models/models';
import {GridService} from '../../global/grid/+state/grid.service';
import {BinaryTree} from '../../common/tree/binary-tree';
import {ElectronService} from '../../+shared/services/electron/electron.service';

@Injectable()
export class WelcomeService{

  constructor(private gridService: GridService, private electron: ElectronService) {

  }


  openTerminal() {
    this.gridService.addNewTab(BinaryTree.ROOT_KEY, TabType.Terminal);
  }

  openSettings() {
    this.gridService.addNewTab(BinaryTree.ROOT_KEY, TabType.Settings);
  }

  openGit() {
    this.electron.openGit();
  }

  openReportIssue() {
    this.electron.openReportIssue();
  }
}
