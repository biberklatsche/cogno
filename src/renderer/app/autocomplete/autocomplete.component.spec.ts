import {
  clear,
  getGridService,
  getHistorySuggesterManager, getNotificationService,
  getPluginSuggesterManager,
  getSettingsService,
} from '../../../test/factory';
import {ElementRef} from '@angular/core';
import {SettingsService} from '../+shared/services/settings/settings.service';
import {AutocompleteComponent} from './autocomplete.component';
import {AutocompleteService} from './+state/autocomplete.service';
import {GridService} from '../global/grid/+state/grid.service';
import {TestHelper} from '../../../test/helper';
import {Suggestion, SuggestionType} from './+state/suggester/suggester';
import {HistorySuggesterManager} from './+state/suggester/history/history-suggester.manager';
import {ShellLocation} from '../common/shellLocation';
import {CursorPosition, ShellType} from '../../../shared/models/models';
import {AutocompleteConfig} from '../../../shared/models/settings';

describe('AutocompleteComponent', () => {
  let component: AutocompleteComponent;
  let service: AutocompleteService;
  let nativeElement: any;
  let gridService: GridService;
  let settingsService: SettingsService;
  let suggestFactory: HistorySuggesterManager;

  beforeEach(() => {
    clear();
    settingsService = getSettingsService();
    gridService = getGridService();
    suggestFactory = getHistorySuggesterManager();
    nativeElement = {contains: (): boolean => true, parentElement: {}};
    const elemRef: ElementRef = {nativeElement: nativeElement};
    service = new AutocompleteService(suggestFactory, getPluginSuggesterManager(), gridService, getNotificationService());
    component = new AutocompleteComponent(service, settingsService, elemRef);
  });

  it('should handle click correct - suggestion exists, click is inside off autocomplete window', async () => {
    const location = new ShellLocation('lars', ShellType.Bash);
    await service.initSuggester(location);
    await service.add({command: 'git push', directory: ['c'], location: location, commandStatus: {returnCode: 0, isRunning: false, startTime: 0, isSuccessfulReturn: true}});
    await service.suggest({
      location: location,
      input: 'gi',
      directory: ['c'],
      cursorPosition: undefined
    });
    jest.spyOn(nativeElement, 'contains').mockReturnValue(true);
    component.click({});
    expect(service.getShow()).toBeTruthy();
  });

  it('should handle click correct - suggestion exists, click is outside off autocomplete window', async () => {
    const location = new ShellLocation('lars', ShellType.Bash);
    await service.initSuggester(location);
    await service.add({command: 'git push', directory: ['c'], location: location, commandStatus: {returnCode: 0, isRunning: false, startTime: 0, isSuccessfulReturn: true}});
    await service.suggest({
      location: location,
      input: 'gi',
      directory: ['c'],
      cursorPosition: undefined
    });
    jest.spyOn(nativeElement, 'contains').mockReturnValue(false);
    component.click({});
    expect(service.getShow()).toBeFalsy();
  });

  it('should return correct icon type ', () => {
    expect(component.getIconByType(SuggestionType.GlobalCommand)).toEqual('mdiDesktopClassic');
    expect(component.getIconByType(SuggestionType.SemiLocalCommand)).toEqual('mdiStarOutline');
    expect(component.getIconByType(SuggestionType.LocalCommand)).toEqual('mdiStar');
    expect(component.getIconByType(SuggestionType.Navigation)).toEqual('mdiFolderMove');
  });

  it('should calculate position correct - move with cursor', async () => {
    const autocompleteSettings: AutocompleteConfig = {
      mode: 'always', ignore: [],
      position: 'cursor'
    }
    settingsService.update({
      settings: {
        ...TestHelper.createSettingsResult().settings,
        autocomplete: autocompleteSettings
      }
    });
    jest.replaceProperty(nativeElement, 'parentElement', {offsetWidth: 1000, offsetHeight: 500});
    const position = component.calculatePosition({
      cursorPosition: {
        x: undefined,
        y: undefined,
        width: undefined,
        height: 10,
        windowX: 11,
        windowY: undefined,
        appWindowX: 13,
        appWindowY: 14,
        line: undefined,
        offsetX: 0
      },
      suggestions: [
        {label: 'abc'} as Suggestion
      ],
      show: true
    });
    expect(position).toEqual({top: '26px', left: '13px', height: '32.2px', width: '68px'});
  });

  it('should calculate position correct - line position', async () => {
    const autocompleteSettings: AutocompleteConfig = {
      mode: 'always', ignore: [],
      position: 'line'
    }
    settingsService.update({
      settings: {
        ...TestHelper.createSettingsResult().settings,
        autocomplete: autocompleteSettings
      }
    });
    jest.replaceProperty(nativeElement, 'parentElement', {offsetWidth: 1000, offsetHeight: 500});
    const position = component.calculatePosition({
      cursorPosition: {
        x: undefined,
        y: undefined,
        width: undefined,
        height: 10,
        windowX: 11,
        windowY: undefined,
        appWindowX: 13,
        appWindowY: 14,
        line: undefined,
        offsetX: 0
      },
      suggestions: [
        {label: 'abc'} as Suggestion
      ],
      show: true
    });
    expect(position).toEqual({top: '26px', left: '11px', height: '32.2px', width: '68px'});
  });
});
