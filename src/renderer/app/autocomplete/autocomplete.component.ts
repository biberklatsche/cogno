import {
  Component,
  ElementRef,
  HostListener,
  Input,
  Pipe,
  PipeTransform,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {Observable} from 'rxjs';
import {map, tap} from 'rxjs/operators';
import {DomSanitizer} from '@angular/platform-browser';
import {AutocompleteService} from './+state/autocomplete.service';
import {CommonModule} from '@angular/common';
import {IconComponent} from '../+shared/components/icon/icon.component';
import {calcHtml} from './html-highlighter';
import {CursorPosition} from '../../../shared/models/models';
import {Icon} from '../+shared/components/icon/icon';
import {SettingsService} from '../+shared/services/settings/settings.service';
import {Suggestion, SuggestionType} from './+state/suggester/suggester';
import {GridService} from '../global/grid/+state/grid.service';

@Pipe({
  name: 'safeHtml',
  standalone: true
})
export class SafeHtmlPipe implements PipeTransform {
  constructor(private sanitized: DomSanitizer) {
  }

  transform(value) {
    return this.sanitized.bypassSecurityTrustHtml(value);
  }
}

@Component({
    selector: 'app-autocomplete',
    templateUrl: './autocomplete.component.html',
    styleUrls: ['./autocomplete.component.scss'],
    encapsulation: ViewEncapsulation.None,
    imports: [
        CommonModule,
        IconComponent,
        SafeHtmlPipe
    ]
})
export class AutocompleteComponent {

  @Input()
  public id: string;

  @ViewChild('suggestionsElem')
  private suggestionsElement: ElementRef;

  suggestionInfo: Observable<{suggestions: Suggestion[]; position: { top: string; left: string; width: string; height: string}; show: boolean}>;
  selectedIndex: Observable<number>;
  selectedSuggestion: Observable<Suggestion>;
  show: Observable<boolean>;

  SuggestionType = SuggestionType;

  constructor(private autocompleteService: AutocompleteService, private settingsService: SettingsService, private elem: ElementRef) {
    this.show = autocompleteService.selectShow();
    this.selectedSuggestion = this.autocompleteService.selectSelectedSuggestion();
    this.selectedIndex = this.autocompleteService.selectSelectedIndex().pipe(
      tap((index) => {
        this.scrollToIndex(index);
      })
    );
    this.suggestionInfo = autocompleteService.selectSuggestionsInfo().pipe(
      map((s) => {
        let position = null;
        if(s.suggestions && s.cursorPosition) {
          s.suggestions.forEach(suggestion => {
            suggestion['html'] = calcHtml(suggestion);
            suggestion['class'] = SuggestionType[suggestion.suggestionType].toLowerCase();
          });
          position = this.calculatePosition(s);
        }
        return {suggestions: s.suggestions, position: position, show: s.show};
      }));
  }

  @HostListener('document:click', ['$event'])
  click(event) {
    if (this.isClickOutside(event)) {
     this.autocompleteService.hide();
    }
  }

  public getIconByType(type: SuggestionType): Icon {
    switch (type) {
      case SuggestionType.GlobalCommand:
        return 'mdiDesktopClassic';
      case SuggestionType.LocalCommand:
        return 'mdiStar';
      case SuggestionType.SemiLocalCommand:
        return 'mdiStarOutline';
      case SuggestionType.Navigation:
        return 'mdiFolderMove';
      case SuggestionType.PluginCommand:
        return 'mdiToyBrick';
    }
  }

  async selectSuggestion(index: number) {
    this.autocompleteService.selectFinally(index);
  }

  private isClickOutside(event) {
    return this.autocompleteService.getShow() && !this.elem.nativeElement.contains(event.target);
  }

  calculatePosition(info: {cursorPosition: CursorPosition; suggestions: Suggestion[]; show: boolean}): { top: string; left: string; width: string; height: string} {
    const terminalWindowWidth = this.elem.nativeElement.parentElement.offsetWidth;
    const terminalWindowHeight =  this.elem.nativeElement.parentElement.offsetHeight;

    const fontSize = this.settingsService.getActiveTheme().fontsize;
    const heightOfFooter = 1.1 * fontSize + 1;
    const heigthOfSuggestion = 1.5 * fontSize;
    const maxHeigthOfSuggestions = heigthOfSuggestion * 8 ;
    const offsetWidthSuggestion = 4 * fontSize;

    const height =  Math.min(info.suggestions.length * heigthOfSuggestion, maxHeigthOfSuggestions) + heightOfFooter ;
    const maxLength = Math.max(...info.suggestions.map(s => s.label.length)) * fontSize / 1.8 + offsetWidthSuggestion;
    const maxWidth = 700;
    const width =  Math.min(maxLength, maxWidth);

    const rootY = info.cursorPosition.appWindowY;
    const rootX = this.settingsService.getAutocompletePosition() === 'cursor' ? info.cursorPosition.appWindowX : info.cursorPosition.windowX;

    let top: number;
    if (rootY + info.cursorPosition.height + height + 20 > terminalWindowHeight) {
      top = rootY - info.cursorPosition.height - 2 - height;
    } else {
      top = rootY + info.cursorPosition.height + 2;
    }
    let left: number;
    if (rootX + width + 20 > terminalWindowWidth) {
      left = rootX - (rootX + width + 20 - terminalWindowWidth);
    } else {
      left = rootX;
    }
    return {'top': top + 'px', left: left + 'px', height: height + 'px', width: width + 'px'};
  }

  scrollToIndex(index: number) {
    if (!this.suggestionsElement) { return; }
    if (index === -1) {
      index = 0;
    }
    const list = this.suggestionsElement.nativeElement as HTMLUListElement;
    if (list.children.length <= index) { return; }
    const targetLi = list.children.item(index);
    targetLi.scrollIntoView(false);
  }

  edit(suggestion: Suggestion) {
    this.autocompleteService.openCommandSettings(suggestion);
  }
}
