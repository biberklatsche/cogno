import {Injectable} from '@angular/core';
import {ShellLocation} from '../../../../common/shellLocation';
import {DbService} from '../../../../+shared/services/db/db.service';

import {Logger} from '../../../../logger/logger';
import {SettingsService} from '../../../../+shared/services/settings/settings.service';
import {AutocompleteConfig} from '../../../../../../shared/models/settings';
import {Normalizer} from '../../../../../../shared/normalizer';
import {ArgumentDictionary} from './argument-dictionary/argument-dictionary';
import {CommandDictionary} from './command-dictionary/command.dictionary';
import {CommandTree} from './command-tree/command.tree';
import {CommandNormalizer} from './normalizer/command-normalizer';
import {CommandBuilder} from './command-builder/command.builder';
import {ScoringService} from './scoring/scoring.service';
import {MaxCommandLength} from '../../autocomplete.service';
import {HistorySuggester} from './history.suggester';
import {Tokenizer} from '../tokenizer';
import {CommandDirectoryRepository} from '../../../../+shared/services/commands/command.repository';

@Injectable({providedIn: 'root'})
export class HistorySuggesterManager {
  private readonly services = new Map<string, {suggester: HistorySuggester, location: ShellLocation}>();

  constructor(private readonly db: DbService, private readonly settings: SettingsService) {
  }

  destroy() {
    this.services.clear();
  }

  public async init(location: ShellLocation): Promise<HistorySuggester> {
    if (this.services.has(location.hash)) {
      Logger.debug(`Suggest service exists for ${location.toString()}.`);
      return Promise.resolve(this.services.get(location.hash).suggester);
    }
    const autocompleteConfig = this.settings.getAutocompleteConfig();
    Logger.debug(`Suggest service does not exists for ${location.toString()} - create a new instance`);
    const suggester = await this.createSuggester(this.db, location, autocompleteConfig);
    Logger.debug('Suggest service created');
    this.services.set(location.hash, {suggester: suggester, location: location});
    return suggester;
  }

  public get(location: ShellLocation): HistorySuggester {
    if (!location?.isValid) {
      Logger.warn('Location is invalid', location);
      return
    }
    return this.services.get(location.hash).suggester;
  }

  private async createSuggester(db: DbService, location: ShellLocation, settings: AutocompleteConfig): Promise<HistorySuggester> {
    const commandDirectoryRepository = new CommandDirectoryRepository(db);
    const argumentDictionary = new ArgumentDictionary(location);
    const commandDictionary = new CommandDictionary(location);
    const commandTree = new CommandTree();
    const normalizer = new Normalizer(location);
    const commandNormalizer = new CommandNormalizer(normalizer);
    const labelBuilder = new CommandBuilder(normalizer);
    const scoring = new ScoringService();
    const ds = await commandDirectoryRepository.init(location);
    Logger.debug('Restore Suggester.');
    for (const directory of ds.directories) {
      commandTree.restoreDirectory(directory);
    }
    for (const command of ds.commands) {
      if (!command.command || command.command.length > MaxCommandLength) {
        continue;
      }
      commandDictionary.restore(command);
      argumentDictionary.restore(Tokenizer.tokenize(command.command), command.executionCount, command.shellTypes);
    }
    for (const commandDirectory of ds.commandDirectories) {
      const directory = ds.directories.find(d => d._id === commandDirectory.directoryId);
      commandTree.restoreCommand(directory.directories, commandDirectory);
    }
    return new HistorySuggester(
      argumentDictionary,
      commandDictionary,
      commandTree,
      commandDirectoryRepository,
      commandNormalizer,
      labelBuilder,
      scoring,
      settings);
  }

  async reload() {
    const suggesters = Array.from(this.services.values());
    this.services.clear();
    await this.db.reload();
    for(const suggester of suggesters) {
      await this.init(suggester.location);
    }
  }
}
