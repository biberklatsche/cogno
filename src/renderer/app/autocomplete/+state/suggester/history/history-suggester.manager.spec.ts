import {DbService} from '../../../../+shared/services/db/db.service';
import {ShellLocation} from '../../../../common/shellLocation';
import {ShellType} from '../../../../../../shared/models/models';
import {TestHelper} from '../../../../../../test/helper';
import {
  clear,
  getDbAdapter,
  getElectronService,
  getSettingsService,
  getHistorySuggesterManager
} from '../../../../../../test/factory';
import {SettingsService} from '../../../../+shared/services/settings/settings.service';
import {ElectronService} from '../../../../+shared/services/electron/electron.service';
import {HistorySuggesterManager} from './history-suggester.manager';
import {Helper} from "../../../../+shared/services/commands/helper";
import {dbVersion} from "../../../../+shared/services/commands/command.repository";

describe('HistorySuggesterManager', () => {
  let db: DbService;
  let settings: SettingsService;
  let factory: HistorySuggesterManager;
  let electron: ElectronService;

  beforeEach(() => {
    clear();
    db = getDbAdapter();
    settings = getSettingsService();
    settings.update(({
      settings: {
        ...TestHelper.createSettings(),
        autocomplete: {ignore: ['npm i'], mode: 'always', position: 'cursor'}
      }
    }));
    factory = getHistorySuggesterManager();
    electron = getElectronService();
    jest.spyOn(electron, 'existsFileSync').mockReturnValue(true);
  });

  it('should return a instance', async () => {
    await factory.init(new ShellLocation('machine', ShellType.Bash));
    expect(factory.get(new ShellLocation('machine', ShellType.Bash))).toBeTruthy();
  });

  it('should return same instance', async () => {
    await factory.init(new ShellLocation('machine', ShellType.Bash));
    await factory.init(new ShellLocation('machine', ShellType.Bash));
    expect(factory.get(new ShellLocation('machine', ShellType.Bash))).toBeTruthy();
  });

  it('should return second instances', async () => {
    await factory.init(new ShellLocation('machine1', ShellType.Bash));
    await factory.init(new ShellLocation('machine2', ShellType.Bash));
    expect(factory.get(new ShellLocation('machine1', ShellType.Bash))).toBeTruthy();
    expect(factory.get(new ShellLocation('machine2', ShellType.Bash))).toBeTruthy();
  });

  it('should save command in db', async () => {
    const location = new ShellLocation('machine', ShellType.Bash);
    await factory.init(location);
    await factory.get(location).saveExecution(location, ['c'], 'git init');
    const t = await db.find({}, Helper.createCommandsDbName(dbVersion));
    expect(t.length).toEqual(1);
  });

  it('should save two commands in db &&', async () => {
    const location = new ShellLocation('machine', ShellType.Bash);
    await factory.init(location);
    await factory.get(location).saveExecution(location, ['c'], 'git init && git status');
    const t = await db.find({}, Helper.createCommandsDbName(dbVersion));
    expect(t.length).toEqual(3);
  });

  it('should save two commands in db ||', async () => {
    const location = new ShellLocation('machine', ShellType.Bash);
    await factory.init(location);
    await factory.get(location).saveExecution(location, ['c'], 'git init || git status');
    const t = await db.find({}, Helper.createCommandsDbName(dbVersion));
    expect(t.length).toEqual(3);
  });

  it('should restore &&', async () => {
    const location = new ShellLocation('machine', ShellType.Bash);
    await factory.init(location);
    await factory.get(location).saveExecution(location, ['c'], 'git init && git status');
    factory.destroy();
    await factory.init(location);

    const t = await factory.get(location).suggest({
      location: location,
      directories: ['c'],
      input: 'git ',
      tokens: ['git'],
      isNavigationSearch: false
    });
    expect(t.length).toEqual(3);
  });

  it('should restore ||', async () => {
    const location = new ShellLocation('machine', ShellType.Bash);
    await factory.init(location);
    await factory.get(location).saveExecution(location, ['c'], 'git init || git status');
    await factory.destroy();
    await factory.init(location);
    const s = await factory.get(location).suggest({
      location: location,
      directories: ['c'],
      input: 'git ',
      tokens: ['git'],
      isNavigationSearch: false
    });
    expect(s.length).toEqual(3);
  });

  it('should restore cd', async () => {
    const location = new ShellLocation('machine', ShellType.Bash);
    await factory.init(location);
    await factory.get(location).saveExecution(location, ['c'], 'cd test');
    factory.destroy();
    await factory.init(location);
    const t = await factory.get(location).suggest({
      location: location,
      directories: ['c'],
      input: 'cd t',
      tokens: ['cd', 't'],
      isNavigationSearch: true
    });
    expect(t.length).toEqual(1);
  });

  it('should restore cd and command', async () => {
    const location = new ShellLocation('machine', ShellType.Bash);
    await factory.init(location);
    await factory.get(location).saveExecution(location, ['c'], 'cd test && git commit');
    factory.destroy();
    await factory.init(location);
    const t = await factory.get(location).suggest({
      location: location,
      directories: ['c'],
      input: 'git c',
      tokens: ['git', 'c'],
      isNavigationSearch: false
    });

    expect(t.length).toEqual(1);

  });


  it('should ignore commands from settings', async () => {
    const location = new ShellLocation('machine', ShellType.Bash);
    await factory.init(location);
    await factory.get(location).saveExecution(location, ['c'], 'npm i');
    const t = await factory.get(location).suggest({
      location: location,
      directories: ['c'],
      input: 'npm',
      tokens: ['npm'],
      isNavigationSearch: false
    });
    expect(t.length).toEqual(0);

  });

  it('should ignore commands from settings - empty space', async () => {
    const location = new ShellLocation('machine', ShellType.Bash);
    await factory.init(location);
    await factory.get(location).saveExecution(location, ['c'], ' npm i  ');
    const t = await factory.get(location).suggest({
      location: location,
      directories: ['c'],
      input: 'npm',
      tokens: ['npm'],
      isNavigationSearch: false
    });
    expect(t.length).toEqual(0);

  });

  it('should not remove camelcase folder names on bash', async () => {
    const location = new ShellLocation('machine', ShellType.Bash);
    await factory.init(location);
    await factory.get(location).saveExecution(location, ['c', 'temp'], ' npm start');
    await factory.get(location).saveExecution(location, ['c', 'Temp'], ' npm start');
    const t = await factory.get(location).suggest({
      location: location,
      directories: ['c'],
      input: 'cd t',
      tokens: ['cd', 't'],
      isNavigationSearch: true
    });
    expect(t.length).toEqual(2);

  });

  it('should remove camelcase folder names on powershell', async () => {
    const location = new ShellLocation('machine', ShellType.Powershell);
    await factory.init(location);
    await factory.get(location).saveExecution(location, ['c', 'temp'], ' npm start');
    await factory.get(location).saveExecution(location, ['c', 'Temp'], ' npm start');
    const t = await factory.get(location).suggest({
      location: location,
      directories: ['c'],
      input: 'cd t',
      tokens: ['cd', 't'],
      isNavigationSearch: true
    });
    expect(t.length).toEqual(1);
  });
});
