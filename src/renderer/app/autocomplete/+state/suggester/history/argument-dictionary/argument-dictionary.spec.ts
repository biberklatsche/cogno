import {ArgumentDictionary} from './argument-dictionary';
import {CommandNormalizer} from '../normalizer/command-normalizer';
import {ShellType} from '../../../../../../../shared/models/models';
import {NormalizedCommand, NormalizedSuggestResult} from '../models';
import {Normalizer} from '../../../../../../../shared/normalizer';
import {ShellLocation} from '../../../../../common/shellLocation';
import {SuggestRequestBuilder} from '../../suggestRequestBuilder';
import {TerminalInfo} from '../../../../../+shared/models/terminal-info';
import {TestHelper} from '../../../../../../../test/helper';

describe('ArgumentDictionary', () => {

  let commandNormalizer: CommandNormalizer;
  let requestBuilder: SuggestRequestBuilder;
  let argsDictionary: ArgumentDictionary;

  beforeEach(() => {
    const normalizer = new Normalizer(new ShellLocation('', ShellType.Bash));
    commandNormalizer = new CommandNormalizer(normalizer);
    requestBuilder = new SuggestRequestBuilder(normalizer);
    argsDictionary = new ArgumentDictionary(new ShellLocation('', ShellType.Bash));
  });

  it('should reduce single args', () => {
    const commands: NormalizedCommand[] = [
      ...commandNormalizer.normalize([''], 'git commit -m "test1"', [ShellType.Bash]),
      ...commandNormalizer.normalize([''], 'git commit -m "test2"', [ShellType.Bash]),
      ...commandNormalizer.normalize([''], 'git commit -m "test3"', [ShellType.Bash])
    ];
    argsDictionary.add(...commands);
    const data = requestBuilder.build(TestHelper.buildAutocompleteRequest([''], 'git commit', ShellType.Bash));
    const result: NormalizedSuggestResult = { suggestions: [], scoringValues: {}};
    result.suggestions = [{shellCommand: 'git commit -m "test1"'}, {shellCommand: 'git commit -m "test2"'}, {shellCommand: 'git commit -m "test3"'}];
    argsDictionary.apply(data, result);
    expect(result.suggestions.length).toEqual(1);
    expect(result.suggestions[0].shellCommand).toEqual('git commit -m "{{?}}"');
  });

  it('should not reduce single arg if it is only one time in suggestion', () => {
    const commands: NormalizedCommand[] = [
      ...commandNormalizer.normalize([''], 'git commit -m "test1"', [ShellType.Bash]),
      ...commandNormalizer.normalize([''], 'git commit -m "test2"', [ShellType.Bash]),
      ...commandNormalizer.normalize([''], 'git commit -m "test3"', [ShellType.Bash])
    ];
    argsDictionary.add(...commands);
    const data = requestBuilder.build(TestHelper.buildAutocompleteRequest([''], 'git commit', ShellType.Bash));
    const result: NormalizedSuggestResult = { suggestions: [], scoringValues: {}};
    result.suggestions = [{shellCommand: 'git commit -m "test1"'}];
    argsDictionary.apply(data, result);
    expect(result.suggestions[0].shellCommand).toEqual('git commit -m "test1"');
  });

  it('should work with =', () => {
    argsDictionary.add(...commandNormalizer.normalize([''], 'git commit -m --date="abc" "test"', [ShellType.Bash]));
    argsDictionary.add(...commandNormalizer.normalize([''], 'git commit -m --date="cde" "test2"', [ShellType.Bash]));
    argsDictionary.add(...commandNormalizer.normalize([''], 'git commit -m --date="efg" "test3"', [ShellType.Bash]));
    const data = requestBuilder.build(TestHelper.buildAutocompleteRequest([''], 'git commit', ShellType.Bash));
    const result: NormalizedSuggestResult = { suggestions: [], scoringValues: {}};
    result.suggestions = [{shellCommand: 'git commit -m --date="abc" "test"'}, {shellCommand: 'git commit -m --date="cde" "test2"'}, {shellCommand: 'git commit -m --date="efg" "test3"'}];
    argsDictionary.apply(data, result);
    expect(result.suggestions[0].shellCommand).toEqual('git commit -m --date="{{?}}" "{{?}}"');
  });

  it('should ignore cd', () => {
    argsDictionary.add(...commandNormalizer.normalize([''],'cd test1', [ShellType.Bash]));
    argsDictionary.add(...commandNormalizer.normalize([''], 'cd test2', [ShellType.Bash]));
    argsDictionary.add(...commandNormalizer.normalize([''], 'cd test3', [ShellType.Bash]));
    const data = requestBuilder.build(TestHelper.buildAutocompleteRequest([''], 'cd te', ShellType.Bash));
    const result: NormalizedSuggestResult = { suggestions: [], scoringValues: {}};
    result.suggestions = [{shellCommand: 'cd test1'}, {shellCommand: 'cd test2'}, {shellCommand: 'cd test3'}];
    argsDictionary.apply(data, result);
    expect(result.suggestions.length).toEqual(3);
  });

  it('should work with null or undefined', () => {
    const result: NormalizedSuggestResult = { suggestions: [], scoringValues: {}};
    expect(() => argsDictionary.apply(null, result)).not.toThrow();
    expect(() => argsDictionary.apply(undefined, result)).not.toThrow();
  });

  it('should work after restore', () => {
    argsDictionary.restore(['git', 'commit', '-m ', '--date=', '"abc"', '"test1"'], 1, [ShellType.Bash]);
    argsDictionary.restore(['git', 'commit', '-m ', '--date=', '"cde"', '"test2"'], 1, [ShellType.Bash]);
    argsDictionary.restore(['git', 'commit', '-m ', '--date=', '"efg"', '"test3"'], 1, [ShellType.Bash]);
    const data = requestBuilder.build(TestHelper.buildAutocompleteRequest([''], 'git commit', ShellType.Bash));
    const result: NormalizedSuggestResult = { suggestions: [], scoringValues: {}};
    result.suggestions = [{shellCommand: 'git commit -m --date="abc" "test1"'}, {shellCommand: 'git commit -m --date="cde" "test2"'}, {shellCommand: 'git commit -m --date="efg" "test3"'}];
    argsDictionary.apply(data, result);
    expect(result.suggestions[0].shellCommand).toEqual('git commit -m --date="{{?}}" "{{?}}"');
  });

  it('should work with restore null, undefined or empty', () => {
    argsDictionary.restore([], 1, [ShellType.Bash]);
    argsDictionary.restore(null, 1, [ShellType.Bash]);
    argsDictionary.restore(undefined, 1, [ShellType.Bash]);
    const data = requestBuilder.build(TestHelper.buildAutocompleteRequest([''], 'git commit', ShellType.Bash));
    const result: NormalizedSuggestResult = { suggestions: [], scoringValues: {}};
    result.suggestions = [{shellCommand: 'git commit -m --date="abc" "test1"'}, {shellCommand: 'git commit -m --date="cde" "test2"'}, {shellCommand: 'git commit -m --date="efg" "test3"'}];
    argsDictionary.apply(data, result);
    expect(result.suggestions.length).toEqual(3);
  });

  it('should work with unknown command', () => {
    argsDictionary.add(...commandNormalizer.normalize([''], 'git commit -m --date="abc" "test"', [ShellType.Bash]));
    argsDictionary.add(...commandNormalizer.normalize([''], 'git commit -m --date="cde" "test2"', [ShellType.Bash]));
    argsDictionary.add(...commandNormalizer.normalize([''], 'git commit -m --date="efg" "test3"', [ShellType.Bash]));
    const data = requestBuilder.build(TestHelper.buildAutocompleteRequest([''], 'git', ShellType.Bash));
    const result: NormalizedSuggestResult = { suggestions: [], scoringValues: {}};
    result.suggestions = [{shellCommand: 'git'}, {shellCommand: 'git commit -m --date="abc" "test"'}, {shellCommand: 'git commit -m --date="cde" "test2"'}, {shellCommand: 'git commit -m --date="efg" "test3"'}];
    argsDictionary.apply(data, result);
    expect(result.suggestions[0].shellCommand).toEqual('git');
    expect(result.suggestions[1].shellCommand).toEqual('git commit -m --date="{{?}}" "{{?}}"');
  });

  it('should work with placeholder command at the begin', () => {
    argsDictionary.add(...commandNormalizer.normalize([''], 'git commit -m --date="abc" "test"', [ShellType.Bash]));
    argsDictionary.add(...commandNormalizer.normalize([''], 'git commit -m --date="cde" "test2"', [ShellType.Bash]));
    argsDictionary.add(...commandNormalizer.normalize([''], 'git commit -m --date="efg" "test3"', [ShellType.Bash]));
    argsDictionary.add(...commandNormalizer.normalize([''], 'git commit -m --date="{{date}}" "{{?}}"', [ShellType.Bash]));
    const data = requestBuilder.build(TestHelper.buildAutocompleteRequest([''], 'git', ShellType.Bash));
    const result: NormalizedSuggestResult = { suggestions: [], scoringValues: {}};
    result.suggestions = [{shellCommand: 'git'}, {shellCommand: 'git commit -m --date="{{date}}" "{{?}}"'}, {shellCommand: 'git commit -m --date="abc" "test"'}, {shellCommand: 'git commit -m --date="cde" "test2"'}, {shellCommand: 'git commit -m --date="efg" "test3"'}];
    argsDictionary.apply(data, result);
    expect(result.suggestions[0].shellCommand).toEqual('git');
    expect(result.suggestions[1].shellCommand).toEqual('git commit -m --date="{{date}}" "{{?}}"');
  });

  it('should work with placeholder command at the end', () => {
    argsDictionary.add(...commandNormalizer.normalize([''], 'git commit -m --date="abc" "test"', [ShellType.Bash]));
    argsDictionary.add(...commandNormalizer.normalize([''], 'git commit -m --date="cde" "test2"', [ShellType.Bash]));
    argsDictionary.add(...commandNormalizer.normalize([''], 'git commit -m --date="efg" "test3"', [ShellType.Bash]));
    argsDictionary.add(...commandNormalizer.normalize([''], 'git commit -m --date="{{date}}" "{{?}}"', [ShellType.Bash]));
    const data = requestBuilder.build(TestHelper.buildAutocompleteRequest([''], 'git', ShellType.Bash));
    const result: NormalizedSuggestResult = { suggestions: [], scoringValues: {}};
    result.suggestions = [{shellCommand: 'git'}, {shellCommand: 'git commit -m --date="abc" "test"'}, {shellCommand: 'git commit -m --date="cde" "test2"'}, {shellCommand: 'git commit -m --date="efg" "test3"'}, {shellCommand: 'git commit -m --date="{{date}}" "{{?}}"'}];
    argsDictionary.apply(data, result);
    expect(result.suggestions[0].shellCommand).toEqual('git');
    expect(result.suggestions[1].shellCommand).toEqual('git commit -m --date="{{date}}" "{{?}}"');
  });

  it('should work with cd  command', () => {
    const data = requestBuilder.build(TestHelper.buildAutocompleteRequest([''], 'cd wi', ShellType.Bash));
    const result: NormalizedSuggestResult = { suggestions: [], scoringValues: {}};
    result.suggestions = [{pathToDirectory: ['cogno']}];
    argsDictionary.apply(data, result);
    expect(result.suggestions.length).toEqual(1);
  });
});
