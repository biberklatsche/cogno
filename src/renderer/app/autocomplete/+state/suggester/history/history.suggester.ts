import {ArgumentDictionary} from './argument-dictionary/argument-dictionary';
import {CommandDictionary} from './command-dictionary/command.dictionary';
import {CommandTree, NodeData} from './command-tree/command.tree';
import {CommandNormalizer} from './normalizer/command-normalizer';
import {CommandBuilder} from './command-builder/command.builder';
import {ScoringService} from './scoring/scoring.service';
import {AutocompleteConfig} from '../../../../../../shared/models/settings';
import {ShellLocation} from '../../../../common/shellLocation';
import {NormalizedSuggestResult} from './models';
import {Addable, Suggester, Suggestion, SuggestRequest} from '../suggester';
import {CommandDirectoryRepository} from '../../../../+shared/services/commands/command.repository';
import {Command} from '../../../../+shared/models/command';

export class HistorySuggester implements Suggester, Addable {

  public constructor(private argumentDictionary: ArgumentDictionary,
                     private commandDictionary: CommandDictionary,
                     private commandTree: CommandTree,
                     private commandRepository: CommandDirectoryRepository,
                     private commandNormalizer: CommandNormalizer,
                     private commandBuilder: CommandBuilder,
                     private scoring: ScoringService,
                     private settings: AutocompleteConfig) {
  }

  public suggest(request: SuggestRequest): Promise<Suggestion[]> {
    try {
      const searchResult: NormalizedSuggestResult = { scoringValues: {tokenCount: request.tokens.length || 0}, suggestions: []};
      this.commandDictionary.apply(request, searchResult);
      this.commandTree.apply(request, searchResult);
      this.argumentDictionary.apply(request, searchResult);

      for (const suggestion of searchResult.suggestions) {
        suggestion.location = request.location;
        suggestion.directories = request.directories;
        this.commandBuilder.apply(suggestion, request.tokens);
        this.scoring.apply(suggestion, searchResult.scoringValues);
      }
      // remove duplicates
      const duplicateFree = Array.from(
        new Map(searchResult.suggestions.map(s => [s.label, s])).values()
      );
      // remove result with not enough hits
      const suggestions = duplicateFree.filter(s => s.labelMatches >= request.tokens.length && s.label !== request.input);
      suggestions.sort((a, b) => b.score - a.score);
      return Promise.resolve(suggestions);
    } catch {
      return Promise.resolve([]);
    }
  }

  public saveExecution(location: ShellLocation, directory: string[], input: string): Promise<unknown> {
    if (!input || input?.trim()?.length === 0) {
      return Promise.resolve(null);
    }
    if (this.settings.ignore) {
      for (const ignore of this.settings.ignore) {
        if (input.trim() === ignore) {
          return Promise.resolve(null);
        }
      }
    }

    const normalizedCommands = this.commandNormalizer.normalize(directory, input, [location.shellType]);
    this.argumentDictionary.add(...normalizedCommands);
    const commands = this.commandDictionary.saveExecution(...normalizedCommands);
    const nodes = this.commandTree.saveExecution(...normalizedCommands);
    return this.save(commands, nodes);
  }

  public async saveSelection(suggestion: Suggestion) {
    const command = this.commandDictionary.saveSelection(suggestion);
    const node = this.commandTree.saveSelection(suggestion);
    return this.save([command], [node]);
  }

  private save(commands: Command[], nodes: NodeData[]): Promise<unknown> {
    const promises: Promise<unknown>[] = [];
    for (const node of nodes) {
      if (!!node.commandHash) {
        const command = commands.find(d => d.commandHash === node.commandHash);
        promises.push(this.commandRepository.saveCommand(command));
        promises.push(this.commandRepository.saveCommandDirectory(command, node))
      }
      promises.push(this.commandRepository.saveDirectory(node));
    }
    return Promise.all(promises);
  }
}
