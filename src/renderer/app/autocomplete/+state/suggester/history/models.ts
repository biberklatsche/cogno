import {ShellType} from '../../../../../../shared/models/models';
import {Suggestion} from '../suggester';

export type NormalizedCommand = {
  commandHash?: string; // the hash value of the command
  command?: string; // a executed command (it can not be a navigation command)
  tokens?: string[]; // the tokens of the command, for example git commit -> [git, commit]
  directories: string[]; // the path from root
  isCombinedCommand: boolean;
  shellTypes: ShellType[];
};

export interface NormalizedSuggestResult {
  suggestions: SuggestionCalculation[]; // the suggestions to this search request
  scoringValues: ScoringValues;
}


export type SuggestionCalculation = Suggestion & {
  commandHash?: string; // the hash value of the command
  labelMatches?: number; // the count of matches of input tokens in the command-builder
  labelMatchesStartOfWord?: number; // the count of start of word matches of input tokens in the command-builder
  shellCommand?: string; // the executed command (it can not be a navigation command)
  pathToDirectory?: string[]; // the directories from current location or root to the command location
  isPathToDirectoryFromRoot?: boolean; // indicates if the pathToDirectory is from root
  matchShellType?: MatchShellType; // indicates if the current shell matches the shell where the command was executed
  scoreDetails?: ScoreDetails;
  globalExecutionCount?: number;
  globalExecutionDate?: Date;
  globalSelectionCount?: number;
  globalSelectionDate?: Date;
  executionCount?: number;
  selectionCount?: number;
  selectionDate?: Date;
  executionDate?: Date;
};

export enum MatchShellType {
  No, Partly, Yes
}

export interface ScoreDetails {
  matches?: number;
  directory?: number;
  executionCount?: number;
  selectionCount?: number;
  executionDate?: number;
  selectionDate?: number;
  globalExecutionCount?: number;
  globalSelectionCount?: number;
  globalExecutionDate?: number;
  globalSelectionDate?: number;
}

export interface ScoringValues {
  tokenCount?: number;
  minExecutionDate?: Date;
  maxExecutionDate?: Date;
  minGlobalExecutionDate?: Date;
  maxGlobalExecutionDate?: Date;
  minSelectionDate?: Date;
  maxSelectionDate?: Date;
  minGlobalSelectionDate?: Date;
  maxGlobalSelectionDate?: Date;
}
