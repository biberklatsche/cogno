import {ShellType} from '../../../../../../../shared/models/models';
import {Normalizer} from '../../../../../../../shared/normalizer';
import {CommandNormalizer} from './command-normalizer';
import {ShellLocation} from '../../../../../common/shellLocation';

describe('CommandNormalizer', () => {

  let normalizer: CommandNormalizer;

  beforeEach(() => {
    normalizer = new CommandNormalizer(new Normalizer(new ShellLocation("", ShellType.Bash)));
  });

  it('should remove brackets before split', () => {
    normalizer = new CommandNormalizer(new Normalizer(new ShellLocation("", ShellType.Bash)));
    const commands = normalizer.normalize([], '(cd test && ls)', [ShellType.Bash]);
    expect(commands.length).toBe(2);
    expect(commands[0].directories).toEqual(['test']);
    expect(commands[1].command).toEqual('ls');
  });

  it('should work with empty space in dir name - powershell v1', () => {
    normalizer = new CommandNormalizer(new Normalizer(new ShellLocation("", ShellType.Bash)));
    const commands = normalizer.normalize(['C'], 'cd \'.\\Program Files\\\'', [ShellType.Powershell]);
    expect(commands.length).toBe(1);
    expect(commands[0].directories).toEqual(['C', 'Program Files']);
  });

  it('should work with empty space in dir name - powershell v2', () => {
    normalizer = new CommandNormalizer(new Normalizer(new ShellLocation("", ShellType.Powershell)));
    const commands = normalizer.normalize(['C'], 'cd "Program Files"', [ShellType.Powershell]);
    expect(commands.length).toBe(1);
    expect(commands[0].directories).toEqual(['C', 'Program Files']);
  });

  it('should work with empty space in dir name - powershell v3', () => {
    normalizer = new CommandNormalizer(new Normalizer(new ShellLocation("", ShellType.Powershell)));
    const commands = normalizer.normalize(['C'], 'cd Program` Files', [ShellType.Powershell]);
    expect(commands.length).toBe(1);
    expect(commands[0].directories).toEqual(['C', 'Program Files']);
  });

  it('should work with empty space in dir name - powershell v4', () => {
    normalizer = new CommandNormalizer(new Normalizer(new ShellLocation("", ShellType.Powershell)));
    const commands = normalizer.normalize(['C'], ' cd \'.\\Program Files\\Common Files\\\'', [ShellType.Bash]);
    expect(commands.length).toBe(1);
    expect(commands[0].directories).toEqual(['C', 'Program Files', 'Common Files']);
  });

  it('should work with empty space in dir name - bash v1', () => {
    normalizer = new CommandNormalizer(new Normalizer(new ShellLocation("", ShellType.Bash)));
    const commands = normalizer.normalize(['C'], 'cd Program\\ Files', [ShellType.Bash]);
    expect(commands.length).toBe(1);
    expect(commands[0].directories).toEqual(['C', 'Program Files']);
  });

  it('should work with starting dot', () => {
    normalizer = new CommandNormalizer(new Normalizer(new ShellLocation("", ShellType.Bash)));
    const commands = normalizer.normalize(['C', 'Program'], 'cd ./Test', [ShellType.Bash]);
    expect(commands.length).toBe(1);
    expect(commands[0].directories).toEqual(['C', 'Program', 'Test']);
  });

  it('should work with starting dots - bash', () => {
    normalizer = new CommandNormalizer(new Normalizer(new ShellLocation("", ShellType.Bash)));
    const commands = normalizer.normalize(['C', 'Program'], 'cd ../Test', [ShellType.Bash]);
    expect(commands.length).toBe(1);
    expect(commands[0].directories).toEqual(['C', 'Test']);
  });

  it('should work with starting many dots - bash', () => {
    normalizer = new CommandNormalizer(new Normalizer(new ShellLocation("", ShellType.Bash)));
    const commands = normalizer.normalize(['C', 'Program'], 'cd ../../Test', [ShellType.Bash]);
    expect(commands.length).toBe(1);
    expect(commands[0].directories).toEqual(['Test']);
  });

  it('should work with empty space in dir name - bash v2', () => {
    normalizer = new CommandNormalizer(new Normalizer(new ShellLocation("", ShellType.Bash)));
    const commands = normalizer.normalize(['C'], 'cd "Program Files"', [ShellType.Bash]);
    expect(commands.length).toBe(1);
    expect(commands[0].directories).toEqual(['C', 'Program Files']);
  });

  it('should work with empty space in dir name - bash v3', () => {
    normalizer = new CommandNormalizer(new Normalizer(new ShellLocation("", ShellType.Bash)));
    const commands = normalizer.normalize(['C'], 'cd Program\\ Files', [ShellType.Bash]);
    expect(commands.length).toBe(1);
    expect(commands[0].directories).toEqual(['C', 'Program Files']);
  });
});
