import {NormalizedCommand} from '../models';
import {Hash} from '../../../../../+shared/models/hash/hash';
import { Normalizer } from '../../../../../../../shared/normalizer';
import {ShellType} from '../../../../../../../shared/models/models';
import {Tokenizer} from '../../tokenizer';

export class CommandNormalizer {

  constructor(private normalizer: Normalizer) {}

  public normalize(directories: string[], command: string, shellTypes: ShellType[]): NormalizedCommand[] {
    command = command.replace('(', '').replace(')', '');
    const commands = this.normalizer.split(command);
    const normalized: NormalizedCommand[] = [];
    let existsNavigationCommand = false;
    for (let command of commands) {
      command = command.trim();
      if (this.normalizer.isNavigation(command)) {
        existsNavigationCommand = true;
        const path = this.removeNavigation(command);
        normalized.push({
          directories: this.isSubPath(path) ? Normalizer.joinDirectories(directories, Normalizer.getDirectories(path)) : Normalizer.getDirectories(path),
          isCombinedCommand: false,
          shellTypes: shellTypes
        });
      } else {
        normalized.push({
          command: command,
          tokens: Tokenizer.tokenize(command),
          commandHash: Hash.create(command),
          directories: directories,
          isCombinedCommand: false,
          shellTypes: shellTypes
        });
      }
    }
    if (normalized.length > 1 && !existsNavigationCommand) {
      normalized.push({
        command: command.trim(),
        commandHash: Hash.create(command.trim()),
        directories: directories,
        isCombinedCommand: true,
        shellTypes: shellTypes
      });
    }
    return normalized;
  }

  private removeNavigation(command: string): string {
    return command.replace('cd', '').trim();
  }

  private isSubPath(path: string) {
    return !(path.startsWith('/') || path.includes(':') || path.includes('~'));
  }
}
