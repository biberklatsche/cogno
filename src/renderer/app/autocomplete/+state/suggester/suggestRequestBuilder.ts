import {Normalizer} from '../../../../../shared/normalizer';
import {Tokenizer} from './tokenizer';
import {PlaceholderInfo, SuggestRequest} from './suggester';
import {TerminalInfo} from '../../../+shared/models/terminal-info';
import {AutocompleteRequest} from '../autocomplete.service';

export class SuggestRequestBuilder {

  constructor(private normalizer: Normalizer) {
  }

  public build(request: AutocompleteRequest, activeSuggester?: {name: string; sourceCommand: string}): SuggestRequest {
    let placeholderInfo: PlaceholderInfo;
    if(activeSuggester) {
      const currentInput = request.input.trim();
      const placeholder = `{{${activeSuggester.name}}}`;
      const compare = activeSuggester.sourceCommand.replace(placeholder, '').trim();
      if (currentInput.length >= compare.length) {
        const diff = Normalizer.diff(currentInput, compare);
        placeholderInfo = {currentPlaceholder: placeholder, sourceCommand: activeSuggester.sourceCommand, input: diff};
      }
    }
    const tokens = Tokenizer.tokenize(request.input);
    return {
      placeholderInfo: placeholderInfo,
      directories: request.directory,
      input: request.input,
      tokens: tokens,
      isNavigationSearch: this.normalizer.isNavigation(request.input),
      location: request.location
    };
  }
}
