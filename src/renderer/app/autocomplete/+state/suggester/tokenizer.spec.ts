import {Tokenizer} from './tokenizer';

describe('Tokenizer', () => {
  it('should work with undefined', () => {
    const result = Tokenizer.tokenize(undefined);
    expect(result).toEqual([]);
  });

  it('should work with null', () => {
    const result = Tokenizer.tokenize(null);
    expect(result).toEqual([]);
  });

  it('should work with empty string', () => {
    const result = Tokenizer.tokenize('');
    expect(result).toEqual([]);
  });

  it('should split by whitespace', () => {
    const result = Tokenizer.tokenize('a b');
    expect(result).toEqual(['a', 'b']);
  });

  it('should split by many whitespaces', () => {
    const result = Tokenizer.tokenize('a  b');
    expect(result).toEqual(['a', 'b']);
  });

  it('should split by whitespace at begin and end', () => {
    const result = Tokenizer.tokenize(' a  b ');
    expect(result).toEqual(['a', 'b']);
  });

  it('should not split "', () => {
    const result = Tokenizer.tokenize(' a  "b c" ');
    expect(result).toEqual(['a', '"b c"']);
  });

  it('should work with -', () => {
    const result = Tokenizer.tokenize('a -c');
    expect(result).toEqual(['a', '-c']);
  });

  it('should split =', () => {
    const result = Tokenizer.tokenize('a b=c');
    expect(result).toEqual(['a', 'b=', 'c']);
  });

  it('should work with empty =', () => {
    const result = Tokenizer.tokenize('a b= c');
    expect(result).toEqual(['a', 'b=', 'c']);
  });
});

