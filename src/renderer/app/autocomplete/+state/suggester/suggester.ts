import {ShellLocation} from '../../../common/shellLocation';
import {ScoreDetails} from './history/models';
import {CommandSource} from '../../../+shared/models/command';

export interface Suggester {
  suggest(request: SuggestRequest): Promise<Suggestion[]>;
}

export interface Addable {
  saveExecution(location: ShellLocation, directory: string[], command: string): Promise<unknown>;
}

export type Suggestion = {
  label?: string; // the label in the autocompleter
  labelHighlight?: { text: string; type: HighlightType }[]; // the command-builder which is shown in the autocompleter
  shellCommand?: string; // the command which is executed in the shell
  terminalCommand?: string; // the command which is visible in the terminal
  commandSource?: CommandSource;
  nextPlaceholder?: string;
  directories?: string[]; // the directories from the request
  suggestionType?: SuggestionType;
  executedInDirectory?: string;
  pluginLabel?: string;
  icon?: string;
  subIcon?: string;
  location?: ShellLocation;
  score?: number;
  scoreDetails?: ScoreDetails;
};

export enum HighlightType {
  None, Search, Argument, OpenSpecial, CloseSpecial
}

export enum SuggestionType {
  Navigation, LocalCommand, SemiLocalCommand, GlobalCommand, PluginCommand
}

export type SuggestRequest = {
  placeholderInfo?: PlaceholderInfo,
  directories: string[]; // all directories in path
  input: string; // the user input
  isNavigationSearch: boolean; // is the input like 'cd ..'
  tokens: string[]; // the tokens of the input, for example git commit -> [git, commit]
  location: ShellLocation;
}

export type PlaceholderInfo = {
  input: string;
  sourceCommand: string;
  currentPlaceholder: string;
}
