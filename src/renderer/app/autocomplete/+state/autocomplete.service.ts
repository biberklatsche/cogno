import {Injectable, OnDestroy} from '@angular/core';
import {Observable, Subscription} from 'rxjs';
import {GridService} from '../../global/grid/+state/grid.service';
import {Key} from '../../common/key';
import {createStore} from '../../common/store/store';
import {map} from 'rxjs/operators';
import {CursorPosition, TabType} from '../../../../shared/models/models';
import {Suggester, Suggestion, SuggestionType} from './suggester/suggester';
import {HistorySuggesterManager} from './suggester/history/history-suggester.manager';
import {SuggestRequestBuilder} from './suggester/suggestRequestBuilder';
import {Normalizer} from '../../../../shared/normalizer';
import {Highlighter} from './suggester/highlight';
import {ShellLocation} from '../../common/shellLocation';
import {HistorySuggester} from './suggester/history/history.suggester';
import {PluginSuggesterManager} from './suggester/plugins/plugin-suggester.manager';
import {Logger} from '../../logger/logger';
import {Nav} from '../../settings/+state/model';
import {TerminalCommandStatus} from '../../terminal/+state/osc/terminal-status.observer';
import {NotificationService} from '../../+shared/notification/notification.service';

export type AutocompleteSave = {
  location: ShellLocation;
  directory: string[];
  commandStatus: TerminalCommandStatus;
  command: string;
}

export type AutocompleteRequest = {
  location: ShellLocation;
  input: string;
  directory: string[];
  cursorPosition: CursorPosition;
};

export interface AutocompleteState {
  suggestions: Suggestion[];
  selectedIndex: number;
  show: boolean;
  activeSuggester?: {name: string; sourceCommand: string};
  cursorPosition?: CursorPosition;
}

const initialState: AutocompleteState = {
  suggestions: [],
  selectedIndex: -1,
  show: false,
  cursorPosition: null,
};

export const MaxCommandLength = 1000;

@Injectable({
  providedIn: 'root'
})
export class AutocompleteService implements OnDestroy {

  private store = createStore('autocomplete', initialState);
  private subscriptions: Subscription[] = [];

  constructor(private historySuggesterManager: HistorySuggesterManager, private pluginSuggsterManager: PluginSuggesterManager, private gridService: GridService, private notifictaion: NotificationService) {
    this.subscriptions.push(gridService.selectActiveTabId().subscribe(() => this.store.update({...initialState})));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  public async initSuggester(location: ShellLocation): Promise<HistorySuggester> {
    if (!location.isValid) {
      return;
    }
    return this.historySuggesterManager.init(location);
  }

  public async suggest(request: AutocompleteRequest, force: boolean = false): Promise<void> {

    if (!this.shouldHandled(request, force)) {
      this.store.update({...initialState});
      return Promise.resolve();
    }
    const normalizer = new Normalizer(request.location);
    const activeSuggester = this.store.get(s => s.activeSuggester);
    const suggestRequest = new SuggestRequestBuilder(normalizer).build(request, activeSuggester);
    if(!suggestRequest.placeholderInfo){
      this.store.update({activeSuggester: undefined});
    }
    const highlighter = new Highlighter(normalizer);
    const suggesters = this.getSuggesters(request);
    return Promise.allSettled(suggesters.map(s => s.suggest(suggestRequest))).then(result => {
      const errors = result.filter(r => r.status === 'rejected').map(s => (s as PromiseRejectedResult).reason).flat();
      if (errors.length > 0) {
        this.notifictaion.create().single('plugin error').title(`An Suggester throw an error.`).body(errors.join(',')).duration(5000).showAsError().subscribe();
        Logger.error(errors);
      }

      let suggestions = this.squashDuplicates(result.filter(r => r.status === 'fulfilled').map(s => (s as PromiseFulfilledResult<Suggestion[]>).value).flat());

      suggestions = suggestions.sort((a, b) => (b.score ?? 0) - (a.score ?? 0));
      suggestions = suggestions.slice(0, suggestions.length > 40 ? 40 : suggestions.length);
      suggestions = highlighter.apply(suggestions, suggestRequest.tokens);
      this.store.update({
        suggestions: suggestions,
        show: suggestions.length > 0,
        selectedIndex: -1,
        cursorPosition: request.cursorPosition
      });
    });
  }

  private squashDuplicates(suggestions: Suggestion[]): Suggestion[] {
    const duplicateList = new Map<string, Suggestion>;
    for (const suggestion of suggestions) {
      const key = suggestion.terminalCommand.replace(/[\/\\\s]/g, '');
      if (duplicateList.has(key)) {
        duplicateList.set(key, this.squashDuplicate(duplicateList.get(key), suggestion));
      } else {
        duplicateList.set(key, suggestion);
      }
    }
    return Array.from(duplicateList.values());
  }

  private squashDuplicate(suggestionA: Suggestion, suggestionB: Suggestion): Suggestion {
    const pluginSuggestion = suggestionA.suggestionType === SuggestionType.PluginCommand ? suggestionA : suggestionB.suggestionType === SuggestionType.PluginCommand ? suggestionB : null;
    const otherSuggestion = suggestionA.suggestionType !== SuggestionType.PluginCommand ? suggestionA : suggestionB.suggestionType !== SuggestionType.PluginCommand ? suggestionB : null;
    if (!pluginSuggestion) {
      return otherSuggestion;
    }
    if (!otherSuggestion) {
      return pluginSuggestion;
    }
    otherSuggestion.score += otherSuggestion.suggestionType < SuggestionType.GlobalCommand ? pluginSuggestion.score + otherSuggestion.score : 0;
    otherSuggestion.subIcon = '⭐';
    otherSuggestion.icon = pluginSuggestion.icon;
    return otherSuggestion;
  }

  private getSuggesters(request: AutocompleteRequest): Suggester[] {
    const suggester = this.store.get(s => s.activeSuggester);
    if(suggester && this.pluginSuggsterManager.has(suggester.name)){
      return [this.pluginSuggsterManager.get(suggester.name)];
    }
    const suggesters: Suggester[] = this.pluginSuggsterManager.getByInput(request.input);
    suggesters.push(this.historySuggesterManager.get(request.location));
    return suggesters.filter(s => !!s);
  }

  private shouldHandled(info: AutocompleteRequest, force: boolean): boolean {
    return info && info.location?.isValid && ((info?.input?.trim().length > 0 && info?.input.trim().length <= MaxCommandLength) || force);
  }

  public addImmediate(info: AutocompleteSave): void {
    setImmediate(() => this.add(info));
  }

  public async add(info: AutocompleteSave): Promise<unknown> {
    if (!info || !info.commandStatus.isSuccessfulReturn || info.command.length === 0 || info.command.length > MaxCommandLength) {
      return;
    }
    const suggester = await this.historySuggesterManager.get(info.location);
    return await suggester.saveExecution(info.location, info.directory, info.command);
  }

  handle(event: KeyboardEvent): boolean {
    if (!this.store.get(s => s.show)) {
      return false;
    }
    switch (event.key) {
      case (Key.ArrowLeft):
        if(!Key.isModifierKeyPressed(event)) {
          this.resetActiveSuggester();
        }
        return false;
      case (Key.ArrowRight):
        if(!Key.isModifierKeyPressed(event)) {
          this.resetActiveSuggester();
        }
        return false;
      case (Key.ArrowDown):
        if (Key.isKeyDownEvent(event)) {
          this.handleArrowDown();
        }
        return true;
      case (Key.ArrowUp):
        if (Key.isKeyDownEvent(event)) {
          this.handleArrowUp();
        }
        return true;
      case (Key.Enter):
        if (Key.isKeyDownEvent(event)) {
          return this.selectFinally(this.store.get(s => s.selectedIndex));
        }
        return true;
      case (Key.Escape):
        if (Key.isKeyDownEvent(event)) {
          this.store.update({selectedIndex: -1, show: false, activeSuggester: undefined, suggestions: []});
        }
        return true;
      default:
        break;
    }
    return false;
  }

  hide() {
    this.store.update({...initialState});
  }

  selectFinally(index: number): boolean {
    if (index < 0) {
      this.store.update({...initialState});
      return false;
    }
    const suggestions = this.store.get(s => s.suggestions);
    const selectedSuggestion = suggestions[index];
    this.forceSuggesterByPlaceholder(selectedSuggestion.nextPlaceholder, selectedSuggestion.terminalCommand);
    this.store.update({selectedIndex: -1, show: false, suggestions: []});
    this.saveSelection(selectedSuggestion).then();
    this.gridService.updateSuggestionToExecute(selectedSuggestion);
    return true;
  }

  private async saveSelection(suggestion: Suggestion) {
    if(suggestion.suggestionType === SuggestionType.Navigation) {return;}
    const suggester = this.historySuggesterManager.get(suggestion.location);
    await suggester.saveSelection(suggestion);
  }

  private handleArrowDown() {
    const suggestions = this.store.get(s => s.suggestions);
    let selectedIndex = this.store.get(s => s.selectedIndex);
    selectedIndex = ++selectedIndex < 0 || suggestions.length === 0 ? suggestions.length - 1 : selectedIndex % suggestions.length;
    this.store.update({selectedIndex});
  }

  private handleArrowUp() {
    const suggestions = this.store.get(s => s.suggestions);
    let selectedIndex = this.store.get(s => s.selectedIndex);
    selectedIndex = --selectedIndex < 0 || suggestions.length === 0 ? suggestions.length - 1 : selectedIndex % suggestions.length;
    this.store.update({selectedIndex});
  }

  selectShow(): Observable<boolean> {
    return this.store.select(s => s.show);
  }

  selectSelectedIndex(): Observable<number> {
    return this.store.select(s => s.selectedIndex);
  }

  selectSuggestionsInfo(): Observable<{ cursorPosition: CursorPosition; suggestions: Suggestion[]; show: boolean }> {
    return this.store.select(s => ({
      suggestions: s.suggestions,
      cursorPosition: s.cursorPosition,
      show: s.show,
    }));
  }

  getShow(): boolean {
    return this.store.get(s => s.show);
  }

  selectSelectedSuggestion(): Observable<Suggestion> {
    return this.store.select(s => s.selectedIndex).pipe(
      map(index => {
        if (index !== -1) {
          return this.store.get(state => state.suggestions[index]);
        } else {
          return null;
        }
      })
    );
  }

  getState(): AutocompleteState {
    return {...this.store.get(s => s)};
  }

  resetActiveSuggester(): void {
    this.store.update({activeSuggester: undefined});
  }

  forceSuggesterByPlaceholder(placeholder: string, currentInput: string): void {
    if(!placeholder || placeholder.length < 5) {this.store.update({activeSuggester: undefined}); return;}
    if(!currentInput) {this.store.update({activeSuggester: undefined}); return;}
    const suggesterName = placeholder.substring(2, placeholder.length - 2);
    if(!this.pluginSuggsterManager.has(suggesterName)){this.store.update({activeSuggester: undefined}); return;}
    if(this.store.get(s => s.activeSuggester?.name) === suggesterName) {return;}
    this.store.update({activeSuggester: {name: suggesterName, sourceCommand: currentInput}});
  }

  async reload() {
    await this.historySuggesterManager.reload();
  }

  openCommandSettings(suggestion: Suggestion) {
    this.gridService.addNewTabOnActivePane(TabType.Settings, {selectedNav: Nav.Commands,
      filter: suggestion.label});
  }
}
