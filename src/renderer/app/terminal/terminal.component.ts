import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  NgZone,
  ViewChild
} from '@angular/core';
import {TerminalService, TerminalState} from './+state/terminal.service';
import {TabComponent} from '../+shared/abstract-components/tab/tab.component';
import {ContextMenuService} from './context-menu/+state/context-menu.service';
import {SearchMenuService} from './search/+state/search-menu.service';
import {FooterComponent} from './footer/footer.component';
import {SearchComponent} from './search/search.component';
import {ScrollToBottomComponent} from './scroll-down/scroll-to-bottom.component';
import {ContextMenuComponent} from './context-menu/context-menu.component';
import {CommandContextMenuComponent} from './command-context-menu/command-context-menu.component';
import {CommandContextMenuService} from './command-context-menu/+state/command-context-menu.service';
import {Renderer} from './+state/renderer/renderer';
import {HeaderComponent} from './header/header.component';
import {Shell} from './+state/shell/shell';
import {DecorationHandler} from './+state/handler/decoration.handler';
import {SelectionHandler} from './+state/handler/selection.handler';
import {AsyncPipe, NgIf} from '@angular/common';
import {TerminalStatusObserver} from './+state/osc/terminal-status.observer';
import {TerminalKeyboardService} from './+state/terminal-keyboard.service';


@Component({
    selector: 'app-terminal',
    templateUrl: './terminal.component.html',
    styleUrls: ['./terminal.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [TerminalService, ContextMenuService, SearchMenuService, CommandContextMenuService, Shell, Renderer, DecorationHandler, SelectionHandler, TerminalStatusObserver, TerminalKeyboardService],
    imports: [
        HeaderComponent,
        FooterComponent,
        ContextMenuComponent,
        SearchComponent,
        ScrollToBottomComponent,
        CommandContextMenuComponent,
        AsyncPipe
    ]
})
export class TerminalComponent extends TabComponent<TerminalState> implements AfterViewInit {

  @ViewChild('terminal')
  terminal: ElementRef;

  constructor(
    private terminalService: TerminalService,
    private terminalShortcutService: TerminalKeyboardService,
    private contextMenuService: ContextMenuService,
    private commandContextMenuService: CommandContextMenuService,
    private zone: NgZone) {
    super(terminalService);
  }

  ngAfterViewInit(): void {
    this.terminalService.bind(this.terminal.nativeElement);
    this.terminalShortcutService.init();
    this.terminalService.onCommandMarkerClicked()
      .subscribe((data) => {
        this.zone.run(() => {
          this.commandContextMenuService.openMenuOnPosition(data);
        });
      });
    this.terminalService.select(s => s?.isPaddingRemoved)
      .subscribe((removePadding) => {
        this.zone.run(() => {
          if(removePadding) {
            this.terminal.nativeElement.style.setProperty('--padding-xterm', '0');
            this.terminal.nativeElement.style.setProperty('--padding', '0');
            this.terminal.nativeElement.style.backgroundColor = 'var(--background-color)';
            this.focus();
          } else if(this.terminal.nativeElement.style.getPropertyValue('--padding')) {
            this.terminal.nativeElement.style.removeProperty('--padding-xterm');
            this.terminal.nativeElement.style.removeProperty('--padding');
            this.terminal.nativeElement.style.backgroundColor = '';
            this.focus();
          }
        });
      });
  }

  focus() {
    this.terminalService.updateActiveTab();
  }

  showContextMenuOrPaste(event: any) {
    if(this.terminalService.get(s => s.isAppRunning)) {return;}
    if (this.terminalService.isPasteOnRightClickEnabled() && this.terminalService.hasStringToCopy()) {
      this.terminalService.paste();
      this.terminalService.clearClipboard();
    } else {
      this.terminalService.updateActiveTab();
      const rect = this.terminal.nativeElement.getBoundingClientRect();
      const x = event['clientX'] - rect.left;
      const y = event['clientY'] - rect.top;
      this.contextMenuService.openMenuOnPosition({x, y});
    }
  }
}
