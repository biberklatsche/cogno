import {Observable, Subject} from 'rxjs';
import {
  Dimensions,
  RendererState,
  SessionData,
  SessionDataType
} from '../../../../../shared/models/models';
import {ShellConfig, Theme} from '../../../../../shared/models/settings';
import {IpcChannel} from '../../../../../shared/ipc.chanels';
import {Logger} from '../../../logger/logger';
import {ElectronService} from '../../../+shared/services/electron/electron.service';
import {Selection} from '../models';
import {Injectable, OnDestroy} from '@angular/core';
import {ShellAdapter} from './adapter/shell.adapter';
import {Stack} from '../../../+shared/models/stack';
import {ShellAdapterFactory} from './adapter/factory';
import {Chars} from "../../../../../shared/chars";

@Injectable()
export class Shell implements OnDestroy{
  private _id: string;
  private _dimensions: Dimensions;
  private isClosing = false;
  private adapterStack: Stack<ShellAdapter> = new Stack();
  private readonly _onData = new Subject<string>();
  private readonly _onError = new Subject<any>();
  private readonly _onExit = new Subject<number>();

  sessionListener = (event, datas: SessionData[]) => {
    if (datas && datas.length === 1) {
      const data = datas[0];
      if (data.id === this._id) {
        switch (data.type) {
          case SessionDataType.Error: {
            Logger.debug('shell error from main - close shell', data);
            this._onError.next(data);
            break;
          }
          case SessionDataType.Output: {
            this.ack(data.ackLength);
            this._onData.next(data.data);
            break;
          }
          case SessionDataType.Closed: {
            Logger.debug('shell closed from main', data);
            this._onExit.next(0);
            break;
          }
        }
      }
    }
  };

  constructor(private electron: ElectronService) {
  }

  ngOnDestroy(): void {
    this.exit();
  }

  getActiveShellConfig(): ShellConfig {
    return this.adapterStack.top().config;
  }

  onData(): Observable<string> {
    return this._onData;
  }

  onError(): Observable<any> {
    return this._onError;
  }

  onExit(): Observable<number> {
    return this._onExit;
  }

  get dimensions(): Dimensions {
    return this._dimensions;
  }

  write(data: string) {
    if (!data || data.length === 0) {
      return;
    }
    const command: SessionData = {
      id: this._id,
      type: SessionDataType.Input,
      data: data
    };
    this.electron.send(IpcChannel.Session, command);
  }

  resize(dimensions: Dimensions) {
    this._dimensions = dimensions;
    Logger.debug('send shell resize to main', dimensions);
    const command: SessionData = {
      id: this._id,
      type: SessionDataType.Resize,
      dimensions: dimensions
    };
    this.electron.send(IpcChannel.Session, command);
  }

  paste(stringToPaste: string, rendererState: RendererState, selection: Selection) {
    const replaced = stringToPaste.replace(/\r\n/gm, '\t').replace(/\n/gm, '\t');
    this.write(this.adapterStack.top().paste(replaced, rendererState, selection));
  }

  clearLine(rendererState: RendererState) {
    this.write(this.adapterStack.top().clearLine(rendererState));
  }

  clearLineToEnd(rendererState: RendererState) {
    this.write(this.adapterStack.top().clearLineToEnd(rendererState));
  }

  clearLineToStart(rendererState: RendererState) {
    this.write(this.adapterStack.top().clearLineToStart(rendererState));
  }

  goToNextArgument(rendererState: RendererState): string {
    const result = this.adapterStack.top().goToNextArgument(rendererState);
    this.write(result.command);
    return result.placeholder;
  }

  goToPreviousArgument(rendererState: RendererState): string {
    const result = this.adapterStack.top().goToPreviousArgument(rendererState);
    this.write(result.command);
    return result.placeholder;
  }

  deletePreviousWord(rendererState: RendererState) {
    this.write(this.adapterStack.top().deletePreviousWord(rendererState));
  }

  deleteNextWord(rendererState: RendererState) {
    this.write(this.adapterStack.top().deleteNextWord(rendererState));
  }

  cut(rendererState: RendererState, selection: Selection) {
    this.write(this.adapterStack.top().cut(rendererState, selection));
  }

  abortTask() {
    this.write(this.adapterStack.top().abortTask());
  }

  cursorLeft(rendererState: RendererState): boolean {
    const command = this.adapterStack.top().cursorLeft(rendererState);
    if(!command) {return false;}
    this.write(command);
    return true;
  }

  cursorRight(rendererState: RendererState): boolean {
    const command = this.adapterStack.top().cursorRight(rendererState);
    if(!command) {return false;}
    this.write(command);
    return true;
  }

  cursorToNextWord(rendererState: RendererState): number {
    const result: {steps: number; command: string} = this.adapterStack.top().cursorToNextWord(rendererState);
    this.write(result.command);
    return result.steps;
  }

  cursorToPreviousWord(rendererState: RendererState): number {
    const result: {steps: number; command: string} = this.adapterStack.top().cursorToPreviousWord(rendererState);
    this.write(result.command);
    return result.steps;
  }

  cursorToStartOfLine(rendererState: RendererState): number {
    const result: {steps: number; command: string} = this.adapterStack.top().cursorToStartOfLine(rendererState);
    this.write(result.command);
    return result.steps;
  }

  cursorToEndOfLine(rendererState: RendererState): number {
    const result: {steps: number; command: string} = this.adapterStack.top().cursorToEndOfLine(rendererState);
    this.write(result.command);
    return result.steps;
  }

  exit() {
    if (!this.isClosing) {
      this.isClosing = true;
      Logger.debug('send shell close to main');
      const command: SessionData = {
        id: this._id,
        type: SessionDataType.Exit
      };
      this.electron.send(IpcChannel.Session, command);
    }
    this.removeOutputGuard();
  }

  init(shellConfig: ShellConfig, id: string) {
    if (!shellConfig) {
      throw new Error('Missing shell config.');
    }
    this.adapterStack.push(ShellAdapterFactory.create(shellConfig));
    if (id) {
      this._id = id;
      const startCommand: SessionData = {
        id: this._id,
        type: SessionDataType.New,
        settings: shellConfig
      };
      Logger.debug('send shell start command', startCommand);
      this.electron.send(IpcChannel.Session, startCommand);
      this.electron.on(IpcChannel.Session, this.sessionListener);
    }
  }

  pushSubShell(shellConfig: ShellConfig) {
    if (!shellConfig) {
      throw new Error('Missing shell config.');
    }
    shellConfig.isSubshell = true;
    this.adapterStack.push(ShellAdapterFactory.create(shellConfig));
  }

  private removeOutputGuard() {
    if (!this.adapterStack.size()) {
      return;
    }
    this.adapterStack.pop();
  }

  private ack(length: number) {
    const command: SessionData = {
      id: this._id,
      type: SessionDataType.Ack,
      ackLength: length
    };
    this.electron.send(IpcChannel.Session, command);
  }

  isLastSubShell(): boolean {
    return this.adapterStack.filter(s => s.config.isSubshell)?.length <= 1;
  }

  injectAdvancedFeatures(script: string, theme: Theme) {
    const commands = this.adapterStack.top().injectCommands(script, theme);
    for (const command of commands) {
      this.write(command + Chars.Enter.asString);
    }
  }

  getConfigMachineName(): string | undefined {
    return this.adapterStack.top().config.machineName;
  }
}
