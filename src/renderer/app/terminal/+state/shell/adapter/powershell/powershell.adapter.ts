import {ShellAdapter} from '../shell.adapter';
import {Chars} from '../../../../../../../shared/chars';
import {ColorName, ShellConfig, Theme} from '../../../../../../../shared/models/settings';

const gitBranchFunc = `function Get-BranchName {
    try {
        $branch = git rev-parse --abbrev-ref HEAD;
        if ($branch -eq "HEAD") {
            $branch = git rev-parse --short HEAD;
            return $branch;
        }
        else {
            return $branch;
        }
    } catch {
        return "";
    }
}`;

export class PowershellAdapter extends ShellAdapter {

  injectCommands(script: string, theme: Theme): string[] {
    // Split the script into lines
    const lines = script.split(/\r?\n/);

    // Process each line
    const processedLines = lines
      .map(line => {
        // Remove comments (anything after '#')
        const commentIndex = line.indexOf('#');
        if (commentIndex !== -1) {
          line = line.substring(0, commentIndex);
        }
        // Trim whitespace
        return line.trim();
      })
      .filter(line => line.length > 0); // Remove empty lines

    // Join the lines with semicolons
    const injectScript = processedLines.join('; ');

    const changePromptScript = theme.promptVersion > 0 ? this.changePromptCommand(theme) + ' ' : '';
    const cleanup = this.config.isDebugModeEnabled ? '' : this.clear() + this.deleteHistory();
    // Return the one-liner
    return  [' $OutputEncoding = [System.Console]::OutputEncoding = [System.Console]::InputEncoding = [System.Text.Encoding]::Utf8; $PSDefaultParameterValues[\'*:Encoding\'] = \'utf8\'; ' + changePromptScript + injectScript + cleanup];
  }

  private static prompts = [
    ['', (colors: {
      foreground?: ColorName;
      background?: ColorName;
    }[]) => `Write-Host $promptString -ForegroundColor ${this.toTerminalColor(colors[0].foreground)};`],
    ['', (colors: {
      foreground?: ColorName;
      background?: ColorName;
    }[]) => `Write-Host $promptString -NoNewline -BackgroundColor ${this.toTerminalColor(colors[0].background)} -ForegroundColor ${this.toTerminalColor(colors[0].foreground)}; Write-Host ${Chars.TriangleLeft.asString} -ForegroundColor ${this.toTerminalColor(colors[0].background)};`],
    ['', (colors: {
      foreground?: ColorName;
      background?: ColorName;
    }[]) => `Write-Host $promptString -NoNewline -BackgroundColor ${this.toTerminalColor(colors[0].background)} -ForegroundColor ${this.toTerminalColor(colors[0].foreground)}; Write-Host ${Chars.TriangleLeft.asString}${Chars.TriangleSeparatorLeft.asString} -ForegroundColor ${this.toTerminalColor(colors[0].background)};`],
    ['', (colors: {
      foreground?: ColorName;
      background?: ColorName;
    }[]) => `Write-Host $promptString -NoNewline -BackgroundColor ${this.toTerminalColor(colors[0].background)} -ForegroundColor ${this.toTerminalColor(colors[0].foreground)}; Write-Host ${Chars.CircleLeft.asString} -ForegroundColor ${this.toTerminalColor(colors[0].background)};`],
    ['', (colors: {
      foreground?: ColorName;
      background?: ColorName;
    }[]) => `Write-Host $promptString -NoNewline -BackgroundColor ${this.toTerminalColor(colors[0].background)} -ForegroundColor ${this.toTerminalColor(colors[0].foreground)}; Write-Host ${Chars.CircleLeft.asString}${Chars.CircleSeparatorLeft.asString} -ForegroundColor ${this.toTerminalColor(colors[0].background)};`],
    ['', (colors: {
      foreground?: ColorName;
      background?: ColorName;
    }[]) => `Write-Host $promptString -NoNewline -BackgroundColor ${this.toTerminalColor(colors[0].background)} -ForegroundColor ${this.toTerminalColor(colors[0].foreground)}; Write-Host ${Chars.TriangleLeft.asString} -NoNewline -BackgroundColor ${this.toTerminalColor(colors[1].background)} -ForegroundColor ${this.toTerminalColor(colors[0].background)}; Write-Host $branchName -NoNewline -BackgroundColor ${this.toTerminalColor(colors[1].background)} -ForegroundColor ${this.toTerminalColor(colors[1].foreground)}; Write-Host ${Chars.TriangleLeft.asString} -ForegroundColor ${this.toTerminalColor(colors[1].background)};`],
    ['', (colors: {
      foreground?: ColorName;
      background?: ColorName;
    }[]) => `Write-Host $promptString -NoNewline -BackgroundColor ${this.toTerminalColor(colors[0].background)} -ForegroundColor ${this.toTerminalColor(colors[0].foreground)}; Write-Host ${Chars.TriangleLeft.asString} -NoNewline -BackgroundColor ${this.toTerminalColor(colors[1].background)} -ForegroundColor ${this.toTerminalColor(colors[0].background)}; Write-Host $branchName -NoNewline -BackgroundColor ${this.toTerminalColor(colors[1].background)} -ForegroundColor ${this.toTerminalColor(colors[1].foreground)}; Write-Host ${Chars.TriangleLeft.asString}${Chars.TriangleSeparatorLeft.asString} -ForegroundColor ${this.toTerminalColor(colors[1].background)};`],
    ['', (colors: {
      foreground?: ColorName;
      background?: ColorName;
    }[]) => `Write-Host $promptString -NoNewline -BackgroundColor ${this.toTerminalColor(colors[0].background)} -ForegroundColor ${this.toTerminalColor(colors[0].foreground)}; Write-Host ${Chars.CircleLeft.asString} -NoNewline -BackgroundColor ${this.toTerminalColor(colors[1].background)} -ForegroundColor ${this.toTerminalColor(colors[0].background)}; Write-Host $branchName -NoNewline -BackgroundColor ${this.toTerminalColor(colors[1].background)} -ForegroundColor ${this.toTerminalColor(colors[1].foreground)}; Write-Host ${Chars.CircleLeft.asString} -ForegroundColor ${this.toTerminalColor(colors[1].background)};`],
    ['', (colors: {
      foreground?: ColorName;
      background?: ColorName;
    }[]) => `Write-Host $promptString -NoNewline -BackgroundColor ${this.toTerminalColor(colors[0].background)} -ForegroundColor ${this.toTerminalColor(colors[0].foreground)}; Write-Host ${Chars.CircleLeft.asString} -NoNewline -BackgroundColor ${this.toTerminalColor(colors[1].background)} -ForegroundColor ${this.toTerminalColor(colors[0].background)}; Write-Host $branchName -NoNewline -BackgroundColor ${this.toTerminalColor(colors[1].background)} -ForegroundColor ${this.toTerminalColor(colors[1].foreground)}; Write-Host ${Chars.CircleLeft.asString}${Chars.CircleSeparatorLeft.asString} -ForegroundColor ${this.toTerminalColor(colors[1].background)};`],

    [Chars.BottomLeftCorner.asString, (colors: {
      foreground?: ColorName;
      background?: ColorName;
    }[]) => `Write-Host ${Chars.TopLeftCorner.asString} -noNewLine; Write-Host ${Chars.TriangleRight.asString} -noNewLine -ForegroundColor ${this.toTerminalColor(colors[0].background)}; Write-Host $promptString -NoNewline -BackgroundColor ${this.toTerminalColor(colors[0].background)} -ForegroundColor ${this.toTerminalColor(colors[0].foreground)}; Write-Host ${Chars.TriangleLeft.asString} -ForegroundColor ${this.toTerminalColor(colors[0].background)};`],
    [Chars.BottomLeftCorner.asString, (colors: {
      foreground?: ColorName;
      background?: ColorName;
    }[]) => `Write-Host ${Chars.TopLeftCorner.asString} -noNewLine; Write-Host ${Chars.TriangleRight.asString} -noNewLine -ForegroundColor ${this.toTerminalColor(colors[0].background)}; Write-Host $promptString -NoNewline -BackgroundColor ${this.toTerminalColor(colors[0].background)} -ForegroundColor ${this.toTerminalColor(colors[0].foreground)}; Write-Host ${Chars.TriangleLeft.asString}${Chars.TriangleSeparatorLeft.asString} -ForegroundColor ${this.toTerminalColor(colors[0].background)};`],
    [Chars.BottomLeftCorner.asString, (colors: {
      foreground?: ColorName;
      background?: ColorName;
    }[]) => `Write-Host ${Chars.TopLeftCorner.asString} -noNewLine; Write-Host ${Chars.CircleRight.asString} -noNewLine -ForegroundColor ${this.toTerminalColor(colors[0].background)}; Write-Host $promptString -NoNewline -BackgroundColor ${this.toTerminalColor(colors[0].background)} -ForegroundColor ${this.toTerminalColor(colors[0].foreground)}; Write-Host ${Chars.CircleLeft.asString} -ForegroundColor ${this.toTerminalColor(colors[0].background)};`],
    [Chars.BottomLeftCorner.asString, (colors: {
      foreground?: ColorName;
      background?: ColorName;
    }[]) => `Write-Host ${Chars.TopLeftCorner.asString} -noNewLine; Write-Host ${Chars.CircleRight.asString} -noNewLine -ForegroundColor ${this.toTerminalColor(colors[0].background)}; Write-Host $promptString -NoNewline -BackgroundColor ${this.toTerminalColor(colors[0].background)} -ForegroundColor ${this.toTerminalColor(colors[0].foreground)}; Write-Host ${Chars.CircleLeft.asString}${Chars.CircleSeparatorLeft.asString} -ForegroundColor ${this.toTerminalColor(colors[0].background)};`],
    [Chars.BottomLeftCorner.asString, (colors: {
      foreground?: ColorName;
      background?: ColorName;
    }[]) => `Write-Host ${Chars.TopLeftCorner.asString} -noNewLine; Write-Host ${Chars.TriangleRight.asString} -noNewLine -ForegroundColor ${this.toTerminalColor(colors[0].background)}; Write-Host $promptString -NoNewline -BackgroundColor ${this.toTerminalColor(colors[0].background)} -ForegroundColor ${this.toTerminalColor(colors[0].foreground)}; Write-Host ${Chars.TriangleLeft.asString} -NoNewline -BackgroundColor ${this.toTerminalColor(colors[1].background)} -ForegroundColor ${this.toTerminalColor(colors[0].background)}; Write-Host $branchName -NoNewline -BackgroundColor ${this.toTerminalColor(colors[1].background)} -ForegroundColor ${this.toTerminalColor(colors[1].foreground)}; Write-Host ${Chars.TriangleLeft.asString} -ForegroundColor ${this.toTerminalColor(colors[1].background)};`],
    [Chars.BottomLeftCorner.asString, (colors: {
      foreground?: ColorName;
      background?: ColorName;
    }[]) => `Write-Host ${Chars.TopLeftCorner.asString} -noNewLine; Write-Host ${Chars.TriangleRight.asString} -noNewLine -ForegroundColor ${this.toTerminalColor(colors[0].background)}; Write-Host $promptString -NoNewline -BackgroundColor ${this.toTerminalColor(colors[0].background)} -ForegroundColor ${this.toTerminalColor(colors[0].foreground)}; Write-Host ${Chars.TriangleLeft.asString} -NoNewline -BackgroundColor ${this.toTerminalColor(colors[1].background)} -ForegroundColor ${this.toTerminalColor(colors[0].background)}; Write-Host $branchName -NoNewline -BackgroundColor ${this.toTerminalColor(colors[1].background)} -ForegroundColor ${this.toTerminalColor(colors[1].foreground)}; Write-Host ${Chars.TriangleLeft.asString}${Chars.TriangleSeparatorLeft.asString} -ForegroundColor ${this.toTerminalColor(colors[1].background)};`],
    [Chars.BottomLeftCorner.asString, (colors: {
      foreground?: ColorName;
      background?: ColorName;
    }[]) => `Write-Host ${Chars.TopLeftCorner.asString} -noNewLine; Write-Host ${Chars.CircleRight.asString} -noNewLine -ForegroundColor ${this.toTerminalColor(colors[0].background)}; Write-Host $promptString -NoNewline -BackgroundColor ${this.toTerminalColor(colors[0].background)} -ForegroundColor ${this.toTerminalColor(colors[0].foreground)}; Write-Host ${Chars.CircleLeft.asString} -NoNewline -BackgroundColor ${this.toTerminalColor(colors[1].background)} -ForegroundColor ${this.toTerminalColor(colors[0].background)}; Write-Host $branchName -NoNewline -BackgroundColor ${this.toTerminalColor(colors[1].background)} -ForegroundColor ${this.toTerminalColor(colors[1].foreground)}; Write-Host ${Chars.CircleLeft.asString} -ForegroundColor ${this.toTerminalColor(colors[1].background)};`],
    [Chars.BottomLeftCorner.asString, (colors: {
      foreground?: ColorName;
      background?: ColorName;
    }[]) => `Write-Host ${Chars.TopLeftCorner.asString} -noNewLine; Write-Host ${Chars.CircleRight.asString} -noNewLine -ForegroundColor ${this.toTerminalColor(colors[0].background)}; Write-Host $promptString -NoNewline -BackgroundColor ${this.toTerminalColor(colors[0].background)} -ForegroundColor ${this.toTerminalColor(colors[0].foreground)}; Write-Host ${Chars.CircleLeft.asString} -NoNewline -BackgroundColor ${this.toTerminalColor(colors[1].background)} -ForegroundColor ${this.toTerminalColor(colors[0].background)}; Write-Host $branchName -NoNewline -BackgroundColor ${this.toTerminalColor(colors[1].background)} -ForegroundColor ${this.toTerminalColor(colors[1].foreground)}; Write-Host ${Chars.CircleLeft.asString}${Chars.CircleSeparatorLeft.asString} -ForegroundColor ${this.toTerminalColor(colors[1].background)};`]

  ];

  constructor(shellConfig: ShellConfig) {
    super(shellConfig);
  }

  changePromptCommand(theme: Theme): string {
    const promptHeading = PowershellAdapter.prompts[theme.promptVersion - 1][1](theme.colors.promptColors);
    const prompt = `
    $promptString = $(Get-Location);
    $branchName = " ${Chars.Branch.asString} $(Get-BranchName)";
    ${promptHeading}
    "${PowershellAdapter.prompts[theme.promptVersion - 1][0]}${theme.prompt} "
    `;
    return gitBranchFunc.replace(/(\r\n|\n|\r)/gm, '') + '; $global:COGNO_PS1 = "' + prompt
      .replace(/"/gm, '`"')
      .replace(/\$/gm, '`$')
      .replace(/(\r\n|\n|\r)/gm, '') + '";';
  }

  private deleteHistory(): string {
    return ' Clear-History; [Microsoft.PowerShell.PSConsoleReadLine]::ClearHistory();';
  }

  private clear(): string {
    return ' Clear-Host';
  }

  cursorLeftSequence(): string {
    return `${Chars.Escape.asString}[D`;
  }

  cursorRightSequence(): string {
    return `${Chars.Escape.asString}[C`;
  }

  backspaceSequence(): string {
    return `${Chars.Delete.asString}`;
  }

  abortTask(): string {
    return Chars.EndOfText.asString;
  }

  static toTerminalColor(color: ColorName): string {
    switch (color) {
      case ColorName.white:
        return 'White';
      case ColorName.brightWhite:
        return 'White';
      case ColorName.cyan:
        return 'DarkCyan';
      case ColorName.brightCyan:
        return 'Cyan';
      case ColorName.blue:
        return 'DarkBlue';
      case ColorName.brightBlue:
        return 'Blue';
      case ColorName.magenta:
        return 'DarkMagenta';
      case ColorName.brightMagenta:
        return 'Magenta';
      case ColorName.green:
        return 'DarkGreen';
      case ColorName.brightGreen:
        return 'Green';
      case ColorName.black:
        return 'Black';
      case ColorName.brightBlack:
        return 'Black';
      case ColorName.yellow:
        return 'DarkYellow';
      case ColorName.brightYellow:
        return 'Yellow';
      case ColorName.red:
        return 'DarkRed';
      case ColorName.brightRed:
        return 'Red';
      default:
        return 'Cyan';
    }
  }
}
