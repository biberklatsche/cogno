import {ZshAdapter} from './zsh.adapter';
import {ColorName} from '../../../../../../../shared/models/settings';
import {TestHelper} from '../../../../../../../test/helper';

describe('ZSH Adapter', () => {

  it('should calc terminal colors - empty', () => {
    expect(ZshAdapter.toTerminalColor(null)).toBeTruthy();
    expect(ZshAdapter.toTerminalColor(undefined)).toBeTruthy();
  });

  it('should work calc terminal colors', () => {
    let defaultColorCount = 0;
    for (const [key, value] of Object.entries(ColorName)) {
      const color = ZshAdapter.toTerminalColor(value);
      expect(color).toBeTruthy();
      if (color === '%f') {
        defaultColorCount++;
      }
    }
    expect(defaultColorCount).toBe(6);
  });
});
