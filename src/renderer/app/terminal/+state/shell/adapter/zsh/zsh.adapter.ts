import {ShellAdapter} from '../shell.adapter';
import {Char, Chars} from '../../../../../../../shared/chars';
import {ColorName, ShellConfig, Theme} from '../../../../../../../shared/models/settings';

const gitBranchFunc = 'git_branch() { git branch 2> /dev/null | sed -n -e "s/^\\* \\(.*\\)/\\1/p"; };';

const workingDir = '%d';
const gitBranch = '$(git_branch)';

export class ZshAdapter extends ShellAdapter {
  injectCommands(script: string, theme: Theme): string[] {

    let inHereDoc = false;
    let hereDocDelimiter = '';

    // Convert the script into a one-liner
    const injectScript = ' ' + script
      .split('\n')
      .map(line => line.trim())
      .filter(line => line && !line.startsWith('#') && !line.startsWith('!'))
      .map(line => {
        // Handle here-documents
        if (inHereDoc) {
          if (line === hereDocDelimiter) {
            inHereDoc = false;
          }
          return line;
        } else if (line.includes('<<')) {
          inHereDoc = true;
          hereDocDelimiter = line.split('<<')[1].trim();
          return line;
        }

        // Handle function definitions and control structures
        if (line.endsWith('{')) {
          return line;
        } else {
          return line.endsWith(';') ? line : line + ';';
        }
      })
      .join(' ');
    const changePromptScript = theme.promptVersion > 0 ? ' export LANG=en_US.UTF-8; export LC_ALL=en_US.UTF-8; export LC_MESSAGES=en_US.UTF-8; export LC_NUMERIC=en_US.UTF-8; export LC_MONETARY=en_US.UTF-8; export LC_COLLATE=en_US.UTF-8;' +  this.changePromptCommand(theme) + ' ' : '';
    const cleanup = this.config.isDebugModeEnabled ? '' : ZshAdapter.clear() + ZshAdapter.deleteHistory();
    return [
      ' setopt HIST_IGNORE_SPACE;',
      changePromptScript + injectScript + cleanup
    ];
  }

  private static deleteHistory(): string {
    return '  history -p;';
  }

  private static clear(): string {
    return ' clear;';
  }

  private static prompts = [
    ['', (colors: {foreground?: ColorName; background?: ColorName}[]) => `${this.toTerminalColor(colors[0].foreground)}${workingDir}${this.toTerminalColor(ColorName.foreground)}${this.toTerminalBackgroundColor(ColorName.background)}`],
    ['', (colors: {foreground?: ColorName; background?: ColorName}[]) => `${this.toTerminalBackgroundColor(colors[0].background)}${this.toTerminalColor(colors[0].foreground)}${workingDir}${this.toTerminalColor(colors[0].background)}${this.toTerminalBackgroundColor(colors[0].foreground)}${Chars.TriangleLeft.asUtf8}${this.toTerminalColor(ColorName.foreground)}${this.toTerminalBackgroundColor(ColorName.background)}`],
    ['', (colors: {foreground?: ColorName; background?: ColorName}[]) => `${this.toTerminalBackgroundColor(colors[0].background)}${this.toTerminalColor(colors[0].foreground)}${workingDir}${this.toTerminalColor(colors[0].background)}${this.toTerminalBackgroundColor(colors[0].foreground)}${Chars.TriangleLeft.asUtf8}${Chars.TriangleSeparatorLeft.asUtf8}${this.toTerminalColor(ColorName.foreground)}${this.toTerminalBackgroundColor(ColorName.background)}`],
    ['', (colors: {foreground?: ColorName; background?: ColorName}[]) => `${this.toTerminalBackgroundColor(colors[0].background)}${this.toTerminalColor(colors[0].foreground)}${workingDir}${this.toTerminalColor(colors[0].background)}${this.toTerminalBackgroundColor(colors[0].foreground)}${Chars.CircleLeft.asUtf8}${this.toTerminalColor(ColorName.foreground)}${this.toTerminalBackgroundColor(ColorName.background)}`],
    ['', (colors: {foreground?: ColorName; background?: ColorName}[]) => `${this.toTerminalBackgroundColor(colors[0].background)}${this.toTerminalColor(colors[0].foreground)}${workingDir}${this.toTerminalColor(colors[0].background)}${this.toTerminalBackgroundColor(colors[0].foreground)}${Chars.CircleLeft.asUtf8}${Chars.CircleSeparatorLeft.asUtf8}${this.toTerminalColor(ColorName.foreground)}${this.toTerminalBackgroundColor(ColorName.background)}`],
    ['', (colors: {foreground?: ColorName; background?: ColorName}[]) => `${this.toTerminalBackgroundColor(colors[0].background)}${this.toTerminalColor(colors[0].foreground)}${workingDir}${this.toTerminalColor(colors[0].background)}${this.toTerminalBackgroundColor(colors[1].background)}${Chars.TriangleLeft.asUtf8}${this.toTerminalBackgroundColor(colors[1].background)}${this.toTerminalColor(colors[1].foreground)} ${Chars.Branch.asUtf8} ${gitBranch}${this.toTerminalBackgroundColor(colors[1].foreground)}${this.toTerminalColor(colors[1].background)}${Chars.TriangleLeft.asUtf8}${this.toTerminalColor(ColorName.foreground)}${this.toTerminalBackgroundColor(ColorName.background)}`],
    ['', (colors: {foreground?: ColorName; background?: ColorName}[]) => `${this.toTerminalBackgroundColor(colors[0].background)}${this.toTerminalColor(colors[0].foreground)}${workingDir}${this.toTerminalColor(colors[0].background)}${this.toTerminalBackgroundColor(colors[1].background)}${Chars.TriangleLeft.asUtf8}${this.toTerminalBackgroundColor(colors[1].background)}${this.toTerminalColor(colors[1].foreground)} ${Chars.Branch.asUtf8} ${gitBranch}${this.toTerminalBackgroundColor(colors[1].foreground)}${this.toTerminalColor(colors[1].background)}${Chars.TriangleLeft.asUtf8}}${Chars.TriangleSeparatorLeft.asUtf8}${this.toTerminalColor(ColorName.foreground)}${this.toTerminalBackgroundColor(ColorName.background)}`],
    ['', (colors: {foreground?: ColorName; background?: ColorName}[]) => `${this.toTerminalBackgroundColor(colors[0].background)}${this.toTerminalColor(colors[0].foreground)}${workingDir}${this.toTerminalColor(colors[0].background)}${this.toTerminalBackgroundColor(colors[1].background)}${Chars.CircleLeft.asUtf8}${this.toTerminalBackgroundColor(colors[1].background)}${this.toTerminalColor(colors[1].foreground)} ${Chars.Branch.asUtf8} ${gitBranch}${this.toTerminalBackgroundColor(colors[1].foreground)}${this.toTerminalColor(colors[1].background)}${Chars.CircleLeft.asUtf8}${this.toTerminalColor(ColorName.foreground)}${this.toTerminalBackgroundColor(ColorName.background)}`],
    ['', (colors: {foreground?: ColorName; background?: ColorName}[]) => `${this.toTerminalBackgroundColor(colors[0].background)}${this.toTerminalColor(colors[0].foreground)}${workingDir}${this.toTerminalColor(colors[0].background)}${this.toTerminalBackgroundColor(colors[1].background)}${Chars.CircleLeft.asUtf8}${this.toTerminalBackgroundColor(colors[1].background)}${this.toTerminalColor(colors[1].foreground)} ${Chars.Branch.asUtf8} ${gitBranch}${this.toTerminalBackgroundColor(colors[1].foreground)}${this.toTerminalColor(colors[1].background)}${Chars.CircleLeft.asUtf8}}${Chars.CircleSeparatorLeft.asUtf8}${this.toTerminalColor(ColorName.foreground)}${this.toTerminalBackgroundColor(ColorName.background)}`],

    [Chars.BottomLeftCorner.asString, (colors: {foreground?: ColorName; background?: ColorName}[]) => `${Chars.TopLeftCorner.asUtf8}${this.toTerminalColor(colors[0].background)}${this.toTerminalBackgroundColor(colors[0].foreground)}${Chars.TriangleRight.asUtf8}${this.toTerminalBackgroundColor(colors[0].background)}${this.toTerminalColor(colors[0].foreground)}${workingDir}${this.toTerminalColor(colors[0].background)}${this.toTerminalBackgroundColor(colors[0].foreground)}${Chars.TriangleLeft.asUtf8}${this.toTerminalColor(ColorName.foreground)}${this.toTerminalBackgroundColor(ColorName.background)}`],
    [Chars.BottomLeftCorner.asString, (colors: {foreground?: ColorName; background?: ColorName}[]) => `${Chars.TopLeftCorner.asUtf8}${this.toTerminalColor(colors[0].background)}${this.toTerminalBackgroundColor(colors[0].foreground)}${Chars.TriangleRight.asUtf8}${this.toTerminalBackgroundColor(colors[0].background)}${this.toTerminalColor(colors[0].foreground)}${workingDir}${this.toTerminalColor(colors[0].background)}${this.toTerminalBackgroundColor(colors[0].foreground)}${Chars.TriangleLeft.asUtf8}${Chars.TriangleSeparatorLeft.asUtf8}${this.toTerminalColor(ColorName.foreground)}${this.toTerminalBackgroundColor(ColorName.background)}`],
    [Chars.BottomLeftCorner.asString, (colors: {foreground?: ColorName; background?: ColorName}[]) => `${Chars.TopLeftCorner.asUtf8}${this.toTerminalColor(colors[0].background)}${this.toTerminalBackgroundColor(colors[0].foreground)}${Chars.CircleRight.asUtf8}${this.toTerminalBackgroundColor(colors[0].background)}${this.toTerminalColor(colors[0].foreground)}${workingDir}${this.toTerminalColor(colors[0].background)}${this.toTerminalBackgroundColor(colors[0].foreground)}${Chars.CircleLeft.asUtf8}${this.toTerminalColor(ColorName.foreground)}${this.toTerminalBackgroundColor(ColorName.background)}`],
    [Chars.BottomLeftCorner.asString, (colors: {foreground?: ColorName; background?: ColorName}[]) => `${Chars.TopLeftCorner.asUtf8}${this.toTerminalColor(colors[0].background)}${this.toTerminalBackgroundColor(colors[0].foreground)}${Chars.CircleRight.asUtf8}${this.toTerminalBackgroundColor(colors[0].background)}${this.toTerminalColor(colors[0].foreground)}${workingDir}${this.toTerminalColor(colors[0].background)}${this.toTerminalBackgroundColor(colors[0].foreground)}${Chars.CircleLeft.asUtf8}${Chars.CircleSeparatorLeft.asUtf8}${this.toTerminalColor(ColorName.foreground)}${this.toTerminalBackgroundColor(ColorName.background)}`],
    [Chars.BottomLeftCorner.asString, (colors: {foreground?: ColorName; background?: ColorName}[]) => `${Chars.TopLeftCorner.asUtf8}${this.toTerminalColor(colors[0].background)}${this.toTerminalBackgroundColor(colors[0].foreground)}${Chars.TriangleRight.asUtf8}${this.toTerminalBackgroundColor(colors[0].background)}${this.toTerminalColor(colors[0].foreground)}${workingDir}${this.toTerminalColor(colors[0].background)}${this.toTerminalBackgroundColor(colors[1].background)}${Chars.TriangleLeft.asUtf8}${this.toTerminalBackgroundColor(colors[1].background)}${this.toTerminalColor(colors[1].foreground)} ${Chars.Branch.asUtf8} ${gitBranch}${this.toTerminalBackgroundColor(colors[1].foreground)}${this.toTerminalColor(colors[1].background)}${Chars.TriangleLeft.asUtf8}${this.toTerminalColor(ColorName.foreground)}${this.toTerminalBackgroundColor(ColorName.background)}`],
    [Chars.BottomLeftCorner.asString, (colors: {foreground?: ColorName; background?: ColorName}[]) => `${Chars.TopLeftCorner.asUtf8}${this.toTerminalColor(colors[0].background)}${this.toTerminalBackgroundColor(colors[0].foreground)}${Chars.TriangleRight.asUtf8}${this.toTerminalBackgroundColor(colors[0].background)}${this.toTerminalColor(colors[0].foreground)}${workingDir}${this.toTerminalColor(colors[0].background)}${this.toTerminalBackgroundColor(colors[1].background)}${Chars.TriangleLeft.asUtf8}${this.toTerminalBackgroundColor(colors[1].background)}${this.toTerminalColor(colors[1].foreground)} ${Chars.Branch.asUtf8} ${gitBranch}${this.toTerminalBackgroundColor(colors[1].foreground)}${this.toTerminalColor(colors[1].background)}${Chars.TriangleLeft.asUtf8}}${Chars.TriangleSeparatorLeft.asUtf8}${this.toTerminalColor(ColorName.foreground)}${this.toTerminalBackgroundColor(ColorName.background)}`],
    [Chars.BottomLeftCorner.asString, (colors: {foreground?: ColorName; background?: ColorName}[]) => `${Chars.TopLeftCorner.asUtf8}${this.toTerminalColor(colors[0].background)}${this.toTerminalBackgroundColor(colors[0].foreground)}${Chars.CircleRight.asUtf8}${this.toTerminalBackgroundColor(colors[0].background)}${this.toTerminalColor(colors[0].foreground)}${workingDir}${this.toTerminalColor(colors[0].background)}${this.toTerminalBackgroundColor(colors[1].background)}${Chars.CircleLeft.asUtf8}${this.toTerminalBackgroundColor(colors[1].background)}${this.toTerminalColor(colors[1].foreground)} ${Chars.Branch.asUtf8} ${gitBranch}${this.toTerminalBackgroundColor(colors[1].foreground)}${this.toTerminalColor(colors[1].background)}${Chars.CircleLeft.asUtf8}${this.toTerminalColor(ColorName.foreground)}${this.toTerminalBackgroundColor(ColorName.background)}`],
    [Chars.BottomLeftCorner.asString, (colors: {foreground?: ColorName; background?: ColorName}[]) => `${Chars.TopLeftCorner.asUtf8}${this.toTerminalColor(colors[0].background)}${this.toTerminalBackgroundColor(colors[0].foreground)}${Chars.CircleRight.asUtf8}${this.toTerminalBackgroundColor(colors[0].background)}${this.toTerminalColor(colors[0].foreground)}${workingDir}${this.toTerminalColor(colors[0].background)}${this.toTerminalBackgroundColor(colors[1].background)}${Chars.CircleLeft.asUtf8}${this.toTerminalBackgroundColor(colors[1].background)}${this.toTerminalColor(colors[1].foreground)} ${Chars.Branch.asUtf8} ${gitBranch}${this.toTerminalBackgroundColor(colors[1].foreground)}${this.toTerminalColor(colors[1].background)}${Chars.CircleLeft.asUtf8}}${Chars.CircleSeparatorLeft.asUtf8}${this.toTerminalColor(ColorName.foreground)}${this.toTerminalBackgroundColor(ColorName.background)}`]
  ];

  constructor(shellConfig: ShellConfig) {
    super(shellConfig);
  }

  changePromptCommand(theme: Theme): string {
    if (!theme) { return '';}
    const promptCharUnicode = Char.convertUnicode(ZshAdapter.prompts[theme.promptVersion-1][0]) + Char.convertUnicode(theme.prompt);
    const prompt = ZshAdapter.prompts[theme.promptVersion-1][1](theme.colors.promptColors);
    return ` setopt PROMPT_SUBST; ${gitBranchFunc} COGNO_PS1=$'\\n${prompt}\\n${promptCharUnicode} '`;
  }

  cursorLeftSequence(): string {
    return `${Chars.Escape.asString}[D`;
  }
  cursorRightSequence(): string {
    return `${Chars.Escape.asString}[C`;
  }
  backspaceSequence(): string {
    return `${Chars.Backspace.asString}`;
  }

  abortTask(): string {
    return Chars.EndOfText.asString;
  }

  static toTerminalColor(color: ColorName): string {
    switch (color) {
      case ColorName.white:
        return '%F{white}';
      case ColorName.brightWhite:
        return '%F{white}';
      case ColorName.cyan:
        return '%F{cyan}';
      case ColorName.brightCyan:
        return '%F{cyan}';
      case ColorName.blue:
        return '%F{blue}';
      case ColorName.brightBlue:
        return '%F{blue}';
      case ColorName.magenta:
        return '%F{magenta}';
      case ColorName.brightMagenta:
        return '%F{magenta}';
      case ColorName.green:
        return '%F{green}';
      case ColorName.brightGreen:
        return '%F{green}';
      case ColorName.black:
        return '%F{black}';
      case ColorName.brightBlack:
        return '%F{black}';
      case ColorName.yellow:
        return '%F{yellow}';
      case ColorName.brightYellow:
        return '%F{yellow}';
      case ColorName.red:
        return '%F{red}';
      case ColorName.brightRed:
        return '%F{red}';
      case ColorName.background:
        return '%k';
      case ColorName.foreground:
        return '%f';
      default:
        return '%f';
    }
  }

  static toTerminalBackgroundColor(color: ColorName): string {
    switch (color) {
      case ColorName.white:
        return '%K{white}';
      case ColorName.brightWhite:
        return '%K{white}';
      case ColorName.cyan:
        return '%K{cyan}';
      case ColorName.brightCyan:
        return '%K{cyan}';
      case ColorName.blue:
        return '%K{blue}';
      case ColorName.brightBlue:
        return '%K{blue}';
      case ColorName.magenta:
        return '%K{magenta}';
      case ColorName.brightMagenta:
        return '%K{magenta}';
      case ColorName.green:
        return '%K{green}';
      case ColorName.brightGreen:
        return '%K{green}';
      case ColorName.black:
        return '%K{black}';
      case ColorName.brightBlack:
        return '%K{black}';
      case ColorName.yellow:
        return '%K{yellow}';
      case ColorName.brightYellow:
        return '%K{yellow}';
      case ColorName.red:
        return '%K{red}';
      case ColorName.brightRed:
        return '%K{red}';
      case ColorName.background:
        return '%k';
      case ColorName.foreground:
        return '%f';
      default:
        return '%k';
    }
  }
}
