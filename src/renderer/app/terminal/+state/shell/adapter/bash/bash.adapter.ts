import {ShellAdapter} from '../shell.adapter';
import {Char, Chars} from '../../../../../../../shared/chars';
import {ColorName, ShellConfig, Theme} from '../../../../../../../shared/models/settings';

const gitBranchFunc = 'git_branch() { git branch 2> /dev/null | sed -e "/^[^*]/d" -e "s/* \\(.*\\)/\\1/"; }';
const gitDirtyFunc = 'git_dirty() { [[ $(git status --porcelain 2> /dev/null; ) ]] && echo "1"; }';
const escapePrompts = ['!', '$'];

export class BashAdapter extends ShellAdapter {

  injectCommands(script: string, theme: Theme): string[] {
    let inHereDoc = false;
    let hereDocDelimiter = '';

    // Convert the script into a one-liner
    const injectScript = script
      .split('\n')
      .map(line => line.trim())
      .filter(line => line && !line.startsWith('#') && !line.startsWith('!'))
      .map(line => {
        // Handle here-documents
        if (inHereDoc) {
          if (line === hereDocDelimiter) {
            inHereDoc = false;
          }
          return line;
        } else if (line.includes('<<')) {
          inHereDoc = true;
          hereDocDelimiter = line.split('<<')[1].trim();
          return line;
        }

        // Handle function definitions and control structures
        if (line.endsWith('{')) {
          return line;
        } else {
          return line.endsWith(';') ? line : line + ';';
        }
      })
      .join(' ');
    const changePromptScript = theme.promptVersion > 0 ? 'export LANG=en_US.UTF-8; export LC_ALL=en_US.UTF-8; export LC_MESSAGES=en_US.UTF-8; export LC_NUMERIC=en_US.UTF-8; export LC_MONETARY=en_US.UTF-8; export LC_COLLATE=en_US.UTF-8;' + this.changePromptCommand(theme) + ' && ' : ''
    const cleanup = this.config.isDebugModeEnabled ? '' : this.clear() + this.deleteHistory();
    return  [' HISTCONTROL=ignoreboth; history -a; ' + changePromptScript + injectScript + cleanup];
  }

  private static prompts = [
    ['', (colors: {foreground?: ColorName; background?: ColorName}[]) => `${this.toTerminalColor(colors[0].foreground)}\${PWD//[^[:ascii:]]/?}${this.resetColors()}`],
    ['',(colors: {foreground?: ColorName; background?: ColorName}[]) => `${this.toTerminalColor(colors[0].foreground)}${this.toTerminalBackgroundColor(colors[0].background)}\${PWD//[^[:ascii:]]/?}${this.resetColors()}${this.toTerminalColor(colors[0].background)}${Chars.TriangleLeft.asString}${this.resetColors()}`],
    ['',(colors: {foreground?: ColorName; background?: ColorName}[]) => `${this.toTerminalColor(colors[0].foreground)}${this.toTerminalBackgroundColor(colors[0].background)}\${PWD//[^[:ascii:]]/?}${this.resetColors()}${this.toTerminalColor(colors[0].background)}${Chars.TriangleLeft.asString}${Chars.TriangleSeparatorLeft.asString}${this.resetColors()}`],
    ['',(colors: {foreground?: ColorName; background?: ColorName}[]) => `${this.toTerminalColor(colors[0].foreground)}${this.toTerminalBackgroundColor(colors[0].background)}\${PWD//[^[:ascii:]]/?}${this.resetColors()}${this.toTerminalColor(colors[0].background)}${Chars.CircleLeft.asString}${this.resetColors()}`],
    ['',(colors: {foreground?: ColorName; background?: ColorName}[]) => `${this.toTerminalColor(colors[0].foreground)}${this.toTerminalBackgroundColor(colors[0].background)}\${PWD//[^[:ascii:]]/?}${this.resetColors()}${this.toTerminalColor(colors[0].background)}${Chars.CircleLeft.asString}${Chars.CircleSeparatorLeft.asString}${this.resetColors()}`],
    ['',(colors: {foreground?: ColorName; background?: ColorName}[]) => `${this.toTerminalColor(colors[0].foreground)}${this.toTerminalBackgroundColor(colors[0].background)}\${PWD//[^[:ascii:]]/?}${this.resetColors()}${this.toTerminalColor(colors[0].background)}${this.toTerminalBackgroundColor(colors[1].background)}${Chars.TriangleLeft.asString}${this.toTerminalColor(colors[1].foreground)}${this.toTerminalBackgroundColor(colors[1].background)} ${Chars.Branch.asString} \$(git_branch)${this.resetColors()}${this.toTerminalColor(colors[1].background)}${Chars.TriangleLeft.asString}${this.resetColors()}`],
    ['',(colors: {foreground?: ColorName; background?: ColorName}[]) => `${this.toTerminalColor(colors[0].foreground)}${this.toTerminalBackgroundColor(colors[0].background)}\${PWD//[^[:ascii:]]/?}${this.resetColors()}${this.toTerminalColor(colors[0].background)}${this.toTerminalBackgroundColor(colors[1].background)}${Chars.TriangleLeft.asString}${this.toTerminalColor(colors[1].foreground)}${this.toTerminalBackgroundColor(colors[1].background)} ${Chars.Branch.asString} \$(git_branch)${this.resetColors()}${this.toTerminalColor(colors[1].background)}${Chars.TriangleLeft.asString}${Chars.TriangleSeparatorLeft.asString}${this.resetColors()}`],
    ['',(colors: {foreground?: ColorName; background?: ColorName}[]) => `${this.toTerminalColor(colors[0].foreground)}${this.toTerminalBackgroundColor(colors[0].background)}\${PWD//[^[:ascii:]]/?}${this.resetColors()}${this.toTerminalColor(colors[0].background)}${this.toTerminalBackgroundColor(colors[1].background)}${Chars.CircleLeft.asString}${this.toTerminalColor(colors[1].foreground)}${this.toTerminalBackgroundColor(colors[1].background)} ${Chars.Branch.asString} \$(git_branch)${this.resetColors()}${this.toTerminalColor(colors[1].background)}${Chars.CircleLeft.asString}${this.resetColors()}`],
    ['',(colors: {foreground?: ColorName; background?: ColorName}[]) => `${this.toTerminalColor(colors[0].foreground)}${this.toTerminalBackgroundColor(colors[0].background)}\${PWD//[^[:ascii:]]/?}${this.resetColors()}${this.toTerminalColor(colors[0].background)}${this.toTerminalBackgroundColor(colors[1].background)}${Chars.CircleLeft.asString}${this.toTerminalColor(colors[1].foreground)}${this.toTerminalBackgroundColor(colors[1].background)} ${Chars.Branch.asString} \$(git_branch)${this.resetColors()}${this.toTerminalColor(colors[1].background)}${Chars.CircleLeft.asString}${Chars.CircleSeparatorLeft.asString}${this.resetColors()}`],

    [Chars.BottomLeftCorner.asString, (colors: {foreground?: ColorName; background?: ColorName}[]) => `${Chars.TopLeftCorner.asString}${this.toTerminalColor(colors[0].background)}${Chars.TriangleRight.asString}${this.resetColors()}${this.toTerminalColor(colors[0].foreground)}${this.toTerminalBackgroundColor(colors[0].background)}\${PWD//[^[:ascii:]]/?}${this.resetColors()}${this.toTerminalColor(colors[0].background)}${Chars.TriangleLeft.asString}${this.resetColors()}`],
    [Chars.BottomLeftCorner.asString, (colors: {foreground?: ColorName; background?: ColorName}[]) => `${Chars.TopLeftCorner.asString}${this.toTerminalColor(colors[0].background)}${Chars.TriangleRight.asString}${this.resetColors()}${this.toTerminalColor(colors[0].foreground)}${this.toTerminalBackgroundColor(colors[0].background)}\${PWD//[^[:ascii:]]/?}${this.resetColors()}${this.toTerminalColor(colors[0].background)}${Chars.TriangleLeft.asString}${Chars.TriangleSeparatorLeft.asString}${this.resetColors()}`],
    [Chars.BottomLeftCorner.asString, (colors: {foreground?: ColorName; background?: ColorName}[]) => `${Chars.TopLeftCorner.asString}${this.toTerminalColor(colors[0].background)}${Chars.CircleRight.asString}${this.resetColors()}${this.toTerminalColor(colors[0].foreground)}${this.toTerminalBackgroundColor(colors[0].background)}\${PWD//[^[:ascii:]]/?}${this.resetColors()}${this.toTerminalColor(colors[0].background)}${Chars.CircleLeft.asString}${this.resetColors()}`],
    [Chars.BottomLeftCorner.asString, (colors: {foreground?: ColorName; background?: ColorName}[]) => `${Chars.TopLeftCorner.asString}${this.toTerminalColor(colors[0].background)}${Chars.CircleRight.asString}${this.resetColors()}${this.toTerminalColor(colors[0].foreground)}${this.toTerminalBackgroundColor(colors[0].background)}\${PWD//[^[:ascii:]]/?}${this.resetColors()}${this.toTerminalColor(colors[0].background)}${Chars.CircleLeft.asString}${Chars.CircleSeparatorLeft.asString}${this.resetColors()}`],
    [Chars.BottomLeftCorner.asString, (colors: {foreground?: ColorName; background?: ColorName}[]) => `${Chars.TopLeftCorner.asString}${this.toTerminalColor(colors[0].background)}${Chars.TriangleRight.asString}${this.resetColors()}${this.toTerminalColor(colors[0].foreground)}${this.toTerminalBackgroundColor(colors[0].background)}\${PWD//[^[:ascii:]]/?}${this.resetColors()}${this.toTerminalColor(colors[0].background)}${this.toTerminalBackgroundColor(colors[1].background)}${Chars.TriangleLeft.asString}${this.toTerminalColor(colors[1].foreground)}${this.toTerminalBackgroundColor(colors[1].background)} ${Chars.Branch.asString} \$(git_branch)${this.resetColors()}${this.toTerminalColor(colors[1].background)}${Chars.TriangleLeft.asString}${this.resetColors()}`],
    [Chars.BottomLeftCorner.asString, (colors: {foreground?: ColorName; background?: ColorName}[]) => `${Chars.TopLeftCorner.asString}${this.toTerminalColor(colors[0].background)}${Chars.TriangleRight.asString}${this.resetColors()}${this.toTerminalColor(colors[0].foreground)}${this.toTerminalBackgroundColor(colors[0].background)}\${PWD//[^[:ascii:]]/?}${this.resetColors()}${this.toTerminalColor(colors[0].background)}${this.toTerminalBackgroundColor(colors[1].background)}${Chars.TriangleLeft.asString}${this.toTerminalColor(colors[1].foreground)}${this.toTerminalBackgroundColor(colors[1].background)} ${Chars.Branch.asString} \$(git_branch)${this.resetColors()}${this.toTerminalColor(colors[1].background)}${Chars.TriangleLeft.asString}${Chars.TriangleSeparatorLeft.asString}${this.resetColors()}`],
    [Chars.BottomLeftCorner.asString, (colors: {foreground?: ColorName; background?: ColorName}[]) => `${Chars.TopLeftCorner.asString}${this.toTerminalColor(colors[0].background)}${Chars.CircleRight.asString}${this.resetColors()}${this.toTerminalColor(colors[0].foreground)}${this.toTerminalBackgroundColor(colors[0].background)}\${PWD//[^[:ascii:]]/?}${this.resetColors()}${this.toTerminalColor(colors[0].background)}${this.toTerminalBackgroundColor(colors[1].background)}${Chars.CircleLeft.asString}${this.toTerminalColor(colors[1].foreground)}${this.toTerminalBackgroundColor(colors[1].background)} ${Chars.Branch.asString} \$(git_branch)${this.resetColors()}${this.toTerminalColor(colors[1].background)}${Chars.CircleLeft.asString}${this.resetColors()}`],
    [Chars.BottomLeftCorner.asString, (colors: {foreground?: ColorName; background?: ColorName}[]) => `${Chars.TopLeftCorner.asString}${this.toTerminalColor(colors[0].background)}${Chars.CircleRight.asString}${this.resetColors()}${this.toTerminalColor(colors[0].foreground)}${this.toTerminalBackgroundColor(colors[0].background)}\${PWD//[^[:ascii:]]/?}${this.resetColors()}${this.toTerminalColor(colors[0].background)}${this.toTerminalBackgroundColor(colors[1].background)}${Chars.CircleLeft.asString}${this.toTerminalColor(colors[1].foreground)}${this.toTerminalBackgroundColor(colors[1].background)} ${Chars.Branch.asString} \$(git_branch)${this.resetColors()}${this.toTerminalColor(colors[1].background)}${Chars.CircleLeft.asString}${Chars.CircleSeparatorLeft.asString}${this.resetColors()}`],
  ];

  constructor(shellConfig: ShellConfig) {
    super(shellConfig);
  }

  private deleteHistory(): string {
    return ' for i in {1..1}; do history -d $(history 1 | awk \'{print $1}\') > /dev/null 2>&1; done && history -w';
  }

  private clear(): string {
    return ' clear;';
  }

  changePromptCommand(theme: Theme): string {
    if (!theme) {return '';}
    const promptHeading = BashAdapter.prompts[theme.promptVersion-1][1](theme.colors.promptColors);
    const prompt = Char.convertUnicode(BashAdapter.prompts[theme.promptVersion-1][0]) + (escapePrompts.indexOf(theme.prompt) > -1 ? theme.prompt : Char.convertUnicode(theme.prompt));
    return `${gitBranchFunc} && ${gitDirtyFunc} && prompt() { printf "%b" "\\n${promptHeading}\\n${prompt} "; } && COGNO_PS1='$(prompt)'`;
  }

  cursorLeftSequence(): string {
    return `${Chars.Escape.asString}[D`;
  }
  cursorRightSequence(): string {
    return `${Chars.Escape.asString}[C`;
  }

  backspaceSequence(): string {
    return `${Chars.Backspace.asString}`;
  }
  abortTask(): string {
    return Chars.EndOfText.asString;
  }

  private static resetColors(): string {
    return '\\e[0m';
  }

  static toTerminalBackgroundColor(color: ColorName): string {
    switch (color) {
      case ColorName.white:
        return '\\e[47m';
      case ColorName.brightWhite:
        return '\\e[47;1m';
      case ColorName.cyan:
        return '\\e[46m';
      case ColorName.brightCyan:
        return '\\e[46;1m';
      case ColorName.blue:
        return '\\e[44m';
      case ColorName.brightBlue:
        return '\\e[44;1m';
      case ColorName.magenta:
        return '\\e[45m';
      case ColorName.brightMagenta:
        return '\\e[45;1m';
      case ColorName.green:
        return '\\e[42m';
      case ColorName.brightGreen:
        return '\\e[42;1m';
      case ColorName.black:
        return '\\e[40m';
      case ColorName.brightBlack:
        return '\\e[40;1m';
      case ColorName.yellow:
        return '\\e[43m';
      case ColorName.brightYellow:
        return '\\e[43;1m';
      case ColorName.red:
        return '\\e[41m';
      case ColorName.brightRed:
        return '\\e[41;1m';
      default:
        return '\\e[46m';
    }
  }

  static toTerminalColor(color: ColorName): string {
    switch (color) {
      case ColorName.white:
        return '\\e[37m';
      case ColorName.brightWhite:
        return '\\e[37;1m';
      case ColorName.cyan:
        return '\\e[36m';
      case ColorName.brightCyan:
        return '\\e[36;1m';
      case ColorName.blue:
        return '\\e[34m';
      case ColorName.brightBlue:
        return '\\e[34;1m';
      case ColorName.magenta:
        return '\\e[35m';
      case ColorName.brightMagenta:
        return '\\e[35;1m';
      case ColorName.green:
        return '\\e[32m';
      case ColorName.brightGreen:
        return '\\e[32;1m';
      case ColorName.black:
        return '\\e[30m';
      case ColorName.brightBlack:
        return '\\e[30;1m';
      case ColorName.yellow:
        return '\\e[33m';
      case ColorName.brightYellow:
        return '\\e[33;1m';
      case ColorName.red:
        return '\\e[31m';
      case ColorName.brightRed:
        return '\\e[31;1m';
      default:
        return '\\e[36m';
    }
  }
}
