import {BashAdapter} from './bash.adapter';
import {ColorName} from '../../../../../../../shared/models/settings';
import {TestHelper} from '../../../../../../../test/helper';
import {ZshAdapter} from '../zsh/zsh.adapter';

describe('Bash Adapter', () => {

  it('should calc terminal colors - empty', () => {
    expect(BashAdapter.toTerminalColor(null)).toBeTruthy();
    expect(BashAdapter.toTerminalColor(undefined)).toBeTruthy();
  });

  it('should calc terminal colors', () => {
    let defaultColorCount = 0;
    for (const [key, value] of Object.entries(ColorName)) {
      const color = BashAdapter.toTerminalColor(value);
      expect(color).toBeTruthy();
      if (color === '\\e[36m') {
        defaultColorCount++;
      }
    }
    expect(defaultColorCount).toBe(8);
  });
});
