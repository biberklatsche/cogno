import {IMarker, Terminal} from '@xterm/xterm';
import {FitAddon} from '@xterm/addon-fit';
import {LigaturesAddon} from '@xterm/addon-ligatures';
import {WebglAddon} from '@xterm/addon-webgl';
import {WebLinksAddon} from '@xterm/addon-web-links';
import {Observable, Subject, Subscription} from 'rxjs';
import {Theme} from '../../../../../shared/models/settings';
import {Dimensions, Position} from '../../../../../shared/models/models';
import {ISearchOptions, SearchAddon} from '@xterm/addon-search';
import {Logger} from '../../../logger/logger';
import {Unicode11Addon} from '@xterm/addon-unicode11';
import {platform} from 'os';
import {CanvasAddon} from '@xterm/addon-canvas';
import {SettingsService} from '../../../+shared/services/settings/settings.service';
import {debounceTime} from 'rxjs/operators';
import {Selection, Viewport} from '../models';
import {ViewportAddon} from './viewport.addon';
import {LinkHandler} from '../handler/link.handler';
import {Injectable, OnDestroy} from '@angular/core';

@Injectable()
export class Renderer implements OnDestroy {
  private subscriptions: Subscription[] = [];
  public terminal: Terminal;
  private bufferLength = 0;
  private fitAddon: FitAddon = new FitAddon();
  private searchAddon = new SearchAddon();
  private unicodeAddon = new Unicode11Addon();
  private weblinksAddon = new WebLinksAddon((event, url) => this._linkClicked.next(url));
  private viewportAddon: ViewportAddon = new ViewportAddon();
  private ligaturesAddon: LigaturesAddon;
  private webglAddon: WebglAddon;
  private canvasAddon: CanvasAddon;
  private _linkClicked = new Subject<string>();
  private _bufferShrinked = new Subject<number>();
  private _onData = new Subject<string>();
  private theme: Theme;

  public readonly pathRegexp = new RegExp('([A-z]\\:){0,1}[\\\\/]{0,1}([A-z0-9-_+]+[\\\\/])+([A-z0-9_\\.-]+(\\.[A-z]{2,3})+)');
  public readonly linkRegexp = new RegExp('(http[s]?:\\/\\/(www\\.)?|ftp:\\/\\/(www\\.)?|www\\.){1}([0-9A-Za-z-\\.@:%_\+~#=]+)+((\\.[a-zA-Z]{2,3})?)(/[^\\s]*)?(\\?(.)*)?');


  constructor(
    private settingsService: SettingsService
    ) {
    this.terminal = new Terminal({
      overviewRulerWidth: 20,
      cursorStyle: 'bar',
      smoothScrollDuration: 0,
      allowTransparency: true,
      altClickMovesCursor: true,
      windowsPty: platform() === 'win32' ? {backend: 'conpty'} : null,
      allowProposedApi: true,
      windowOptions: {
        pushTitle: true, //handle CSI Ps=22 vim on gitbash uses this to enter full screen
        popTitle: true //handle CSI Ps=23 vim on gitbash uses this to leaf full screen
      }
    });
    this.subscriptions.push(this.settingsService.selectActiveThemeWithScrollback().subscribe(t => this.setTheme(t.theme, t.scrollbackLines)));
  }

  ngOnDestroy() {
    this._linkClicked.complete();
    this._bufferShrinked.complete();
    this.subscriptions.forEach(s => s.unsubscribe());
    this.canvasAddon?.dispose();
    this.webglAddon?.dispose();
    this.searchAddon?.dispose();
    this.fitAddon?.dispose();
    this.unicodeAddon?.dispose();
    this.viewportAddon?.dispose();
    this.terminal.dispose();
    this.terminal = null;
  }

  private applyAddons() {
    this.terminal.loadAddon(this.fitAddon);
    this.terminal.loadAddon(this.searchAddon);
    this.terminal.loadAddon(this.unicodeAddon);
    this.terminal.loadAddon(this.weblinksAddon);

    this.terminal.loadAddon(this.viewportAddon);
    this.terminal.options.linkHandler = new LinkHandler(this._linkClicked);
    this.terminal.unicode.activeVersion = '11';
    if (this.theme.enableWebgl) {
      if (!this.webglAddon) {
        this.webglAddon = new WebglAddon();
      }
      this.terminal.loadAddon(this.webglAddon);
    } else {
      if (!this.canvasAddon) {
        this.canvasAddon = new CanvasAddon();
      }
      this.terminal.loadAddon(this.canvasAddon);
    }
  }

  public bind(element: HTMLElement) {
    this.applyAddons();
    this.terminal.onData((data: string) => {
      this._onData.next(data);
    });
    this.terminal.onWriteParsed((e) => {
      if (this.bufferLength > this.terminal.buffer.normal?.length) {
        this._bufferShrinked.next(this.terminal.buffer.normal?.length);
      }
      this.bufferLength = this.terminal.buffer.normal?.length;
    });
    this.terminal.open(element);
    if (this.theme.enableLigatures) {
      if (!this.ligaturesAddon) {
        this.ligaturesAddon = new LigaturesAddon();
      }
      this.terminal.loadAddon(this.ligaturesAddon);
    }
  }

  private setTheme(t: Theme, scrollbackLines: number) {
    const shouldResize = !!this.theme && this.theme.padding !== t.padding;
    this.theme = t;
    this.terminal.options.scrollback = scrollbackLines;
    this.terminal.options.fontSize = this.theme.fontsize;
    this.terminal.options.fontFamily = `${this.theme.fontFamily}`;
    this.terminal.options.fontWeight = this.theme.fontWeight;
    this.terminal.options.fontWeightBold = this.theme.fontWeightBold;
    this.terminal.options.cursorStyle = 'bar';
    this.terminal.options.cursorWidth = this.theme.cursorWidth;
    this.terminal.options.cursorBlink = this.theme.cursorBlink;
    this.terminal.options.cursorStyle = this.theme.cursorStyle;
    this.terminal.options.theme = {
      background: '#00000000',
      cursor: this.theme.colors.cursor ? `${this.theme.colors.cursor}CC` : `${this.theme.colors.highlight}CC`,
      cursorAccent: this.theme.colors.highlight,
      foreground: this.theme.colors.foreground,
      selectionBackground: `${this.theme.colors.highlight}99`,
      black: this.theme.colors.black,
      red: this.theme.colors.red,
      green: this.theme.colors.green,
      yellow: this.theme.colors.yellow,
      blue: this.theme.colors.blue,
      magenta: this.theme.colors.magenta,
      cyan: this.theme.colors.cyan,
      white: this.theme.colors.brightWhite,
      brightBlack: this.theme.colors.brightBlack,
      brightRed: this.theme.colors.brightRed,
      brightGreen: this.theme.colors.brightGreen,
      brightYellow: this.theme.colors.brightYellow,
      brightBlue: this.theme.colors.brightBlue,
      brightMagenta: this.theme.colors.brightMagenta,
      brightCyan: this.theme.colors.brightCyan,
      brightWhite: this.theme.colors.brightWhite,
    };
    if (shouldResize) {
      setTimeout(() => this.fit(), 200);
    }
  }

  public get dimensions(): Dimensions {
    return {cols: this.terminal?.cols, rows: this.terminal?.rows};
  }

  public proposeDimensions(): Dimensions {
    return this.fitAddon.proposeDimensions();
  }

  public fit() {
    try {
      this.fitAddon.fit();
    } catch (e) {
      Logger.error(e);
    }
  }

  public onViewportChanged(): Observable<Viewport> {
    return this.viewportAddon.onViewportChanged();
  }

  public getCurrentViewport(): Viewport {
    return this.viewportAddon.getCurrentViewport();
  }

  public hasSelection(): boolean {
    return this.terminal && this.terminal.hasSelection();
  }

  getSelection(): Selection {
    const hasSelection = this.terminal.hasSelection();
    if (!hasSelection) {
      return {text: '', start: undefined, end: undefined, hasSelection: false};
    }
    const selection = this.terminal.getSelection();
    const buffer = this.terminal.getSelectionPosition();
    return {
      text: selection,
      start: {x: buffer.start.x, y: buffer.start.y},
      end: {x: buffer.end.x, y: buffer.end.y},
      hasSelection: true
    };
  }

  public clearSelection() {
    this.terminal.clearSelection();
    this.searchAddon.clearDecorations();
  }

  public onSearchResult(): Observable<{ resultCount: number; resultIndex: number }> {
    const obs = new Subject<{ resultCount: number; resultIndex: number }>();
    this.searchAddon.onDidChangeResults((event) => {
      obs.next(event);
    });
    return obs.asObservable();
  }

  public onBinary(): Observable<any> {
    const obs = new Subject<Buffer>();
    this.terminal.onBinary((event) => {
      obs.next(Buffer.from(event, 'binary'));
    });
    return obs.asObservable();
  }

  public onData(): Observable<string> {
    return this._onData.asObservable();
  }

  public write(data: string) {
    this.terminal.write(data);
  }

  public reset() {
    this.terminal.reset();
  }

  public clear() {
    this.terminal.clear();
  }

  public focus() {
    this.terminal.focus();
  }

  public onHttpLinkClicked(): Observable<string> {
    return this._linkClicked.asObservable();
  }

  public findNext(searchString: string, options: ISearchOptions) {
    this.searchAddon.findNext(searchString, this.enrichOptions(options));
  }

  public findPrevious(searchString: string, options: ISearchOptions) {
    this.searchAddon.findPrevious(searchString, this.enrichOptions(options));
  }


  public selectText(startPosition: Position, length: number) {
    this.terminal.select(startPosition.x, startPosition.y, length);
  }

  private enrichOptions(options: ISearchOptions): ISearchOptions {
    return {
      ...options, decorations: {
        activeMatchBackground: this.theme.colors.highlight,
        activeMatchBorder: this.theme.colors.red,
        matchBackground: this.theme.colors.brightBlue,
        matchOverviewRuler: `${this.theme.colors.highlight}88`,
        activeMatchColorOverviewRuler: this.theme.colors.highlight
      }
    };
  }

  scrollToBottom() {
    this.terminal.scrollToBottom();
  }

  scrollTo(marker: IMarker, offset = 0) {
    this.terminal.scrollToLine(marker.line - offset);
  }

  onSelectionChanged(): Observable<string> {
    const obs = new Subject<string>();
    this.terminal.onSelectionChange((event) => {
      if (this.settingsService.getIsCopyOnSelectEnabled() && this.terminal.getSelection()?.trim()) {
        obs.next(this.terminal.getSelection()?.trim());
      }
    });
    return obs.asObservable().pipe(debounceTime(200));
  }

  onBufferShrinks(): Observable<number> {
    return this._bufferShrinked.asObservable();
  }



  readLines(startLine: number, endLine?: number) {
    const content = [];
    if(!endLine) {
      endLine = this.terminal.buffer.active.length;
    }
    for (let line = startLine; line <= endLine; line++) {
      const bufferLine = this.terminal.buffer.active.getLine(line);
      if (!bufferLine) {
        break;
      }
      content.push(bufferLine.translateToString(true));
    }
    return content.reduce((previous, current) => `${previous}\n${current}`).trim();
  }

}
