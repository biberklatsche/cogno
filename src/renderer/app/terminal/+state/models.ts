import {CursorPosition, Position} from '../../../../shared/models/models';
import {IMarker} from '@xterm/xterm';

export type Selection = {
  hasSelection: boolean;
  text: string;
  start: Position;
  end: Position;
};

export type UserInput = {
  input: string;
  startLine: number;
  lines: {line: number; content: string}[];
  cursorPosition: CursorPosition;
  pressedKey: string;
};

export type RenderCommand = {
  id: string;
  command?: string;
  startLine: number;
  endLine?: number;
  offsetStartLine: number;
  offsetEndLine?: number;
};

export type Viewport = {
  lastBufferOverflow: number;
  startLine: number;
  endLine: number;
  lineOfCursor: number;
};
