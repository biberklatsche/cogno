import {Injectable, OnDestroy} from '@angular/core';
import {Position, RendererState} from '../../../../shared/models/models';
import {GridService} from '../../global/grid/+state/grid.service';
import {OnReady, TabService} from '../../+shared/abstract-components/tab/+state/tab.service';
import {firstValueFrom, Observable, Subscription} from 'rxjs';
import {Shell} from './shell/shell';
import {debounceTime, filter, tap} from 'rxjs/operators';
import {Renderer} from './renderer/renderer';
import {SettingsService} from '../../+shared/services/settings/settings.service';
import {WindowService} from '../../+shared/services/window/window.service';
import {PasteService} from '../../global/paste-history-menu/+state/paste.service';
import {Chars} from '../../../../shared/chars';
import {NotificationService} from '../../+shared/notification/notification.service';
import {ElectronService} from '../../+shared/services/electron/electron.service';
import {CommandDecorationEntry, DecorationHandler} from './handler/decoration.handler';
import {IMarker} from '@xterm/xterm';
import {ShellLocation} from '../../common/shellLocation';
import {AutocompleteRequest, AutocompleteService} from '../../autocomplete/+state/autocomplete.service';
import {SearchOptions} from '../search/+state/models';
import {TerminalTab} from '../../+shared/models/tab';
import {StatusTerminalAdapter, TerminalCommand, TerminalStatusObserver} from './osc/terminal-status.observer';
import {CopyObserver} from './osc/copy.observer';
import {Normalizer} from '../../../../shared/normalizer';
import {SelectionHandler} from './handler/selection.handler';
import {TabnameObserver} from './osc/tabname.observer';
import {Key} from '../../common/key';
import {ShellConfig} from "../../../../shared/models/settings";

export interface TerminalState {
  workingDir: string[];
  machine: string;
  user: string;
  promptState: RendererState;
  isScrollButtonVisible: boolean;
  bookmarkPositionIndex: number;
  isRemoteShell: boolean;
  isAdvancedEnabled: boolean;
  areShortcutsEnabled: boolean;
  isAppRunning: boolean;
  isPaddingRemoved: boolean;
  isCommandRunning: boolean;
  wasLastCommandSuccessful?: boolean;
}

const initialState: TerminalState = {
  workingDir: [],
  machine: null,
  user: null,
  isScrollButtonVisible: false,
  bookmarkPositionIndex: -1,
  isRemoteShell: false,
  isAdvancedEnabled: false,
  areShortcutsEnabled: true,
  isAppRunning: false,
  isPaddingRemoved: false,
  isCommandRunning: false,
  wasLastCommandSuccessful: null,
  promptState: null
};

@Injectable()
export class TerminalService extends TabService<TerminalState> implements OnReady, OnDestroy {

  private copyObserver = new CopyObserver();
  private tabnameObserver = new TabnameObserver();
  private decorationHandler = new DecorationHandler();

  constructor(protected gridService: GridService,
              private autocompleteService: AutocompleteService,
              private settings: SettingsService,
              private windowService: WindowService,
              private pasteService: PasteService,
              private notificationService: NotificationService,
              private electronService: ElectronService,
              private shell: Shell,
              private renderer: Renderer,
              private selectionHandler: SelectionHandler,
              private terminalStatusObserver: TerminalStatusObserver
  ) {
    super(gridService, initialState);
    this.untilDestroy(
      this.pasteService.onSelectPasteEntry().subscribe(command => {
        const isActiveTab = this.store.id === this.gridService.getActiveTabId();
        if (isActiveTab) {
          this.shell.paste(command, this.store.get(s => s.promptState), this.renderer.getSelection());
        }
      })
    );
  }

  previousArgument() {
    const promptState = this.store.get(s => s.promptState);
    const placeholder = this.shell.goToPreviousArgument(promptState);
    const currentInput = promptState.currentInput.input?.replace(placeholder, '');
    this.autocompleteService.forceSuggesterByPlaceholder(placeholder, currentInput);
  }

  nextArgument() {
    const promptState = this.store.get(s => s.promptState);
    const placeholder = this.shell.goToNextArgument(promptState);
    const currentInput = promptState.currentInput.input?.replace(placeholder, '');
    this.autocompleteService.forceSuggesterByPlaceholder(placeholder, currentInput);
  }

  selectTextToEndOfLine() {
    const promptState = this.store.get(s => s.promptState);
    const steps = this.shell.cursorToEndOfLine(promptState);
    if (!!steps) {
      this.selectionHandler.selectText('right', promptState.cursorPosition, steps);
    }
  }

  selectTextToStartOfLine() {
    const promptState = this.store.get(s => s.promptState);
    const steps = this.shell.cursorToStartOfLine(promptState);
    if (!!steps) {
      this.selectionHandler.selectText('left', promptState.cursorPosition, steps);
    }
  }

  selectWordRight() {
    const promptState = this.store.get(s => s.promptState);
    const steps = this.shell.cursorToNextWord(promptState);
    if (!!steps) {
      this.selectionHandler.selectText('right', promptState.cursorPosition, steps);
    }
  }

  selectWordLeft() {
    const promptState = this.store.get(s => s.promptState);
    const steps = this.shell.cursorToPreviousWord(promptState);
    if (!!steps) {
      this.selectionHandler.selectText('left', promptState.cursorPosition, steps);
    }
  }

  selectTextRight() {
    const promptState = this.store.get(s => s.promptState);
    if (this.shell.cursorRight(promptState)) {
      this.selectionHandler.selectText('right', promptState.cursorPosition);
    }
  }

  selectTextLeft() {
    const promptState = this.store.get(s => s.promptState);
    if (this.shell.cursorLeft(promptState)) {
      this.selectionHandler.selectText('left', promptState.cursorPosition);
    }
  }

  ngOnDestroy(): void {
    this.decorationHandler.destroy();
    this.terminalStatusObserver.destroy();
    this.unsubscribe();
  }

  focus() {
    super.focus();
    this.renderer.focus();
  }

  async onReady() {
    const tab = this.gridService.getTab(this.store.id) as TerminalTab;
    this.untilDestroy(
      this.gridService.selectActiveTabId().pipe(filter(id => id === this.store.id)).subscribe((id) => {
        this.renderer.focus();
      }),
      this.gridService.onFocusTab().pipe(filter(id => id === this.store.id)).subscribe(() => {
        this.renderer.focus();
      })
    );
    this.untilDestroy(...this.initShell(), ...this.initXterm());
    this.shell.init(tab.config, this.store.id);
    if (this.gridService.getActiveTabId() === this.store.id) {
      setTimeout(() => this.renderer.focus());
    }
    let injectPromise: Promise<unknown>;
    if (tab.config.injectionType === 'Auto') {
      injectPromise = firstValueFrom(this.terminalStatusObserver.injected$.pipe(filter(injected => injected)));
    } else {
      injectPromise = firstValueFrom(this.terminalStatusObserver.shellReady$.pipe(tap(() => {
        if(tab.config.injectionType === 'Remote') {
          setTimeout(() => this.shell.write(tab.config.remoteCommand + Chars.Enter.asString));
        }
      })));
    }
    return tab.config.isDebugModeEnabled ? Promise.resolve() : injectPromise;
  }

  onCommandMarkerClicked(): Observable<{
    id: string;
    position: Position;
    hasBookmark: boolean;
    command: string;
    returnCode: number;
    startTime: number;
    endTime: number;
  }> {
    return this.decorationHandler.onCommandDecorationClicked();
  }

  outsideViewDecoration(): Observable<CommandDecorationEntry> {
    return this.decorationHandler.outsideViewDecoration();
  }

  injectAdvancedFeatures(): void {
    this.electronService.loadScript(this.shell.getActiveShellConfig()).subscribe(script => this.shell.injectAdvancedFeatures(script, this.settings.getActiveTheme()));
  }

  private initXterm(): Subscription[] {
    return [
      this.processRendererData(),
      this.processViewportChanged(),
      this.processResize(),
      this.processHttpLinkClicked(),
      this.processSelectionChanged()
    ];
  }

  private initShell(): Subscription[] {
    return [
      this.processShellOutput(),
      this.processShellError(),
      this.processShellExit(),
      this.processExecuteCommandFromGlobal()
    ];
  }

  destroy() {
    super.destroy();
  }

  bind(element: HTMLElement) {

    // TODO: Das hier aufräumen und auslagern. TerminalStatusObserver muss initialisiert werden, nachdem xtermjs bereit ist.

    const tab = this.gridService.getTab(this.store.id) as TerminalTab;
    this.terminalStatusObserver.bind(new StatusTerminalAdapter(this.renderer.terminal), tab.config, this.settings.getActiveTheme());
    this.copyObserver.bind(this.renderer.terminal);
    this.tabnameObserver.bind(this.renderer.terminal);
    this.decorationHandler.bind(this.renderer.terminal);

    this.renderer.terminal.parser.registerCsiHandler({final: 't'}, (n) => {
      if (n.length === 3 && n[0] === 22 && n[1] === 0 && n[2] === 0) {
        this.autocompleteService.hide();
        this.store.update({isAppRunning: true, areShortcutsEnabled: false, isPaddingRemoved: this.settings.getActiveTheme().removeFullScreenAppPadding});
        this.gridService.updateIsAppRunning(this.store.id, true);
        return true;
      }
      if (n.length === 3 && n[0] === 23 && n[1] === 0 && n[2] === 0) {
        this.autocompleteService.hide();
        this.store.update({isAppRunning: false, areShortcutsEnabled: true, isPaddingRemoved: false});
        this.decorationHandler.proofDecorationsOnNextTick = true;
        this.gridService.updateIsAppRunning(this.store.id, false);
        return true;
      }
      return false;
    });

    this.renderer.terminal.parser.registerCsiHandler({prefix: '?', final: 'h'}, (n) => {
      if(n.length === 1 && n[0] === 1049) {
        this.autocompleteService.hide();
        this.store.update({isAppRunning: true, areShortcutsEnabled: false, isPaddingRemoved: this.settings.getActiveTheme().removeFullScreenAppPadding});
        this.gridService.updateIsAppRunning(this.store.id, true);
      }
      return false;
    });
    this.renderer.terminal.parser.registerCsiHandler({prefix: '?', final: 'l'}, (n) => {
      if(n.length === 1 && n[0] === 1049) {
        this.autocompleteService.hide();
        this.store.update({isAppRunning: false, areShortcutsEnabled: true, isPaddingRemoved: false});
        this.gridService.updateIsAppRunning(this.store.id, false);
        this.decorationHandler.proofDecorationsOnNextTick = true;
      }
      return false;
    });

    this.subscription.push(this.copyObserver.copy$.pipe(filter(c => !!c)).subscribe(c => {
      this.electronService.clipboardWrite(c);
    }));

    this.subscription.push(this.tabnameObserver.tabname$.pipe(filter(tabName => !!tabName)).subscribe(tabName => {
      if(this.store.get(s => s.isAppRunning || !s.isAdvancedEnabled)) {
        this.gridService.updateTabName(this.store.id, {command: tabName});
      }
    }));

    this.subscription.push(this.terminalStatusObserver.injected$.subscribe(injected => {
      if(injected) {
        this.store.update({isAdvancedEnabled: true});
      }
    }));

    this.subscription.push(this.terminalStatusObserver.forceInjection$.subscribe((event) => {
      this.shell.pushSubShell({
        name: '',
        path: '',
        injectionType: this.shell.getActiveShellConfig().injectionType,
        isDebugModeEnabled: this.shell.getActiveShellConfig().isDebugModeEnabled,
        type: this.shell.getActiveShellConfig().remoteType || this.shell.getActiveShellConfig().type,
        machineName: event.machine,
      });
      this.injectAdvancedFeatures();
    }));

    this.subscription.push(this.terminalStatusObserver.shellReady$.subscribe(() => {
      if(this.shell.getActiveShellConfig().injectionType === 'Auto') {
        this.store.update({isAdvancedEnabled: true});
        this.injectAdvancedFeatures();
      }
    }));

    this.subscription.push(this.terminalStatusObserver.command$.pipe(filter(c => !!c)).subscribe((c) => {
      this.gridService.updateIsCommandRunning(this.store.id, c.status.isRunning);
      this.autocompleteService.hide();
      const machine = this.store.get(s => s.machine);
      const workingDir = this.store.get(s => s.workingDir);
      this.gridService.updateTabName(this.store.id, {directory: workingDir, command: c.status.isRunning ? c.command : undefined});
      this.store.update({isCommandRunning: c.status.isRunning});
      this.decorationHandler.renderCommandDecoration(c, this.renderer.getCurrentViewport());
      if (this.shouldCloseHostShell(c)) {
        setTimeout(() => this.shell.exit(), 500);
      }
      if(!c.status.isRunning && c.command.trim().length > 0) {
        this.store.update({wasLastCommandSuccessful: c.status.isSuccessfulReturn});
        this.autocompleteService.addImmediate({
          location: new ShellLocation(machine, this.shell.getActiveShellConfig().type),
          directory: workingDir,
          commandStatus: c.status,
          command: c.command
        });
        this.store.update({
          isRemoteShell: this.isRemoteShellInit(c)
        });
      }
    }));
    this.subscription.push(this.terminalStatusObserver.input$.pipe(filter(c => !!c)).subscribe((c) => {
      const inputChanged = c.text !== this.store.get(s => s.promptState?.currentInput)?.input;
      const cursorChanged = c.cursor.x !== this.store.get(s => s.promptState?.cursorPosition)?.x;
      const machine = this.store.get(s => s.machine);
      const workingDir = this.store.get(s => s.workingDir);

      if(!machine) {return;} // shell is not fully initialized
      if(!inputChanged && ! cursorChanged) {return;} // input and cursor does not change
      this.store.update({promptState: {currentInput: {input: c.text, lines: c.lines}, cursorPosition: c.cursor}});
      if(c.keyEvent && c.keyEvent.key !== Key.ArrowUp && c.keyEvent.key !== Key.ArrowDown) {
        if(this.settings.getAutocompleteMode() === 'always') {
          const autocompleteRequest: AutocompleteRequest = {
            input: c.textToCursor,
            cursorPosition: c.cursor,
            directory: workingDir,
            location: new ShellLocation(machine, this.shell.getActiveShellConfig().type),
          };
          this.autocompleteService.suggest(autocompleteRequest);
        } else {
          this.autocompleteService.suggest(undefined);
        }
      }
    }));
    this.subscription.push(this.terminalStatusObserver.environment$.pipe(filter(e => !!e)).subscribe((d) => {
      // TODO: Shellinfo aufräumen. timestamp, returnCode, branch und is dirty weg, ShellType aber mit rein.
      const workingDir = Normalizer.getDirectories(d.workingDir);
      const machine = this.shell.getConfigMachineName() || d.machine;
      const user = d.user;
      this.store.update(s => ({user: user, machine: machine, workingDir: workingDir}));
      this.autocompleteService.initSuggester(new ShellLocation(machine, this.shell.getActiveShellConfig().type));
      this.gridService.updateTabName(this.store.id, {directory: workingDir});
      if((this.shell.getActiveShellConfig().injectionType === 'Auto' || this.shell.getActiveShellConfig().injectionType === 'Remote') && !d.isAutoInjection) {
        this.notificationService
          .create()
          .body('Automatic script injection is enabled, and a manual script injection has also been performed. This configuration may lead to errors.')
          .title('Warning')
          .single('warningMixedInjection')
          .showAsWarning().subscribe();
      }
    }));

    this.renderer.bind(element);
    setTimeout(() => {
      this.renderer.fit();
      this.shell.resize(this.renderer.dimensions);
      this.renderer.focus();
    });
  }

  attachCustomKeyEventHandler(customKeyEventHandler: (event: KeyboardEvent) => boolean) {
    this.renderer.terminal.attachCustomKeyEventHandler(customKeyEventHandler);
  }

  updateActiveTab(): void {
    this.gridService.updateActiveTab(this.store.id);
  }

  scrollToBottom(): void {
    this.renderer.scrollToBottom();
  }

  public search(options: SearchOptions, incremental: boolean, forward = false) {
    if (forward) {
      this.renderer.findNext(`${options.searchText}`, {
        caseSensitive: options.caseSensitive,
        regex: options.regex,
        wholeWord: options.wholeWord,
        incremental: incremental
      });
    } else {
      this.renderer.findPrevious(`${options.searchText}`, {
        caseSensitive: options.caseSensitive,
        regex: options.regex,
        wholeWord: options.wholeWord,
        incremental: incremental
      });
    }
  }

  private processRendererData(): Subscription {
    return this.renderer.onData().subscribe((data) => {
      this.shell.write(data);
    });
  }

  public selectSearchResult(): Observable<{ resultCount: number; resultIndex: number }> {
    return this.renderer.onSearchResult();
  }

  private isRemoteShellInit(command: TerminalCommand): boolean {
    return !command.endMarker && (this.isPossibleSsh(command) || this.isPossibleDocker(command));
  }

  private isPossibleSsh(command: TerminalCommand): boolean {
    return command.command?.trim().startsWith('ssh ');
  }

  private isPossibleDocker(command: TerminalCommand): boolean {
    return command.command?.trim().startsWith('docker ') && (command.command.includes('-t') || command.command.includes('-it'));
  }

  private shouldCloseHostShell(command: TerminalCommand): boolean {
    return command.command?.trim() === 'exit' && this.shell.isLastSubShell();
  }

  private processViewportChanged(): Subscription {
    return this.renderer.onViewportChanged().subscribe(viewport => {
      this.decorationHandler.calcDecorationsInViewport(viewport);
      this.store.update({isScrollButtonVisible: viewport.endLine < viewport.lineOfCursor - 10});
    });
  }

  private processShellOutput(): Subscription {
    return this.shell.onData().subscribe((data) => {
      if (data) {
        this.renderer.write(data);
      }
    });
  }

  private processShellExit(): Subscription {
    return this.shell.onExit().subscribe((data) => {
      this.gridService.closeTabs(this.store.id);
    });
  }

  private processShellError(): Subscription {
    return this.shell.onError().subscribe((data) => {
      this.notificationService.create().title('Whoops!').body('Maybe there is something wrong in your settings-file.', data?.error).single('session-error').showAsError().subscribe();
    });
  }

  private processExecuteCommandFromGlobal(): Subscription {
    return this.gridService.onExecuteSuggestion()
      .subscribe(s => {
        if (s.tabId === this.store.id) {
          this.focus();
          const promptState = this.store.get(s => s.promptState);
          this.shell.clearLine(promptState);
          this.terminalStatusObserver.forceNextCommand(s.suggestion.shellCommand);
          this.shell.write(s.suggestion.shellCommand);
        }
      });
  }

  public select<T>(selector: (state: TerminalState) => T): Observable<T> {
    return this.store.select(selector);
  }

  public get<T>(selector: (state: TerminalState) => T): T {
    return this.store.get(selector);
  }

  private processResize(): Subscription {
    return this.windowService.onResize.pipe(debounceTime(100)).subscribe(r => {
      const pDims = this.renderer.proposeDimensions();
      const dims = this.renderer.dimensions;
      const ptyDims = this.shell.dimensions;
      if (pDims && pDims.rows && pDims.cols) {
        if (pDims.cols !== dims.cols || pDims.rows !== dims.rows) {
          this.renderer.fit();
          this.decorationHandler.proofDecorationsOnNextTick = true;
        }
        if (pDims.cols !== ptyDims.cols || pDims.rows !== ptyDims.rows) {
          this.shell.resize(pDims);
        }
      }
    });
  }

  copy() {
    if (this.selectionHandler.hasSelection()) {
      this.electronService.clipboardWrite(this.selectionHandler.getSelection().text);
      this.selectionHandler.clearSelection();
    }
  }

  hasStringToCopy(): boolean {
    return this.electronService.clipboardRead()?.length > 0;
  }

  copyOrAbort() {
    if (this.selectionHandler.hasSelection()) {
      this.copy();
    } else {
      this.abortTask();
    }
  }

  cut() {
    if (this.selectionHandler.hasSelection()) {
      const selection = this.selectionHandler.getSelection();
      this.electronService.clipboardWrite(selection.text);
      this.shell.cut(this.store.get(s => s.promptState), selection);
      this.selectionHandler.clearSelection();
    }
  }

  paste() {
    const stringToPaste = this.electronService.clipboardRead();
    if (stringToPaste) {
      this.shell.paste(stringToPaste, this.store.get(s => s.promptState), this.selectionHandler.getSelection());
      this.pasteService.add(stringToPaste);
      this.selectionHandler.clearSelection();
    }
  }

  /**
   * Replaces the selection in the terminal with the pressed key.
   * @param event
   */
  replaceSelection(event: {escapeSequence: string; key: string; isPrintableChar: boolean}) {
    if (this.renderer.hasSelection()) {
      this.shell.paste(event.escapeSequence, this.store.get(s => s.promptState), this.renderer.getSelection());
      this.renderer.clearSelection();
    }
  };

  clearBuffer() {
    this.decorationHandler.resetSelections();
    this.decorationHandler.clear();
    this.renderer.clear();
  }

  private processHttpLinkClicked(): Subscription {
    return this.renderer.onHttpLinkClicked().subscribe(link => {
      this.electronService.openExternal(link);
    });
  }

  private processSelectionChanged(): Subscription {
    return this.renderer.onSelectionChanged().subscribe(selection => {
      this.electronService.clipboardWrite(selection);
    });
  }

  showAutocompletion() {
    if (!this.store.get(s => s.isCommandRunning)) {
      const autocompleteRequest: AutocompleteRequest = {
        input: this.store.get(s => s.promptState?.currentInput?.input) || '',
        cursorPosition: this.store.get(s => s.promptState?.cursorPosition),
        directory: this.store.get(s => s.workingDir),
        location: new ShellLocation(this.store.get(s => s.machine), this.shell.getActiveShellConfig().type),
      };
      this.autocompleteService.suggest(autocompleteRequest, true);
    }
  }

  getCurrentDirectory(): string[] {
    return this.store.get(s => s.workingDir);
  }

  clearClipboard() {
    this.electronService.clipboardClear();
  }

  isPasteOnRightClickEnabled(): boolean {
    return this.settings.getIsPasteOnRightClickEnabled();
  }

  scrollToPreviousCommand(): void {
    const marker = this.decorationHandler.getPreviousCommandMarker();
    this.scrollToMarker(marker, 1);
  }

  scrollToNextCommand(): void {
    const marker = this.decorationHandler.getNextCommandMarker();
    this.scrollToMarker(marker, 1);
  }

  scrollToPreviousBookmark() {
    const marker = this.decorationHandler.getPreviousBookmarkMarker();
    this.scrollToMarker(marker, 1);
  }

  scrollToNextBookmark() {
    const marker = this.decorationHandler.getNextBookmarkMarker();
    this.scrollToMarker(marker, 1);
  }

  private scrollToMarker(marker: IMarker | undefined, offset = 0) {
    if (!marker) {
      this.scrollToBottom();
    } else {
      this.renderer.scrollTo(marker, offset);
    }
  }

  copyCommand(id: string) {
    const command = this.decorationHandler.getCommand(id);
    if (!command && command.command) {
      return;
    }
    this.electronService.clipboardWrite(command.command);
  }

  addBookmark(id: string) {
    this.decorationHandler.addBookmark(id);
  }

  removeBookmark(id: string) {
    this.decorationHandler.removeBookmark(id);
  }

  scrollToCommandTop(id: string) {
    const command = this.decorationHandler.getCommand(id);
    if (!command.startMarker) {
      return;
    }
    this.renderer.scrollTo(command.startMarker);
  }

  scrollToCommandBottom(id: string) {
    const command = this.decorationHandler.getCommand(id);
    if (!command.endMarker) {
      this.renderer.scrollToBottom();
    } else {
      this.renderer.scrollTo(command.endMarker);
    }
  }

  copyOutput(id: string) {
    const command = this.decorationHandler.getCommand(id);
    if (!command.startMarker) {
      return;
    }
    let output = '';
    if (!command.endMarker) {
      output = this.renderer.readLines(command.startMarker.line + command.outputOffset);
    } else {
      output = this.renderer.readLines(command.startMarker.line + command.outputOffset, command.endMarker.line);
    }
    this.electronService.clipboardWrite(output);
  }

  toggleShortcutsEnabled() {
    this.store.update(s => ({...s, areShortcutsEnabled: !s.areShortcutsEnabled}));
  }

  isActive() {
    return this.store.id === this.gridService.getActiveTabId();
  }

  clearTerminal() {
    this.renderer.clear();
  }

  clearLine() {
    this.shell.clearLine(this.store.get(s => s.promptState));
  }

  clearLineToStart() {
    this.shell.clearLineToStart(this.store.get(s => s.promptState));
  }

  clearLineToEnd() {
    this.shell.clearLineToEnd(this.store.get(s => s.promptState));
  }

  goToNextWord() {
    this.shell.cursorToNextWord(this.store.get(s => s.promptState));
  }

  deletePreviousWord() {
    this.shell.deletePreviousWord(this.store.get(s => s.promptState));
  }

  goToPreviousWord() {
    this.shell.cursorToPreviousWord(this.store.get(s => s.promptState));
  }

  deleteNextWord() {
    this.shell.deleteNextWord(this.store.get(s => s.promptState));
  }

  abortTask() {
    this.shell.abortTask();
    this.terminalStatusObserver.forceNextReturnSuccessful();
  }

  isCommandRunning(): boolean {
    return this.store.get(s => s.isCommandRunning);
  }

  execute(command: string) {
    this.shell.write(command + Chars.Enter.asString);
  }

  hasSelection(): boolean {
    return this.renderer.hasSelection();
  }

  clearSelection() {
    this.selectionHandler.clearSelection();
  }

  areShortcutsEnabled(): boolean  {
    return this.store.get(s => s.areShortcutsEnabled);
  }

  write(key: string) {
    this.shell.write(key);
  }

  getActiveShellConfig(): ShellConfig {
    return this.shell.getActiveShellConfig();
  }
}
