import {Injectable} from '@angular/core';
import {CursorPosition, Position} from '../../../../../shared/models/models';
import {Renderer} from '../renderer/renderer';
import {Selection} from '../models';

@Injectable()
export class SelectionHandler {

  private lastCursorPlace?: 'start' | 'end';

  constructor(private renderer: Renderer) {
  }

  public selectText(direction: 'left' | 'right', cursor: CursorPosition, stepSize: number = 1): void {
    const selection = this.renderer.getSelection();
    let startOfSelection: Position;
    let endOfSelection: Position;
    let length = 0;
    if(selection.hasSelection) {
      startOfSelection = selection.start;
      endOfSelection = selection.end;
      length = selection.text.replace(/(\r\n|\n|\r)/gm, '').length;
    } else {
      this.lastCursorPlace = direction === 'left' ? 'start' : 'end';
      startOfSelection = {x: cursor.x, y: cursor.line};
      endOfSelection = {x: cursor.x, y: cursor.line};
      length = 0;
    }

    const currentCursorPlace = this.calcCursorPlace(direction, startOfSelection, endOfSelection);

    if (direction === 'left' && currentCursorPlace === 'start') {
      length += stepSize;
      startOfSelection.x -= stepSize;
    } else if (direction === 'right' && currentCursorPlace === 'start') {
      length -= stepSize;
      startOfSelection.x += stepSize;
    } else if (direction === 'left' && currentCursorPlace === 'end') {
      length -= stepSize;
    } else if (direction === 'right' && currentCursorPlace === 'end') {
      length += stepSize;
    }
    this.lastCursorPlace = currentCursorPlace;
    this.renderer.selectText(startOfSelection, length);
  }

  calcCursorPlace(direction: 'left' | 'right', startOfSelection: Position, endOfSelection: Position): 'start' | 'end' {
    if (startOfSelection.x === endOfSelection.x) {
      return direction === 'left' ? 'start' : 'end';
    }
    return this.lastCursorPlace;
  }

  public clearSelection(): void {
    this.renderer.clearSelection();
    this.lastCursorPlace = undefined;
  }

  hasSelection() {
    return this.renderer.hasSelection();
  }

  getSelection(): Selection {
    return this.renderer.getSelection();
  }
}
