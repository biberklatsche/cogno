import {IDecoration, IMarker, Terminal} from '@xterm/xterm';
import {Viewport} from '../models';
import {Observable, Subject} from 'rxjs';
import {Position} from '../../../../../shared/models/models';
import {mdiBookmark, mdiDotsVertical} from '@mdi/js';
import {IDisposable} from 'node-pty';
import {TerminalCommand, TerminalCommandStatus} from '../osc/terminal-status.observer';

export class DecorationHandler {
  private terminal: Terminal;

  public proofDecorationsOnNextTick = false;

  private _decorations: Map<string, CommandDecorationEntry> = new Map<string, CommandDecorationEntry>();
  private _commandIndex = -1;
  private _bookmarkIndex = -1;
  private _decorationClicked = new Subject<{
    id: string;
    position: Position;
    hasBookmark: boolean;
    command: string;
    returnCode: number;
    startTime: number;
    endTime: number;
  }>();
  private _outsideViewDecoration = new Subject<CommandDecorationEntry>();

  private disposables: IDisposable[] = [];

  bind(terminal: Terminal) {
    this.terminal = terminal;
  }

  destroy(){
    for (const disposable of this.disposables) {
      disposable.dispose();
    }
    this.clear();
    this._decorationClicked.complete();
  }

  clear() {
    for (const entry of this._decorations.values()) {
      entry.commandDecoration?.dispose();
      entry.bookmarkDecoration?.dispose();
      entry.promptDecoration?.dispose();
    }
    this._decorations.clear();
    this._commandIndex = -1;
    this._bookmarkIndex = -1;
  }

  renderCommandDecoration(command: TerminalCommand, viewport: Viewport) {
    this.resetSelections();
    if (!this._decorations.has(command.id)) {
      const entry: CommandDecorationEntry = {
        id: command.id,
        command: command,
        startBufferLine: command.startMarker.line
      };
      entry.promptDecoration = this.createBubbleDecoration(command.startMarker, entry);
      entry.menuDecoration = this.createMenuDecoration(command.startMarker, entry);
      this._decorations.set(command.id, entry);
    }
    const entry = this._decorations.get(command.id);
    if(!!command.endMarker) {
      entry.endBufferLine = command.endMarker.line;
      entry.command = command;
      if(entry.promptDecoration?.element) {
        entry.promptDecoration.element.classList.add(this.getReturnCodeClassName(command.status));
      }
    }
    this.calcDecorationsInViewport(viewport);
  }

  private createDecoration(marker: IMarker): IDecoration | undefined {
    return this.terminal.registerDecoration({
      marker,
      width: 0,
    });
  }

  private createBubbleDecoration(marker: IMarker, entry: CommandDecorationEntry): IDecoration {
    const bubbleDecoration = this.createDecoration(marker);
    bubbleDecoration?.onRender(element => {
      const returnCodeClassName = this.getReturnCodeClassName(entry.command.status);
      if (element.classList.contains('command-bubble')) {
        return;
      }
      element.classList.add('command-bubble');
      element.classList.add(returnCodeClassName);
    });
    return bubbleDecoration;
  }

  private createMenuDecoration(marker: IMarker, entry: CommandDecorationEntry): IDecoration {
    const menuDecoration = this.createDecoration(marker);
    menuDecoration?.onRender(element => {
      const that = this;
      if (element.classList.contains('command-menu')) {
        return;
      }
      element.classList.add('command-menu');
      element.setAttribute('data-keytip', '1');
      element.setAttribute('data-keytip-parent', '');
      this.renderMenuIcon(element);
      element.onclick = (event) => {
        const element = event.target as HTMLElement;
        // Get the bounding rectangle of the element
        const rect = element.getBoundingClientRect();
        // Coordinates relative to the viewport
        const x = rect.right;
        const y = rect.bottom;
        event.stopPropagation();
        event.preventDefault();
        that._decorationClicked.next({
          id: entry.id,
          position: {x: x, y: y},
          hasBookmark: !!entry.bookmarkDecoration,
          command: entry.command.command,
          returnCode: entry.command.status.returnCode,
          startTime: entry.command.status.startTime,
          endTime: entry.command.status.endTime,
        });
      };
    });
    return menuDecoration;
  }

  private renderMenuIcon(node: HTMLElement) {
    this.renderIcon(node, mdiDotsVertical);
  }

  private renderBookmarkIcon(node: HTMLElement) {
    this.renderIcon(node, mdiBookmark);
  }

  private renderIcon(node: HTMLElement, svgPath: string) {
    const iconSvg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    const iconPath = document.createElementNS(
      'http://www.w3.org/2000/svg',
      'path'
    );
    iconSvg.setAttribute('viewBox', '0 0 24 24');
    iconSvg.classList.add('post-icon');
    iconPath.setAttribute(
      'd',
      svgPath);
    iconSvg.appendChild(iconPath);
    return node.appendChild(iconSvg);
  }

  private pixelToNumber(px: string) {
    const s = px.replace('px', '');
    return Number.parseInt(s, 10);
  }

  public addBookmark(id: string): void {
    this.resetSelections();
    const entry = this._decorations.get(id);
    const decoration = this.createBookmarkDecoration(entry.command.startMarker);
    decoration.onRender(element => {
      if (element.classList.contains('command-bookmark')) {
        return;
      }
      element.classList.add('command-bookmark');
      this.renderBookmarkIcon(element);
    });
    entry.bookmarkDecoration = decoration;
  }

  createBookmarkDecoration(marker: IMarker): IDecoration {
    const offset  =  marker.line - (this.terminal.buffer.active.cursorY + this.terminal.buffer.active.baseY);
    const bookmarkMarker = this.terminal.registerMarker(offset);
    return this.terminal.registerDecoration({
      marker: bookmarkMarker,
      overviewRulerOptions: {
        color: 'var(--highlight-color)',
        position: 'right'
      }
    });
  }

  public removeBookmark(id: string): void {
    const entry = this._decorations.get(id);
    entry.bookmarkDecoration.marker.dispose();
    entry.bookmarkDecoration.dispose();
    entry.bookmarkDecoration = undefined;
  }

  public onCommandDecorationClicked(): Observable<{
    id: string;
    position: Position;
    hasBookmark: boolean;
    command: string;
    returnCode: number;
    startTime: number;
    endTime: number;
  }> {
    return this._decorationClicked.asObservable();
  }

  public outsideViewDecoration(): Observable<CommandDecorationEntry> {
    return this._outsideViewDecoration.asObservable();
  }

  calcDecorationsInViewport(viewport: Viewport) {
    if(this.proofDecorationsOnNextTick) {
      this.proofDecorationsOnNextTick = false;
      this.proofDecorations();
    }

    const entries = this.getViewportEntries(viewport);
    this._outsideViewDecoration.next(entries.outsideEntry ? entries.outsideEntry : undefined);
    for (const oldEntry of entries.oldEntries) {
      oldEntry.commandDecoration?.dispose();
      oldEntry.commandDecoration = undefined;
    }
    for (const newEntry of entries.newEntries) {
      if (newEntry.commandDecoration) {
        newEntry.commandDecoration?.dispose();
        newEntry.commandDecoration = undefined;
      }

      const decorationStartLine = Math.max(newEntry.command.startMarker.line, viewport.startLine);
      const cursorLine = this.terminal.buffer.active.baseY + this.terminal.buffer.active.cursorY;
      const offset = decorationStartLine - cursorLine;
      let height = 1;
      if (!!newEntry?.command?.endMarker) {
        const decorationEndLine = Math.min(newEntry.command.endMarker.line, viewport.endLine);
        height = decorationEndLine - decorationStartLine + 1;
      }
      newEntry.commandDecoration = this.createDecoration(this.createMarker(offset));
      newEntry.commandDecoration.onRender(element => {
        element.id = newEntry.id;
        element.classList.add('command-line');
        element.classList.add(this.getReturnCodeClassName(newEntry.command.status));
        element.style.height = height * this.pixelToNumber(element.style.lineHeight) + 'px';
      });
    }
  }

  createMarker(offset: number): IMarker {
    return this.terminal.registerMarker(offset);
  }

  private getReturnCodeClassName(status?: TerminalCommandStatus): string {
    if (status?.returnCode === undefined || status?.returnCode === null) {
      return 'running';
    }
    return status?.isSuccessfulReturn ? 'success' : 'error';
  }


  private getViewportEntries(viewport: Viewport): {
    oldEntries: CommandDecorationEntry[];
    newEntries: CommandDecorationEntry[];
    outsideEntry?: CommandDecorationEntry | undefined;
  } {
    const newEntries = [];
    const oldEntries = [];
    let outsideEntry: CommandDecorationEntry | undefined;
    for (const entry of this._decorations.values()) {
      if (
        !entry.command.startMarker.isDisposed && entry.command.startMarker.line < viewport.startLine &&
        (!entry.command.endMarker || entry.command.endMarker.line >= viewport.startLine)
      ) {
        outsideEntry = entry;
      }
      if (
        !entry.command.endMarker &&
        (entry.command.startMarker.line < viewport.startLine ||
          entry.command.startMarker.line > viewport.endLine)
      ) {
        if (entry.commandDecoration) {
          oldEntries.push(entry);
        }
        continue;
      }
      if (
        entry.command.startMarker.line < viewport.startLine && entry.command.endMarker.line < viewport.startLine ||
        entry.command.startMarker.line > viewport.endLine && entry.command.endMarker.line > viewport.endLine
      ) {
        if (entry.commandDecoration) {
          oldEntries.push(entry);
        }
        continue;
      }
      newEntries.push(entry);
    }
    return {oldEntries, newEntries, outsideEntry};
  }

  getSortedListOfMarker(): IMarker[] {
    const decorations = Array.from(this._decorations.values());
    decorations.sort((a, b) => a.id.localeCompare(b.id));
    return decorations.map(d => d.command.startMarker);
  }

  getSortedListOfBookmarkMarker(): IMarker[] {
    const bookmarkDecorations = Array.from(this._decorations.values()).filter(d => d.bookmarkDecoration);
    bookmarkDecorations.sort((a, b) => a.id.localeCompare(b.id));
    return bookmarkDecorations.map(d => d.command.startMarker);
  }

  getNextCommandMarker(): IMarker | undefined {
    this._commandIndex++;
    const markers = this.getSortedListOfMarker();
    if (this._commandIndex >= markers.length || markers.length === 0) {
      this._commandIndex = -1;
      return undefined;
    }
    return markers[this._commandIndex];
  }

  getPreviousCommandMarker(): IMarker | undefined {
    this._commandIndex--;
    const markers = this.getSortedListOfMarker();
    if (this._commandIndex === -1 || markers.length === 0) {
      return undefined;
    }
    if (this._commandIndex === -2) {
      this._commandIndex = markers.length - 1;
    }
    return markers[this._commandIndex];
  }

  getNextBookmarkMarker(): IMarker {
    const bookmarks = this.getSortedListOfBookmarkMarker();
    this._bookmarkIndex++;
    if (this._bookmarkIndex >= bookmarks.length || bookmarks.length === 0) {
      this._bookmarkIndex = -1;
      return undefined;
    }
    return bookmarks[this._bookmarkIndex];
  }

  getPreviousBookmarkMarker(): IMarker {
    const bookmarks = this.getSortedListOfBookmarkMarker();
    this._bookmarkIndex--;
    if (this._bookmarkIndex === -1 || bookmarks.length === 0) {
      return undefined;
    }
    if (this._bookmarkIndex === -2) {
      this._bookmarkIndex = bookmarks.length - 1;
    }
    return bookmarks[this._bookmarkIndex];
  }

  resetSelections() {
    this._commandIndex = -1;
    this._bookmarkIndex = -1;
  }

  getCommand(id: string) {
    return this._decorations.get(id)?.command;
  }

  private proofDecorations() {
    for (const decoration of this._decorations.values()) {
      this.restoreDecorations(decoration);
    }
  }

  private restoreDecorations(entry: CommandDecorationEntry) {
    const currentCursorBufferLine = this.terminal.buffer.active.baseY + this.terminal.buffer.active.cursorY;
    if(entry.command.startMarker.isDisposed) {
      const offset = entry.startBufferLine - currentCursorBufferLine;
      const restoredMarker = this.terminal.registerMarker(offset);
      entry.startBufferLine = restoredMarker.line;
      entry.command.startMarker = restoredMarker;
      entry.promptDecoration = this.createBubbleDecoration(restoredMarker, entry);
      entry.menuDecoration = this.createMenuDecoration(restoredMarker, entry);
    }
    if(entry.command.endMarker?.isDisposed) {
      const offset = entry.endBufferLine - currentCursorBufferLine;
      const restoredMarker = this.terminal.registerMarker(offset);
      entry.endBufferLine = restoredMarker.line;
      entry.command.endMarker = restoredMarker;
    }
  }
}

export type CommandDecorationEntry = {
  id: string;
  command: TerminalCommand;
  startBufferLine: number; // if the marker for start is disposed, we can use this to restore the marker
  endBufferLine?: number; // if the marker for end is disposed, we can use this to restore the marker
  commandDecoration?: IDecoration;
  promptDecoration?: IDecoration;
  menuDecoration?: IDecoration;
  bookmarkDecoration?: IDecoration;
};
