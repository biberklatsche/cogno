import {IBufferLine, IEvent, IMarker, Terminal} from '@xterm/xterm';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {IDisposable} from 'node-pty';
import {CursorPosition, Position} from '../../../../../shared/models/models';
import {Queue} from '../../../+shared/models/queue';
import {Uuid} from '../../../common/uuid';
import {Injectable, NgZone} from '@angular/core';
import {PromptFinder} from './prompt.finder';
import {ShellConfig, Theme} from '../../../../../shared/models/settings';
import {Key} from '../../../common/key';
import {debounceTime} from 'rxjs/operators';
import {isSuccessfulReturn} from "../../../common/return-codes";

export type TerminalCommand = {
  id: string;
  command: string;
  startMarker: IMarker;
  outputOffset?: number;
  endMarker?: IMarker;
  status: TerminalCommandStatus;
};
export type TerminalCommandStatus = { isRunning: boolean; returnCode?: number; startTime: number; endTime?: number; isSuccessfulReturn?: boolean };

export type TerminalInput = {
  text: string;
  textToCursor: string;
  cursor: CursorPosition;
  lines: { line: number; lineText: string }[];
  keyEvent: KeyboardEvent;
};
export type TerminalEnvironment = { machine: string; user: string; workingDir: string; timestamp: number; isAutoInjection: boolean };

type Event =
  PromptStartEvent
  | PromptEndEvent
  | CommandStartEvent
  | CommandEndEvent
  | InjectFeaturesEvent
  | CommandRepairEvent;

type InjectFeaturesEvent = {
  type: 'COGNO:INJECT';
  machine: string | undefined;
};

type CommandRepairEvent = {
  type: 'COGNO:COMMAND_REPAIR';
};

type PromptStartEvent = {
  type: 'COGNO:PROMPT_START';
  user: string;
  machine: string;
  directory: string;
  timestamp: number;
  isAutoInjection: boolean;
};

type PromptEndEvent = {
  type: 'COGNO:PROMPT_END';
};

type CommandStartEvent = {
  type: 'COGNO:COMMAND_START';
};

type CommandEndEvent = {
  type: 'COGNO:COMMAND_END';
  returnCode: number;
};

@Injectable()
export class TerminalStatusObserver {

  private terminal: IStatusTerminalAdapter;

  private promptFinder: PromptFinder | undefined;

  public readonly OSC_CODE = 733;

  private lastKeyEvent: KeyboardEvent;

  /**
   * The current Prompt Marker.
   * For example if the prompt is like:
   *
   * /my/directory
   * $
   *
   * The marker would be at the line '/my/directory' .
   */
  private commandPromptMarker: IMarker | undefined = undefined;

  /**
   * The column offset of the current Prompt.
   * For example if the prompt is like:
   *
   * /my/directory
   * $_
   *
   * The the offset is 2. Because '$_' has a length of 2;
   */
  private columnOffset: number | undefined = undefined;
  /** The line offset of the prompt.
   * For example if the prompt is like:
   *
   * /my/directory
   * $_
   *
   * The offset is +1.
   * Because the prompt is recognized after '$_' but the prompt itself is over 2 lines in the terminal.
   */
  private lineOffset: number;

  /**
   * TODO: This is a hack.
   * The max position of the cursor during the current command input.
   * We need this, because of terminal-autocompletion.
   *
   * For example some shells will expand the input like "git commit " with "-am 'test'".
   * If we just execute "git commit", the observer will read the full suggested line from the buffer "git commit -am 'test'".
   * So we have to remove "-am 'test'".
   */
  private maxCursorPosition: Position;
  private forceCommand?: string;
  private forceSuccessReturn: boolean = false;

  private commandId: string | undefined = undefined;

  // The current running command
  private _command = new BehaviorSubject<TerminalCommand>(undefined);
  // The current input in the terminal
  private _input = new BehaviorSubject<TerminalInput>(undefined);
  // The current terminal environment like machine name, user, working dir
  private _environment = new BehaviorSubject<TerminalEnvironment>(undefined);
  // A flag that indicates if the cogno advanced script is injected or not.
  // This is triggered by the first occurence of OSC_CODE:COGNO...
  private _injected = new BehaviorSubject<boolean>(false);
  // A flag that indicates if the Shell is ready.
  // This is triggerd by the first write to the Terminal.
  private _shellReady = new Subject<boolean>();
  private _forceInjection = new Subject<{ machine: string }>();

  private _queue: Queue<Event> = new Queue<Event>();

  private firstWriteDisposable: IDisposable;
  private firstWriteTimeout: any;

  private disposables: IDisposable[] = [];

  private lastLineEnterPressedOffset: number | undefined;

  private remotePromptTerminator: string;

  constructor(private ngZone: NgZone) {

  }

  public get command$(): Observable<TerminalCommand> {
    return this._command.asObservable();
  }

  public get input$(): Observable<TerminalInput> {
    return this._input.asObservable();
  }

  public get environment$(): Observable<TerminalEnvironment> {
    return this._environment;
  }

  public get injected$(): Observable<boolean> {
    return this._injected.asObservable().pipe(debounceTime(300));
  }

  public get forceInjection$(): Observable<{ machine: string }> {
    return this._forceInjection.asObservable(); // .pipe(debounceTime(300));
  }

  public get shellReady$(): Observable<boolean> {
    return this._shellReady.asObservable();
  }

  bind(terminal: IStatusTerminalAdapter, config: ShellConfig, theme: Theme): void {
    this.terminal = terminal;
    this.remotePromptTerminator = theme.prompt;
    let hasFired = false;
    if (config.injectionType === 'Auto') {
      this.promptFinder = new PromptFinder(theme.prompt, true);
    } else {
      this.promptFinder = new PromptFinder(config.promptTerminator, config.usesFinalSpacePromptTerminator || false);
    }
    this.firstWriteDisposable = this.terminal.onWriteParsed(() => {
      if (hasFired || this.firstWriteTimeout) {
        return; // Exit if already fired or if the timeout is set
      }
      this.firstWriteTimeout = setTimeout(() => {
        hasFired = true; // Ensure it doesn't execute again
        this.firstWriteDisposable?.dispose();
        this.firstWriteDisposable = undefined;
        this.firstWriteTimeout = undefined;
        this._shellReady.next(true); // Notify that the shell is ready
      }, 500);
    });

    this.disposables.push(this.terminal.onWriteParsed(() => this.tick()));
    this.disposables.push(this.terminal.onKey((e) => this.handleKeyEvent(e.domEvent)));
    this.disposables.push(this.terminal.registerOscHandler(this.OSC_CODE, (data) => this.handleOsc(data)));
  }

  destroy() {
    for (const disposable of this.disposables) {
      disposable.dispose();
    }
  }

  /**
   * ```
   * ${PS1}${OSC_PROMPT}<userinput>
   * <commandoutput>
   * ```
   * @param data
   * @private
   */
  private handleOsc(data: string): boolean {
    if (this.firstWriteTimeout) {
      clearTimeout(this.firstWriteTimeout);
      this.firstWriteDisposable.dispose();
      this.firstWriteTimeout = undefined;
      this._shellReady.next(true);
    }
    // this happens after a resize. the marker is disposed and must be repaired
    if (this.commandPromptMarker?.isDisposed) {
      this._queue.enqueue({
        type: 'COGNO:COMMAND_REPAIR'
      });
    }
    if (data.startsWith('COGNO:PROMPT') && this._injected.value === false) {
      this._injected.next(true);
    }
    if (data.startsWith('COGNO:PROMPT')) {
      const environmentData = data.replace('COGNO:PROMPT;', '').split('|');
      const returnCode = environmentData[0];
      const user = environmentData[1];
      const machine = environmentData[2];
      const directory = environmentData[3];
      const timestamp = Number.parseInt(environmentData[4], 10);
      const isAutoInjection = environmentData[environmentData.length-1] === 'auto';
      this._queue.enqueue({
        type: 'COGNO:COMMAND_END',
        returnCode: Number.parseInt(returnCode, 10)
      });
      this._queue.enqueue({type: 'COGNO:PROMPT_START', user, machine, directory, timestamp, isAutoInjection});
      this._queue.enqueue({type: 'COGNO:PROMPT_END'});
    }
    if (data.startsWith('COGNO:INJECT')) {
      this._queue.clear();
      this.promptFinder = new PromptFinder(this.remotePromptTerminator, true);
      const machine = data.replace('COGNO:INJECT;', '').replace('COGNO:INJECT', '');
      this._queue.enqueue({type: 'COGNO:INJECT', machine: machine});
    }
    this.tick();
    return true;
  }

  private tick() {
    this.ngZone.run(() => {
      let shouldHandleNextEvent = true;
      while (!this._queue.isEmpty() && shouldHandleNextEvent) {
        const event = this._queue.peek();
        switch (event.type) {
          case 'COGNO:PROMPT_START':
            shouldHandleNextEvent = this.handlePromptStart(event);
            break;
          case 'COGNO:PROMPT_END':
            shouldHandleNextEvent = this.handlePromptEnd(event);
            break;
          case 'COGNO:COMMAND_START':
            shouldHandleNextEvent = this.handleCommandStart(event);
            break;
          case 'COGNO:COMMAND_END':
            shouldHandleNextEvent = this.handleCommandEnd(event);
            break;
          case 'COGNO:INJECT':
            shouldHandleNextEvent = this.handleInject(event);
            break;
          case 'COGNO:COMMAND_REPAIR':
            shouldHandleNextEvent = this.handleCommandRepair(event);
            break;
        }
      }
      if (this.commandPromptMarker && !this._command?.value?.status?.isRunning) {
        const start = this.commandPromptMarker.line + this.lineOffset;
        const end = this.terminal.currentCursorLineIndex();
        const cursorPosition = this.readCursorPosition();
        if (this.isGreaterThanMaxCursorPosition(cursorPosition)) {
          this.maxCursorPosition = {x: cursorPosition.x, y: cursorPosition.y};
        }
        const lines = this.readLines(start, end);
        let textToCursor: string;
        const text = lines.map(i => i.lineText).join('');
        if (lines.length === 0) {
          textToCursor = '';
        } else {
          const linesWithoutLast = lines.slice(0, lines.length - 1);
          const lastLine = lines[lines.length - 1];
          textToCursor = linesWithoutLast.map(i => i.lineText).join('') + lastLine.lineText.substring(0, cursorPosition.x - (lines[0].line === cursorPosition.line ? cursorPosition.offsetX : 0));
        }
        if(this.forceCommand && this.lastKeyEvent && this.lastKeyEvent.key !== Key.Enter) {
          this.forceCommand = undefined;
        }
        this._input.next({
          text: text,
          textToCursor:  textToCursor,
          cursor: cursorPosition,
          lines: lines,
          keyEvent: this.lastKeyEvent
        });
      }
    });
  };

  private handlePromptStart(event: PromptStartEvent): boolean {
    this._environment.next({
      machine: event.machine,
      user: event.user,
      workingDir: event.directory,
      timestamp: event.timestamp,
      isAutoInjection: event.isAutoInjection
    });
    this._queue.dequeue();
    this.maxCursorPosition = undefined;
    return true;
  }

  private handlePromptEnd(event: PromptEndEvent): boolean {
    // We have a new prompt but this is maybe not rendered at the moment.
    // Look in the current cursor line if we have a prompt
    // if we do not find a prompt, keep the event in the queue and wait for the next rendering.
    const cursorLine = this.terminal.currentCursorLine();
    const nextPrompt = this.promptFinder.tryToFindPrompt(cursorLine?.translateToString());
    if (nextPrompt) {
      this._queue.dequeue();
      if (this.commandPromptMarker) {
        this.commandPromptMarker.dispose();
        // TODO: What should we do?
        console.warn('We have already a marker.');
      }

      if (this.lineOffset === undefined) {
        const currentLine = this.terminal.currentCursorLineIndex();
        let promptHeight = currentLine + 1;
        for (let lineIndex = currentLine; lineIndex >= 0; lineIndex--) {
          const line = this.terminal.getLine(lineIndex);
          const lineText = line?.translateToString(true)?.trim();
          if (!lineText || lineText.length === 0) {
            promptHeight = currentLine - lineIndex;
            break;
          }
        }
        this.lineOffset = promptHeight - 1;
      }
      this.commandPromptMarker = this.terminal.registerMarker(-this.lineOffset);

      this.columnOffset = nextPrompt.length;
      this.commandId = Uuid.createSortable();
      this._input.next({
        text: '',
        textToCursor: '',
        cursor: this.readCursorPosition(),
        lines: [],
        keyEvent: undefined
      });
      return true;
    } else if (nextPrompt && this.commandPromptMarker) {
      console.log('############FUCK!!!!!!');
    }
    return false;
  }

  private readLines(startLine: number, endLine: number): { line: number; lineText: string }[] {
    const lines: { line: number; lineText: string }[] = [];
    for (let lineIndex = startLine; lineIndex <= endLine; lineIndex++) {
      const line = this.terminal.getLine(lineIndex);
      if (!line) {
        continue;
      }
      const startColumn = lineIndex === startLine ? this.columnOffset : 0;
      const endColumn = lineIndex === endLine ? this.maxCursorPosition?.x : undefined;
      let lineText = line.translateToString(true, startColumn, endColumn)?.trimStart();
      if (!lineText) {
        continue;
      }
      if (lineIndex === endLine) { // last line
        const spaceCount = this.terminal.currentCursorX() - lineText.length - startColumn;
        if (spaceCount > 0) {
          lineText += ' '.repeat(spaceCount);
        }
      }
      lines.push({line: lineIndex, lineText: lineText});
    }
    return lines;
  }

  private readCursorPosition(): CursorPosition {
    const height = this.terminal.cellHeight();
    const width = this.terminal.cellWidth();
    const rect = this.terminal.getBoundingClientRect();
    const x = this.terminal.currentCursorX();
    const y = this.terminal.currentCursorY();
    const windowX = rect.x;
    const windowY = rect.y;
    const appWindowX = windowX + x * width;
    const appWindowY = windowY + y * height;
    return {
      x: x,
      y: y,
      offsetX: this.columnOffset,
      windowY: windowY,
      windowX: windowX,
      appWindowX: appWindowX,
      appWindowY: appWindowY,
      height: height,
      width: width,
      line: this.terminal.currentCursorLineIndex(),
    };
  }

  private handleCommandStart(event: CommandStartEvent) {
    if (!this.commandPromptMarker) {
      this._queue.dequeue();
      return true;
    }
    if (this._command.value) {
      this._queue.dequeue();
      return true;
    }
    if (this.commandPromptMarker.line + this.lastLineEnterPressedOffset === this.terminal.currentCursorLineIndex()) {
      return false;
    }
    const start = this.commandPromptMarker.line + this.lineOffset;
    const end = this.commandPromptMarker.line + this.lastLineEnterPressedOffset;

    const lines = this.readLines(start, end);

    const newCommand: TerminalCommand = {
      id: this.commandId,
      startMarker: this.commandPromptMarker,
      outputOffset: this.lineOffset + (end - start) + 1,
      command: this.forceCommand ?? lines.map(i => i.lineText).join('').trim(),
      status: {
        startTime: Date.now(),
        isRunning: true
      }
    };
    this._command.next(newCommand);
    this._queue.dequeue();
    this.forceCommand = undefined;
    this.lastLineEnterPressedOffset = undefined;
    return true;
  }

  private handleCommandEnd(event: CommandEndEvent) {
    if (!this.commandPromptMarker || !this._input.value) {
      this._queue.dequeue();
      return false;
    }
    // We have a finished command.
    // Look in the current cursor line if we have a prompt.
    // if we find a prompt, search for the previous empty line or another prompt.
    const lineIndexToScan = this.terminal.currentCursorLineIndex();
    if (this.commandPromptMarker && this.commandPromptMarker.line - this.columnOffset === lineIndexToScan) {
      // We scan the same line where the current start marker is;
      return false;
    }
    const prompt = this.promptFinder.tryToFindPrompt(this.terminal.getLine(lineIndexToScan)?.translateToString());
    if (!prompt) {
      return false;
    }
    const newPromptLineStart = lineIndexToScan - this.lineOffset;
    const commandEndLine = newPromptLineStart - 1;
    // We have a command or just a ENTER was pressed without inserting something
    const command = this._command.value ? {...this._command.value} : {
      id: this.commandId,
      startMarker: this.commandPromptMarker,
      command: '',
      status: {
        startTime: Date.now(),
        isRunning: true
      }
    };
    command.status = {
      startTime: command.status.startTime,
      isRunning: false,
      endTime: Date.now(),
      isSuccessfulReturn: this.forceSuccessReturn || isSuccessfulReturn(event.returnCode),
      returnCode: event.returnCode
    };
    command.endMarker = this.terminal.registerMarker(commandEndLine - lineIndexToScan);
    this._command.next(command);
    this.commandPromptMarker = undefined;
    this.columnOffset = undefined;
    this.commandId = undefined;
    this.forceSuccessReturn = false;
    this._command.next(undefined);
    this._queue.dequeue();
    return true;
  }

  private handleInject(event: InjectFeaturesEvent) {
    this._queue.clear();
    this._injected.next(false);
    this.columnOffset = undefined;
    this._command.next(undefined);
    this.commandPromptMarker = undefined;
    this._command.next(undefined);
    this._environment.next(undefined);
    this._input.next(undefined);
    this.commandId = undefined;
    this._forceInjection.next({machine: event.machine.trim().length === 0 ? undefined : event.machine});
    return true;
  }

  private isGreaterThanMaxCursorPosition(cursorPosition: CursorPosition) {
    if (!this.maxCursorPosition) {
      return true;
    }
    if (this.maxCursorPosition.y < cursorPosition.y) {
      // we have a new line;
      return true;
    }
    if (this.maxCursorPosition.x < cursorPosition.x) {
      // we have a new column
      return true;
    }
    return false;
  }

  private handleKeyEvent(domEvent: KeyboardEvent) {
    this.lastKeyEvent = domEvent;
    if (domEvent.key === Key.Enter && !this._command.value?.status?.isRunning) {
      this.lastLineEnterPressedOffset = this.terminal.currentCursorLineIndex() - this.commandPromptMarker?.line;
      this._queue.enqueue({type: 'COGNO:COMMAND_START'});
    }
  }

  forceNextCommand(command: string) {
    this.forceCommand = command;
    this.lastKeyEvent = undefined;
  }

  private handleCommandRepair(event: InjectFeaturesEvent | CommandRepairEvent) {
    const cursorLine = this.terminal.currentCursorLine();
    const nextPrompt = this.promptFinder.tryToFindPrompt(cursorLine?.translateToString());
    if (nextPrompt) {
      this.commandPromptMarker = this.terminal.registerMarker(-this.lineOffset);
      this._queue.dequeue();
      return true;
    }
    return false;
  }

  forceNextReturnSuccessful() {
    this.forceSuccessReturn = true;
  }
}

export interface IStatusTerminalAdapter {
  registerMarker(offset: number): IMarker;

  registerOscHandler(ident: number, callback: (data: string) => boolean | Promise<boolean>): IDisposable;

  onWriteParsed(listener: (arg1: void, arg2: void) => any): IDisposable;

  onKey(listener: (arg: { key: string; domEvent: KeyboardEvent }) => void): IDisposable;

  currentCursorLineIndex(): number;

  currentCursorLine(): IBufferLine | undefined;

  getLine(index: number): IBufferLine | undefined;

  currentCursorX(): number;

  currentCursorY(): number;

  cellHeight(): number;

  cellWidth(): number;

  getBoundingClientRect(): { x: number; y: number };
}

export class StatusTerminalAdapter implements IStatusTerminalAdapter {

  constructor(private terminal: Terminal) {
  }

  getBoundingClientRect(): { x: number; y: number } {
    return (this.terminal as any)._core.element.getElementsByClassName('xterm-screen')[0].getBoundingClientRect();
  }

  cellHeight(): number {
    return (this.terminal as any)._core.viewport._renderDimensions.css.cell.height;

  }

  cellWidth(): number {
    return (this.terminal as any)._core.viewport._renderDimensions.css.cell.width;
  }

  onWriteParsed(listener: (arg1: void, arg2: void) => any): IDisposable {
    return this.terminal.onWriteParsed(listener);
  }

  onKey(listener: (arg: { key: string; domEvent: KeyboardEvent }) => void): IDisposable {
    return this.terminal.onKey(listener);
  }

  currentCursorY(): number {
    return this.terminal.buffer.active.cursorY;
  }

  getLine(index: number): IBufferLine {
    return this.terminal.buffer.active.getLine(index);
  }

  currentCursorLineIndex(): number {
    return this.terminal.buffer.active.baseY + this.terminal.buffer.active.cursorY;
  }

  currentCursorLine(): IBufferLine | undefined {
    return this.terminal.buffer.active.getLine(this.currentCursorLineIndex());
  }

  currentCursorX(): number {
    return this.terminal.buffer.active.cursorX;
  }

  registerOscHandler(ident: number, callback: (data: string) => boolean | Promise<boolean>): IDisposable {
    return this.terminal.parser.registerOscHandler(ident, callback);
  }

  registerMarker(offset: number): IMarker {
    return this.terminal.registerMarker(offset);
  }

}
