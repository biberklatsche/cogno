import {IStatusTerminalAdapter, TerminalStatusObserver} from './terminal-status.observer';
import {IBufferCell, IBufferLine, IMarker} from '@xterm/xterm';
import {clear, getNgZone} from '../../../../../test/factory';
import {filter, first, last, take} from 'rxjs/operators';
import {IDisposable} from 'node-pty';
import {TestHelper} from '../../../../../test/helper';
import {Key} from '../../../common/key';

describe('TerminalStatusObserver', () => {
  let observer: TerminalStatusObserver;
  let terminal: TestTerminal;

  beforeEach(() => {
    terminal = new TestTerminal();
    observer = new TerminalStatusObserver(getNgZone());
  });

  afterEach(() => {
    clear();
  });

  test('bind() should bind to all event listener', () => {
    expect(terminal.onWriteParsedCallback).toBeUndefined();
    expect(terminal.onKeyCallback).toBeUndefined();
    expect(terminal.oscCallback).toBeUndefined();
    observer.bind(terminal, {...TestHelper.createShellConfig()}, {...TestHelper.createTheme()});
    expect(terminal.oscCode).toBe(observer.OSC_CODE);
    expect(terminal.onWriteParsedCallback).not.toBeUndefined();
    expect(terminal.onKeyCallback).not.toBeUndefined();
    expect(terminal.oscCallback).not.toBeUndefined();
  });

  test('destroy() should dispose all registered disposables', () => {
    observer.bind(terminal, {...TestHelper.createShellConfig()}, {...TestHelper.createTheme()});
    observer.destroy();
    expect(terminal.onWriteParsedCallback).toBeUndefined();
    expect(terminal.onKeyCallback).toBeUndefined();
    expect(terminal.oscCallback).toBeUndefined();
  });

  test('first COGNO:PROMPT; should return fire injected', (done) => {
    observer.bind(terminal, {...TestHelper.createShellConfig(), injectionType: 'Auto', promptTerminator: '#', usesFinalSpacePromptTerminator: true}, {...TestHelper.createTheme()});
    observer.injected$.pipe(first()).subscribe(s => {
      expect(s).toBe(false);
    });
    terminal.write('unknown');
    observer.injected$.pipe(first()).subscribe(s => {
      expect(s).toBe(false);
    });
    terminal.write(createTestPrompt());
    observer.injected$.pipe(first()).subscribe(s => {
      expect(s).toBe(true);
      done();
    });
  });

  test('valid COGNO:PROMPT; with autoinjection should return correct environment', (done) => {
    observer.bind(terminal, {...TestHelper.createShellConfig(), injectionType: 'Auto', promptTerminator: '#', usesFinalSpacePromptTerminator: true}, {...TestHelper.createTheme()});
    observer.environment$.pipe(filter(env => !!env)).subscribe(s => {
      expect(s).toEqual({machine: 'machine', user: 'user', workingDir: '/home', timestamp: 1234, isAutoInjection: true});
      done();
    });
    terminal.write('# ', createTestPrompt(0, 'user', 'machine', '/home', 1234, true));
  });

  test('valid COGNO:PROMPT; without autoinjection should return correct environment', (done) => {
    observer.bind(terminal, {...TestHelper.createShellConfig(), injectionType: 'Auto', promptTerminator: '#', usesFinalSpacePromptTerminator: true}, {...TestHelper.createTheme()});
    observer.environment$.pipe(filter(env => !!env)).subscribe(s => {
      expect(s).toEqual({machine: 'machine', user: 'user', workingDir: '/home', timestamp: 1234, isAutoInjection: false});
      done();
    });
    terminal.write('# ', createTestPrompt(0, 'user', 'machine', '/home', 1234, false));
  });

  test('valid COGNO:PROMPT; should return correct environment', (done) => {
    observer.bind(terminal, {...TestHelper.createShellConfig(), injectionType: 'Auto', promptTerminator: '#', usesFinalSpacePromptTerminator: true}, {...TestHelper.createTheme()});
    observer.environment$.pipe(filter(env => !!env)).subscribe(s => {
      expect(s).toEqual({machine: 'machine', user: 'user', workingDir: '/home', timestamp: 1234, isAutoInjection: true});
      done();
    });
    terminal.write('# ', createTestPrompt(0, 'user', 'machine', '/home', 1234, true));
  });

  test('input should fire command ls', (done) => {
    observer.bind(terminal, {...TestHelper.createShellConfig(), injectionType: 'Manual', promptTerminator: '#', usesFinalSpacePromptTerminator: true}, {...TestHelper.createTheme()});
    observer.command$.pipe(take(2), last()).subscribe(s => {
      expect(s).toEqual(
        {
          command: 'ls',
          id: expect.any(String),
          outputOffset: 1,
          startMarker: {
            dispose: expect.any(Function),
            id: 1,
            isDisposed: false,
            line: 0
          },
          status: {
            isRunning: true,
            startTime: expect.any(Number),
          }
        }
      );
      done();
    });
    terminal.write('# ', createTestPrompt(0, 'user', 'machine', '/home', 1234));
    terminal.append('ls');
    terminal.enter();
    terminal.write('ls output');
  });

  test('valid COGNO:PROMPT; should fire empty input', (done) => {
    observer.bind(terminal, {...TestHelper.createShellConfig(), injectionType: 'Manual', promptTerminator: '#', usesFinalSpacePromptTerminator: true}, {...TestHelper.createTheme()});
    observer.input$.pipe(take(2), last()).subscribe(s => {
      expect(s).toEqual(
        {
          cursor: {
            appWindowX: 11,
            appWindowY: 1,
            height: 12,
            line: 0,
            offsetX: 2,
            width: 5,
            windowX: 1,
            windowY: 1,
            x: 2,
            y: 0
          },
          lines: [],
          text: '',
          textToCursor: ''
        }
      );
      done();
    });
    terminal.write('# ', createTestPrompt(0, 'user', 'machine', '/home', 1234));
  });


});

function createTestPrompt(returnCode: number = 0, user: string = 'user', machine: string = 'machine', directory: string = '/home', timestamp: number = 1234, withAutoinjection: boolean = true): string {
  return `COGNO:PROMPT;${returnCode}|${user}|${machine}|${directory}|${timestamp}${withAutoinjection ? '|auto' : ''}`;
}

class TestTerminal implements IStatusTerminalAdapter {
  cellHeight(): number {
    return 12;
  }
  cellWidth(): number {
     return 5;
  }
  getBoundingClientRect(): { x: number; y: number } {
     return {x: 1, y: 1};
  }
  public oscCode: number;
  public oscCallback: (data: string) => boolean | Promise<boolean>;
  public onWriteParsedCallback: (arg1: void, arg2: void) => any;
  public onKeyCallback: (arg: { key: string; domEvent: KeyboardEvent }) => void;
  public lines: string[] = [];
  public markerCounter = 0;

  registerMarker(offset: number): IMarker {
    return {
      onDispose: undefined,
      id: ++this.markerCounter,
      isDisposed: false,
      line: this.lines.length - 1 - offset,
      dispose(): void {}
    };
  }

  registerOscHandler(oscCode: number, callback: (data: string) => boolean | Promise<boolean>): IDisposable {
    this.oscCode = oscCode;
    this.oscCallback = callback;
    const that = this;
    return {
      dispose() {
        that.oscCallback = undefined;
      }
    };
  }

  onWriteParsed(listener: (arg1: any, arg2: any) => void): IDisposable {
    this.onWriteParsedCallback = listener;
    const that = this;
    return {
      dispose() {
        that.onWriteParsedCallback = undefined;
      }
    };
  }

  onKey(listener: (arg: { key: string; domEvent: KeyboardEvent }) => void): IDisposable {
    this.onKeyCallback = listener;
    const that = this;
    return {
      dispose() {
        that.onKeyCallback = undefined;
      }
    };
  }

  currentCursorLineIndex(): number {
    return this.lines.length - 1;
  }

  currentCursorLine(): IBufferLine {
    return this.getLine(this.currentCursorLineIndex());
  }

  getLine(index: number): IBufferLine {
    return this.getBufferLine(this.lines[index]);
  }

  private getBufferLine(line: string): IBufferLine {
    return {
      isWrapped: false, length: line.length, getCell(x: number, cell?: IBufferCell): IBufferCell | undefined {
        return undefined;
      },
      translateToString(trimRight?: boolean, startColumn?: number, endColumn?: number): string {
        startColumn = startColumn === undefined ? 0 : startColumn;
        endColumn = endColumn === undefined ? line.length : endColumn;
        const result = line.substring(startColumn, endColumn);
        return trimRight ? result.trimEnd() : result;
      },
    };
  }

  currentCursorX(): number {
    return this.currentCursorLine().length;
  }

  currentCursorY(): number {
    return this.currentCursorLineIndex();
  }

  write(...lines: string[]) {
    lines.forEach((line) => {
      if(line.startsWith('COGNO:PROMPT;')){
        this.oscCallback(line);
      } else {
        this.lines.push(line);
      }
    });
    this.onWriteParsedCallback();
  }

  append(input: string) {
    this.lines[this.lines.length - 1] += input;
    this.onWriteParsedCallback();
  }

  enter() {
    this.onKeyCallback({key: 'Enter', domEvent: TestHelper.createKeyboardEvent({key: Key.Enter})});
    this.onWriteParsedCallback();
  }
}
