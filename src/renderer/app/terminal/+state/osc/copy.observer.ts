import {Terminal} from '@xterm/xterm';
import {IDisposable} from 'node-pty';
import {Observable, Subject} from 'rxjs';

export class CopyObserver {

  public readonly OSC_CODE = 52;

  private terminal: Terminal
  private disposables: IDisposable[] = [];
  private _copy: Subject<string> = new Subject<string>();
  public get copy$(): Observable<string> {
    return this._copy.asObservable();
  }

  bind(terminal: Terminal) {
    this.terminal = terminal;
    this.disposables.push(this.terminal.parser.registerOscHandler(this.OSC_CODE, (data) => this.handleOsc(data)));
  }

  destroy(){
    for (const disposable of this.disposables) {
      disposable.dispose();
    }
  }

  private handleOsc(data: string): boolean {
    if (data.startsWith('c;')) {
      data = data.substring(2);
    }
    this._copy.next(new Buffer(data, 'base64').toString());
    return true;
  }
}
