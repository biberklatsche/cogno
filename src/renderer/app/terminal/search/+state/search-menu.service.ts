import {Injectable, OnDestroy} from '@angular/core';
import {GlobalMenuService} from '../../../+shared/abstract-components/menu/+state/global-menu.service';
import {TerminalService} from '../../+state/terminal.service';
import {createStore, Store} from '../../../common/store/store';
import {MenuService} from '../../../+shared/abstract-components/menu/menu.service';
import {Observable, Subscription} from 'rxjs';
import {map} from 'rxjs/operators';
import {GridService} from '../../../global/grid/+state/grid.service';
import {SearchOptions, SearchResult} from './models';

export interface SearchMenuState {
  options: SearchOptions;
  result: SearchResult;
};

const initialState: SearchMenuState =
  {
    options: {
      searchText: '',
      regex: false,
      caseSensitive: false,
      wholeWord: false
    },
    result: {
      hasMatch: false, count: 0, position: 0
    }
  };

@Injectable()
export class SearchMenuService extends MenuService implements OnDestroy {

  private store: Store<SearchMenuState>;
  private subscriptions: Subscription[] = [];

  constructor(
    menuService: GlobalMenuService,
    private terminalService: TerminalService,
    private gridService: GridService
  ) {
    super('TerminalSearch', menuService);
  }

  init(): void {
    this.store = createStore(this.terminalService.getTabId() + 'searchMenu', {...initialState});
    this.subscriptions.push(this.terminalService.selectSearchResult().subscribe(result => {
      if (!result || result.resultCount <= 0) {
        this.store.update({result: {hasMatch: false, count: 0, position: 0}});
      } else {
        this.store.update({result: {hasMatch: true, count: result.resultCount, position: result.resultIndex + 1}});
      }
    }));
  }

  ngOnDestroy(): void {
    this.store.destroy();
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  selectIsActive(): Observable<boolean> {
    return super.selectIsActive().pipe(map(globalActive => {
      return globalActive && this.gridService.getActiveTabId() === this.terminalService.getTabId();
    }));
  }

  selectSearchOptions(): Observable<SearchOptions> {
    return this.store.select(s => s.options);
  }

  selectSearchResult(): Observable<{ hasMatch: boolean, count: number, position: number }> {
    return this.store.select(s => s.result);
  }

  focusTerminal(): void {
    this.terminalService.focus();
  }

  search(searchText: string): void {
    this.store.update(s => ({options: {...s.options, searchText: searchText}}));
    this._search(true);
  }

  toggleRegex(): void {
    this.store.update(s => ({options: {...s.options, regex: !s.options.regex}}));
    this._search(true);
  }

  toggleWholeWord(): void {
    this.store.update(s => ({options: {...s.options, wholeWord: !s.options.wholeWord}}));
    this._search(true);
  }

  toggleCaseSensitive(): void {
    this.store.update(s => ({options: {...s.options, caseSensitive: !s.options.caseSensitive}}));
    this._search(true);
  }

  reset(): void {
    this.store.update({...initialState});
  }

  clear(): void {
    this.store.update(s => ({options: {...s.options, searchText: ''}}));
    this._search(false);
  }

  searchPrevious(): void {
    this._search(false);
  }

  searchNext(): void {
    this._search(false, true);
  }

  private _search(incremental: boolean, forward = false): void {
    this.terminalService.search(this.store.get(s => s.options), incremental, forward);
  }
}
