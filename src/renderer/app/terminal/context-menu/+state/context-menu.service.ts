import {Injectable, OnDestroy} from '@angular/core';
import {Position} from '../../../../../shared/models/models';
import {GlobalMenuService} from '../../../+shared/abstract-components/menu/+state/global-menu.service';
import {TerminalService} from '../../+state/terminal.service';
import {createStore, Store} from '../../../common/store/store';
import {MenuService} from '../../../+shared/abstract-components/menu/menu.service';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {GridService} from '../../../global/grid/+state/grid.service';
import {PasteService} from '../../../global/paste-history-menu/+state/paste.service';
import {SearchMenuService} from '../../search/+state/search-menu.service';
import {SettingsService} from '../../../+shared/services/settings/settings.service';
import {Path} from '../../../../../shared/path';
import {ElectronService} from '../../../+shared/services/electron/electron.service';

export interface ContextMenuState {
  clickPosition: Position;
}

@Injectable()
export class ContextMenuService extends MenuService implements OnDestroy{

  private store: Store<ContextMenuState>;
  constructor(
    private gridService: GridService,
    private menuService: GlobalMenuService,
    private terminalService: TerminalService,
    private pasteService: PasteService,
    private settingsService: SettingsService,
    private electronService: ElectronService,
    private searchService: SearchMenuService) {
    super('TerminalContext', menuService);
  }

  init(): void {
    this.store = createStore(this.terminalService.getTabId() + 'contextMenu', {clickPosition: null});
  }

  selectIsActive(): Observable<boolean> {
    return super.selectIsActive().pipe(map(globalActive => globalActive && this.gridService.getActiveTabId() === this.terminalService.getTabId()));
  }

  ngOnDestroy(): void {
    this.store.destroy();
  }

  openMenuOnPosition(clickPosition: Position) {
    super.closeMenu();
    this.store.update({
      clickPosition
    });
    super.openMenu();
  }

  getClickPosition() {
    return this.store.get(s => s.clickPosition);
  }

  copy() {
    this.terminalService.copy();
    this.closeMenu();
  }

  paste() {
    this.terminalService.paste();
    this.closeMenu();
  }

  showPasteHistory() {
    this.closeMenu();
    this.pasteService.openMenu();
  }

  find() {
    this.closeMenu();
    this.searchService.openMenu();
  }

  clearBuffer() {
    this.terminalService.clearBuffer();
    this.closeMenu();
  }

  scrollToPreviousCommand() {
    this.terminalService.scrollToPreviousCommand();
    this.closeMenu();
  }

  scrollToNextCommand() {
    this.terminalService.scrollToNextCommand();
    this.closeMenu();
  }

  scrollToPreviousBookmark() {
    this.terminalService.scrollToPreviousBookmark();
    this.closeMenu();
  }

  scrollToNextBookmark() {
    this.terminalService.scrollToNextBookmark();
    this.closeMenu();
  }

  openFileManagerHere() {
    const dir = this.terminalService.getCurrentDirectory();
    const path = Path.toOsPath(dir);
    this.electronService.openExternal(path);
    this.closeMenu();
  }
}
