import { FilterNamePipe } from './filter-name.pipe';
import {TestHelper} from '../../../../test/helper';

describe('FilterNamePipe', () => {
  it('should create an instance', () => {
    const pipe = new FilterNamePipe();
    expect(pipe).toBeTruthy();
  });

  it('should filter name - null', () => {
    const pipe = new FilterNamePipe();
    expect(pipe.transform(null, null)).toBe(null);
  });

  it('should filter name - null and search string', () => {
    const pipe = new FilterNamePipe();
    expect(pipe.transform(null, 'tes')).toBe(null);
  });

  it('should filter name - empty array and null', () => {
    const pipe = new FilterNamePipe();
    expect(pipe.transform([], null)).toEqual([]);
  });

  it('should filter theme by name', () => {
    const pipe = new FilterNamePipe();
    const themaA = {...TestHelper.createSettings().themes[0], name: 'atest'};
    const themaB = {...TestHelper.createSettings().themes[0], name: 'btest'};
    expect(pipe.transform([themaA, themaB], 'a')).toEqual([themaA]);
    expect(pipe.transform([themaA, themaB], 'b')).toEqual([themaB]);
  });

  it('should filter shell by name', () => {
    const pipe = new FilterNamePipe();
    const shellA = {...TestHelper.createSettings().shells[0], name: 'atest'};
    const shellB = {...TestHelper.createSettings().shells[0], name: 'btest'};
    expect(pipe.transform([shellA, shellB], 'a')).toEqual([shellA]);
    expect(pipe.transform([shellA, shellB], 'b')).toEqual([shellB]);
  });
});
