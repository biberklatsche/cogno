import {Injectable} from '@angular/core';
import {createStore, Store} from '../../../common/store/store';
import {ElectronService} from '../../../+shared/services/electron/electron.service';
import {PluginConfig} from '../../../+shared/models/plugin-config';
import {Observable} from 'rxjs';
import {PluginsLoader} from './plugins.loader';
import {PluginFileManager} from '../../../+shared/services/plugins/plugin.file.manager';
import * as semver from 'semver';
import {SettingsTabService} from '../../+state/settings-tab.service';
import {PluginSuggesterManager} from '../../../autocomplete/+state/suggester/plugins/plugin-suggester.manager';

export interface PluginsState {
  availablePlugins: PluginConfig[];
  installedPlugins: PluginConfig[];
  selectedPlugin: PluginConfig;
  isPluginsLoading: boolean;
}

export const initialState: PluginsState = {
  availablePlugins: [],
  installedPlugins: [],
  selectedPlugin: null,
  isPluginsLoading: false
};

@Injectable()
export class PluginsSettingsService {

  private store: Store<PluginsState>;

  constructor(
    private tabService: SettingsTabService,
    private electron: ElectronService,
    private loader: PluginsLoader,
    private fileManager: PluginFileManager,
    private pluginManager: PluginSuggesterManager) {
  }

  public init(id: string) {
    this.store = createStore(id,  initialState);
  }

  getState(): PluginsState {
    return {...this.store.get(s => s)};
  }

  loadPlugins() {
    const installedPlugins = this.fileManager.getInstalledPlugins();
    this.store.update({availablePlugins: [], installedPlugins: installedPlugins, selectedPlugin: installedPlugins.length > 0 ? installedPlugins[0] : null, isPluginsLoading: true});
    this.loadPluginsFromNpm(installedPlugins);
  }

  loadPluginsFromNpm(installedPlugins: PluginConfig[]) {
    this.loader.loadPlugins().subscribe({
      next: (plugins) => {
        const availablePlugins: PluginConfig[] = [];
        for (const plugin of plugins) {
          const installedPlugin = installedPlugins.find(p => p.name === plugin.name);
          if(!installedPlugin) {
            availablePlugins.push(plugin);
          } else {
            installedPlugin.availableVersion = plugin.availableVersion;
            installedPlugin.isUpdateAvailable = semver.gt(plugin.availableVersion, installedPlugin.installedVersion);
            installedPlugin.tarballUrl = plugin.tarballUrl;
          }
        }
        this.store.update({availablePlugins: availablePlugins, isPluginsLoading: false});
        if (!this.store.get(s => s.selectedPlugin) && availablePlugins.length > 0) {
          this.store.update({selectedPlugin: availablePlugins[0]});
        }
      },
      error: (err => this.store.update({availablePlugins: [], isPluginsLoading: false}))
    });
  }

  selectAvailablePlugins(): Observable<PluginConfig[]> {
    return this.store.select(s => s.availablePlugins);
  }

  selectIsPluginsLoading(): Observable<boolean> {
    return this.store.select(s => s.isPluginsLoading);
  }

  selectInstalledPlugins(): Observable<PluginConfig[]> {
    return this.store.select(s => s.installedPlugins);
  }

  selectSelectedPlugin(): Observable<PluginConfig> {
    return this.store.select(s => s.selectedPlugin);
  }

  selectPlugin(plugin: PluginConfig) {
    this.store.update({selectedPlugin: plugin});
  }

  openExternal(link: string) {
    this.electron.openExternal(link);
  }

  async installPlugin() {
    this.tabService.updateIsLoading(true);
    let selectedPlugin = this.store.get(s => s.selectedPlugin);
    const path = await this.loader.downloadPlugin(selectedPlugin.tarballUrl);
    await this.fileManager.installPlugin(selectedPlugin, path);
    const availablePlugins = [...this.store.get(s => s.availablePlugins)];
    const index = availablePlugins.findIndex(a => a.name === selectedPlugin.name);
    availablePlugins.splice(index, 1);
    setTimeout(() => {
      const installedPlugins = this.fileManager.getInstalledPlugins();
      selectedPlugin = installedPlugins.find(s => s.name === selectedPlugin.name);
      this.store.update({installedPlugins, availablePlugins, selectedPlugin: selectedPlugin});
      this.tabService.updateIsLoading(false);
      this.pluginManager.destroy();
      this.pluginManager.loadPlugins();
    }, 1000);
  }

  async uninstallPlugin() {
    this.tabService.updateIsLoading(true);
    const selectedPlugin = {...this.store.get(s => s.selectedPlugin)};
    await this.fileManager.uninstallPlugin(selectedPlugin);
    const installedPlugins = [...this.store.get(s => s.installedPlugins)];
    const index = installedPlugins.findIndex(a => a.name === selectedPlugin.name);
    installedPlugins.splice(index, 1);
    this.store.update({installedPlugins, selectedPlugin: null});
    this.loadPluginsFromNpm(installedPlugins);
    setTimeout(() => {
      this.tabService.updateIsLoading(false);
      this.pluginManager.destroy();
      this.pluginManager.loadPlugins();
    }, 1000);
  }

  async updatePlugin() {
    this.tabService.updateIsLoading(true);
    const selectedPlugin = {...this.store.get(s => s.selectedPlugin)};
    await this.fileManager.uninstallPlugin(selectedPlugin);
    const path = await this.loader.downloadPlugin(selectedPlugin.tarballUrl);
    const installPath = await this.fileManager.installPlugin(selectedPlugin, path);
    const installedPlugins = [...this.store.get(s => s.installedPlugins)];
    selectedPlugin.isInstalled = true;
    selectedPlugin.isUpdateAvailable = false;
    selectedPlugin.installedVersion = selectedPlugin.availableVersion;
    selectedPlugin.installDir = installPath;
    const index = installedPlugins.findIndex(a => a.name === selectedPlugin.name);
    installedPlugins.splice(index, 1, selectedPlugin);
    this.store.update({installedPlugins, selectedPlugin: selectedPlugin});
    setTimeout(() => {
      this.tabService.updateIsLoading(false);
      this.pluginManager.destroy();
      this.pluginManager.loadPlugins();
    }, 1000);
  }
}
