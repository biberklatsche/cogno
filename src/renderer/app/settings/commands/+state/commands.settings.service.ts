import {Injectable} from '@angular/core';
import {CommandRepository} from '../../../+shared/services/commands/command.repository';
import {DbService} from '../../../+shared/services/db/db.service';
import {CommandEntity} from '../../../+shared/services/commands/entities';
import {createStore, Store} from '../../../common/store/store';
import {combineLatest, Observable} from 'rxjs';
import {RegexpHelper} from '../../../common/regexp';
import {Tokenizer} from '../../../autocomplete/+state/suggester/tokenizer';
import {map} from 'rxjs/operators';
import {Hash} from '../../../+shared/models/hash/hash';
import {AutocompleteService} from '../../../autocomplete/+state/autocomplete.service';
import {PluginSuggesterManager} from '../../../autocomplete/+state/suggester/plugins/plugin-suggester.manager';
import {CommandSource} from '../../../+shared/models/command';

export type TableEntry = CommandEntity & {
  tool: string;
  parameters: Parameter[];
};

export type Parameter = {
  name: string;
  placeholder?: string;
  isVariable: boolean;
}

export interface CommandsState {
  commands: TableEntry[];
  selectedCommand?: TableEntry;
  commandToEdit?: TableEntry;
  commandToEditUnchangedHash?: string;
  showEditCommand: boolean;
  mode?: 'copy' | 'edit';
  search: string;
  plugins: {label: string, placeholder: string}[];
}

export const initialState: CommandsState = {
  commands: [],
  search: '',
  selectedCommand: undefined,
  showEditCommand: false,
  commandToEdit: undefined,
  commandToEditUnchangedHash: undefined,
  plugins: []
};


@Injectable()
export class CommandsSettingsService {

  private commandRepository: CommandRepository;
  private store: Store<CommandsState>;

  constructor(readonly db: DbService,
              private autocompleteService: AutocompleteService,
              private readonly pluginManager: PluginSuggesterManager) {
    this.commandRepository = new CommandRepository(db);
  }

  public init(id: string) {
    this.store = createStore(id, initialState);
    this.commandRepository.init().then();
    this.store.update({plugins: [{label: 'None', placeholder: '?'}, ...this.pluginManager.getPlugins()]});
  }

  destroy() {
  }

  setSelectedCommand(selectedCommand: TableEntry) {
    this.store.update({selectedCommand});
  }

  async loadCommands() {
    const tableEntries = await this.load();
    this.store.update({commands: tableEntries});
  }

  private async load(): Promise<TableEntry[]> {
    const search = this.store.get(s => s.search);
    const filter = {
      command: new RegExp(RegexpHelper.escapeStrongRegExp(search))
    };
    const commands = await this.commandRepository.find(filter);
    return commands.map(command => {
      const tokens = Tokenizer.tokenize(command.command);
      return {...command, tool: tokens[0], parameters: tokens.length > 1 ? tokens.slice(1, tokens.length).map(p => this.mapParameter(p)) : []};
    });
  }

  private mapParameter(param: string): Parameter {
    const isVariable = param.startsWith('{{') && param.endsWith('}}');
    const placeholder = isVariable ? param.substring(2, param.length - 2) : undefined;
    return {name: param, isVariable: isVariable, placeholder};
  }

  selectCommands(): Observable<TableEntry[]> {
    return this.store.select(s => s.commands);
  }

  async setSearch(search: string) {
    this.store.update({search, selectedCommand: undefined});
    await this.loadCommands();
  }

  selectSearch() {
    return this.store.select(s => s.search);
  }

  selectSelectedCommand() {
    return this.store.select(s => s.selectedCommand);
  }

  setCommandToEdit(command: TableEntry): void {
    const copy = JSON.parse(JSON.stringify(command)) as TableEntry;
    copy.commandSource = CommandSource.EDITOR;
    this.store.update({commandToEdit: copy, commandToEditUnchangedHash: command.commandHash});
  }

  selectCommandToEdit() {
    return this.store.select(s => s.commandToEdit);
  }

  selectIsValid(): Observable<boolean> {
    return combineLatest([this.store.select(s => s.commandToEdit), this.store.select(s => s.commandToEditUnchangedHash)])
      .pipe(map(([entry, hash]) => {
        if(!entry || !hash || !entry.commandHash) {return false;}
        return entry.commandHash.localeCompare(hash) !== 0;
      }));
  }

  async delete(entry: TableEntry) {
    await this.commandRepository.delete(entry._id);
    await this.autocompleteService.reload();
    await this.loadCommands();

  }

  updateCommandString(command: string) {
    let commandToEdit = this.store.get(s => s.commandToEdit);
    const tokens = Tokenizer.tokenize(command);
    const hash = Hash.create(command.trim());
    this.store.update({commandToEdit: {
        ...commandToEdit,
        command: command,
        tool: tokens[0],
        parameters: tokens.length > 1 ? tokens.slice(1, tokens.length).map(p => this.mapParameter(p)) : [],
        commandHash: hash
      }});
  }

  updatePlaceholder(parameter: Parameter, placeholder?: string) {
    parameter.isVariable = true;
    parameter.placeholder = placeholder;
    parameter.name = `{{${placeholder}}}`;
    const command = this.store.get(s => s.commandToEdit);
    const parameterString = command.parameters.map(p => p.name).join(' ');
    this.updateCommandString(`${command.tool} ${parameterString}`);
  }

  async saveCommandToEdit() {
    const commandToEdit = this.store.get(s => s.commandToEdit);
    const mode = this.store.get(s => s.mode);
    const entity = await this.commandRepository.get(commandToEdit._id);
    if(entity && commandToEdit.commandHash === commandToEdit._id) {
      // command exists -> return
      return;
    }
    commandToEdit.command = commandToEdit.command.trim();
    if(mode === 'edit') {
      await this.commandRepository.delete(entity._id);
    }
    commandToEdit._id = Hash.create(commandToEdit.command.trim());
    this.store.update({mode: undefined, commandToEdit: undefined, commandToEditUnchangedHash: undefined, selectedCommand: commandToEdit});
    const command: Pick<TableEntry, keyof CommandEntity> = commandToEdit;
    await this.save(command);
    await this.loadCommands();
  }

  private async save(command: CommandEntity) {
    const c: Pick<TableEntry, keyof CommandEntity> = command;
    await this.commandRepository.save({
      _id: command._id,
      command: command.command,
      commandHash: command.commandHash,
      executionCount: command.executionCount,
      selectionCount: command.selectionCount,
      shellTypes: command.shellTypes,
      commandSource: command.commandSource,
    });
    await this.autocompleteService.reload();
  }

  getSelectedCommand(): CommandEntity | undefined{
    return this.store.get(s => s.selectedCommand);
  };

  selectShowEditCommand(): Observable<boolean> {
    return this.store.select(s => s.showEditCommand);
  }

  setShowEditCommand(show: boolean, event?: 'copy' | 'edit'): void {
    this.store.update({showEditCommand: show, mode: event});
  }

  selectPlugins(): Observable<{label: string; placeholder: string}[]> {
    return this.store.select(s => s.plugins);
  }

  toggleVariableParameter(param: Parameter) {
    param.isVariable = !param.isVariable;
    param.placeholder = param.isVariable ? '?' : undefined;
    param.name = param.isVariable ? '{{?}}' : '?';
    const command = this.store.get(s => s.commandToEdit);
    const parameterString = command.parameters.map(p => p.name).join(' ');
    this.updateCommandString(`${command.tool} ${parameterString}`);
  }
}
