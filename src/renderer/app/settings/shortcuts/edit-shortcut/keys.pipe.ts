import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'keys',
  standalone: true
})
export class KeysPipe implements PipeTransform {

  transform(keys: string[], ...args: unknown[]): unknown {
    return keys.join(' + ');
  }

}
