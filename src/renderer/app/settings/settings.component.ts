import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {TabComponent} from '../+shared/abstract-components/tab/tab.component';
import { Nav } from './+state/model';
import {SettingsTabService, SettingsState} from './+state/settings-tab.service';
import {CommonModule} from '@angular/common';
import {LoaderComponent} from '../+shared/components/loader/loader.component';
import {GeneralComponent} from './general/general.component';
import {ThemesComponent} from './themes/themes.component';
import {ShortcutsComponent} from './shortcuts/shortcuts.component';
import {IconComponent} from '../+shared/components/icon/icon.component';
import {AutocompleteComponent} from './autocomplete/autocomplete.component';
import {PluginsComponent} from './plugins/plugins.component';
import {CommandsComponent} from './commands/commands.component';
@Component({
    selector: 'app-settings',
    templateUrl: './settings.component.html',
    styleUrls: ['./settings.component.scss'],
    providers: [SettingsTabService],
    imports: [
        CommonModule,
        LoaderComponent,
        GeneralComponent,
        ThemesComponent,
        ShortcutsComponent,
        AutocompleteComponent,
        CommandsComponent,
        PluginsComponent
    ]
})
export class SettingsComponent extends TabComponent<SettingsState> implements OnInit{

  public selectedNav: Observable<Nav>;
  public isLoading: Observable<boolean>;
  public Nav = Nav;

  constructor(private service: SettingsTabService) {
    super(service);
  }

  ngOnInit() {
    super.ngOnInit();
    this.selectedNav = this.service.selectSelectedNav();
    this.isLoading = this.service.selectIsLoading();
  }

  selectNav(nav: Nav) {
    this.service.updateNavigation(nav);
  }

  openSettingsExternal() {
    this.service.openSettingsExternal();
  }
}
