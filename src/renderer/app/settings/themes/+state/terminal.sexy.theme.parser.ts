import {ColorName, Theme} from '../../../../../shared/models/settings';

export namespace TerminalSexyThemeParser {
  export function parse(themeAsString: string, themeName: string, themeId: string, defaultTheme: Theme): Theme {
    const terminalSexyTheme = JSON.parse(themeAsString);
    return {
      fontsize: defaultTheme ? defaultTheme.fontsize : 14,
      appFontsize: defaultTheme ? defaultTheme.appFontsize : 14,
      cursorWidth: defaultTheme ? defaultTheme.cursorWidth : 3,
      fontFamily: defaultTheme ? defaultTheme.fontFamily : 'monospace',
      appFontFamily: defaultTheme ? defaultTheme.appFontFamily : 'Roboto',
      image: '',
      name: themeName,
      author: terminalSexyTheme.author,
      terminalSexyId: themeId,
      padding: '1',
      paddingAsArray: [1, 1, 1, 1],
      isInstalled: false,
      isDefault: false,
      isImageAvailable: defaultTheme ? defaultTheme.isImageAvailable : false,
      imageOpacity: 80,
      prompt: defaultTheme ? defaultTheme.prompt : '⯈',
      promptVersion: defaultTheme ? defaultTheme.promptVersion : 1,
      fontWeight: defaultTheme ? defaultTheme.fontWeight : 'normal',
      fontWeightBold: defaultTheme ? defaultTheme.fontWeightBold : 'bold',
      colors: {
        foreground: terminalSexyTheme.foreground,
        background: terminalSexyTheme.background,
        highlight: terminalSexyTheme.color[6],
        black: terminalSexyTheme.color[0],
        red: terminalSexyTheme.color[1],
        green: terminalSexyTheme.color[2],
        yellow: terminalSexyTheme.color[3],
        blue: terminalSexyTheme.color[4],
        magenta: terminalSexyTheme.color[5],
        cyan: terminalSexyTheme.color[6],
        white: terminalSexyTheme.color[7],
        brightBlack: terminalSexyTheme.color[8],
        brightRed: terminalSexyTheme.color[9],
        brightGreen: terminalSexyTheme.color[10],
        brightYellow: terminalSexyTheme.color[11],
        brightBlue: terminalSexyTheme.color[12],
        brightMagenta: terminalSexyTheme.color[13],
        brightCyan: terminalSexyTheme.color[14],
        brightWhite: terminalSexyTheme.color[15],
        cursor: terminalSexyTheme.color[6],
        promptColors: [{foreground: ColorName.cyan, background: ColorName.black}, {
          foreground: ColorName.cyan,
          background: ColorName.black
        }]
      }
    };
  }

  export function create(path: string): Theme {
    const splittedPath = path.split('/');
    return {
      fontsize: 14,
      appFontsize: 14,
      fontWeightBold: 'bold',
      fontWeight: 'normal',
      cursorWidth: 3,
      prompt: '$',
      promptVersion: 1,
      removeFullScreenAppPadding: true,
      fontFamily: 'monospace',
      appFontFamily: 'Roboto',
      isInstalled: false,
      padding: '1',
      paddingAsArray: [1, 1, 1, 1],
      image: '',
      name: splittedPath[splittedPath.length - 1],
      terminalSexyId: path,
      isDefault: false,
      isImageAvailable: false,
      colors: null
    };
  }

  export function createAll(paths: string[]): Theme[] {
    const themes = [];
    for (const path of paths) {
      themes.push(create(path));
    }
    return themes;
  }
}
