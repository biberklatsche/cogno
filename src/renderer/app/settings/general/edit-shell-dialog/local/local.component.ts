import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {ShellConfig} from '../../../../../../shared/models/settings';
import {GeneralSettingsService} from '../../+state/general.settings.service';
import {Key} from '../../../../common/key';
import {SettingsService} from '../../../../+shared/services/settings/settings.service';

@Component({
    selector: 'app-local',
    templateUrl: './local.component.html',
    imports: [
        CommonModule,
        FormsModule
    ]
})
export class LocalComponent implements OnInit {

  selectedShell: Observable<ShellConfig>;
  availableShells: Observable<ShellConfig[]>;
  isLastShell: Observable<boolean>;

  constructor(private generalSettingsService: GeneralSettingsService, private settingsService: SettingsService) {
    this.availableShells = generalSettingsService.selectAvailableShells();
    this.selectedShell = generalSettingsService.selectShell();
    this.isLastShell = generalSettingsService.selectIsLastShell();
  }

  ngOnInit(): void {
  }

  setShell(event: Event) {
    const selectElement = event.target as HTMLSelectElement;
    const selectedIndex = selectElement.selectedIndex;
    this.generalSettingsService.updateSelectedShellLocal(selectedIndex);
  }

  toggleUseAsDefaultShell() {
    this.generalSettingsService.toggleUseAsDefaultShellLocal();
  }

  changeFilePath($event: FocusEvent | KeyboardEvent) {
    if (($event as KeyboardEvent)?.key === Key.Enter || $event instanceof FocusEvent) {
      const value = ($event.target as HTMLInputElement).value;
      this.generalSettingsService.setDirectory(value);
    }
  }

  openFilePathSelection() {
    this.generalSettingsService.openFilePathSelectionLocal();
  }

  changeName($event: Event) {
    const value = ($event.target as HTMLInputElement).value;
    this.generalSettingsService.setName(value);
  }

  changeArguments($event: Event) {
    const value = ($event.target as HTMLInputElement).value;
    this.generalSettingsService.setArguments(value);
  }

  changePromptTerminator($event: Event) {
    const value = ($event.target as HTMLInputElement).value;
    this.generalSettingsService.setPromptTerminator(value);
  }

  openDocumentation(page: string, event: Event) {
    event.stopPropagation();
    this.generalSettingsService.openDocumentation(page);
  }

  toggleDebugMode() {
    this.generalSettingsService.toggleDebugMode();
  }

  toggleFinalSpace() {
    this.generalSettingsService.toggleFinalSpace();
  }

  setInjectionType(event: Event) {
    this.generalSettingsService.setInjectionType((event.target as any).value);
  }
}
