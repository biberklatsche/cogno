import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {ShellConfig} from '../../../../../../shared/models/settings';
import {ShellType} from '../../../../../../shared/models/models';
import {GeneralSettingsService} from '../../+state/general.settings.service';
import {IconComponent} from '../../../../+shared/components/icon/icon.component';

@Component({
    selector: 'app-remote',
    templateUrl: './remote.component.html',
    imports: [
        CommonModule,
        FormsModule
    ]
})
export class RemoteComponent implements OnInit {

  selectedShell: Observable<ShellConfig>;
  availableShells: Observable<ShellConfig[]>;
  isLastShell: Observable<boolean>;
  shellTypes = [ShellType.Bash];

  constructor(private generalSettingsService: GeneralSettingsService) {
    this.availableShells = generalSettingsService.selectAvailableShells();
    this.selectedShell = generalSettingsService.selectShell();
    this.isLastShell = generalSettingsService.selectIsLastShell();
  }

  ngOnInit(): void {
  }

  setHostShell(event: any) {
    const selectElement = event.target as HTMLSelectElement;
    const selectedIndex = selectElement.selectedIndex;
    this.generalSettingsService.updateHostShellRemote(selectedIndex);
  }

  setRemoteShell(event: any) {
    this.generalSettingsService.updateTargetShellRemote((event.target as any).value);
  }

  changeName($event: Event) {
    const value = ($event.target as HTMLInputElement).value;
    this.generalSettingsService.setName(value);
  }

  changeArguments($event: Event) {
    const value = ($event.target as HTMLInputElement).value;
    this.generalSettingsService.setArguments(value);
  }

  changeRemoteCommand($event: Event) {
    const value = ($event.target as HTMLInputElement).value;
    this.generalSettingsService.setConnectionCommandRemote(value);
  }

  openDocumentation(page: string) {
    this.generalSettingsService.openDocumentation(page);
  }

  toggleDebugMode() {
    this.generalSettingsService.toggleDebugMode();
  }

  setInjectionType(event: Event) {
    this.generalSettingsService.setInjectionType((event.target as any).value);
  }
}
