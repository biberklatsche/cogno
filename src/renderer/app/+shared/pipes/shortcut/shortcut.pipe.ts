import {Pipe, PipeTransform} from '@angular/core';
import * as os from 'os';

@Pipe({
  name: 'shortcut',
  standalone: true
})
export class ShortcutPipe implements PipeTransform {

  transform(shortcut: string): string {
    if (!shortcut) {
      return shortcut;
    }
    switch (this.getOs()) {
      case 'darwin':
        return shortcut
          .replace('CommandOrControl', '⌘/⌃')
          .replace('Command', '⌘')
          .replace('Control', '⌃')
          .replace('Alt', '⌥');
      default:
        return shortcut.replace('CommandOrControl', 'Ctrl').replace('Command', 'Ctrl')
          .replace('Control', 'Ctrl');
    }
  }

  getOs(): string {
    return os.platform();
  }

}
