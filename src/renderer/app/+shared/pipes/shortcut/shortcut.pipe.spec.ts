import { ShortcutPipe } from './shortcut.pipe';

describe('ShortcutPipe', () => {

  let pipe: ShortcutPipe;

  beforeEach(() => {
    pipe = new ShortcutPipe();
  });

  it('null', () => {
    expect(pipe.transform(null)).toBe(null);
  });

  it('undefined', () => {
    expect(pipe.transform(undefined)).toBe(undefined);
  });

  it('darwin', () => {
    jest.spyOn(pipe, 'getOs').mockReturnValue('darwin');
    expect(pipe.transform('CommandOrControl + A')).toBe('⌘/⌃ + A');
  });

  it('darwin', () => {
    jest.spyOn(pipe, 'getOs').mockReturnValue('darwin');
    expect(pipe.transform('Command + A')).toBe('⌘ + A');
  });

  it('darwin', () => {
    jest.spyOn(pipe, 'getOs').mockReturnValue('darwin');
    expect(pipe.transform('Control + A')).toBe('⌃ + A');
  });

  it('other', () => {
    jest.spyOn(pipe, 'getOs').mockReturnValue('win32');
    expect(pipe.transform('CommandOrControl + A')).toBe('Ctrl + A');
  });

  it('other', () => {
    jest.spyOn(pipe, 'getOs').mockReturnValue('win32');
    expect(pipe.transform('Control + A')).toBe('Ctrl + A');
  });

  it('other', () => {
    jest.spyOn(pipe, 'getOs').mockReturnValue('win32');
    expect(pipe.transform('Command + A')).toBe('Ctrl + A');
  });
});
