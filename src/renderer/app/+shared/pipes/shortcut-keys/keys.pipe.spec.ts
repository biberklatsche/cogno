import {KeysPipe} from '../../../settings/shortcuts/edit-shortcut/keys.pipe';
import {ShortcutKeysPipe} from './keys.pipe';
import * as os from 'node:os';

describe('KeysPipe', () => {
  let pipe: ShortcutKeysPipe;

  beforeEach(() => {
    pipe = new ShortcutKeysPipe();
  });

  it('null', () => {
    expect(pipe.transform(null)).toBe('');
  });

  it('undefined', () => {
    expect(pipe.transform(undefined)).toBe('');
  });

  it('empty', () => {
    expect(pipe.transform([])).toBe('');
  });

  it('alt', () => {
    if(os.platform() === 'darwin'){
      expect(pipe.transform(['Alt', 'A'])).toBe('⌥+A');
    } else {
      expect(pipe.transform(['Alt', 'A'])).toBe('Alt+A');
    }
  });

  it('commandOrControl', () => {
    if(os.platform() === 'darwin'){
      expect(pipe.transform(['CommandOrControl', 'A'])).toBe('⌘/⌃+A');
    } else {
      expect(pipe.transform(['CommandOrControl', 'A'])).toBe('Ctrl+A');
    }
  });

  it('other', () => {
    expect(pipe.transform(['A'])).toBe('A');
  });
});
