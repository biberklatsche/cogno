import {Component} from '@angular/core';
import {NotificationContext, NotificationService, NotificationType} from './notification.service';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {CommonModule} from '@angular/common';
import {IconComponent} from '../components/icon/icon.component';

@Component({
    selector: 'app-notification',
    templateUrl: './notification.component.html',
    styleUrls: ['./notification.component.scss'],
    imports: [
        CommonModule,
        IconComponent
    ]
})
export class NotificationComponent {

  public isVisible: Observable<boolean>;
  public notificationsTop: Observable<NotificationContext[]>;
  public notificationsBottom: Observable<NotificationContext[]>;
  public NotificationType = NotificationType;

  constructor(private readonly service: NotificationService) {
    this.isVisible = service.isVisible;
    this.notificationsTop = service.notifications.pipe(map(notifications => notifications.filter(n => n.place === 'top')));
    this.notificationsBottom = service.notifications.pipe(map(notifications => notifications.filter(n => n.place === 'bottom')));
  }

  close(message: NotificationContext) {
    message.response.next(false);
    message.response.complete();
  }

  confirm(message: NotificationContext) {
    message.response.next(true);
    message.response.complete();
  }

  cancel(message: NotificationContext) {
    message.response.next(false);
    message.response.complete();
  }
}
