import {TabType} from '../../../../shared/models/models';
import {ShellConfig} from '../../../../shared/models/settings';
import {Icon} from '../components/icon/icon';
import {Nav} from '../../settings/+state/model';

export type Tab = DefaultTab | TerminalTab | SettingsTab;

export type BaseTab = {
  id: string;
  tabType: TabType;
  icon: Icon;
  name: string;
  subName?: string;
  isClosing: boolean;
  isLoading: boolean;
  hasError: boolean;
  isSelected: boolean;
  isCommandRunning: boolean;
  isAppRunning: boolean;
}

export type DefaultTab = BaseTab & {
  tabType: TabType.About | TabType.ReleaseNotes;
}

export type TerminalTab = BaseTab & {
  tabType: TabType.Terminal;
  config: ShellConfig;
  directory?: string[];
  path?: string;
}

export type SettingsTab = BaseTab & {
  tabType: TabType.Settings;
  config: SettingsTabConfig;
}

export type SettingsTabConfig = {
  selectedNav: Nav;
  filter: string;
}
