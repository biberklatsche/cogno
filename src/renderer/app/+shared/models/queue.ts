export class Queue<T> {
  private items: T[] = [];

  // Add an element to the back of the queue
  enqueue(item: T): void {
    this.items.push(item);
  }

  // Remove and return the front element from the queue
  dequeue(): T | undefined {
    if (this.isEmpty()) {
      return undefined;
    }
    return this.items.shift();
  }

  // Return the front element without removing it
  peek(): T | undefined {
    if (this.isEmpty()) {
      return undefined;
    }
    return this.items[0];
  }

  // Check if the queue is empty
  isEmpty(): boolean {
    return this.items.length === 0;
  }

  // Return the size of the queue
  size(): number {
    return this.items.length;
  }

  // Clear all elements from the queue
  clear(): void {
    this.items = [];
  }
}
