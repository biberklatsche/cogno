import {RendererState, ShellInfo, ShellType} from '../../../../shared/models/models';
import {ShellLocation} from '../../common/shellLocation';

export interface TerminalInfo extends RendererState {
  location: ShellLocation;
  directory: string[];
  returnCode?: number;
}
