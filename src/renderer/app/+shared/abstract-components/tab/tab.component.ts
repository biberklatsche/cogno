import {Component, Input, OnInit} from '@angular/core';
import {TabService} from './+state/tab.service';
import {Observable} from 'rxjs';

@Component({
    template: '',
    standalone: false
})
export abstract class TabComponent<State> implements OnInit{

  @Input()
  public tabId: string;
  public isStoreReady: Observable<boolean>;

  protected constructor(protected tabService: TabService<State>) {
    this.isStoreReady = tabService.selectIsStoreReady();
  }

  ngOnInit(): void {
    this.tabService.init(this.tabId);
  }
}
