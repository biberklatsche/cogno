import {ShellType, TabType} from '../../../../../../shared/models/models';
import {ShellConfig} from '../../../../../../shared/models/settings';
import {Injectable} from '@angular/core';
import {Uuid} from '../../../../common/uuid';
import {BaseTab, DefaultTab, SettingsTab, SettingsTabConfig, Tab, TerminalTab} from '../../../models/tab';
import {Path} from '../../../../../../shared/path';
import {SettingsService} from '../../../services/settings/settings.service';
import {Icon} from '../../../components/icon/icon';
import {GridService} from '../../../../global/grid/+state/grid.service';

@Injectable({
  providedIn: 'root'
})
export class TabFactory {

  constructor(private settingsService: SettingsService) {
  }

  public createTab(type: TabType, tabConfig?: ShellConfig | SettingsTabConfig, activeTab?: Tab): Tab {
    switch (type) {
      case TabType.Terminal:
        return this.createTerminalTab({config: tabConfig as ShellConfig}, activeTab);
      case TabType.Settings:
        return this.createSettingsTab({config: tabConfig as SettingsTabConfig});
      default:
        return this.createDefaultTab({tabType: type});
    }
  }

  public restoreTab(tab: Tab): Tab {
    switch (tab.tabType) {
      case TabType.Terminal: {
        const shellConfig = {...this.settingsService.getShells().find(s => s.type === tab.config.type)};
        shellConfig.workingDir = tab.directory ? Path.toOsPath(tab.directory) : shellConfig.workingDir;
        return this.createTerminalTab({...tab, config: shellConfig});
      }
      case TabType.Settings: {
        return this.createSettingsTab({...tab});
      }
      default: {
        return this.createDefaultTab({...tab})
      }
    }
  }

  private createTerminalTab(tab: Partial<TerminalTab>, activeTab?: Tab): TerminalTab {
    tab.tabType = TabType.Terminal;
    const config = tab.config ? {...tab.config} : {...this.settingsService.getDefaultShellConfig()};
    if(activeTab && activeTab.tabType === TabType.Terminal && this.settingsService.getIsOpenTabInSameDirectoryEnabled() ) {
      config.workingDir = Path.toOsPath((activeTab as TerminalTab).directory);
    }
    tab.config = config;
    return {
      ...this.createBaseTab(tab, config),
      tabType: tab.tabType,
      config: config,
      isCommandRunning: false,
      path: tab.path || '',
      directory: tab.directory || []
    };
  }

  private createDefaultTab(tab: Partial<DefaultTab>): DefaultTab {
    return {
      ...this.createBaseTab(tab),
      tabType: tab.tabType
    };
  }

  private createSettingsTab(tab: Partial<SettingsTab>): SettingsTab {
    tab.tabType = TabType.Settings;
    return {
      ...this.createBaseTab(tab),
      tabType: TabType.Settings,
      config: tab.config
    };
  }

  private createBaseTab(tab: Partial<Tab>, config?: ShellConfig): BaseTab {
    return {
      id: `${tab.tabType}_${Uuid.create()}`,
      tabType: tab.tabType,
      icon: this.createTabIcon(tab, config),
      name: this.getTabName(tab.tabType),
      isClosing: false,
      isSelected: tab.isSelected === undefined ? true : tab.isSelected,
      isLoading: true,
      hasError: false,
      isCommandRunning: false,
      isAppRunning: false,
    };
  }

  createTabIcon(tab: Partial<Tab>, config: ShellConfig): Icon {
    switch (tab.tabType) {
      case TabType.Terminal: {
        if (config.injectionType === 'Remote') {
          return 'mdiLanConnect';
        }
        switch (config.type) {
          case ShellType.Powershell: return 'mdiPowershell';
          case ShellType.GitBash: return 'mdiGit';
          case ShellType.ZSH: return 'mdiPercentBoxOutline';
          case ShellType.Bash: return 'mdiConsole';
        }
        break;
      }
      case TabType.About:
        return 'mdiInformation';
      case TabType.Settings:
        return 'mdiCog';
      case TabType.ReleaseNotes:
        return 'mdiRocketLaunch';
    }
    return 'mdiAbTesting';
  }


  private getTabName(tapType: TabType): string {
    switch (tapType) {
      case TabType.About:
        return 'About';
      case TabType.ReleaseNotes:
        return 'Release Notes';
      case TabType.Settings:
        return 'Settings';
      case TabType.Terminal:
        return 'Terminal';
      default:
        return 'Tab';
    }
  }
}
