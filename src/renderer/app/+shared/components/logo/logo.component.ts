import {Component, Input, OnInit} from '@angular/core';
import {CommonModule} from '@angular/common';

@Component({
    selector: 'app-logo',
    templateUrl: './logo.component.html',
    styleUrls: ['./logo.component.scss'],
    imports: [
        CommonModule
    ]
})
export class LogoComponent implements OnInit {

  @Input()
  public backgroundColor: string;

  @Input()
  public color: string;
  public pathColorStyle: any;
  public circleColorStyle: any;

  constructor() { }



  ngOnInit(): void {
    this.circleColorStyle = {'stroke': this.color, 'fill': this.backgroundColor};
    this.pathColorStyle = {'fill': this.color};
  }
}
