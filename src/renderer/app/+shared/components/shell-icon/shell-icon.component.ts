import {Component, Input, ViewEncapsulation} from '@angular/core';
import {CommonModule} from '@angular/common';
import {IconComponent} from '../icon/icon.component';
import {ShellConfig} from '../../../../../shared/models/settings';
import {ShellType} from '../../../../../shared/models/models';

@Component({
    selector: 'app-shell-icon',
    styles: [`
    app-shell-icon {
      display: flex;
      align-items: baseline;
    }

    app-shell-icon app-icon {
      fill: var(--foreground-color-20d);
      height: 100%;
      width: 100%;
    }
  `],
    template: `
    @if (shell.injectionType !== 'Remote'){
      <ng-container [ngSwitch]="shell.type">
          <app-icon *ngSwitchCase="ShellType.ZSH" name="mdiPercentBoxOutline"></app-icon>
          <app-icon *ngSwitchCase="ShellType.Bash" name="mdiConsole"></app-icon>
          <app-icon *ngSwitchCase="ShellType.GitBash" name="mdiGit"></app-icon>
          <app-icon *ngSwitchCase="ShellType.Powershell" name="mdiPowershell"></app-icon>
      </ng-container>
    } @else {
      <app-icon name="mdiLanConnect"></app-icon>
    }

  `,
    encapsulation: ViewEncapsulation.None,
    imports: [
        CommonModule,
        IconComponent
    ]
})
export class ShellIconComponent {
  @Input()
  public shell: ShellConfig;
  protected readonly ShellType = ShellType;
}
