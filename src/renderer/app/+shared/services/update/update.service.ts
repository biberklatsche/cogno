import {Injectable} from '@angular/core';
import {ElectronService} from '../electron/electron.service';
import {IpcChannel} from '../../../../../shared/ipc.chanels';
import {Logger} from '../../../logger/logger';
import * as JSON5 from 'json5';

@Injectable({
  providedIn: 'root'
})
export class UpdateService{

  constructor(private electron: ElectronService) {

  }
  installUpdate() {
    Logger.debug('Install Update and show release notes on next startup.');
    window.localStorage.setItem('showReleaseNotes', 'true');
    this.electron.send(IpcChannel.InstallUpdate);
  }

  getShowReleaseNotes(): boolean {
    return window.localStorage.getItem('showReleaseNotes') ? JSON5.parse(window.localStorage.getItem('showReleaseNotes')) : false;
  }

  disableShowReleaseNotes(): void {
    window.localStorage.setItem('showReleaseNotes', 'false');
  }
}
