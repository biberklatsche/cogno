import { ColorService } from './color.service';
import {TestHelper} from '../../../../../test/helper';
import {SettingsService} from '../settings/settings.service';
import {getColorService, getSettingsService} from '../../../../../test/factory';

describe('ColorService', () => {
  let service: ColorService;
  let settingsService: SettingsService;
  const theme = TestHelper.createTheme();

  beforeEach(() => {
    settingsService = getSettingsService();
    jest.spyOn(settingsService, 'getActiveTheme').mockReturnValue(theme);
    service = getColorService();
  });

  it('should create colors', () => {
    expect(service.getColorByName('')).toEqual(theme.colors.white+ 'FF');
    expect(service.getColorByName('A', 'AA')).toEqual(theme.colors.blue + 'AA');
  });
});
