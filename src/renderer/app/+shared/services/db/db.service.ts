import {Injectable} from '@angular/core';
import {StringUtils} from '../../../../../shared/string-utils';
import {ElectronService} from '../electron/electron.service';
import {
  DBUpsert,
  DBFind,
  DBFindOne,
  DBInit,
  Id,
  IDbContext,
  DBRemove, DBAdd, DBAddAll, DBUpdate, DBReload, DBExistsFile, DBDirectoryPath
} from '../../../../../shared/db-context';
import {IpcChannel} from '../../../../../shared/ipc.chanels';


@Injectable({ providedIn: 'root' })
export class DbService implements IDbContext {

  constructor(private _electron: ElectronService) {
  }

  getDbDirectoryPath(): Promise<string> {
    const request: DBDirectoryPath = {requestType: 'rootDir'};
    return this._electron.invokePromise<string>(IpcChannel.DB, request);
  }

  existsDbFile(db: string): Promise<boolean> {
    const request: DBExistsFile = {requestType: 'existsFile', dbName: db};
    return this._electron.invokePromise<boolean>(IpcChannel.DB, request);
  }

  add<T extends Id>(element: T, db: string): Promise<T> {
    const request: DBAdd<T> = {requestType: 'add', dbName: db, element: element};
    return this._electron.invokePromise<T>(IpcChannel.DB, request);
  }

  addAll<T extends Id>(elements: T[], db: string): Promise<T[]> {
    const request: DBAddAll<T> = {requestType: 'addAll', dbName: db, elements: elements};
    return this._electron.invokePromise<T[]>(IpcChannel.DB, request);
  }

  update<T extends Id>(element: T, db: string): Promise<void> {
    const request: DBUpdate<T> = {requestType: 'update', dbName: db, element: element};
    return this._electron.invokePromise<void>(IpcChannel.DB, request);
  }

  public async init(db: string): Promise<boolean> {
    const request: DBInit = {requestType: 'init', dbName: db};
    return this._electron.invokePromise<boolean>(IpcChannel.DB, request);
  }

  public async loadData<T>(dbName: string): Promise<T[]> {
    await this.init(dbName);
    return this.find<T>({}, dbName);
  }

  public findOne<T>(query: any, db: string): Promise<T> {
    const request: DBFindOne = {requestType: 'findOne', dbName: db, query: query};
    return this._electron.invokePromise<T>(IpcChannel.DB, request);
  }

  public find<T>(query: any, db: string): Promise<T[]> {
    const request: DBFind = {requestType: 'find', dbName: db, query: query};
    return this._electron.invokePromise<T[]>(IpcChannel.DB, request);
  }

  public async remove(id: string, db: string): Promise<void> {
    const request: DBRemove = {requestType: 'remove', dbName: db, id: id};
    return this._electron.invokePromise<void>(IpcChannel.DB, request);
  }

  public async exists(id: string, db: string): Promise<boolean> {
    const elem = await this.findOne({_id: id}, db);
    return !!elem;
  }

  public async upsert<T extends Id>(element: T, db: string): Promise<void> {
    const request: DBUpsert<T> = {requestType: 'upsert', dbName: db, element: element};
    return this._electron.invokePromise<void>(IpcChannel.DB, request);
  }

  public async getAll<T extends Id>(dbName: string): Promise<T[]> {
    return this.find<T>({}, dbName);
  }

  public async reload(): Promise<void> {
    const request: DBReload = {requestType: 'reload'};
    return this._electron.invokePromise<void>(IpcChannel.DB, request);
  }
}
