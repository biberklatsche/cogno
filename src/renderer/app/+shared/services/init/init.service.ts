import {Injectable, NgZone} from '@angular/core';
import {ElectronService} from '../electron/electron.service';
import {IpcChannel} from '../../../../../shared/ipc.chanels';
import {ParseResult} from '../../../../../shared/parser/settings.parser';
import {Logger} from '../../../logger/logger';
import {SettingsService} from '../settings/settings.service';
import {TelemetryService} from '../analytics/telemetry.service';
import {ElectronUpdateInfo, WindowState} from '../../../../../shared/models/models';
import {WindowButtonsService} from '../../../global/window-buttons/+state/window-buttons.service';
import {PasteService} from '../../../global/paste-history-menu/+state/paste.service';

@Injectable({
  providedIn: 'root'
})
export class InitService{

  constructor(
    private electronService: ElectronService,
    private settingsService: SettingsService,
    private telemetryService: TelemetryService,
    private pasteService: PasteService,
    private windowButtonService: WindowButtonsService,
    private zone: NgZone
  ) {
  }
  public init(): void {
    this.electronService?.on(IpcChannel.SettingsLoaded, (sender, settingsResult: ParseResult) => {
     this.settingsLoaded(settingsResult);
    });
    this.electronService?.on(IpcChannel.DarkMode, (sender, args: any[]) => {
      this.zone?.run(() => {
        this.settingsService.setDarkMode(args[0]);
      });
    });
    this.electronService?.on(IpcChannel.UpdateInfo, (sender, args: any[]) => {
      Logger.debug('New update info', args);
      const updateInfo = args[0] as ElectronUpdateInfo;
      this.zone?.run(() => {
        this.windowButtonService.updateIsUpdateAvailable(updateInfo.isUpdateAvailable && !updateInfo.isRunning);
      });
    });
    this.electronService?.on(IpcChannel.WindowStateChanged, (sender, state) => {
      this.zone?.run(() => {
        switch (state) {
          case WindowState.Maximized: {
            this.windowButtonService.updateIsFullScreen(false);
            this.windowButtonService.updateIsMaximised(true);
            break;
          }
          case WindowState.FullScreen: {
            this.windowButtonService.updateIsFullScreen(true);
            this.windowButtonService.updateIsMaximised(true);
            break;
          }
          case WindowState.Unmaximized: {
            this.windowButtonService.updateIsMaximised(false);
            this.windowButtonService.updateIsFullScreen(false);
            break;
          }
        }
      });
    });
  }

  public settingsLoaded(settingsResult: ParseResult): void {
    Logger.debug('Settings loaded', settingsResult);
    if (settingsResult) {
      this.zone.run(() => {
        if (settingsResult.isValid) {
          this.initApp(settingsResult);
        } else if (!settingsResult.isValid) {
          this.error(settingsResult);
        }
      });
    }
  }

  private error(settingsResult: ParseResult) {
    this.settingsService.update({errors: settingsResult.errors, showOnboarding: settingsResult.showOnboarding});
  }

  private initApp(settingsResult: ParseResult) {
    this.settingsService.update({
      settings: settingsResult.settings,
      errors: undefined,
      showOnboarding: settingsResult.showOnboarding,
    });
    this.pasteService.loadHistory();
    if (settingsResult.settings?.general && settingsResult.settings?.shells?.length > 0) {
      if (settingsResult.settings.general.enableTelemetry) {
        Logger.debug('Telemetry enabled');
        this.telemetryService.enable();
      } else {
        Logger.debug('Telemetry disabled');
        this.telemetryService.disable();
      }
    }
  }

  private activateMemoryLogging() {
    const gc = () => {
      global.gc();
      console.log('force gc');
    };
    const getMemory = () => {
      Object.entries(process.memoryUsage()).map(x => console.log(x[0], x[1] / (1000.0 * 1000), 'MB'));
    };
    if (global.gc) {
      setInterval(getMemory, 1000);
      setInterval(gc, 10000);
    }
  }
}
