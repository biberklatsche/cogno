import {Injectable} from '@angular/core';
import {ElectronService} from '../electron/electron.service';
import {Logger} from '../../../logger/logger';
import {PluginConfig} from '../../models/plugin-config';
import * as zlib from 'zlib';
import * as tar from 'tar';

@Injectable({
  providedIn: 'root'
})
export class PluginFileManager {

  private readonly nameOfWorkerFile = '.worker.js';
  private readonly nameOfConfigFile = 'package.json';

  private workerContent = `
const plugin = require('./main');
onmessage = function(e) {
  switch(e.data.methode) {
    case 'suggest':
      Promise.resolve(plugin.suggest(e.data.data)).then(data => postMessage(data)).catch(e => postMessage({error: e}));
      break;
  }
};
module.export = plugin;
            `;
  constructor(private electron: ElectronService) {
  }

  public getInstalledPlugins(): PluginConfig[] {
    Logger.info('load plugins');
    const pluginData: PluginConfig[] = [];
    const fs = this.electron.getFs();
    const path = this.electron.getPath();
    const pluginDirs = this.getPluginDirs();
    for (const pluginDir of pluginDirs) {
      try {
        const pathOfWorker = path.join(pluginDir, this.nameOfWorkerFile);
        const pathOfConfig = path.join(pluginDir, this.nameOfConfigFile);
        if (!fs?.existsSync(pathOfWorker)) {
          fs.writeFileSync(pathOfWorker, this.workerContent);
        }
        const config = JSON.parse(fs.readFileSync(pathOfConfig).toString());
        Logger.info('found plugin', pluginDir);
        pluginData.push({
          availableVersion: null,
          repository: null,
          tarballUrl: null,
          isInstalled: true,
          isUpdateAvailable: false,
          name: config.name,
          type: 'Autocomplete',
          description: config.description,
          email: config.email,
          icon: config.cogno?.icon || '',
          regexp: config.cogno?.regexp,
          placeholder: config.cogno?.placeholder,
          label: config.cogno?.label || '',
          username: config.author,
          homepage: config.homepage,
          installedVersion: config.version,
          installDir: pluginDir,
          workerPath: pathOfWorker
        });
      } catch (e) {
        Logger.warn(`Could not load plugin "${pluginDir}"`, e);
      }
    }
    return pluginData;
  }

  private getPluginDirs(): string[] {
    const pluginDirs: string[] = [];
    try {
      const path = this.electron.getPath();
      const fs = this.electron.getFs();

      const pluginsPath = this.electron.getPluginPath();
      if(!fs?.existsSync(pluginsPath)) {
        fs.mkdirSync(pluginsPath);
      }
      const dirs = this.electron.getFs().readdirSync(pluginsPath);
      for (const dir of dirs) {
        const pluginDir = path.join(pluginsPath, dir);
        if (fs?.existsSync(pluginDir) && fs.statSync(pluginDir).isDirectory()) {
          pluginDirs.push(pluginDir);
        }
      }
    } catch (e) {
      Logger.error(e);
    }
    return pluginDirs;
  }

  public async installPlugin(plugin: PluginConfig, pluginTarPath: string) {
    const fs = this.electron.getFs();
    const path = this.electron.getPath();
    const sanitizedName = plugin.name.replace(/[/\\?%*:|"<>]/g, '_');
    const pluginPath = path.join(this.electron.getPluginPath(), sanitizedName);
    try{fs?.mkdirSync(pluginPath);} catch {}
    Logger.info('unpack',  pluginTarPath);
    const tarReadStream = fs.createReadStream(pluginTarPath);
    const gunzipStream = zlib.createGunzip();
    tarReadStream.pipe(gunzipStream);
    gunzipStream.pipe(
      tar.extract({
        cwd: pluginPath, // Set the destination directory
        strip: 1,
        filter: (path, stat) =>
          // Extract only the specified folder
           path.startsWith('package')
        ,
      })
    );
    tarReadStream.on('error', (err) => console.error('Error reading tar file:', err));
    gunzipStream.on('error', (err) => console.error('Error decompressing:', err));
    return pluginPath;
  }

  async uninstallPlugin(selectedPlugin: PluginConfig) {
    this.electron.getFs().rmSync(selectedPlugin.installDir, { recursive: true, force: true });
  }
}
