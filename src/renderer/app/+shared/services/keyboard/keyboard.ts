/**
 * Copyright (c) 2014 The xterm.js authors. All rights reserved.
 * Copyright (c) 2012-2013, Christopher Jeffrey (MIT License)
 * @license MIT
 */

import { C0 } from './escape-sequences';

export function evaluateKeyboardEvent(ev: KeyboardEvent): string {
  const modifiers = (ev.shiftKey ? 1 : 0) | (ev.altKey ? 2 : 0) | (ev.ctrlKey ? 4 : 0) | (ev.metaKey ? 8 : 0);
  let result: string;
  switch (ev.keyCode) {
    case 0:
      if (ev.key === 'UIKeyInputUpArrow') {
        result = C0.ESC + '[A';
      }
      else if (ev.key === 'UIKeyInputLeftArrow') {
        result = C0.ESC + '[D';
      }
      else if (ev.key === 'UIKeyInputRightArrow') {
        result = C0.ESC + '[C';
      }
      else if (ev.key === 'UIKeyInputDownArrow') {
        result = C0.ESC + '[B';
      }
      break;
    case 8:
      // backspace
      if (ev.altKey) {
        result = C0.ESC + C0.DEL; // \e ^?
        break;
      }
      result = C0.DEL; // ^?
      break;
    case 9:
      // tab
      if (ev.shiftKey) {
        result = C0.ESC + '[Z';
        break;
      }
      result = C0.HT;
      break;
    case 13:
      // return/enter
      result = ev.altKey ? C0.ESC + C0.CR : C0.CR;
      break;
    case 27:
      // escape
      result = C0.ESC;
      if (ev.altKey) {
        result = C0.ESC + C0.ESC;
      }
      break;
    case 37:
      // left-arrow
      if (ev.metaKey) {
        break;
      }
      result = C0.ESC + '[D';
      break;
    case 39:
      // right-arrow
      if (ev.metaKey) {
        break;
      }
      if (modifiers) {
        result = C0.ESC + '[1;' + (modifiers + 1) + 'C';
      } else {
        result = C0.ESC + '[C';
      }
      break;
    case 38:
      // up-arrow
      if (ev.metaKey) {
        break;
      }
      if (modifiers) {
        result = C0.ESC + '[1;' + (modifiers + 1) + 'A';
        // HACK: Make Alt + up-arrow behave like Ctrl + up-arrow
        // http://unix.stackexchange.com/a/108106
        // macOS uses different escape sequences than linux
      } else {
        result = C0.ESC + '[A';
      }
      break;
    case 40:
      // down-arrow
      if (ev.metaKey) {
        break;
      }
      if (modifiers) {
        result = C0.ESC + '[1;' + (modifiers + 1) + 'B';
      } else {
        result = C0.ESC + '[B';
      }
      break;
    case 45:
      // insert
      if (!ev.shiftKey && !ev.ctrlKey) {
        // <Ctrl> or <Shift> + <Insert> are used to
        // copy-paste on some systems.
        result = C0.ESC + '[2~';
      }
      break;
    case 46:
      // delete
      if (modifiers) {
        result = C0.ESC + '[3;' + (modifiers + 1) + '~';
      } else {
        result = C0.ESC + '[3~';
      }
      break;
    case 36:
      // home
      if (modifiers) {
        result = C0.ESC + '[1;' + (modifiers + 1) + 'H';
      } else {
        result = C0.ESC + '[H';
      }
      break;
    case 35:
      // end
      if (modifiers) {
        result = C0.ESC + '[1;' + (modifiers + 1) + 'F';
      } else {
        result = C0.ESC + '[F';
      }
      break;
    case 33:
      // page up
      if (ev.ctrlKey) {
        result = C0.ESC + '[5;' + (modifiers + 1) + '~';
      } else {
        result = C0.ESC + '[5~';
      }
      break;
    case 34:
      // page down
      if (ev.ctrlKey) {
        result = C0.ESC + '[6;' + (modifiers + 1) + '~';
      } else {
        result = C0.ESC + '[6~';
      }
      break;
    case 112:
      // F1-F12
      if (modifiers) {
        result = C0.ESC + '[1;' + (modifiers + 1) + 'P';
      } else {
        result = C0.ESC + 'OP';
      }
      break;
    case 113:
      if (modifiers) {
        result = C0.ESC + '[1;' + (modifiers + 1) + 'Q';
      } else {
        result = C0.ESC + 'OQ';
      }
      break;
    case 114:
      if (modifiers) {
        result = C0.ESC + '[1;' + (modifiers + 1) + 'R';
      } else {
        result = C0.ESC + 'OR';
      }
      break;
    case 115:
      if (modifiers) {
        result = C0.ESC + '[1;' + (modifiers + 1) + 'S';
      } else {
        result = C0.ESC + 'OS';
      }
      break;
    case 116:
      if (modifiers) {
        result = C0.ESC + '[15;' + (modifiers + 1) + '~';
      } else {
        result = C0.ESC + '[15~';
      }
      break;
    case 117:
      if (modifiers) {
        result = C0.ESC + '[17;' + (modifiers + 1) + '~';
      } else {
        result = C0.ESC + '[17~';
      }
      break;
    case 118:
      if (modifiers) {
        result = C0.ESC + '[18;' + (modifiers + 1) + '~';
      } else {
        result = C0.ESC + '[18~';
      }
      break;
    case 119:
      if (modifiers) {
        result = C0.ESC + '[19;' + (modifiers + 1) + '~';
      } else {
        result = C0.ESC + '[19~';
      }
      break;
    case 120:
      if (modifiers) {
        result = C0.ESC + '[20;' + (modifiers + 1) + '~';
      } else {
        result = C0.ESC + '[20~';
      }
      break;
    case 121:
      if (modifiers) {
        result = C0.ESC + '[21;' + (modifiers + 1) + '~';
      } else {
        result = C0.ESC + '[21~';
      }
      break;
    case 122:
      if (modifiers) {
        result = C0.ESC + '[23;' + (modifiers + 1) + '~';
      } else {
        result = C0.ESC + '[23~';
      }
      break;
    case 123:
      if (modifiers) {
        result = C0.ESC + '[24;' + (modifiers + 1) + '~';
      } else {
        result = C0.ESC + '[24~';
      }
      break;
    default:
      // a-z and space
      if (ev.ctrlKey && !ev.shiftKey && !ev.altKey && !ev.metaKey) {
        if (ev.keyCode >= 65 && ev.keyCode <= 90) {
          result = String.fromCharCode(ev.keyCode);
        } else if (ev.keyCode === 32) {
          result = C0.NUL;
        } else if (ev.keyCode >= 51 && ev.keyCode <= 55) {
          // escape, file sep, group sep, record sep, unit sep
          result = String.fromCharCode(ev.keyCode - 51 + 27);
        } else if (ev.keyCode === 56) {
          result = C0.DEL;
        } else if (ev.keyCode === 219) {
          result = C0.ESC;
        } else if (ev.keyCode === 220) {
          result = C0.FS;
        } else if (ev.keyCode === 221) {
          result = C0.GS;
        }
      } else if (ev.key && !ev.ctrlKey && !ev.altKey && !ev.metaKey && ev.keyCode >= 48 && ev.key.length === 1) {
        // Include only keys that that result in a _single_ character; don't include num lock, volume up, etc.
        result = ev.key;
      } else if (ev.key && ev.ctrlKey) {
        if (ev.key === '_') { // ^_
          result = C0.US;
        }
        if (ev.key === '@') { // ^ + shift + 2 = ^ + @
          result = C0.NUL;
        }
      }
      break;
  }
  return result || '';
}
