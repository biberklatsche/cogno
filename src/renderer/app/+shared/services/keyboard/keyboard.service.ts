import {Injectable} from '@angular/core';
import {fromEvent, Observable, Subject, Subscription} from 'rxjs';
import {Shortcut, ShortcutKey, Shortcuts, toShortcutString} from '../../../../../shared/models/shortcuts';
import {filter} from 'rxjs/operators';
import {SettingsService} from '../settings/settings.service';
import {evaluateKeyboardEvent} from './keyboard';
import {Key} from '../../../common/key';
import {NotificationService} from '../../notification/notification.service';


@Injectable({
  providedIn: 'root'
})
export class KeyboardService {
  private globalShortcutSubscription: Subscription;
  private _onShortcut: Subject<Shortcut> = new Subject<Shortcut>();
  private _onNoneMetaKey: Subject<{escapeSequence: string; key: string; isPrintableChar: boolean}> = new Subject<{escapeSequence: string; key: string; isPrintableChar: boolean}>();

  constructor(private settingsService: SettingsService, private notification: NotificationService) {
  }

  activateGlobalShortcuts() {
    if (!this.globalShortcutSubscription) {
      this.globalShortcutSubscription = fromEvent<KeyboardEvent>(document, 'keydown').subscribe(e => this.handleShortcut(e));
    }
  }

  deactivateGlobalShortcuts() {
    if (this.globalShortcutSubscription) {
      this.globalShortcutSubscription.unsubscribe();
      this.globalShortcutSubscription = undefined;
    }
  }

  handleShortcut(e: KeyboardEvent, shouldHandleShortcut: boolean = true): boolean {
    if(!shouldHandleShortcut) {
      return false;
    }
    if(!this.isShortcut(e)) {
      return false;
    }
    if(this.isKeyDownEvent(e)){
      const shortcuts = this.toShortcut(e);
      if(shortcuts.length === 1 || shortcuts.some(s =>  s.key === 'abortTask')) {
        this._onShortcut.next(shortcuts[0]);
      } else {
        this.notification.create()
            .title('Duplicate Shortcut Used!')
            .body(shortcuts[0].shortcut)
            .footer('The following commands share the same shortcut:', ...shortcuts.map(s => `- ${s.label}`))
            .dialog('Open Settings', 'Ignore')
            .duration(7000)
            .showAsWarning()
            .subscribe(openSettings => {
              if(openSettings) {
                const settings = this.settingsService.getShortcutsAsMap().get(this.settingsService.getShortcuts().openSettings)[0];
                this._onShortcut.next(settings);
              }
            });
        }
      }
    return true;
  }

  handleKeyPressed(e: KeyboardEvent): boolean {
    if(!this.isMarkerKey(e)) {
      if(this.isKeyDownEvent(e)) {
        this._onNoneMetaKey.next({escapeSequence: evaluateKeyboardEvent(e), key: e.key, isPrintableChar: this.isPrintableChar(e)});
      }
      return true;
    }
    return false;
  }

  private isPrintableChar(event: KeyboardEvent): boolean {
    // todo: expand
    return event.key !== Key.Escape &&
      event.key !== Key.ArrowUp &&
      event.key !== Key.ArrowDown &&
      event.key !== Key.ArrowLeft &&
      event.key !== Key.ArrowRight &&
      event.key !== Key.Home &&
      event.key !== Key.End &&
      event.key !== Key.Enter &&
      event.key !== Key.Meta;
  }

  private isKeyDownEvent(e: KeyboardEvent): boolean {
    return e.type === 'keydown';
  }

  onAlias(): Observable<Shortcut> {
    return this._onShortcut.pipe(
      filter((shortcut) => !!shortcut?.command)
    );
  }

  onShortcut(...keys: ShortcutKey[]): Observable<Shortcut> {
    return this._onShortcut.pipe(
      filter((shortcut) => keys.length === 0 || !!keys.find(key => shortcut?.key === key))
    );
  }

  onNoneMetaKey(): Observable<{escapeSequence: string; key: string; isPrintableChar: boolean}> {
    return this._onNoneMetaKey.asObservable();
  }

  public isShortcut(e: KeyboardEvent): boolean {
    return this.toShortcut(e).length > 0;
  }

  selectShortcuts(): Observable<Shortcuts> {
    return this.settingsService.selectShortcuts();
  }

  executeShortcut(s: Shortcut) {
    this._onShortcut.next(s);
  }

  private isMarkerKey(e: KeyboardEvent) {
    return e.key === Key.Alt ||
      e.key === Key.Control ||
      e.key === Key.Meta ||
      e.key === Key.Shift;
  }

  private toShortcut(e: KeyboardEvent): Shortcut[] {
    const shortcuts: Map<string, Shortcut[]> = this.settingsService.getShortcutsAsMap();
    let shortcut = toShortcutString(e);
    if(shortcuts.has(shortcut)) {
      return shortcuts.get(shortcut);
    }
    shortcut = toShortcutString(e, true);
    if(shortcuts.has(shortcut)) {
      return shortcuts.get(shortcut);
    }
    return [];
  }
}
