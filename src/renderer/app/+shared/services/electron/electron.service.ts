import {ipcRenderer, IpcRenderer, IpcRendererEvent, clipboard, net, dialog} from 'electron';
import * as fs from 'fs';
import * as os from 'os';
import {IpcChannel} from '../../../../../shared/ipc.chanels';
import * as path from 'path';
import {Injectable} from '@angular/core';
import {from, Observable} from 'rxjs';
import {Logger} from '../../../logger/logger';
import {map} from 'rxjs/operators';
import {ShellConfig} from '../../../../../shared/models/settings';

@Injectable({
  providedIn: 'root'
})
export class ElectronService {

  private APP_CONFIG_DIR_PATH = '';
  private APP_DIR_PATH = '';
  private APP_TEMP_DIR_PATH = '';
  private VERSION = '';

  private ipcRenderer: typeof ipcRenderer;
  private clipboard: typeof clipboard;
  private dialog: typeof dialog;
  private fs: typeof fs;
  private path: typeof path;
  private os: typeof os;

  constructor() {
    if(process.versions.hasOwnProperty('electron')) {
      this.ipcRenderer = window.require('electron').ipcRenderer;
      this.clipboard = window.require('electron').clipboard;
      this.dialog = window.require('electron').dialog;
      this.os = window.require('os');
      this.fs = window.require('fs');
      this.path = window.require('path');

      this.ipcRenderer.on(IpcChannel.AppDirs, (sender, dirs: {
        configDir: string;
        appDir: string;
        tempDir: string;
      }) => {
        this.APP_CONFIG_DIR_PATH = dirs.configDir;
        this.APP_DIR_PATH = dirs.appDir;
        this.APP_TEMP_DIR_PATH = dirs.tempDir;
      });
      this.ipcRenderer.on(IpcChannel.Version, (sender, version: string) => {
        this.VERSION = version;
      });
    }
  }

  getPath(): path.PlatformPath {
    return path;
  }

  getOSRelease(): string {
    return this.os.release();
  }

  getConfigDirectoryPath(): string {
    return this.APP_CONFIG_DIR_PATH;
  }

  getTempDirectoryPath(): string {
    return this.APP_TEMP_DIR_PATH;
  }

  getThirdPartyLicensePath(): string {
    return path?.join(this.APP_DIR_PATH, 'build', 'THIRD-PARTY-LICENSES.txt');
  }

  getSettingsPath(): string {
    return path?.join(this.getConfigDirectoryPath(), 'settings.json');
  }

  getLogPath(): string {
    return path?.join(this.getConfigDirectoryPath(), 'cogno.log');
  }

  getPluginPath(): string {
    return this.path?.join(this.getConfigDirectoryPath(), 'plugins');
  }

  openPath(link: string) {
    Logger.debug(`Open path '${link}'`);
    this.send(IpcChannel.OpenPath, link);
  }

  openExternal(link: string) {
    Logger.debug(`Open external '${link}'`);
    this.send(IpcChannel.OpenExternal, link);
  }

  getVersion(): string {
    return this.VERSION;
  }

  isNightlyVersion(): boolean {
    return this.VERSION.includes('-nightly');
  }

  openDevtools() {
    this.send(IpcChannel.OpenDevTools);
  }

  existsFileSync(file: string): boolean {
    return this.fs.existsSync(file);
  }

  clipboardWrite(text: string) {
    Logger.debug(`Write to clipboard '${text}'`);
    this.clipboard.writeText(text);
  }

  clipboardRead(): string {
    Logger.debug('Read from clipboard');
    return this.clipboard.readText();
  }

  clipboardClear() {
    Logger.debug('Clear clipboard');
    this.clipboard.clear();
  }

  on(channel: string, listener: (event: IpcRendererEvent, ...args: any[]) => void): IpcRenderer {
    Logger.debug(`Add listener to channel '${channel}'`);
    return this.ipcRenderer?.on(channel, listener);
  }

  removeListener(channel: string, listener: (...args: any[]) => void) {
    Logger.debug(`Remove listener from channel '${channel}'`);
    this.ipcRenderer?.removeListener(channel, listener);
  }

  send(channel: string, ...args: any[]): void {
    return this.ipcRenderer?.send(channel, ...args);
  }

  invoke<T>(channel: string, ...args: any[]): Observable<T> {
    return from(this.invokePromise<T>(channel, ...args));
  }

  invokePromise<T>(channel: string, ...args: any[]): Promise<T> {
    return this.ipcRenderer?.invoke(channel, ...args);
  }

  httpGet(hostname: string, port: number, urlPath: string): Observable<Buffer> {
    return this.invoke<Buffer>(IpcChannel.Http, {
      method: 'GET',
      protocol: 'https:',
      hostname: hostname,
      port: port,
      path: urlPath
    }).pipe(map(data => data));
  }

  downloadFile(tarballUrl: string, pathToTar: string) {
    return this.invoke(IpcChannel.DownloadFile, { url: tarballUrl, destination: pathToTar});
  }

  openFileDialog(type: 'image' | 'folder'): Observable<string | null> {
    return this.invoke<Electron.OpenDialogReturnValue>(IpcChannel.OpenFileDialog, type).pipe(map(v => {
      if (v.canceled || v.filePaths.length === 0) {
        return null;
      } else {
        return v.filePaths[0];
      }
    }));
  }

  openGit() {
    this.openExternal('https://gitlab.com/biberklatsche/cogno');
  }

  openReportIssue() {
    this.openExternal('https://gitlab.com/biberklatsche/cogno/-/issues/new');
  }

  openDonation() {
    this.openExternal('https://ko-fi.com/cognorocks');
  }

  openReddit() {
    this.openExternal('https://www.reddit.com/r/cogno/');
  }

  getFs() {
    return this.fs;
  }

  loadScript(shell: ShellConfig): Observable<string> {
    return this.invoke<string>(IpcChannel.LoadScriptContent, shell.type);
  }
}
