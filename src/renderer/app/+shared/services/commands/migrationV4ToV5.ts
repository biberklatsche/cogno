import {ShellLocation} from '../../../common/shellLocation';
import {Helper} from './helper';
import {CommandDirectoryEntity, CommandEntity, DirectoryEntity} from './entities';
import {CommandDirectoryRepository, DirectoryRepository} from './command.repository';
import {ShellType} from '../../../../../shared/models/models';
import * as fs from 'fs';
import {Hash} from '../../models/hash/hash';
import {CommandSource} from '../../models/command';
import {IDbContext} from '../../../../../shared/db-context';

export namespace MigrationV4ToV5 {

  export async function migrate(location: ShellLocation, db: IDbContext) {
    const commandsDirectoriesDbName = Helper.createCommandsDirectoriesDbName(location, 5);
    if (await db.existsDbFile(commandsDirectoriesDbName)) {
      return Promise.resolve(true);
    }
    const dataV4 = await loadDataFromDbV4(db);
    await migrateFromV4ToCommandsDb(db, location, dataV4);
    await migrateFromV4ToDirectoriesDb(db, location, dataV4);
    await migrateFromV4ToCommandsDirectoriesDb(db, location, dataV4);
  }

  async function migrateFromV4ToCommandsDirectoriesDb(db: IDbContext, location: ShellLocation, data: V4Type[]): Promise<any> {
    const commandsDirectoriesDbName = Helper.createCommandsDirectoriesDbName(location, 5);
    await db.init(commandsDirectoriesDbName);
    const commandDirectories: Map<string, CommandDirectoryEntity> = new Map<string, CommandDirectoryEntity>();
    for (const d of data) {
      if (!d.directories || d.directories.length === 0 || !d.command || d.isDeleted) {
        continue;
      }
      const id = CommandDirectoryRepository.createCommandDirectoryId(d.command, d.directories);
      if (commandDirectories.has(id)) {
        continue;
      }
      const newData: CommandDirectoryEntity = {
        _id: id,
        lastExecutionDate: d.executionDate,
        lastSelectionDate: undefined,
        executionCount: d.executionCount,
        selectionCount: 0,
        commandId: Hash.create(d.command),
        directoryId: DirectoryRepository.createDirectoryId(d.directories)
      };
      commandDirectories.set(id, newData);
    }
    return db.addAll(Array.from(commandDirectories.values()), commandsDirectoriesDbName);
  }

  async function migrateFromV4ToDirectoriesDb(db: IDbContext, location: ShellLocation, data: V4Type[]): Promise<any> {
    const directoriesDbName = Helper.createDirectoriesDbName(location, 5);
    await db.init(directoriesDbName);
    const directories: Map<string, DirectoryEntity> = new Map<string, DirectoryEntity>();
    for (const d of data) {
      if (!d.directories || d.directories.length === 0 || d.isDeleted) {
        continue;
      }
      const id = DirectoryRepository.createDirectoryId(d.directories);
      if (directories.has(id)) {
        continue;
      }
      const newData: DirectoryEntity = {
        _id: id,
        lastExecutionDate: d.globalExecutionDate,
        lastSelectionDate: undefined,
        executionCount: d.globalExecutionCount,
        selectionCount: 0,
        directories: d.directories
      };
      directories.set(id, newData);
    }
    return db.addAll(Array.from(directories.values()), directoriesDbName);
  }

  async function migrateFromV4ToCommandsDb(db: IDbContext, location: ShellLocation, data: V4Type[]): Promise<any> {
    const commandsDbName = Helper.createCommandsLocationDbName(location, 5);
    await db.init(commandsDbName);
    const commands: Map<string, CommandEntity> = new Map<string, CommandEntity>();
    for (const d of data) {
      if (!d.command || d.isDeleted) {
        continue;
      }
      const id = Hash.create(d.command);
      if (commands.has(id)) {
        continue;
      }
      const newData: CommandEntity = {
        _id: id,
        commandHash: id,
        lastExecutionDate: d.globalExecutionDate,
        executionCount: d.globalExecutionCount,
        selectionCount: 0,
        command: d.command,
        shellTypes: d.shellTypes,
        commandSource: CommandSource.TERMINAL
      };
      commands.set(id, newData);
    }
    return db.addAll(Array.from(commands.values()), commandsDbName);

  }

  async function loadDataFromDbV4(db: IDbContext): Promise<{
    executionDate: Date;
    executionCount: number;
    globalExecutionDate: Date;
    globalExecutionCount: number;
    directories: string[];
    command: string;
    shellTypes: ShellType[];
    isDeleted: boolean;
    _id: string;
  }[]> {
    const data: Promise<V4Type[]>[] = [];
    const path = await db.getDbDirectoryPath();
    const files = fs.readdirSync(path).filter(f => f.endsWith('_v4.json'));
    for (const file of files) {
      data.push(db.loadData<V4Type>(file.replace('.json', '')));
    }
    return data.length > 0 ? Promise.all(data) : Promise.resolve([]);
  }

  type V4Type = {
    executionDate: Date;
    executionCount: number;
    globalExecutionDate: Date;
    globalExecutionCount: number;
    directories: string[];
    command: string;
    shellTypes: ShellType[];
    isDeleted: boolean;
    _id: string;
  };
}
