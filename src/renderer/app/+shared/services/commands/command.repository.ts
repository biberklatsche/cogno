import {Helper} from './helper';
import {Migration} from './migration';
import {CommandDirectoryEntity, CommandEntity, DirectoryEntity} from './entities';
import {ShellLocation} from '../../../common/shellLocation';
import {Hash} from '../../models/hash/hash';
import {Command, Directory} from '../../models/command';
import {IDbContext} from '../../../../../shared/db-context';
import {DbService} from '../db/db.service';


export const dbVersion = 6;
export class CommandRepository {
  public commandsDbName: string;
  constructor(private readonly db: DbService) {

  }
  async init(): Promise<boolean> {
    this.commandsDbName = Helper.createCommandsDbName(dbVersion);
    return this.db.init(this.commandsDbName);
  }

  async getAll(): Promise<CommandEntity[]> {
    return this.find({});
  }

  async find(filter: any): Promise<CommandEntity[]> {
    return this.db.find<CommandEntity>(filter, this.commandsDbName);
  }

  async get(id: string): Promise<CommandEntity> {
    return await this.db.findOne<CommandEntity>({_id: id}, this.commandsDbName);
  }

  async save(commandEntity: CommandEntity): Promise<unknown> {
    if(!commandEntity._id) {commandEntity._id = Hash.create(commandEntity.command.trim())}
    return await this.db.upsert(commandEntity, this.commandsDbName);
  }

  async delete(id: string) {
    await this.db.remove(id, this.commandsDbName);
  }
}

export class DirectoryRepository {
  public directoryDbName: string;

  constructor(private readonly db: IDbContext) {}
  async init(location: ShellLocation): Promise<boolean> {
    if (!location?.isValid) {
      throw new Error(`${location.toString()} is not valid.`);
    }
    this.directoryDbName = Helper.createDirectoriesDbName(location, dbVersion);
    return this.db.init(this.directoryDbName);
  }
  async getAll(): Promise<DirectoryEntity[]> {
    return this.db.find<DirectoryEntity>({}, this.directoryDbName);
  }
  async saveDirectory(directory: Directory): Promise<void> {
    if(!directory) return;
    const d: DirectoryEntity = {
      directories: directory.directories,
      executionCount: directory.executionCount || 0,
      lastExecutionDate: directory.lastExecutionDate,
      selectionCount: directory.selectionCount || 0,
      lastSelectionDate: directory.lastSelectionDate,
      _id: DirectoryRepository.createDirectoryId(directory.directories)
    };
    return await this.save(d);
  }

  async save(directoryEntity: DirectoryEntity): Promise<void> {
    return await this.db.upsert(directoryEntity, this.directoryDbName);
  }

  static createDirectoryId(directories: string[]): string {
    return directories.join('_');
  }
}

export class CommandDirectoryRepository {

  public commandRepository: CommandRepository;
  public directoryRepository: DirectoryRepository;
  public commandDirectoryDbName: string;


  constructor(private readonly db: DbService) {}

  async init(location: ShellLocation): Promise<{commands: CommandEntity[]; directories: DirectoryEntity[]; commandDirectories: CommandDirectoryEntity[]}> {
    if (!location.isValid) {
      throw new Error(`${location.toString()} is not valid.`);
    }
    this.commandDirectoryDbName = Helper.createCommandsDirectoriesDbName(location, dbVersion);
    await Migration.migrate(this.db, location);
    this.commandRepository = new CommandRepository(this.db);
    await this.commandRepository.init();
    this.directoryRepository = new DirectoryRepository(this.db);
    await this.directoryRepository.init(location);
    await this.db.init(this.commandDirectoryDbName);
    return this.getAllData();
  }

  async getAllData(): Promise<{commands: CommandEntity[]; directories: DirectoryEntity[]; commandDirectories: CommandDirectoryEntity[]}> {
    const commands = await this.commandRepository.getAll();
    const commandDirectories = await this.db.find<CommandDirectoryEntity>({}, this.commandDirectoryDbName);
    const directories = await this.directoryRepository.getAll();
    const commandMap = commands.reduce((result, command) => ({ ...result, [command._id]: command}), {});
    const directoryMap = directories.reduce((result, directory) => ({ ...result, [directory._id]: directory}), {});
    const cleanedCommandDirectories = commandDirectories.filter(cd => !!commandMap[cd.commandId] && !!directoryMap[cd.directoryId]);
    return {commands, directories, commandDirectories: cleanedCommandDirectories};
  }

  async saveCommandDirectory(command: Command, directory: Directory): Promise<unknown> {
    if (!command || !directory) {
      return Promise.resolve();
    }
    const commandDirectory: CommandDirectoryEntity = {
      _id: CommandDirectoryRepository.createCommandDirectoryId(command.command, directory.directories),
      commandId: Hash.create(command.command),
      directoryId: DirectoryRepository.createDirectoryId(directory.directories),
      lastExecutionDate: directory.lastExecutionDate,
      executionCount: directory.executionCount || 0,
      lastSelectionDate: directory.lastSelectionDate,
      selectionCount: directory.selectionCount || 0
    };
    return this.db.upsert(commandDirectory, this.commandDirectoryDbName);
  }

  async getCommand(id: string): Promise<CommandEntity> {
    return this.commandRepository.get(id);
  }

  async saveCommand(command: Command): Promise<unknown> {
    if (!command) {
      return Promise.resolve();
    }
    const commandEntity: CommandEntity = {
      command: command.command,
      executionCount: command.executionCount || 0,
      selectionCount: command.selectionCount || 0,
      commandHash: command.commandHash,
      lastExecutionDate: command.lastExecutionDate,
      lastSelectionDate: command.lastSelectionDate,
      shellTypes: command.shellTypes,
      commandSource: command.commandSource,
      _id: command.commandHash
    };
    return this.commandRepository.save(commandEntity);
  }

  async saveDirectory(directory: Directory): Promise<void> {
    return await this.directoryRepository.saveDirectory(directory);
  }

  static createCommandDirectoryId(command: string, directories: string[]): string {
    return `${Hash.create(command)}_${directories.join('_')}`;
  }
}
