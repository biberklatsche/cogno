import {CommandRepository, DirectoryRepository} from './command.repository';
import {DbService} from '../db/db.service';
import { ShellLocation } from '../../../common/shellLocation';
import {TestHelper} from '../../../../../test/helper';
import {Hash} from '../../models/hash/hash';
import {CommandEntity} from './entities';
import {CommandSource} from '../../models/command';
import {TestDbService} from '../../../../../test/test-db';
import {getDbAdapter} from '../../../../../test/factory';

describe('CommandPersister', () => {

  let db: DbService;
  let commandRepository: CommandRepository;
  let directoryRepository: DirectoryRepository;
  let location: ShellLocation;

  beforeEach(() => {
    location = new ShellLocation('machine', TestHelper.createShellConfig().type);
    db = getDbAdapter();
    commandRepository = new CommandRepository(db);
    directoryRepository = new DirectoryRepository(db);
  });

  it('should throw an error if location is not valid', () => {
    const repo = new DirectoryRepository(db);
    expect(() => repo.init(new ShellLocation('', TestHelper.createShellConfig().type))).rejects.toThrow();
  });

  it('should save directory null', async () => {
    await directoryRepository.init(location);
    await directoryRepository.saveDirectory(null);
    const directories = await directoryRepository.getAll();
    expect(directories.length).toBe(0);
  });

  it('should save directory undefined', async () => {
    await directoryRepository.init(location);
    await directoryRepository.saveDirectory(undefined);
    const directories = await directoryRepository.getAll();
    expect(directories.length).toBe(0);
  });

  it('should save command data', async () => {
    const data: CommandEntity = {
      _id: Hash.create('git init'),
      selectionCount: 0,
      commandHash: Hash.create('git init'),
      lastExecutionDate: new Date(1000),
      executionCount: 1,
      command: 'git init',
      shellTypes: [],
      commandSource: CommandSource.TERMINAL,
    };
    await commandRepository.init();
    await commandRepository.save(data);
    const commands = await commandRepository.getAll();
    expect(commands.map(r => {
      r._id = undefined;
      return r;
    })).toEqual([{...data, _id: undefined}]);
  });

  it('should update data', async () => {
    const data1: CommandEntity  = {
      _id: undefined,
      selectionCount: 0,
      commandHash: Hash.create('git init'),
      lastExecutionDate: new Date(),
      executionCount: 1,
      command: 'git init',
      shellTypes: [],
      commandSource: CommandSource.TERMINAL,
    };
    const data2: CommandEntity  = {
      _id: undefined,
      selectionCount: 0,
      commandHash: Hash.create('git init'),
      lastExecutionDate: new Date(),
      executionCount: 2,
      command: 'git init',
      shellTypes: [],
      commandSource: CommandSource.TERMINAL,
    };
    await commandRepository.init();
    await commandRepository.save(data1);
    await commandRepository.save(data2);
    const commands = await commandRepository.getAll();
    expect(commands).toEqual([{...data2, _id: data1._id}]);
  });

  it('should delete command', async () => {
    const data1: CommandEntity = {
      _id: Hash.create('git init'), selectionCount: 0,
      commandHash: Hash.create('git init'),
      lastExecutionDate: new Date(),
      executionCount: 1,
      command: 'git init',
      shellTypes: [],
      commandSource: CommandSource.TERMINAL,
    };
    await commandRepository.init();
    await commandRepository.save(data1);
    let command = await commandRepository.get(data1._id);
    expect(command).toEqual(data1);
    await commandRepository.delete(data1._id);
    command = await commandRepository.get(data1._id);
    expect(command).toEqual(null);
  });
});
