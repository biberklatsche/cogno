import {ShellLocation} from '../../../common/shellLocation';

export namespace Helper {
  export function createOldDbName(location: ShellLocation, version: number = null) {
    let sVersion = '';
    if (version) {
      sVersion += `_v${version}`;
    }
    return (`${location.shellType}${location.machine}${sVersion}`).toLowerCase();
  }

  export function createDbName(location: ShellLocation, version: number) {
    return (`${location.machine}_v${version}`).toLowerCase();
  }

  export function createCommandsLocationDbName(location: ShellLocation, version: number) {
    return (`${location.machine}_commands_v${version}`).toLowerCase();
  }

  export function createCommandsDbName(version: number) {
    return (`commands_v${version}`).toLowerCase();
  }

  export function createDirectoriesDbName(location: ShellLocation, version: number) {
    return (`${location.machine}_${location.shellType}_directories_v${version}`).toLowerCase();
  }

  export function createCommandsDirectoriesDbName(location: ShellLocation, version: number) {
    return (`${location.machine}_${location.shellType}_commands_directories_v${version}`).toLowerCase();
  }
}
