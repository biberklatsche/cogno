import {ShellLocation} from '../../../common/shellLocation';
import {Helper} from './helper';
import {CommandEntity} from './entities';
import * as fs from 'fs';
import * as path from 'path';
import {Logger} from '../../../logger/logger';
import {Hash} from '../../models/hash/hash';
import {CommandSource} from '../../models/command';
import {IDbContext} from '../../../../../shared/db-context';

export namespace MigrationV5ToV6 {

  export async function migrate(location: ShellLocation, db: IDbContext) {
    const commandsDbName = Helper.createCommandsDbName(6);
    if (await db.existsDbFile(commandsDbName)) {
      return Promise.resolve(true);
    }
    await migrateCommandsV5ToV6(db);
    await migrateOtherFilesV5ToV6(db);
  }

  async function migrateCommandsV5ToV6(db: IDbContext): Promise<any> {
    const path = await db.getDbDirectoryPath();
    const files = fs.readdirSync(path).filter(f => f.endsWith('commands_v5.json'));
    const commands: Map<string, CommandEntity> = new Map<string, CommandEntity>();
    for (const file of files) {
      const commandsInFile = await db.loadData<CommandEntity>(file.replace('.json', ''));
      for (const command of commandsInFile) {
        if (command['isDeleted']) {continue;}
        if(!command.commandHash) {
          command.commandHash = Hash.create(command.command);
        }
        if(!command.commandSource) {
          command.commandSource = CommandSource.TERMINAL
        }
        if(!commands.has(command._id)) {
          commands.set(command._id, command);
        } else {
          commands.get(command._id).executionCount += command.executionCount;
          command.lastExecutionDate = commands.get(command._id).lastExecutionDate < command.lastExecutionDate ? command.lastExecutionDate :  commands.get(command._id).lastExecutionDate;
        }
      }
    }
    const dbName = Helper.createCommandsDbName(6);
    await db.init(dbName);
    return db.addAll(Array.from(commands.values()), dbName);
  }

  async function migrateOtherFilesV5ToV6(db: IDbContext): Promise<any> {
    const p = await db.getDbDirectoryPath();
    const files = fs.readdirSync(p).filter(f => f.endsWith('directories_v5.json'));
    for (const file of files) {
      const newFileName = file.replace('_v5.', '_v6.');
      Logger.debug(`Rename ${file} to ${newFileName}`);
      fs.copyFileSync(path.resolve(p, file), path.resolve(p, newFileName));
    }
  }
}
