import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ElectronService} from '../electron/electron.service';
import {ShellConfig} from '../../../../../shared/models/settings';
import {IpcChannel} from '../../../../../shared/ipc.chanels';
import { map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ShellsLoaderService {


  constructor(private electron: ElectronService) {
  }

  loadShells(): Observable<ShellConfig[]> {
    return this.electron.invoke<ShellConfig[]>(IpcChannel.GetAvailableShells).pipe(map(shells => shells?.filter(shell => !!shell)));
  }
}
