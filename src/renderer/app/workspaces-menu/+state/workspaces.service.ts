import {Injectable, NgZone, OnDestroy} from '@angular/core';
import {WorkspaceNode, WorkspaceTree, Workspace} from '../../+shared/models/workspace';
import {DbService} from '../../+shared/services/db/db.service';
import {Observable, Subscription} from 'rxjs';
import {BinaryNode, BinaryTree} from '../../common/tree/binary-tree';
import {Pane, SplitDirection} from '../../+shared/models/pane';
import {Uuid} from '../../common/uuid';
import {GridService} from '../../global/grid/+state/grid.service';
import {createStore, Store} from '../../common/store/store';
import {MenuService} from '../../+shared/abstract-components/menu/menu.service';
import {GlobalMenuService} from '../../+shared/abstract-components/menu/+state/global-menu.service';
import {TabFactory} from '../../+shared/abstract-components/tab/+state/tab.factory';
import {ElectronService} from '../../+shared/services/electron/electron.service';
import {IpcChannel} from '../../../../shared/ipc.chanels';
import {TerminalTab} from '../../+shared/models/tab';
import {debounceTime} from 'rxjs/operators';
import {Logger} from '../../logger/logger';
import {IDbContext} from '../../../../shared/db-context';

interface WorkspacesState {
  isLoading: boolean;
  activeWorkspace: Workspace;
  selectedWorkspace: Workspace;
  workspaces: Workspace[];
};

const initialState: WorkspacesState = {
  isLoading: false,
  selectedWorkspace: null,
  activeWorkspace: null,
  workspaces: []
};

@Injectable({
  providedIn: 'root'
})
export class WorkspacesService extends MenuService implements OnDestroy {

  private store: Store<WorkspacesState> = createStore('workspaces', initialState);

  public static readonly DB_NAME = 'workspaces';

  private subscriptions: Subscription[] = [];

  constructor(
    private gridService: GridService,
    private db: DbService,
    protected menuService: GlobalMenuService,
    private tabFactory: TabFactory,
    private electronService: ElectronService,
    private zone: NgZone) {
    super('Workspaces', menuService);
    this.subscriptions.push(this.gridService.onGridChanged().pipe(debounceTime(500)).subscribe(() => {
      const workspace = this.store.get(s => s.activeWorkspace);
      if(workspace && workspace.isAutosaveEnabled){
        Logger.debug('Autosave workspace.', workspace.name);
        this.saveWorkspace(workspace).catch(e => Logger.warn('Could not autosave workspace.'));
      }
    }));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  public updateNameInput(value: string) {
    this.store.update(s => ({selectedWorkspace: {...s.selectedWorkspace, name: value}}));
  }

  public toggleAutosave() {
    this.store.update(s => ({selectedWorkspace: {...s.selectedWorkspace, isAutosaveEnabled: !s.selectedWorkspace?.isAutosaveEnabled}}));
  }

  public addWorkspace() {
    const newWorkspace = {
      id: '',
      name: '',
      panes: null,
      isAutosaveEnabled: false
    };
    this.upsertWorkspace(newWorkspace);
    this.store.update({selectedWorkspace: {...newWorkspace}});
  }

  public openEditInput(workspace: Workspace) {
    return this.store.update({selectedWorkspace: {...workspace}});
  }

  public closeInput() {
    const newList = [...this.store.get(s => s.workspaces)];
    const index = newList.findIndex(t => t.id === '');
    if(index > -1) {
      newList.splice(index, 1);
    }
    this.store.update({selectedWorkspace: undefined, workspaces: newList});
  }

  private upsertWorkspace(workspace: Workspace, selectedWorkspaceId: string = undefined) {
    const newList = [...this.store.get(s => s.workspaces)];
    const currentConfigIndex = newList.findIndex(w => w.id === (selectedWorkspaceId !== undefined ? selectedWorkspaceId : workspace.id));
    if(currentConfigIndex === -1) {
      newList.push(workspace);
    } else {
      newList.splice(currentConfigIndex, 1, workspace);
    }
    this.store.update({workspaces: newList});
  }

  async loadWorkspaces() {
    this.store.update({isLoading: true});
    await this.db.init(WorkspacesService.DB_NAME);
    const workspacesDb = await this.db.getAll<{ _id: string; panes: WorkspaceTree; name: string; isAutosaveEnabled: boolean }>(WorkspacesService.DB_NAME);
    const workspaces = workspacesDb.map(workspaceDb => ({
      id: workspaceDb._id,
      name: workspaceDb.name,
      panes: workspaceDb.panes,
      isAutosaveEnabled: !!workspaceDb.isAutosaveEnabled,
    }));
    Logger.debug('Workspaces loaded.', workspaces);
    this.zone.run(() => this.store.update({workspaces, isLoading: false}));
  }

  async saveSelectedWorkspace() {
    await this.saveWorkspace(this.store.get(s => s.selectedWorkspace));
    this.store.update({selectedWorkspace: undefined});
  }

  async saveWorkspace(workspace: Workspace) {
    const activeWorkspace = this.store.get(s => s.activeWorkspace);
    if(workspace.id === '') { // new Workspace
      const tree = this.gridService.getTree();
      workspace.panes = this.createWorkspaceTree(tree);
      workspace.id = Uuid.create();
      this.upsertWorkspace(workspace, '');
      this.store.update({activeWorkspace: workspace});
    } else if (activeWorkspace?.id === workspace.id) { // edited Workspace is active workspace
      const tree = this.gridService.getTree();
      workspace.panes = this.createWorkspaceTree(tree);
      this.upsertWorkspace(workspace);
      this.store.update({activeWorkspace: workspace});
    } else {
      this.upsertWorkspace(workspace);
    }
    await this.db.upsert({_id: workspace.id, panes: workspace.panes, name: workspace.name, isAutosaveEnabled: workspace.isAutosaveEnabled}, WorkspacesService.DB_NAME);
  }

  deleteWorkspace(workspace: Workspace) {
    const newList = [...this.store.get(s => s.workspaces)];
    const index = newList.findIndex(t => t.id === workspace.id);
    newList.splice(index, 1);
    this.store.update({workspaces: newList, selectedWorkspace: undefined});
    this.db.remove(workspace.id, WorkspacesService.DB_NAME);
  }

  selectIsNameValid(): Observable<boolean> {
    return this.store.select(s => s.selectedWorkspace?.name?.length > 0);
  }

  selectWorkspaces(): Observable<Workspace[]> {
    return this.store.select(s => s.workspaces);
  }

  selectIsLoading(): Observable<boolean> {
    return this.store.select(s => s.isLoading);
  }

  private createWorkspaceTree(tree: BinaryTree<Pane>): WorkspaceTree {
    const rootNode = this.createWorkspaceFromPaneRecursive(tree.root);
    const workspaceTree = new WorkspaceTree();
    workspaceTree._root = rootNode;
    return workspaceTree;
  }

  resetGrid() {
    this.electronService.send(IpcChannel.ChangeWindowName, 'Cogno');
    this.store.update({activeWorkspace: undefined});
    this.gridService.resetTree();
    this.closeMenu();
  }

  restoreGridFromWorkspace(workspace: Workspace) {
    this.store.update({activeWorkspace: {...workspace}});
    const rootNode = this.restorePaneFromWorkspaceRecursive(workspace.panes._root);
    this.electronService.send(IpcChannel.ChangeWindowName, workspace.name);
    this.gridService.restoreTree(new BinaryTree<Pane>(rootNode));
    this.closeMenu();
  }

  private restorePaneFromWorkspaceRecursive(source: WorkspaceNode): BinaryNode<Pane> {
    const newNode = this.createNode(source);
    if (source._left) {
      newNode.addToNode(this.restorePaneFromWorkspaceRecursive(source._left), 'l');
    }
    if (source._right) {
      newNode.addToNode(this.restorePaneFromWorkspaceRecursive(source._right), 'r');
    }
    return newNode;
  }

  private createNode(source: WorkspaceNode): BinaryNode<Pane> {
    const node = new BinaryNode<Pane>();
    node.data = new Pane();
    if (source._data.splitDirection === SplitDirection.Horizontal || source._data.splitDirection === SplitDirection.Vertical) {
      node.data.splitDirection = source._data.splitDirection;
      node.data.ratio = source._data.ratio ? source._data.ratio : 0.5;
    } else {
      node.data.tabs = [];
      source._data.tabs.forEach(tab => {
        if (tab['shellConfig']) {
          (tab as TerminalTab).config = tab['shellConfig'];
        }
        node.data.tabs.push(this.tabFactory.restoreTab(tab));
      });
    }
    return node;
  }

  private createWorkspaceFromPaneRecursive(source: BinaryNode<Pane>): WorkspaceNode {
    const newNode = this.createWorkspaceNode(source);
    if (source.left) {
      newNode._left = this.createWorkspaceFromPaneRecursive(source.left);
    }
    if (source.right) {
      newNode._right = this.createWorkspaceFromPaneRecursive(source.right);
    }
    return newNode;
  }

  private createWorkspaceNode(source: BinaryNode<Pane>): WorkspaceNode {
    const node = new WorkspaceNode();
    node._data = new Pane();
    if (source.data.splitDirection === SplitDirection.Horizontal || source.data.splitDirection === SplitDirection.Vertical) {
      node._data.splitDirection = source.data.splitDirection;
      node._data.ratio = source.data.ratio ? source.data.ratio : 0.5;
    } else {
      node._data.tabs = [];
      source.data.tabs.forEach(tab => {
        node._data.tabs.push(tab);
      });
    }
    return node;
  }

  selectActiveWorkspace(): Observable<Workspace> {
    return this.store.select(s => s.activeWorkspace);
  }

  selectSelectedWorkspace(): Observable<Workspace> {
    return this.store.select(s => s.selectedWorkspace);
  }

  getWorkspaces(): Workspace[] {
    return this.store.get(s => s.workspaces);
  }

  getStore(): Store<WorkspacesState>  {
    return this.store;
  }
}
