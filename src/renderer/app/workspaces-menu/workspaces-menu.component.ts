import {Component, ElementRef, ViewEncapsulation} from '@angular/core';
import {Observable} from 'rxjs';
import {map, tap} from 'rxjs/operators';
import {Workspace} from '../+shared/models/workspace';
import {ColorService} from '../+shared/services/color/color.service';
import {MenuComponent} from '../+shared/abstract-components/menu/menu.component';
import {WorkspacesService} from './+state/workspaces.service';
import {Key} from '../common/key';
import {KeyboardService} from '../+shared/services/keyboard/keyboard.service';
import {CommonModule} from '@angular/common';
import {IconComponent} from '../+shared/components/icon/icon.component';
import {CopyEditDeleteComponent} from '../+shared/components/copy-edit-delete/copy-edit-delete.component';
import {LoaderComponent} from '../+shared/components/loader/loader.component';
import {KeytipService} from '../+shared/services/keyboard/keytip.service';

@Component({
    selector: 'app-workspaces-menu',
    templateUrl: './workspaces-menu.component.html',
    styleUrls: ['./workspaces-menu.component.scss'],
    encapsulation: ViewEncapsulation.None,
    imports: [
        CommonModule,
        IconComponent,
        CopyEditDeleteComponent,
        LoaderComponent
    ]
})
export class WorkspacesMenuComponent extends MenuComponent {

  selectedWorkspace$: Observable<Workspace>;
  activeWorkspace$: Observable<Workspace>;
  isInputVisible$: Observable<boolean>;
  isNameValid$: Observable<boolean>;
  workspaces$: Observable<Workspace[]>;
  isLoading$: Observable<boolean>;
  isActive$: Observable<boolean>;

  constructor(protected elementRef: ElementRef,
              protected keyboardService: KeyboardService,
              protected keytipService: KeytipService,
              private workspacesService: WorkspacesService,
              private colorService: ColorService) {
    super(elementRef, workspacesService, keyboardService, keytipService);

    this.isActive$ = this.workspacesService.selectIsActive().pipe(
      tap(isActive =>  {
        if(isActive) {
          this.workspacesService.loadWorkspaces();
        } else {
          this.workspacesService.closeInput();
        }
      })
    );

    this.isNameValid$ = workspacesService.selectIsNameValid(); // .pipe(map(n => n.length > 0));
    this.workspaces$ = workspacesService.selectWorkspaces().pipe(map(vs => {
      vs.forEach(v => {
        v.token = v.name.length > 0 ? v.name[0].toUpperCase() : '?';
        v.colorOpacity = this.colorService.getColorByName(v.name, 'AA');
        v.color = this.colorService.getColorByName(v.name);
      });
      return vs;
    }));
    this.isLoading$ = workspacesService.selectIsLoading();
    this.selectedWorkspace$ = workspacesService.selectSelectedWorkspace();
    this.activeWorkspace$ = workspacesService.selectActiveWorkspace();
    this.isInputVisible$ = this.selectedWorkspace$.pipe(
      map(cs => !!cs),
      tap(isVisible => {
        if (isVisible) {
          setTimeout(() => {
            document.getElementById('inputField')?.focus();
          });
        }
      })
    );

  }

  nameChanged(event: KeyboardEvent): void {
    if (event.key === Key.Enter) {
      this.saveWorkspace(event);
    } else {
      this.workspacesService.updateNameInput((event.target as any).value);
    }
    event.stopPropagation();
  }

  addWorkspace(event: Event) {
    this.workspacesService.addWorkspace();
    event.stopPropagation();
  }

  closeInputField(event: Event) {
    this.workspacesService.closeInput();
    event.stopPropagation();
  }

  saveWorkspace(event: Event) {
    this.workspacesService.saveSelectedWorkspace();
    setTimeout(() => {
      this.elementRef.nativeElement.focus();
    });
    event.stopPropagation();
  }

  restoreWorkspace(workspace: Workspace, event: Event) {
    this.workspacesService.restoreGridFromWorkspace(workspace);
    event.stopPropagation();
  }

  closeWorkspace(event: any) {
    this.workspacesService.resetGrid();
    event.stopPropagation();
  }

  handleEvent(workspace: Workspace, $event: 'edit' | 'delete') {
    switch ($event) {
      case 'edit':
        this.workspacesService.openEditInput(workspace);
        break;
      case 'delete':
        this.workspacesService.deleteWorkspace(workspace);
        break;
    }
  }

  toggleAutosave() {
    this.workspacesService.toggleAutosave();
  }
}
