import {AboutComponent} from './about.component';
import {AboutService} from './+state/about.service';
import {ElectronService} from '../+shared/services/electron/electron.service';
import {BinaryTree} from '../common/tree/binary-tree';
import {TabType} from '../../../shared/models/models';
import {clear, getElectronService, getGridService, getKeyboardService} from '../../../test/factory';
import {GridService} from '../global/grid/+state/grid.service';

describe('AboutComponent', () => {
  let component: AboutComponent;
  let service: AboutService;
  let electronService: ElectronService;
  let gridService: GridService;

  beforeEach(() => {
    clear();
    electronService = getElectronService();
    jest.spyOn(electronService, 'getVersion').mockReturnValue('1.0.0');

    gridService = getGridService();
    gridService.addNewTab(BinaryTree.ROOT_KEY, TabType.About);

    service = new AboutService(gridService, electronService, getKeyboardService());
    component = new AboutComponent(service);
    component.tabId = gridService.getActiveTabId();
  });

  it('should init version ', (done) => {
    component.ngOnInit();
    component.version.subscribe(version => {
      expect(version).toEqual('1.0.0');
      done();
    });
  });

  it('should init isNightly false', (done) => {
    component.ngOnInit();
    component.isNightly.subscribe(isNightly => {
      expect(isNightly).toBeFalsy();
      done();
    });
  });

  it('should init isNightly true', (done) => {
    jest.spyOn(electronService, 'getVersion').mockReturnValue('1.0.0-nightly');
    jest.spyOn(electronService, 'isNightlyVersion').mockReturnValue(true);
    component.ngOnInit();
    component.isNightly.subscribe(isNightly => {
      expect(isNightly).toBeTruthy();
      done();
    });
  });

  it('should init userId', (done) => {
    component.ngOnInit();
    component.userId.subscribe(userId => {
      expect(userId).toBe('');
      done();
    });
  });

  it('should open third party licence', () => {
    jest.spyOn(electronService, 'getThirdPartyLicensePath').mockReturnValue('/thirdparty.txt');
    const openPathSpy = jest.spyOn(electronService, 'openPath').mockImplementation(() => {});
    component.ngOnInit();
    component.openThirdPartyLicenses();
    expect(openPathSpy.mock.calls.length).toEqual(1);
    expect(openPathSpy.mock.calls[0][0]).toEqual('/thirdparty.txt');
  });

  it('should open log', () => {
    jest.spyOn(electronService, 'getLogPath').mockReturnValue('/log.txt');
    const openPathSpy = jest.spyOn(electronService, 'openPath').mockImplementation(() => {});
    component.ngOnInit();
    component.openLogs();
    expect(openPathSpy.mock.calls.length).toEqual(1);
    expect(openPathSpy.mock.calls[0][0]).toEqual('/log.txt');
  });

  it('should open Devtools', () => {
    const openDevtoolsSpy = jest.spyOn(electronService, 'openDevtools').mockImplementation(() => {});
    component.ngOnInit();
    component.openDevtools();
    expect(openDevtoolsSpy.mock.calls.length).toEqual(1);
  });
});
