import {Injectable, OnDestroy} from '@angular/core';
import {OnReady, TabService} from '../../+shared/abstract-components/tab/+state/tab.service';
import {GridService} from '../../global/grid/+state/grid.service';
import {ElectronService} from '../../+shared/services/electron/electron.service';
import {Observable} from 'rxjs';
import {AppConfig} from '../../../environments/environment';
import {KeyboardService} from '../../+shared/services/keyboard/keyboard.service';

export interface AboutState {
  version: string;
  isNightly: boolean;
  userId: string;

  isProduction: boolean;
}

export const initialState: AboutState = {
  version: '',
  isNightly: false,
  userId: '',
  isProduction: false
};

@Injectable()
export class AboutService extends TabService<AboutState> implements OnReady, OnDestroy{

  constructor(gridService: GridService, private electron: ElectronService, private keyboardService: KeyboardService) {
    super(gridService, initialState);
    keyboardService.activateGlobalShortcuts();
  }

  onReady(): void {
    this.store.update({
      userId: window.localStorage?.userId || '',
      version: this.electron.getVersion(),
      isNightly: this.electron.isNightlyVersion(),
      isProduction: AppConfig.production
    });
  }

  openThirdPartyLicenses() {
    this.electron.openPath(this.electron.getThirdPartyLicensePath());
  }

  openLogs() {
    this.electron.openPath(this.electron.getLogPath());
  }

  openDevtools() {
    this.electron.openDevtools();
  }

  selectUserId(): Observable<string> {
    return this.store.select(s => s.userId);
  }

  selectIsNightly(): Observable<boolean> {
    return this.store.select(s => s.isNightly);
  }

  selectVersion(): Observable<string> {
    return this.store.select(s => s.version);
  }

  selectIsProduction(): Observable<boolean> {
    return this.store.select(s => s.isProduction);
  }

  ngOnDestroy(): void {
    this.unsubscribe();
    this.keyboardService.deactivateGlobalShortcuts();
  }
}
