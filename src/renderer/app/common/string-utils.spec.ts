
import {StringUtils} from '../../../shared/string-utils';

describe('StringUtils', () => {

  it('replace whitespace', () => {
    expect(StringUtils.toFileName('a b')).toEqual('a_b');
  });

  it('replace slash and backslash', () => {
    expect(StringUtils.toFileName('a/b\\c')).toEqual('a-b-c');
  });

  it('should create hash', () => {
    expect(StringUtils.hash('a')).not.toEqual(StringUtils.hash('b'));
    expect(StringUtils.hash('a')).toEqual(StringUtils.hash('a'));
  });
});


