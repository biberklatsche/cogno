export namespace RegexpHelper {
  export function escapeWeekRegExp(s: string) {
    if (s === null) {
      return '';
    }
    return s.replace(/[.*+?^${}()|\[\]\\/]/g, '.?');
  }

  export function escapeStrongRegExp(s: string) {
    if (s === null) {
      return '';
    }
    return s.replace(/[.*+?^${}()|\[\]\\/]/g, '\\$&');
  }

  export function regexIndexOf(s: string, regex: RegExp, startIndex: number): {index: number; length: number} {
    const match = regex.exec(s.substring(startIndex || 0));
    if(!match) {return {index: -1, length: 0};}
    const index = (match.index >= 0) ? (match.index + (startIndex || 0)) : match.index;
    const length = (match.index >= 0) ? match[0].length : 0;
    return {index, length};
  }

  export function regexIndexOfReverse(s: string, regex: RegExp, startIndex: number): {index: number; length: number} {
    const substring = s.substring(0, startIndex || 0);
    const match = substring.match(regex)?.pop();
    if(!match) {return {index: -1, length: 0};}
    return {index: s.lastIndexOf(match, startIndex - (match?.length || 0)), length: match.length};
  }

  export const RegexCommandPlaceholder = '{{[a-zA-Z0-9_?\-]{1,}}}';
}
