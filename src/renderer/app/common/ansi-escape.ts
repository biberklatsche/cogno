export namespace AnsiEscSequence {
  export const saveCursorPosition = '\\033[s';
  export const restoreCursorPosition = '\\033[u';

  export function linesUp(n: number) {
    return `\\033[${n}A`;
  }

  export function moveCursorLeft(n: number) {
    return `${String.fromCharCode(27)}[D`.repeat(n);
  }

  export function moveCursorRight(n: number) {
    return `${String.fromCharCode(27)}[C`.repeat(n);
  }

  export function clearFromCursorToEnd() {
    return '\u001b[1K';
  }

  export const clearLineFromCursorToStart = `${String.fromCharCode(27)}[1;5H`;
  export const clearLineFromCursorToEnd = `${String.fromCharCode(27)}[1;5F`;
}
