import {Component, InjectionToken, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {SafeStyle} from '@angular/platform-browser';
import {combineLatest, combineLatestAll, Observable, Subject, Subscription} from 'rxjs';
import {Color} from '../../shared/color';
import {Logger} from './logger/logger';
import {SettingsService} from './+shared/services/settings/settings.service';
import {filter, tap} from 'rxjs/operators';
import {GridComponent} from './global/grid/grid.component';
import {ErrorComponent} from './error/error.component';
import {CommonModule} from '@angular/common';
import {OnboardingComponent} from './onboarding/onboarding.component';
import {NotificationComponent} from './+shared/notification/notification.component';
import {InitService} from './+shared/services/init/init.service';
import {Theme} from '../../shared/models/settings';
import {DEFAULT_SETTINGS} from '../../shared/default-settings';
import {DbService} from './+shared/services/db/db.service';

export const DB_SERVICE_TOKEN = new InjectionToken<DbService>('DbService');

@Component({
  selector: 'body',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None,
  imports: [
    CommonModule,
    GridComponent,
    ErrorComponent,
    OnboardingComponent,
    NotificationComponent
  ],
  providers: [
    { provide: DB_SERVICE_TOKEN, useClass: DbService }
  ]
})
export class AppComponent implements OnInit, OnDestroy {

  subcribtions: Subscription[] = [];
  showError: Subject<boolean> = new Subject<boolean>();
  showOnboarding: Observable<boolean>;
  style: SafeStyle | undefined;
  imageFilter: Subject<string> = new Subject<string>();

  constructor(
    initService: InitService,
    private settingsService: SettingsService) {
    initService.init();
  }

  ngOnInit(): void {
    this.subcribtions.push(this.initSettings().subscribe());
    this.showOnboarding = this.settingsService.selectShowOnboarding();
  }

  public initSettings(): Observable<any> {
    return combineLatest([this.settingsService.selectActiveTheme(), this.settingsService.selectHasErrors()]).pipe(
      tap(([theme, hasError]) => {
        if (hasError) {
          Logger.warn('Could not load settings! Use default color definitions.');
          this.setStyle(DEFAULT_SETTINGS.themes[0]);
          this.showError.next(true);
        } else if (theme){
          this.setStyle(theme);
          this.showError.next(false);
        }
      }));
  }

  private setStyle(theme: Theme): void {
    const isLightTheme = Color.isLight(theme.colors.background);
    const backgroundFactor = this.getBackgroundFactor(theme, isLightTheme);
    const shadowFactor = this.getShadowFactor(isLightTheme);
    const factor = isLightTheme ? -1 : 1;
    if (theme.isImageAvailable) {
      this.imageFilter.next(`blur(${theme.imageBlur === undefined ? 10 : theme.imageBlur}px)`);
    } else {
      this.imageFilter.next('');
    }

    document.documentElement.style.setProperty('--background-color', `${theme.colors.background}`);
    document.documentElement.style.setProperty('--background-color-10l', `${Color.lightenDarkenColor(theme.colors.background, backgroundFactor * 10)}`);
    document.documentElement.style.setProperty('--background-color-20l', `${Color.lightenDarkenColor(theme.colors.background, backgroundFactor * 20)}`);
    document.documentElement.style.setProperty('--background-color-30l', `${Color.lightenDarkenColor(theme.colors.background, backgroundFactor * 30)}`);
    document.documentElement.style.setProperty('--background-color-40l', `${Color.lightenDarkenColor(theme.colors.background, backgroundFactor * 40)}`);
    document.documentElement.style.setProperty('--background-color-10d', `${Color.lightenDarkenColor(theme.colors.background, backgroundFactor * -10)}`);
    document.documentElement.style.setProperty('--background-color-20d', `${Color.lightenDarkenColor(theme.colors.background, backgroundFactor * -20)}`);
    document.documentElement.style.setProperty('--background-color-30d', `${Color.lightenDarkenColor(theme.colors.background, backgroundFactor * -30)}`);
    document.documentElement.style.setProperty('--background-color-40d', `${Color.lightenDarkenColor(theme.colors.background, backgroundFactor * -40)}`);
    document.documentElement.style.setProperty('--color-shadow1', `${Color.lightenDarkenColor(theme.colors.background, shadowFactor * -10)}`);
    document.documentElement.style.setProperty('--color-shadow2', `${Color.lightenDarkenColor(theme.colors.background, shadowFactor * -20)}`);
    document.documentElement.style.setProperty('--color-shadow3', `${Color.lightenDarkenColor(theme.colors.background, shadowFactor * -30)}`);
    document.documentElement.style.setProperty('--foreground-color', `${theme.colors.foreground}`);
    document.documentElement.style.setProperty('--foreground-color-10l', `${Color.lightenDarkenColor(theme.colors.foreground, factor * 10)}`);
    document.documentElement.style.setProperty('--foreground-color-20l', `${Color.lightenDarkenColor(theme.colors.foreground, factor * 20)}`);
    document.documentElement.style.setProperty('--foreground-color-30l', `${Color.lightenDarkenColor(theme.colors.foreground, factor * 30)}`);
    document.documentElement.style.setProperty('--foreground-color-40l', `${Color.lightenDarkenColor(theme.colors.foreground, factor * 40)}`);
    document.documentElement.style.setProperty('--foreground-color-10d', `${Color.lightenDarkenColor(theme.colors.foreground, factor * -10)}`);
    document.documentElement.style.setProperty('--foreground-color-20d', `${Color.lightenDarkenColor(theme.colors.foreground, factor * -20)}`);
    document.documentElement.style.setProperty('--foreground-color-30d', `${Color.lightenDarkenColor(theme.colors.foreground, factor * -30)}`);
    document.documentElement.style.setProperty('--foreground-color-40d', `${Color.lightenDarkenColor(theme.colors.foreground, factor * -40)}`);
    document.documentElement.style.setProperty('--highlight-color', `${theme.colors.highlight}`);
    document.documentElement.style.setProperty('--highlight-color-10l', `${Color.lightenDarkenColor(theme.colors.highlight, factor * 10)}`);
    document.documentElement.style.setProperty('--highlight-color-20l', `${Color.lightenDarkenColor(theme.colors.highlight, factor * 20)}`);
    document.documentElement.style.setProperty('--highlight-color-30l', `${Color.lightenDarkenColor(theme.colors.highlight, factor * 30)}`);
    document.documentElement.style.setProperty('--highlight-color-40l', `${Color.lightenDarkenColor(theme.colors.highlight, factor * 40)}`);
    document.documentElement.style.setProperty('--highlight-color-10d', `${Color.lightenDarkenColor(theme.colors.highlight, factor * -10)}`);
    document.documentElement.style.setProperty('--highlight-color-20d', `${Color.lightenDarkenColor(theme.colors.highlight, factor * -20)}`);
    document.documentElement.style.setProperty('--highlight-color-30d', `${Color.lightenDarkenColor(theme.colors.highlight, factor * -30)}`);
    document.documentElement.style.setProperty('--highlight-color-40d', `${Color.lightenDarkenColor(theme.colors.highlight, factor * -40)}`);
    document.documentElement.style.setProperty('--color-green', `${theme.colors.green}`);
    document.documentElement.style.setProperty('--color-red', `${theme.colors.red}`);
    document.documentElement.style.setProperty('--color-blue', `${theme.colors.blue}`);
    document.documentElement.style.setProperty('--color-yellow', `${theme.colors.yellow}`);
    document.documentElement.style.setProperty('--color-white', `${theme.colors.white}`);
    document.documentElement.style.setProperty('--color-black', `${theme.colors.black}`);
    document.documentElement.style.setProperty('--cursor-color', `${theme.colors.cursor}`);
    document.documentElement.style.setProperty('--shadow1', '0 1px 1px 0 var(--color-shadow3), 0 2px 1px -1px var(--color-shadow1), 0 1px 3px 0 var(--color-shadow2)');
    document.documentElement.style.setProperty('--shadow2', '0 2px 2px 0 var(--color-shadow3), 0 3px 1px -2px var(--color-shadow1), 0 1px 5px 0 var(--color-shadow2)');
    document.documentElement.style.setProperty('--shadow3', '0 3px 4px 0 var(--color-shadow3), 0 3px 3px -2px var(--color-shadow1), 0 1px 8px 0 var(--color-shadow2)');
    document.documentElement.style.setProperty('--appFontSize', `${theme.appFontsize}px`);
    document.documentElement.style.setProperty('--fontSize', `${theme.fontsize}px`);
    document.documentElement.style.setProperty('--fontWeight', `${theme.fontWeight}`);
    document.documentElement.style.setProperty('--fontFamily', `'${theme.fontFamily}'`);
    document.documentElement.style.setProperty('--appFontFamily', `'${theme.appFontFamily}'`);
    document.documentElement.style.setProperty('--padding', `${theme.paddingAsArray[3]}rem`);
    document.documentElement.style.setProperty('--padding-xterm', `${theme.paddingAsArray[0]}rem ${theme.paddingAsArray[1]}rem ${theme.paddingAsArray[2]}rem ${theme.paddingAsArray[3]}rem`);
    document.documentElement.style.setProperty('--color-command-running', `${theme.colors.commandRunning}`);
    document.documentElement.style.setProperty('--color-command-success', `${theme.colors.commandSuccess}`);
    document.documentElement.style.setProperty('--color-command-error', `${theme.colors.commandError}`);
    if (theme.isImageAvailable) {
      const imageUrl = theme.image.trim().startsWith('http') ? `url(${theme.image.trim()})` : `url(file:///${theme.image.trim()})`;
      const color = theme.colors.background + Color.getHexOpacity(theme.imageOpacity ?? 75);
      document.body.style.background = `linear-gradient(to bottom, ${color}, ${color}), ${imageUrl} no-repeat center center fixed`;
      document.body.style.backgroundSize = 'cover';
    }
  }

  private getShadowFactor(isLightTheme: boolean): number {
    if (isLightTheme) {
      return 1;
    }
    return 0.5;
  }

  ngOnDestroy(): void {
    this.subcribtions.forEach(s => s.unsubscribe());
  }

  private getBackgroundFactor(theme: Theme, isLightTheme: boolean) {
    const brightness = Color.getBrightness(theme.colors.background);
    const factor = 1.8 - brightness;
    return isLightTheme ? -factor : factor;
  }
}
