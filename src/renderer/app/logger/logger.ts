import * as log from 'electron-log/renderer';

export namespace Logger {
  const instance = log;

  const instanceVariablesAsString = () => {
    return `[${instance.variables['processType']}]`;
  };

  export function info(...params: any[]) {
    instance.info(instanceVariablesAsString(), ...params);
  }

  export function debug(...params: any[]) {
    instance.debug(instanceVariablesAsString(), ...params);
  }

  export function warn(...params: any[]) {
    instance.warn(instanceVariablesAsString(), ...params);
  }

  export function error(...params: any[]) {
    instance.error(instanceVariablesAsString(), ...params);
  }

  export function silly(...params: any[]) {
    instance.silly(instanceVariablesAsString(), ...params);
  }
}
