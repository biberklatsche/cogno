import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {Font, OnboardingService, Site} from './+state/onboarding.service';
import {map} from 'rxjs/operators';
import {ShellConfig, Theme} from '../../../shared/models/settings';
import {InjectionType, ShellType} from '../../../shared/models/models';
import {EmojiComponent} from './emoji/emoji.component';
import {CommonModule} from '@angular/common';
import {IconComponent} from '../+shared/components/icon/icon.component';
import {TerminalThemePreviewComponent} from '../+shared/components/terminal-theme-preview/terminal-theme-preview.component';

@Component({
    selector: 'app-onboarding',
    templateUrl: './onboarding.component.html',
    styleUrls: ['./onboarding.component.scss'],
    providers: [OnboardingService],
    imports: [
        CommonModule,
        EmojiComponent,
        IconComponent,
        TerminalThemePreviewComponent
    ]
})
export class OnboardingComponent implements OnInit {

  site: Observable<number>;
  shells: Observable<ShellConfig[]>;
  fonts: Observable<Font[]>;
  themes: Observable<Theme[]>;
  selectedShells: Observable<ShellConfig[]>;
  hasPreviousSite: Observable<boolean>;
  hasNextSite: Observable<boolean>;
  enableTelemetry: Observable<boolean>;
  maxSite = Site.By;
  ShellType = ShellType;
  emoji = 1;

  constructor(private store: OnboardingService) {
    this.enableTelemetry = this.store.selectEnableTelemetry()
    this.site = this.store.selectSite();
    this.hasPreviousSite = store.selectHasPreviousSite();
    this.hasNextSite = store.selectHasNextSite();
    this.shells = this.store.selectShells();
    this.selectedShells = this.shells.pipe(map(ss => ss.filter(s => s.isSelected)));
    this.themes = this.store.selectThemes();
    this.fonts = this.store.selectFonts();
  }

  ngOnInit(): void {
    this.init().subscribe();
  }

  init(): Observable<any> {
    return this.store.loadData();
  }

  nextSite() {
    this.store.incrementSite();
  }

  previousSite() {
    this.store.decrementSite();
  }

  finish() {
    this.store.finish();
  }

  toggleShell(shell: ShellConfig) {
    this.store.toggleShell(shell.name);
  }

  toggleDefaultShell(shell: ShellConfig) {
    this.store.toggleDefaultShell(shell.name);
  }

  toggleTheme(theme: Theme) {
    this.store.toggleTheme(theme.name);
  }

  toggleFont(font: Font) {
    this.store.toggleFont(font.name);
  }

  toggleTelemetry() {
    this.store.toggleEnableTelemetry();
  }

  openDocumentation(page: string, $event: MouseEvent) {
    $event.stopPropagation();
    this.store.openDocumentation(page);
  }

  setInjectionType(type: InjectionType) {
    this.store.setInjectionType(type);
  }
}
