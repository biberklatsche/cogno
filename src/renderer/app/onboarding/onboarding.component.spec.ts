import {
  clear,
  getElectronService,
  getFontsLoaderService,
  getOnboardingService,
  getShellsLoaderService
} from '../../../test/factory';
import {OnboardingComponent} from './onboarding.component';
import {Font, OnboardingService, Site} from './+state/onboarding.service';
import {FontsLoaderService} from '../+shared/services/fonts/fonts-loader.service';
import {of} from 'rxjs';
import {ShellsLoaderService} from '../+shared/services/shells/shells-loader.service';
import {TestHelper} from '../../../test/helper';
import {ElectronService} from '../+shared/services/electron/electron.service';
import {IpcChannel} from '../../../shared/ipc.chanels';
import {ColorName, Settings} from '../../../shared/models/settings';
import {ShellType} from '../../../shared/models/models';
import {DEFAULT_MAC_SHORTCUTS, DEFAULT_SHORTCUTS} from '../../../shared/default-settings';
import {platform} from 'os';

describe('OnboardingComponent', () => {
  let component: OnboardingComponent;
  let service: OnboardingService;
  let fontsLoaderService: FontsLoaderService;
  let shellsLoaderService: ShellsLoaderService;
  let electronService: ElectronService;

  beforeEach(() => {
    clear();
    fontsLoaderService = getFontsLoaderService();
    shellsLoaderService = getShellsLoaderService();
    electronService = getElectronService();
    service = getOnboardingService();
    component = new OnboardingComponent(service);
  });

  it('should create an instance', () => {
    expect(component).toBeTruthy();
  });

  it('should load fonts on init', (done) => {
    const fontNames = TestHelper.createFonts();
    const availableFonts: Font[] = fontNames.map(n => ({name: n, isSelected: false}));
    availableFonts[0].isSelected = true;
    jest.spyOn(shellsLoaderService, 'loadShells').mockReturnValue(of([TestHelper.createShellConfig()]));
    jest.spyOn(fontsLoaderService, 'loadFonts').mockReturnValue(of(fontNames));
    component.init().subscribe(() => {
      component.fonts.subscribe(fonts => {
        expect(fonts).toEqual(fonts);
        done();
      });
    });
  });

  it('should work if no font is available', (done) => {
    jest.spyOn(shellsLoaderService, 'loadShells').mockReturnValue(of([TestHelper.createShellConfig()]));
    jest.spyOn(fontsLoaderService, 'loadFonts').mockReturnValue(of([]));
    component.init().subscribe(() => {
      component.fonts.subscribe(fonts => {
        expect(fonts).toEqual([]);
        done();
      });
    });
  });

  it('should load shell on init', (done) => {
    jest.spyOn(shellsLoaderService, 'loadShells').mockReturnValue(of([TestHelper.createShellConfig()]));
    jest.spyOn(fontsLoaderService, 'loadFonts').mockReturnValue(of(TestHelper.createFonts()));
    component.init().subscribe(() => {
      component.shells.subscribe(shells => {
        expect(shells).toEqual([TestHelper.createShellConfig()]);
        done();
      });
    });
  });

  it('should skip shells if only one shell is available', (done) => {
    jest.spyOn(shellsLoaderService, 'loadShells').mockReturnValue(of([TestHelper.createShellConfig()]));
    jest.spyOn(fontsLoaderService, 'loadFonts').mockReturnValue(of(TestHelper.createFonts()));
    const sites = [];
    component.init().subscribe(() => {
      component.site.subscribe((site) => {
        sites.push(site);
        if(site !== Site.By) {
          component.nextSite();
        } else {
          expect(sites).toEqual([Site.Welcome, Site.Theme, Site.Font, Site.Telemetry, Site.By]);
          done();
        }
      });
      component.nextSite();
    });
  });

  it('should show all sites', (done) => {
    jest.spyOn(shellsLoaderService, 'loadShells').mockReturnValue(of([TestHelper.createShellConfig(), TestHelper.createShellConfig()]));
    jest.spyOn(fontsLoaderService, 'loadFonts').mockReturnValue(of(TestHelper.createFonts()));
    const sites = [];
    component.init().subscribe(() => {
      component.site.subscribe((site) => {
        sites.push(site);
        if(site !== Site.By) {
          component.nextSite();
        } else {
          try {
            expect(sites).toEqual([Site.Welcome, Site.Shells, Site.DefaultShell, Site.Theme, Site.Font, Site.Telemetry, Site.By]);
            done();
          } catch (e) {
            done(e);
          }

        }
      });
    });
  });

  it('should save settings', (done) => {
    jest.spyOn(shellsLoaderService, 'loadShells').mockReturnValue(of([TestHelper.createShellConfig(), TestHelper.createShellConfig()]));
    jest.spyOn(fontsLoaderService, 'loadFonts').mockReturnValue(of(TestHelper.createFonts()));
    jest.spyOn(electronService, 'send').mockImplementation((channel: string) => {
    });

    const expectedSettings: Settings =
      {
        shells: [{
          id: 'bash',
          name: 'Bash',
          default: true,
          isSelected: true,
          path: 'C:/bash.exe',
          type: ShellType.Bash,
          workingDir: 'c:/',
          injectionType: 'Auto'
        }, {
          id: 'bash',
          name: 'Bash',
          default: true,
          isSelected: true,
          path: 'C:/bash.exe',
          type: ShellType.Bash,
          workingDir: 'c:/',
          injectionType: 'Auto'
        }],
        themes: [{
          name: 'light',
          fontsize: 12,
          appFontsize: 12,
          padding: '3px',
          paddingAsArray: [3, 3, 3, 3],
          fontWeight: '200',
          fontFamily: 'fontA',
          appFontFamily: 'Roboto',
          cursorWidth: 3,
          cursorStyle: 'bar',
          cursorBlink: true,
          isDefault: true,
          image: 'background.jpg',
          prompt: '$',
          promptVersion: 1,
          colors: {
            foreground: '#7B7B7B',
            background: '#FFFFFF',
            highlight: '#45D298',
            cursor: '#45D298',
            black: '#3B4251',
            red: '#DF5869',
            green: '#45D298',
            yellow: '#F1CE0C',
            blue: '#81A1C1',
            magenta: '#B48DAE',
            cyan: '#88C0D0',
            white: '#E5E9F0',
            brightBlack: '#3B4251',
            brightRed: '#DF5869',
            brightGreen: '#45D298',
            brightYellow: '#F1CE0C',
            brightBlue: '#81A1C1',
            brightMagenta: '#B48DAE',
            brightCyan: '#88C0D0',
            brightWhite: '#E5E9F0',
            promptColors: [{foreground: ColorName.black, background: ColorName.blue}, {
              foreground: ColorName.black,
              background: ColorName.blue
            }]
          }
        }],
        shortcuts: platform() == 'darwin' ? DEFAULT_MAC_SHORTCUTS : DEFAULT_SHORTCUTS,
        general: {
          openTabInSameDirectory: false,
          enablePasteOnRightClick: false,
          enableTelemetry: true,
          enableCopyOnSelect: false,
          scrollbackLines: 100000
        },
        autocomplete: {ignore: [], mode: 'always', position: 'cursor'}
      };

    component.init().subscribe(() => {
      component.site.subscribe((site) => {
        if (site !== Site.By) {
          component.nextSite();
        } else {
          component.finish();
          try {
            expect(electronService.send).toHaveBeenCalledWith(IpcChannel.SaveSettings, expectedSettings);
            done();
          } catch (e) {
            done(e);
          }
        }
      });
    });
  });

});
