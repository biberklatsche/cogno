import {app, dialog, globalShortcut, ipcMain, net, shell} from 'electron';
import {WindowFactory} from './window.factory';
import {SettingsHandler} from './settings.handler';
import {UpdateHandler} from './update.handler';
import {Application} from '../shared/application';
import {SessionHandler} from './session.handler';
import {ShellHandler} from './shell.handler';
import {FontsHandler} from './font.handler';
import {createAppMenu} from './menu';
import {DarkModeHandler} from './dark-mode.handler';
import {IpcChannel} from '../shared/ipc.chanels';
import {Logger} from './logger';
import {LogLevel} from '../shared/loglevel';
import * as fs from 'fs';
import {DBHandler} from "./db.handler";

function getPath(args: string[]) {
  const pathArgs = args.filter(val => !val.startsWith('--'));
  if (pathArgs.length === 0) {
    return undefined;
  }
  return pathArgs[0];
}

const args = process.argv.slice(1);
const serve = args.some(val => val === '--serve');
const debug = args.some(val => val === '--log-debug');
const info = args.some(val => val === '--log-info');


process.on('uncaughtException' as any, err => {
  console.log(err);
});

try {
  const singleInstanceLock = app.requestSingleInstanceLock();
  if (!singleInstanceLock && !serve) {
    app.quit();
  } else {
    app.on('second-instance', (event, argv) => {
      const argsSecondInstance = argv.slice(1);
      if (!argsSecondInstance.some(val => val === '--serve')) {
        Logger.debug('Open second window.');
        SessionHandler.openNextHere(getPath(argsSecondInstance));
        WindowFactory.createWindowWithBounds(serve);
      }
    });
    app.on('window-all-closed', function () {
      Logger.debug('Start quiting the app. Close all child processes.');
      SessionHandler.closeAll();
      const interval = setInterval(() => {
        if (SessionHandler.areAllClosed()) {
          Logger.debug('All child processes have exited. Quitting app.');
          clearInterval(interval);
          if (process.platform !== 'darwin') {
            app.quit();
          }
        } else {
          Logger.debug('Try to remove disposed sessions.');
          SessionHandler.removeDisposedSessions();
        }
      }, 500);
    });
    app.on('ready', () => {
      Application.setAppConfigDirName(serve);
      let logLevel = LogLevel.ERROR;
      if (serve || debug) {
        logLevel = LogLevel.DEBUG;
      } else if (info) {
        logLevel = LogLevel.INFO;
      }
      Logger.init(Application.getAppConfigDirPath(), logLevel);
      SettingsHandler.init(logLevel);
      DBHandler.init();
      WindowFactory.createWindowWithBounds(serve);
      WindowFactory.onReady(() => {
        UpdateHandler.init();
        UpdateHandler.checkForUpdates();
        DarkModeHandler.init();
      });
      SessionHandler.init(getPath(args));
      ShellHandler.init();
      FontsHandler.init();
      createAppMenu();
    });

    app.on('will-quit', () => {
      Logger.debug('Will Quit.');
      globalShortcut.unregisterAll();
      ipcMain.removeHandler(IpcChannel.OpenFileDialog);
      ipcMain.removeHandler(IpcChannel.Http);
      ipcMain.removeAllListeners();
    });

    app.on('quit', () => {
      Logger.debug('Quit.');
      process.exit(0);
    });

    app.on('activate', () => {
      // On OS X it's common to re-create a window in the app when the
      // dock icon is clicked and there are no other windows open.
      if (!WindowFactory.hasAnyWindow() && SettingsHandler.isInitialized()) {
        Logger.debug('Activate Window');
        WindowFactory.createWindowWithBounds(serve);
      }
    });

    // Only one icon when pinned to taskbar on windows
    app.setAppUserModelId('Biberklatsche.Cogno');


    ipcMain.on(IpcChannel.OpenNewWindow, function (evt) {
      Logger.debug('Open new Window.');
      WindowFactory.createWindowWithBounds(serve);
    });

    ipcMain.on(IpcChannel.ChangeWindowName, function (evt, title) {
      Logger.debug('Change Window Name.', evt.sender, title);
      WindowFactory.updateTitle(evt.sender.id, title);
    });

    ipcMain.on(IpcChannel.OpenExternal, function (evt, link) {
      shell.openExternal(link)
        .then((r) => Logger.debug(`Open '${link}' result: ${r}`))
        .catch(e => Logger.error(`Could not open '${link}'.`, e));
    });

    ipcMain.on(IpcChannel.OpenPath, function (evt, link) {
      shell.openPath(link)
        .then((r) => Logger.debug(`Open '${link}' result: ${r}`))
        .catch(e => Logger.error(`Could not open '${link}'.`, e));
    });

    ipcMain.handle(IpcChannel.OpenFileDialog, function (evt, type: 'image' | 'folder') {
      const properties: ('openDirectory' | 'openFile')[] = type === 'folder' ? ['openDirectory'] : ['openFile'];
      const filters = type === 'folder' ? [] : [{name: 'images', extensions: ['png', 'jpg', 'jpeg', 'svg', 'webp']}];
      return dialog.showOpenDialog({
        properties,
        filters
      });
    });

    ipcMain.handle(IpcChannel.Http, (evt, request) => new Promise((resolve, reject) => {
      try {
        const netRequest = net.request(request);
        netRequest.on('response', (response) => {
          let chunks: Buffer[] = [];
          response.on('data', (chunk) => {
            chunks.push(chunk);
          });
          response.on('end', () => {
            resolve(Buffer.concat(chunks));
          });
          response.on('error', err => {
            Logger.warn('Error http', err);
            resolve(null);
          });
        });
        netRequest.on('error', err => {
          Logger.warn('Error http', err);
          resolve(null);
        });
        netRequest.end();
      } catch (e) {
        Logger.warn('Error http', e);
        resolve(null);
      }
    }));

    ipcMain.handle(IpcChannel.DownloadFile, (evt, data) => new Promise((resolve, reject) => {
        const request = net.request(data.url);
        Logger.info('Download', data.url);
        request.on('response', (response) => {
          const fileStream = fs.createWriteStream(data.destination);

          response.on('data', (chunk) => {
            fileStream.write(chunk);
          });

          response.on('end', () => {
            fileStream.end();
            resolve(null);
            Logger.info('Download complete.');
          });

          request.on('error', (err) => {
            Logger.error('Download error:', err.message);
          });
        });
        request.end();
      }
    ));

    ipcMain.on(IpcChannel.Relaunch, function (evt) {
      app.relaunch();
      app.exit();
    });

    ipcMain.on(IpcChannel.Close, function (evt) {
      WindowFactory.close(evt.sender.id);
    });

    ipcMain.on(IpcChannel.Minimize, function (evt) {
      WindowFactory.minimize(evt.sender.id);
    });

    ipcMain.on(IpcChannel.Maximize, function (evt) {
      WindowFactory.maximize(evt.sender.id);
    });

    ipcMain.on(IpcChannel.Unmaximize, function (evt) {
      WindowFactory.unmaximize(evt.sender.id);
    });

    ipcMain.on(IpcChannel.OpenDevTools, function (evt) {
      WindowFactory.openDevTools(evt.sender.id);
    });


    // check for updates every 24 hours
    setInterval(() => {
      UpdateHandler.checkForUpdates();
    }, 24 * 60 * 60 * 1000);
  }
} catch (e) {
  Logger.error('Error while creating window', e);
}
