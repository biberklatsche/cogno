import {SettingsHandler} from './settings.handler';
import {WindowSettings} from '../shared/models/settings';
import {BrowserWindow, globalShortcut} from 'electron';
import * as path from 'path';
import {IpcChannel} from '../shared/ipc.chanels';
import {WindowState} from '../shared/models/models';
import * as chokidar from 'chokidar';
import {Application} from '../shared/application';
import {ParseResult} from '../shared/parser/settings.parser';
import {platform} from 'os';
import {Logger} from './logger';
import * as electronLocalshortcut from 'electron-localshortcut';

export namespace WindowFactory {
  const windows: BrowserWindow[] = [];
  let settingsWatch: chokidar.FSWatcher = null;
  let startupCompleteListener: Function;

  export function hasAnyWindow(): boolean {
    return windows.length > 0;
  }

  function findByWebcontentsId(id: number): BrowserWindow {
    return windows.find(w => w.webContents.id === id);
  }

  export function updateTitle(webContentsId: number, title: any) {
    const window = findByWebcontentsId(webContentsId);
    window.title = title;
  }

  export function minimize(webContentsId: number) {
    findByWebcontentsId(webContentsId)?.minimize();
  }

  export function maximize(webContentsId: number) {
    findByWebcontentsId(webContentsId)?.maximize();
  }


  export function unmaximize(webContentsId: number) {
    findByWebcontentsId(webContentsId)?.unmaximize();
  }

  export function close(webContentsId: number) {
    findByWebcontentsId(webContentsId)?.close();
  }

  export function openDevTools(webContentsId: number) {
    findByWebcontentsId(webContentsId)?.webContents.openDevTools({mode: 'detach'});
  }

  export function createWindowWithBounds(serve: boolean) {
    SettingsHandler.getWindowSettings(data => {
      Logger.debug('start load settings');
      SettingsHandler.loadSettings(settings => {
        Logger.debug('load settings finished');
        Logger.debug('start create window');
        const window = createWindow(data, settings, serve);
        Logger.debug('create window finished');
        setWindowShortcuts(settings, window);
      });
    });
    if (!settingsWatch) {
      settingsWatch = SettingsHandler.watchSettings();
      settingsWatch.on('change', (filePath, stats) => {
        waitForFinalEvent(() => {
          if (filePath) {
            Logger.debug('Settings file changed', filePath);
            SettingsHandler.loadSettings(updatedSettings => {
              Logger.debug('Settings file loaded', JSON.stringify(updatedSettings, null, 4));
              setWindowShortcuts(updatedSettings);
              windows.forEach(window => window?.webContents.send(IpcChannel.SettingsLoaded, updatedSettings));
            });
          }
        }, 500, 'settingsChanged');
      });
    }
  }

  function setWindowShortcuts(settingsResult: ParseResult, browserWindow?: BrowserWindow) {
    if (!settingsResult.isValid) {
      return;
    }
    const mainWindow = windows[0];
    if (!mainWindow) {
      return;
    }
    globalShortcut.unregisterAll();
    globalShortcut.register(settingsResult.settings.shortcuts.bringToFront, () => {
      if (mainWindow.isFocused()) {
        mainWindow.minimize();
      } else {
        mainWindow.show();
        mainWindow.focus();
      }
    });
    const ws = browserWindow ? [browserWindow] : windows;
    for (const window of ws) {
      electronLocalshortcut.unregisterAll(window);
      electronLocalshortcut.register(window, settingsResult.settings.shortcuts.openDevTools, () => window.webContents.openDevTools({mode: 'right'}));
    }
  }

  function createRendererWindow(data: WindowSettings, settings: ParseResult): BrowserWindow {
    const defaultTheme = settings?.settings?.themes?.find(t => t.isDefault);
    const showFrame = process.platform === 'darwin';
    const window = new BrowserWindow({
      width: data.bounds.width,
      height: data.bounds.height,
      minWidth: 850,
      minHeight: 372,
      backgroundColor: defaultTheme?.colors?.background || '#000000',
      frame: showFrame,
      titleBarStyle: 'hidden', // on macos the traffic lights are visible even this is hidden
      show: false,
      webPreferences: {
        nodeIntegrationInWorker: true,
        nodeIntegration: true,
        webSecurity: false, // background image
        enableWebSQL: false,
        spellcheck: false,
        experimentalFeatures: false,
        contextIsolation: false
      }
    });
    Logger.debug(`create browser window, id:  ${window.id}`);
    window.setMenu(null);
    if (data.isInBounds && windows.length === 1) {
      window.setPosition(data.bounds.x, data.bounds.y);
    } else {
      window.center();
    }
    return window;
  }

  function createWindow(data: WindowSettings, settings: ParseResult, serve: boolean): BrowserWindow {

    // Create the browser window.
    const window = createRendererWindow(data, settings);
    // Check if the window its outside of the view (ex: multi monitor setup)

    windows.push(window);

    Logger.debug('Window load index.html');
    if (serve) {
      window.loadURL('http://localhost:4600');
    } else {
      window.loadFile(path.join(__dirname, '../../renderer/index.html'));
    }

    // Emitted when the window is closed.
    window.on('closed', () => {
      // Dereference the window object, usually you would store window
      // in an array if your app supports multi windows, this is the time
      // when you should delete the corresponding element.
      Logger.debug(`remove window with id: ${window.id}`);
      const index = windows.indexOf(window);
      windows.splice(index, 1);
      Logger.debug(`window removed ${windows.length}`);
      if (!hasAnyWindow()) {
        settingsWatch?.close();
        settingsWatch = null;
      }
    });
    window.once('ready-to-show', () => {
      Logger.debug('Window is ready to show');
      window.show();
      window?.webContents.on('did-finish-load', () => {
        Logger.debug('Window did finish load');
        window?.webContents.setZoomFactor(1);
        window?.webContents.setVisualZoomLevelLimits(1, 1);
        window?.webContents.send(IpcChannel.AppDirs, {
          'configDir': Application.getAppConfigDirPath(),
          'appDir': Application.getAppPath(),
          'tempDir': Application.getTempPath()
        });
        window?.webContents.send(IpcChannel.Version, Application.getVersion());
        window?.webContents.send(IpcChannel.SettingsLoaded, settings);
        if (window.isFullScreen() || window.isSimpleFullScreen()) {
          window?.webContents.send(IpcChannel.WindowStateChanged, WindowState.Maximized);
        } else if (window.isMaximized() && platform() !== 'darwin') {
          window?.webContents.send(IpcChannel.WindowStateChanged, WindowState.Maximized);
        } else {
          window?.webContents.send(IpcChannel.WindowStateChanged, WindowState.Unmaximized);
        }
        if (windows.length === 1) {
          startupCompleteListener();
        }
      });
    });

    window.on('resize', () => {
      waitForFinalEvent(() => {
        SettingsHandler.saveWindowSettings(window);
      }, 500, 'resize');
      window?.webContents.send(IpcChannel.OnResize);
    });

    window.on('move', () => {
      waitForFinalEvent(() => {
        SettingsHandler.saveWindowSettings(window);
      }, 500, 'move');
    });

    window.on('maximize', () => {
      window?.webContents.send(IpcChannel.WindowStateChanged, WindowState.Maximized);
    });

    window.on('enter-full-screen', () => {
      window?.webContents.send(IpcChannel.WindowStateChanged, WindowState.FullScreen);
    });

    window.on('leave-full-screen', () => {
      window?.webContents.send(IpcChannel.WindowStateChanged, WindowState.Unmaximized);
    });

    window.on('unmaximize', () => {
      window?.webContents.send(IpcChannel.WindowStateChanged, WindowState.Unmaximized);
    });
    return window;
  }

  export function sendBroadcast(channel: string, ...args: any[]) {
    windows.forEach(window => window.webContents.send(channel, args));
  }

  export function sendMain(channel: string, ...args: any[]) {
    const mainWindow = windows[0];
    if (!mainWindow) {
      return;
    }
    mainWindow.webContents.send(channel, args);
  }

  export function send(webContentsId: number, channel: string, ...args: any[]) {
    findByWebcontentsId(webContentsId)?.webContents.send(channel, args);
  }

  export function onReady(listener: Function) {
    startupCompleteListener = listener;
  }
}

const waitForFinalEvent = (function () {
  const timers = {};
  return function (callback, ms, uniqueId) {
    if (!uniqueId) {
      uniqueId = 'Don\'t call this twice without a uniqueId';
    }
    if (timers[uniqueId]) {
      clearTimeout(timers[uniqueId]);
    }
    timers[uniqueId] = setTimeout(callback, ms);
  };
})();
