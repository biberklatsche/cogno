import {Logger} from './logger';
import {IpcChannel} from '../shared/ipc.chanels';
import {DBRequest, DBRequestType, Id, IDbContext} from '../shared/db-context';
import Nedb from '@seald-io/nedb';
import {StringUtils} from '../shared/string-utils';
import * as fs from 'node:fs';
import * as path from 'node:path';
import {Application} from '../shared/application';
import {ipcMain} from 'electron';

class DbContext {

  private _dbs: { [name: string]: Nedb } = {};

  constructor() {
  }

  private existsDb(db: string) {
    return !!this._dbs[StringUtils.toFileName(db)];
  }

  public existsDbFile(db: string): boolean {
    const dbName = StringUtils.toFileName(db);
    const path = this.createPath(dbName);
    return fs.existsSync(path);
  }

  public async createDb(db: string): Promise<boolean> {
    const dbName = StringUtils.toFileName(db);
    if (this._dbs[dbName]) {
      return;
    }
    const path = this.createPath(dbName);
    const builder = require('@seald-io/nedb');
    let datastore: Nedb =new builder({
      filename: this.createPath(dbName),
      autoload: false
    });
    try {
      await datastore.loadDatabaseAsync();
      this._dbs[dbName] = datastore;
      Logger.debug('loaded db', path);
      return true;
    } catch (e) {
      Logger.error('could not load db', e);
      return false;
    }
  }

  private createPath(dbName: string): string {
    return path.join(this.getDbDirectoryPath(), `${dbName}.json`);
  }

  public getDbDirectoryPath(): string {
    return path.join(Application.getAppConfigDirPath(), 'db');
  }

  public init(db: string): Promise<boolean> {
    if (!this.existsDb(db)) {
      return this.createDb(db);
    }
    return Promise.resolve(true);
  }

  public findOne<T>(query: any, db: string): Promise<T> {
    const dbName = StringUtils.toFileName(db);
    return this._dbs[dbName].findOneAsync(query);
  }

  public find<T>(query: any, db: string): Promise<T[]> {
    const dbName = StringUtils.toFileName(db);
    return this._dbs[dbName].findAsync(query);
  }

  public add<T>(element: T, db: string): Promise<T> {
    const dbName = StringUtils.toFileName(db);
    return this._dbs[dbName].insertAsync(element);
  }

  public addAll<T>(elements: T[], db: string): Promise<T[]> {
    const dbName = StringUtils.toFileName(db);
    return this._dbs[dbName].insertAsync(elements);
  }


  public async update<T extends Id>(element: T, db: string): Promise<void> {
    const dbName = StringUtils.toFileName(db);
    await this._dbs[dbName].updateAsync({_id: element._id}, element);
  }

  public async remove(id: string, db: string): Promise<void> {
    const dbName = StringUtils.toFileName(db);
    await this._dbs[dbName].remove({_id: id}, {});
  }

  public async exists(id: string, db: string): Promise<boolean> {
    const dbName = StringUtils.toFileName(db);
    const elem = await this.findOne({_id: id}, dbName);
    return !!elem;
  }

  public async upsert<T extends Id>(element: T, db: string): Promise<void> {
    const dbName = StringUtils.toFileName(db);
    await this._dbs[dbName].updateAsync({_id: element._id}, element, {upsert: true});
  }

  public async getAll<T extends Id>(dbName: string): Promise<T[]> {
    return this.find<T>({}, dbName);
  }

  public async reload(): Promise<void> {
    for (const dbName in this._dbs) {
      await this._dbs[dbName].loadDatabaseAsync();
    }
  }
}

export namespace DBHandler {
  let context: DbContext;

  export function init() {
    Logger.debug('Init db handler');
    context = new DbContext();
    ipcMain.handle(IpcChannel.DB, async function<T extends Id>(evt, event: DBRequest<T>) {
      return new Promise<any>((resolve, reject) => {
        try {
          switch (event.requestType) {
            case 'init': {
              resolve(context.init(event.dbName));
              break;
            }
            case 'exists': {
              resolve(context.exists(event.id, event.dbName));
              break;
            }
            case 'existsFile': {
              resolve(context.existsDbFile(event.dbName));
              break;
            }
            case 'findOne': {
              resolve(context.findOne(event.query, event.dbName));
              break;
            }
            case 'upsert': {
              resolve(context.upsert(event.element, event.dbName));
              break;
            }
            case 'remove': {
              resolve(context.remove(event.id, event.dbName));
              break;
            }
            case 'find': {
              resolve(context.find(event.query, event.dbName));
              break;
            }
            case 'add': {
              resolve(context.add(event.element, event.dbName));
              break;
            }
            case 'addAll': {
              resolve(context.addAll(event.elements, event.dbName));
              break;
            }
            case 'rootDir': {
              resolve(context.getDbDirectoryPath());
              break;
            }
            case 'update': {
              resolve(context.update(event.element, event.dbName));
              break;
            }
            case 'reload': {
              resolve(context.reload());
              break;
            }
          }
        } catch (e) {
          Logger.error('Error request db', e);
          reject();
        }
      });
    });
  }


}
