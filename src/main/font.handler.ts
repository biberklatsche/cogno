import {IpcChannel} from '../shared/ipc.chanels';
import {ipcMain} from 'electron';
import {FontDescriptor} from 'fontmanager-redux';
import {Logger} from './logger';

const fontManager = require('fontmanager-redux');

export namespace FontsHandler {
  export function init() {
    Logger.debug('Init fonts handler');
    ipcMain.handle(IpcChannel.GetAvailableFonts, async function (evt) {
      return new Promise<string[]>((resolve, reject) => {
        try {
          fontManager.findFonts({monospace: true}, (f: FontDescriptor[]) => {
            try {
              Logger.debug('Found following fonts', f);
              let fonts = f?.map(x => x.family.trim());
              fonts = fonts.filter((item, index) => fonts.indexOf(item) === index);
              fonts = fonts.filter(f => !!f);
              fonts = fonts && fonts.length > 0 ? fonts : ['monospace'];
              resolve(fonts);
            } catch (e) {
              Logger.error('Error load fonts', e);
              resolve(['monospace']);
            }
          });
        } catch (e) {
          Logger.error('Error load fonts', e);
          resolve(['monospace']);
        }
      });
    });

    ipcMain.handle(IpcChannel.GetAvailableAppFonts, async function (evt) {
      return new Promise<string[]>((resolve, reject) => {
        try {
          fontManager.findFonts({}, (f: FontDescriptor[]) => {
            try {
              Logger.debug('Found following app fonts', f);
              let fonts = f?.map(x => x.family.trim());
              fonts = fonts.filter((item, index) => fonts.indexOf(item) === index);
              fonts = fonts.filter(f => !!f);
              if(fonts.indexOf('Roboto') === -1)
              {
                fonts.unshift('Roboto');
              }
              fonts = fonts && fonts.length > 0 ? fonts : ['Roboto'];
              resolve(fonts);
            } catch (e) {
              Logger.error('Error load fonts', e);
              resolve(['Roboto']);
            }
          });
        } catch (e) {
          Logger.error('Error load fonts', e);
          resolve(['Roboto']);
        }
      });
    });
  }
}
