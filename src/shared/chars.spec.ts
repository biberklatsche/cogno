import {Char, Chars} from './chars';

describe('Chars', () => {

  it('should convert unicode - null, empty, whitespace', () => {
    expect(Char.convertUnicode(null)).toBe(null);
    expect(Char.convertUnicode('')).toBe('');
    expect(Char.convertUnicode(' ')).toBe(' ');
    expect(Char.convertUnicode('\\t')).toBe('\\t');
  });

  it('should convert unicode - nothing to convert', () => {
    expect(Char.convertUnicode('abc')).toBe('abc');
  });

  it('should convert unicode', () => {
    expect(Char.convertUnicode('🥳')).toBe('\\U1f973');
    expect(Char.convertUnicode('🥳abc')).toBe('\\U1f973abc');
    expect(Char.convertUnicode('abc🥳abc')).toBe('abc\\U1f973abc');
  });
});
