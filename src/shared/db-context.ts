export type Id = {
  _id: string;
}

export interface IDbContext {
  init(db: string): Promise<boolean>;

  exists(id: string, db: string): Promise<boolean>;

  findOne<T extends Id>(query: any, db: string): Promise<T>;

  find<T extends Id>(query: any, db: string): Promise<T[]>;

  upsert<T extends Id>(element: T, db: string): Promise<void>;

  remove(id: string, db: string): Promise<void>;

  getAll<T extends Id>(db: string): Promise<T[]>;

  add<T extends Id>(element: T, db: string): Promise<T>;

  addAll<T extends Id>(elements: T[], db: string): Promise<T[]>;

  update<T extends Id>(element: T, db: string): Promise<void>;

  loadData<T>(dbOld: string): Promise<T[]>;

  reload(): Promise<void>;

  existsDbFile(commandsDbName: string): Promise<boolean>;

  getDbDirectoryPath(): Promise<string>;
}

export type DBRequestType = 'init' | 'exists' | 'findOne' | 'upsert' | 'remove' | 'find' | 'add' | 'addAll' | 'update' | 'reload' | 'existsFile' | 'rootDir';

type BaseDBRequest = {
  requestType: DBRequestType
}

type BaseNamedDBRequest = BaseDBRequest & {
  dbName: string;
}

export type DBInit = BaseNamedDBRequest & {requestType: 'init'};
export type DBExists = BaseNamedDBRequest & {requestType: 'exists', id: string};
export type DBExistsFile = BaseNamedDBRequest & {requestType: 'existsFile'};
export type DBDirectoryPath = BaseDBRequest & {requestType: 'rootDir'};
export type DBAdd<T> = BaseNamedDBRequest & {requestType: 'add'} & {
  element: T;
};
export type DBUpdate<T> = BaseNamedDBRequest & {requestType: 'update'} & {
  element: T;
};
export type DBAddAll<T> = BaseNamedDBRequest & {requestType: 'addAll'} & {
  elements: T[];
};
export type DBFindOne = BaseNamedDBRequest & {requestType: 'findOne'} & {
  query: any
}
export type DBFind = BaseNamedDBRequest & {requestType: 'find'} & {
  query: any
}

export type DBUpsert<T> = BaseNamedDBRequest & {requestType: 'upsert'} & {
  element: T;
};

export type DBRemove = BaseNamedDBRequest & {requestType: 'remove'} & {
  id: string;
}

export type DBReload = BaseDBRequest & {requestType: 'reload'}

export type DBRequest<T extends Id> = DBInit | DBExists | DBFindOne | DBAdd<T> | DBUpdate<T> | DBAddAll<T> | DBFind | DBUpsert<T> | DBRemove | DBReload | DBExistsFile | DBDirectoryPath;
