import {ShellType} from './models/models';
import {ShellLocation} from '../renderer/app/common/shellLocation';

export class Normalizer {

  private readonly defaultCommandConnector: string;
  private readonly defaultCommandConnectorPre: string;
  private readonly defaultCommandConnectorPost: string;
  private readonly commandConnectorRegexp: string;

  public constructor(public location: ShellLocation) {
    switch (location.shellType) {
      case ShellType.Powershell:
        this.defaultCommandConnector = ';';
        this.defaultCommandConnectorPre = '';
        this.defaultCommandConnectorPost = '';
        this.commandConnectorRegexp = '(?:\\s?;\\s)';
        break;
      default:
        this.defaultCommandConnector = '&&';
        this.defaultCommandConnectorPre = '(';
        this.defaultCommandConnectorPost = ')';
        this.commandConnectorRegexp = '(?:\\s\\&&\\s)|(?:\\s\\|\\|\\s)|(?:\\s&\\s)|(?:\\s?;\\s)';
    }
  }

  static normalizePath(path: string): string {
    path = path
      .replace(/\"/g, '') // remove '"'
      .replace(/\'/g, '') // remove '''
      .replace(/`\s/g, ' ') // remove '` '
      .replace(/\\\s/g, ' ')  // remove '\\ '
      .replace(/\\/g, '/'); // replace '\\' width '/'
    const splits = path.split(':');
    return splits.length > 1 ? splits[0].toLowerCase() + splits[1] : splits[0];
  }

  static diff(currentInput: string, selectedCommand: string): string {
    const diff: string[] = [];
    let currentInputIndex = 0;
    let currentCompareIndex = 0;
    while (currentInputIndex < currentInput.length) {
      const charCurrentInput = currentInput.charAt(currentInputIndex);
      const charCompare = selectedCommand.charAt(currentCompareIndex);
      if (charCurrentInput !== charCompare) {
        diff.push(charCurrentInput);
      } else {
        currentCompareIndex++;
      }
      currentInputIndex++;
    }
    return diff.join('');
  }

  static getDirectories(path: string): string[] {
    if (!path) {return [];}
    const normalized = Normalizer.normalizePath(path);
    return normalized.split('/').filter(s => s !== null && s !== '' && s !== '.');
  }

  static joinDirectories(directoriesA: string[], directoriesB: string[]) {
    const dirsA = directoriesA.filter(d => d !== '.');
    const dirsB = directoriesB.filter(d => d !== '.');
    if (dirsA.length > 0 && dirsA[0] === '..'){
      throw new Error(`Could not join ${dirsA.join(',')} with ${dirsB.join(',')}`);
    }
    return Normalizer.removeDotDots([...dirsA, ...dirsB]);
  }

  private static removeDotDots(directories: string[]): string[] {
    for (let i = 0; i < directories.length; i++) {
      if (directories[i] === '..') {
        if (i > 0) {
          directories.splice(i - 1, 2);
        } else {
          directories.splice(i, 1);
        }
        Normalizer.removeDotDots(directories);
        break;
      }
    }
    return directories;
  }

  toPath(directories: string[], fromRoot = false): string {
    switch (this.location.shellType) {
      case ShellType.Powershell:
        if(fromRoot) {
          const drive = directories[0];
          const path = directories.reduce((a, v, currentIndex) => (currentIndex > 1 ? a : '') + (currentIndex > 1 ? '\\' : '') + v.replace(' ', '` '), '');
          return `${drive}:\\${path}`;
        } else {
          return directories.reduce((a, v, currentIndex) => a + (currentIndex > 0 ? '\\' : '') + v.replace(' ', '` '), '');
        }
      default:
        return directories.reduce((a, v, currentIndex) => a + (currentIndex > 0 || fromRoot ? '/' : '') + v.replace(' ', '\\ '), '');
    }
  }

  isNavigation(command: string): boolean {
    return command.startsWith('cd') || command.endsWith(':') && command.length === 2;
  }

  isConnected(command): boolean {
    return new RegExp(this.commandConnectorRegexp).test(command);
  }

  split(command: string): string[] {
    return this.commandConnectorRegexp ? command.split(new RegExp(this.commandConnectorRegexp, 'g')).filter(f => !!f) : [command];
  }

  combine(commands: string[], addConnectorAsPostix = false): string {
    const cs = commands.filter(c => !!c && c.length > 0);
     const connected = cs.join(` ${this.defaultCommandConnector} `) + (addConnectorAsPostix ? ` ${this.defaultCommandConnector}` : '');
     return addConnectorAsPostix || cs.length <= 1 ? connected : `${this.defaultCommandConnectorPre}${connected}${this.defaultCommandConnectorPost}`;
  }
}
