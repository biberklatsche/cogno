import {InjectionType, ShellType} from './models';
import {FontWeight} from '@xterm/xterm';
import {Shortcuts} from './shortcuts';
import {LogLevel} from '../loglevel';

export interface Settings {
  themes: Theme[];
  shells: ShellConfig[];
  autocomplete: AutocompleteConfig;
  shortcuts: Shortcuts;
  general: GeneralConfig;
  logLevel?: LogLevel;
  appConfigDirPath?: string;
}

export interface GeneralConfig {
  enableCopyOnSelect: boolean;
  openTabInSameDirectory: boolean;
  enableTelemetry: boolean;
  enablePasteOnRightClick: boolean;
  scrollbackLines: number;
}

export interface Theme extends HasName {
  removeFullScreenAppPadding?: boolean;
  terminalSexyId?: string;
  isInstalled?: boolean;
  author?: string;
  fontsize: number;
  appFontsize: number;
  appFontFamily: string;
  fontFamily: string;
  cursorWidth?: number;
  cursorBlink?: boolean;
  cursorStyle?: 'underline' | 'bar';
  fontWeight?: FontWeight;
  fontWeightBold?: FontWeight;
  colors: Colors;
  image?: string;
  imageOpacity?: number;
  imageBlur?: number;
  isImageAvailable?: boolean;
  padding: string;
  paddingAsArray: number[];
  prompt: string;
  promptVersion?: number;
  enableLigatures?: boolean;
  enableWebgl?: boolean;
  isDarkModeTheme?: boolean;
  isDefault?: boolean;
  isActive?: boolean;
}

export interface HasName {
  name: string;
}

export interface Colors {
  /** The default foreground color */
  foreground: string;
  /** The default background color */
  background: string;
  /** The hightlight color */
  highlight: string;
  /** The cursor color */
  cursor?: string;
  /** ANSI black (eg. `\x1b[30m`) */
  black?: string;
  /** ANSI red (eg. `\x1b[31m`) */
  red?: string;
  /** ANSI green (eg. `\x1b[32m`) */
  green?: string;
  /** ANSI yellow (eg. `\x1b[33m`) */
  yellow?: string;
  /** ANSI blue (eg. `\x1b[34m`) */
  blue?: string;
  /** ANSI magenta (eg. `\x1b[35m`) */
  magenta?: string;
  /** ANSI cyan (eg. `\x1b[36m`) */
  cyan?: string;
  /** ANSI white (eg. `\x1b[37m`) */
  white?: string;
  /** ANSI bright black (eg. `\x1b[1;30m`) */
  brightBlack?: string;
  /** ANSI bright red (eg. `\x1b[1;31m`) */
  brightRed?: string;
  /** ANSI bright green (eg. `\x1b[1;32m`) */
  brightGreen?: string;
  /** ANSI bright yellow (eg. `\x1b[1;33m`) */
  brightYellow?: string;
  /** ANSI bright blue (eg. `\x1b[1;34m`) */
  brightBlue?: string;
  /** ANSI bright magenta (eg. `\x1b[1;35m`) */
  brightMagenta?: string;
  /** ANSI bright cyan (eg. `\x1b[1;36m`) */
  brightCyan?: string;
  /** ANSI bright white (eg. `\x1b[1;37m`) */
  brightWhite?: string;

  commandRunning?: string;
  commandSuccess?: string;
  commandError?: string;

  promptColors: {
    foreground?: ColorName;
    background?: ColorName;
  }[];
}

export enum ColorName {
  foreground = 'foreground',
  background = 'background',
  highlight = 'highlight',
  cursor = 'cursor',
  black = 'black',
  red = 'red',
  green = 'green',
  yellow = 'yellow',
  blue = 'blue',
  magenta = 'magenta',
  cyan = 'cyan',
  white = 'white',
  brightBlack = 'brightBlack',
  brightRed = 'brightRed',
  brightGreen = 'brightGreen',
  brightYellow = 'brightYellow',
  brightBlue = 'brightBlue',
  brightMagenta = 'brightMagenta',
  brightCyan = 'brightCyan',
  brightWhite = 'brightWhite',
  commandRunning = 'commandRunning',
  commandSuccess = 'commandSuccess',
  commandError = 'commandError',
}

export interface ShellConfig extends HasName {
  id?: string;
  path: string;
  type: ShellType;
  injectionType: InjectionType;
  default?: boolean;
  workingDir?: string;
  isSelected?: boolean;
  isSubshell?: boolean;
  isDebugModeEnabled?: boolean;
  usesFinalSpacePromptTerminator?: boolean;
  promptTerminator?: string;
  args?: string[];
  remoteType?: ShellType;
  remoteCommand?: string;
  machineName?: string;
}

export interface AutocompleteConfig {
  ignore: string[];
  position: 'cursor' | 'line';
  mode: 'always' | 'shortcut';
}

export interface WindowSettings {
  isMaximized: boolean;
  isInBounds: boolean;
  bounds: Bounds;

}

export interface Bounds {
  width: number;
  height: number;
  x?: number;
  y?: number;
}
