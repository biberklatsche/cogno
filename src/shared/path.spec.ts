import {Path} from './path';
import {ShellType} from './models/models';

describe('Path', () => {

  it('should return correct path', () => {
    expect(Path.toShellPath(null, ShellType.Bash)).toBe('/');
    expect(Path.toShellPath([''], ShellType.Bash)).toBe('/');
    expect(Path.toShellPath(['~'], ShellType.Bash)).toBe('~');
    expect(Path.toShellPath(['c'], ShellType.Bash)).toBe('/c');
    expect(Path.toShellPath(['c'], ShellType.Powershell)).toBe('c:/');
    expect(Path.toShellPath(['d'], ShellType.Powershell)).toBe('d:/');
    expect(Path.toShellPath(['abc'], ShellType.Bash)).toBe('/abc');
    expect(Path.toShellPath(['c', 'aaa', 'bbb'], ShellType.Powershell)).toBe('c:/aaa/bbb');
    expect(Path.toShellPath(['d', 'aaa', 'bbb'], ShellType.Powershell)).toBe('d:/aaa/bbb');
  });
});
