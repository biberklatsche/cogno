import {ShellType} from './models/models';

export namespace Path {
  export function toOsPath(directories: string[]): string {
    return toShellPath(directories, process.platform === 'win32' ? ShellType.Powershell : ShellType.Bash);
  }

  export function toShellPath(directories: string[], shellType: ShellType): string {
    if(!directories) { return '/'; }
    if (shellType === ShellType.Powershell) {
      const drive = directories[0];
      const other = directories.slice(1, directories.length);
      return `${drive}:/${other.join('/')}`;
    } else {
      const path = `/${directories.join('/')}`;
      return path.replace('/~', '~');
    }
  }
}
