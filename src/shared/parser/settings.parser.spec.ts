import {Settings} from '../models/settings';
import {SettingsParser} from './settings.parser';
import {TestHelper} from '../../test/helper';


describe('settings-validator', () => {

  let settings: Settings;

  beforeEach(() => {
    settings = TestHelper.createSettings();
  });

  it('should return valid false if string is undefined', () => {
    expect(SettingsParser.parseSettings(undefined).isValid).toBeFalsy();
  });

  it('should return valid false if string is null', () => {
    expect(SettingsParser.parseSettings(null).isValid).toBeFalsy();
  });

  it('should return valid false if string is empty', () => {
    expect(SettingsParser.parseSettings('').isValid).toBeFalsy();
  });

  it('should return valid false if string is empty object', () => {
    expect(SettingsParser.parseSettings('{}').isValid).toBeFalsy();
  });

  it('should return valid false if string is empty array', () => {
    expect(SettingsParser.parseSettings('[]').isValid).toBeFalsy();
  });

  it('should return valid false if theme contains invalid colors', () => {
    settings.themes[0].colors.highlight = 'abx';
    expect(SettingsParser.parseSettings(JSON.stringify(settings)).isValid).toBeFalsy();
  });

  it('should return valid true if string is correct', () => {
    const result = SettingsParser.parseSettings(JSON.stringify(settings));
    expect(result.isValid).toBeTruthy();
  });

  it('should return valid true if part is missing and enrich is true', () => {
    settings.general = undefined;
    const result = SettingsParser.parseSettings(JSON.stringify(settings), true);
    expect(result.isValid).toBeTruthy();
  });

  it('should enrich shortcuts', () => {
    settings.shortcuts.nextArgument = undefined;
    const result = SettingsParser.parseSettings(JSON.stringify(settings), true);
    expect(result.settings.shortcuts.nextArgument).toBeTruthy();
  });

  it('should enrich themes', () => {
    settings.themes.push(TestHelper.createTheme());
    settings.themes[1].image = 'abc';
    const result = SettingsParser.parseSettings(JSON.stringify(settings), true);
    expect(result.settings.themes[1].image).toBe('');
  });
});
