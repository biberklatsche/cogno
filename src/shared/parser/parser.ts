  export function addErrors(result: ParseResult, ...errors: string[]): void {
  if (errors && errors.length > 0) {
    const e = errors.filter(error => {
      return !!error && error !== '';
    });
    if (e && e.length === 0) {
      return;
    }
    if (!result.errors) {
      result.errors = [];
    }
    result.isValid = false;
    result.errors = result.errors.concat(e);
  }
}

export function addWarnings(result: ParseResult, ...warnings: string[]): void {
  if (warnings && warnings.length > 0) {
    const w = warnings.filter(warning => {
      return !!warning && warning !== '';
    });
    if (w && w.length === 0) {
      return;
    }
    if (!result.warnings) {
      result.warnings = [];
    }
    result.hasWarnings = true;
    result.warnings = result.warnings.concat(w);
  }
}

export interface ParseResult {
  isValid: boolean;
  errors?: string[];
  hasWarnings: boolean;
  warnings?: string[];
}
