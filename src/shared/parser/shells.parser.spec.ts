import {ShellConfig} from '../models/settings';
import {ShellConfigParser} from './shells.parser';
import {TestHelper} from '../../test/helper';

describe('shell-validator', () => {

  let shells: ShellConfig[];

  beforeEach(() => {
    shells = createShells();
  });

  it('should return valid false if string is undefined', () => {
    expect(ShellConfigParser.parseShellConfig(undefined).isValid).toBeFalsy();
  });

  it('should return valid false if string is null', () => {
    expect(ShellConfigParser.parseShellConfig(null).isValid).toBeFalsy();
  });

  it('should return valid false if string is empty', () => {
    expect(ShellConfigParser.parseShellConfig('').isValid).toBeFalsy();
  });

  it('should return valid false if string is empty object', () => {
    expect(ShellConfigParser.parseShellConfig('{}').isValid).toBeFalsy();
  });

  it('should return valid true if string is empty array', () => {
    expect(ShellConfigParser.parseShellConfig('[]').isValid).toBeTruthy();
  });

  it('should return valid true if string is array of shells', () => {
    expect(ShellConfigParser.parseShellConfig(JSON.stringify(shells)).isValid).toBeTruthy();
  });

  it('should enrich injectionType', () => {
    shells[0].injectionType = undefined;
    expect(ShellConfigParser.parseShellConfig(JSON.stringify(shells)).shellConfigs[0].injectionType).toBe('Auto');
  });

  it('should enrich injectionType if isAdvancedEnabled is set to false', () => {
    shells[0]['isAdvancedEnabled'] = false;
    shells[0].injectionType = undefined;
    expect(ShellConfigParser.parseShellConfig(JSON.stringify(shells)).shellConfigs[0].injectionType).toBe('Manual');
  });

  it('should enrich injectionType if isAdvancedEnabled is set to true', () => {
    shells[0]['isAdvancedEnabled'] = true;
    shells[0].injectionType = undefined;
    expect(ShellConfigParser.parseShellConfig(JSON.stringify(shells)).shellConfigs[0].injectionType).toBe('Auto');
  });
});

function createShells(): ShellConfig[] {
  return [createShell('test')];
}

function createShell(name: string): ShellConfig {
  return {...TestHelper.createShellConfig(), name};
}
