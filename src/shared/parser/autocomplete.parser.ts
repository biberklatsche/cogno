import {AutocompleteConfig} from '../models/settings';
import {ParseResult} from './settings.parser';
import * as JSON5 from 'json5';
import {addErrors} from './parser';
import {DEFAULT_SETTINGS} from '../default-settings';

export namespace AutocompleteParser {
  export function validateAutocompleteConfig(general: AutocompleteConfig, enrich = false): AutocompleteResult {
    try {
      const result = {isValid: true, autocompleteConfig: general, hasWarnings: false};
      validate(result, enrich);
      return result;
    } catch (e) {
      return {isValid: false, errors: [e.message], hasWarnings: false};
    }
  }

  export function parseAutocompleteConfig(generalAsString: string): AutocompleteResult {
    try {
      return validateAutocompleteConfig(JSON5.parse(generalAsString) as AutocompleteConfig);
    } catch (e) {
      return {isValid: false, errors: [e.message], hasWarnings: false};
    }
  }

  function validate(result: AutocompleteResult, enrich = false) {
    if (enrich && !result.autocompleteConfig) {
      result.autocompleteConfig = DEFAULT_SETTINGS.autocomplete;
    }
    addErrors(result, !result.autocompleteConfig ? 'The "autocomplete" property is missing.' : null);
    if(result.autocompleteConfig['moveWithCursor'] !== undefined && result.autocompleteConfig.position === undefined) {
      result.autocompleteConfig.position = result.autocompleteConfig['moveWithCursor'] ? 'cursor' : 'line';
    }
    if (enrich && result.autocompleteConfig.position === undefined) {
      result.autocompleteConfig.position = 'cursor';
    }
    addErrors(result, result.autocompleteConfig.position === undefined ? 'The "position" property in "autocomplete" is missing.' : null);
    if (enrich && result.autocompleteConfig.mode === undefined) {
      result.autocompleteConfig.mode = 'always';
    }
    addErrors(result, result.autocompleteConfig.mode === undefined ? 'The "mode" property in "autocomplete" is missing.' : null);
  }
}


export interface AutocompleteResult extends ParseResult {
  autocompleteConfig?: AutocompleteConfig;
}
