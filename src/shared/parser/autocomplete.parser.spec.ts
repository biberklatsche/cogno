import {AutocompleteConfig} from '../models/settings';
import {AutocompleteParser} from './autocomplete.parser';

describe('autocomplete-validator', () => {

  let autocompleteConfig: AutocompleteConfig;

  beforeEach(() => {
    autocompleteConfig = {
      mode: 'always',
      position: 'cursor',
      ignore: ['test']
    };
  });

  it('should return valid false if string is undefined', () => {
    expect(AutocompleteParser.parseAutocompleteConfig(undefined).isValid).toBeFalsy();
  });

  it('should return valid false if string is null', () => {
    expect(AutocompleteParser.parseAutocompleteConfig(null).isValid).toBeFalsy();
  });

  it('should return valid false if string is empty', () => {
    expect(AutocompleteParser.parseAutocompleteConfig('').isValid).toBeFalsy();
  });

  it('should return valid false if string is empty object', () => {
    expect(AutocompleteParser.parseAutocompleteConfig('{}').isValid).toBeFalsy();
  });

  it('should return valid false if string is empty array', () => {
    expect(AutocompleteParser.parseAutocompleteConfig('[]').isValid).toBeFalsy();
  });

  it('should return valid true if is valid', () => {
    expect(AutocompleteParser.parseAutocompleteConfig(JSON.stringify(autocompleteConfig)).isValid).toBeTruthy();
  });
});
