import {ColorName, Theme} from '../models/settings';
import {Color} from '../color';
import * as JSON5 from 'json5';
import {ParseResult} from './settings.parser';
import {addErrors} from './parser';
import * as path from 'node:path';
import * as fs from 'node:fs';

export namespace ThemesParser {

  export function validateThemes(themes: Theme[], enrich = false): ThemesResult {
    try {
      const result = {isValid: true, themes: themes, hasWarnings: false};
      validate(result, enrich);
      return result;
    } catch (e) {
      return {isValid: false, errors: [e.message], hasWarnings: false};
    }
  }

  export function parseThemes(themeAsJson: string): ThemesResult {
    try {
      return validateThemes(JSON5.parse(themeAsJson) as Theme[]);
    } catch (e) {
      return {isValid: false, errors: [e.message], hasWarnings: false};
    }
  }

  function validateImage(result: ThemesResult) {
    for (const theme of result.themes) {
      let error =  theme.imageOpacity !== undefined && theme.imageOpacity < 0 || theme.imageOpacity > 100 ? `ImageOpacity in theme '${theme.name}' must be between 0 and 100.` : '';
      addErrors(result, error);

      error =  theme.imageBlur !== undefined && theme.imageBlur < 0 || theme.imageBlur > 20 ? `ImageBlur in theme '${theme.name}' must be between 0 and 20.` : '';
      addErrors(result, error);

      theme.isImageAvailable = isAvailable(theme.image);
      theme.image = theme.isImageAvailable ? path.posix.normalize(theme.image.replace(new RegExp('\\\\', 'g'), '/')) : '';
    }
  }

  function validate(result: ThemesResult, enrich = false) {
    validateLength(result);
    validateRequiredFields(result, enrich);
    validateImage(result);
    validateColors(result, enrich);
    validatePadding(result);
    validateDefault(result);
    removeOldFields(result);
  }

  function removeOldFields(result: ThemesResult) {
    for (const theme of result.themes) {
      if(theme['scrollbackLines']) {
        delete theme['scrollbackLines'];
      }
    }
  }

  function validateDefault(result: ThemesResult) {
    if (result.themes.filter(t => t.isDefault).length === 0) {
      result.themes[0].isDefault = true;
    }
  }

  function isAvailable(p: string): boolean {
    if (!p) {
      return false;
    }
    return p.startsWith('http') || fs && fs.existsSync(p);
  }

  function validateLength(result: ThemesResult) {
    const error = !(result.themes && result.themes.length > 0) ? 'No theme is defined.' : '';
    addErrors(result, error);
  }

  function validatePadding(result: ThemesResult, enrich = false) {
    if (!result.isValid) {
      return;
    }
    for (const theme of result.themes) {
      let paddings = theme.padding ? theme.padding.split(new RegExp('\\s')).filter(s => !isNaN(Number(s.trim()))).map(p => Number(p.trim())) : [];
      paddings = paddings.map(p => p < 1 ? 1 : p);
      if (paddings.length === 0) {
        theme.paddingAsArray = [1, 1, 1, 1];
      }
      if (paddings.length === 1) {
        theme.paddingAsArray = [1, paddings[0], 1, paddings[0]];
      }
      if (paddings.length >= 2) {
        theme.paddingAsArray = [1, paddings[0], 1, paddings[1]];
      }
      if (theme.removeFullScreenAppPadding === undefined) {
        theme.removeFullScreenAppPadding = true;
      }
    }
  }

  function validateRequiredFields(result: ThemesResult, enrich = false) {
    if (!result.isValid) {
      return;
    }
    for (const theme of result.themes) {
      let error = !theme.colors ? `Theme '${theme.name}' has no 'colors' attribute.` : '';
      addErrors(result, error);

      if (enrich && !theme.fontsize) {
        theme.fontsize = 14;
      }
      error = !theme.fontsize ? `Theme '${theme.name}' has no 'fontsize' attribute.` : '';
      addErrors(result, error);

      if (enrich && !theme.appFontsize) {
        theme.appFontsize = 14;
      }
      error = !theme.appFontsize ? `Theme '${theme.name}' has no 'appFontsize' attribute.` : '';
      addErrors(result, error);

      if (enrich && !theme.fontFamily) {
        theme.fontFamily = 'monospace';
      }
      error = !theme.fontFamily ? `Theme '${theme.name}' has no 'fontFamily' attribute.` : '';
      addErrors(result, error);

      if (enrich && !theme.appFontFamily) {
        theme.appFontFamily = 'Roboto';
      }
      error = !theme.appFontFamily ? `Theme '${theme.name}' has no 'appFontFamily' attribute.` : '';
      addErrors(result, error);

      if (enrich && !theme.cursorWidth) {
        theme.cursorWidth = 3;
      }
      error = !theme.cursorWidth ? `Theme '${theme.name}' has no 'cursorWidth' attribute.` : '';
      addErrors(result, error);

      if (enrich && theme.cursorBlink === undefined) {
        theme.cursorBlink = true;
      }
      error = theme.cursorBlink === undefined ? `Theme '${theme.name}' has no 'cursorBlink' attribute.` : '';
      addErrors(result, error);

      if (enrich && !theme.cursorStyle) {
        theme.cursorStyle = 'bar';
      }
      error = !theme.cursorStyle ? `Theme '${theme.name}' has no 'cursorStyle' attribute.` : '';
      addErrors(result, error);

      if (enrich && !theme.prompt) {
        theme.prompt = '⯈';
      }
      error = !theme.prompt ? `Theme '${theme.name}' as no 'prompt' attribute.` : '';
      addErrors(result, error);

      if (enrich && !theme.imageOpacity) {
        theme.imageOpacity = 0;
      }

      if (enrich && !theme.imageBlur) {
        theme.imageBlur = 0;
      }

      if (enrich && theme.enableLigatures === undefined) {
        theme.enableLigatures = false;
      }

      if (enrich && theme.enableWebgl === undefined) {
        theme.enableWebgl = false;
      }

      if (enrich && !theme.fontWeight) {
        theme.fontWeight = 'normal';
      }

      if (enrich && !theme.fontWeightBold) {
        theme.fontWeightBold = 'bold';
      }

      if (enrich && (theme.promptVersion === null || theme.promptVersion === undefined)) {
        theme.promptVersion = 1;
      }

      if (enrich && !theme.enableWebgl) {
        theme.enableWebgl = false;
      }

      if (enrich && theme.isDarkModeTheme === undefined) {
        theme.isDarkModeTheme = false;
      }
      if (theme.isDefault === undefined ) {
        theme.isDefault = theme['isActive'];
        theme['isActive'] = undefined;
      }
    }
  }

  function validateColors(result: ThemesResult, enrich = false) {
    if (!result.isValid) {
      return;
    }
    for (const theme of result.themes) {
      if (enrich && !theme.colors.cursor) {
        theme.colors.cursor = theme.colors.highlight;
      }
      if (enrich && !theme.colors.commandSuccess) {
        theme.colors.commandSuccess = theme.colors.green;
      }
      if (enrich && !theme.colors.commandError) {
        theme.colors.commandError = theme.colors.red;
      }
      if (enrich && !theme.colors.commandRunning) {
        theme.colors.commandRunning = theme.colors.blue;
      }
      if (enrich && (!theme.colors.promptColors || theme.colors.promptColors.length == 0)) {
        const foregroundColor = theme.colors['prompt'] || ColorName.cyan;
        const backgroundColor = theme.colors['promptBackground'] || ColorName.black;
        if (!theme.colors.promptColors) {
          theme.colors.promptColors = [];
        }
        while(theme.colors.promptColors.length < 2) {
          theme.colors.promptColors.push({foreground: foregroundColor, background: backgroundColor});
        }
      }

      let error = !Color.isValid(theme.colors.highlight) ? createColorErrorMessage('highlight', theme.name) : '';
      addErrors(result, error);
      error = !Color.isValid(theme.colors.background) ? createColorErrorMessage('background', theme.name) : '';
      addErrors(result, error);
      error = !Color.isValid(theme.colors.foreground) ? createColorErrorMessage('foreground', theme.name) : '';
      addErrors(result, error);
      error = !Color.isValid(theme.colors.black) ? createColorErrorMessage('black', theme.name) : '';
      addErrors(result, error);
      error = !Color.isValid(theme.colors.white) ? createColorErrorMessage('white', theme.name) : '';
      addErrors(result, error);
      error = !Color.isValid(theme.colors.green) ? createColorErrorMessage('green', theme.name) : '';
      addErrors(result, error);
      error = !Color.isValid(theme.colors.blue) ? createColorErrorMessage('blue', theme.name) : '';
      addErrors(result, error);
      error = !Color.isValid(theme.colors.yellow) ? createColorErrorMessage('yellow', theme.name) : '';
      addErrors(result, error);
      error = !Color.isValid(theme.colors.cyan) ? createColorErrorMessage('cyan', theme.name) : '';
      addErrors(result, error);
      error = !Color.isValid(theme.colors.red) ? createColorErrorMessage('red', theme.name) : '';
      addErrors(result, error);
      error = !Color.isValid(theme.colors.magenta) ? createColorErrorMessage('magenta', theme.name) : '';
      addErrors(result, error);
      error = !Color.isValid(theme.colors.brightWhite) ? createColorErrorMessage('brightWhite', theme.name) : '';
      addErrors(result, error);
      error = !Color.isValid(theme.colors.brightBlack) ? createColorErrorMessage('brightBlack', theme.name) : '';
      addErrors(result, error);
      error = !Color.isValid(theme.colors.brightRed) ? createColorErrorMessage('brightRed', theme.name) : '';
      addErrors(result, error);
      error = !Color.isValid(theme.colors.brightGreen) ? createColorErrorMessage('brightGreen', theme.name) : '';
      addErrors(result, error);
      error = !Color.isValid(theme.colors.brightYellow) ? createColorErrorMessage('brightYellow', theme.name) : '';
      addErrors(result, error);
      error = !Color.isValid(theme.colors.brightBlue) ? createColorErrorMessage('brightBlue', theme.name) : '';
      addErrors(result, error);
      error = !Color.isValid(theme.colors.brightMagenta) ? createColorErrorMessage('brightMagenta', theme.name) : '';
      addErrors(result, error);
      error = !Color.isValid(theme.colors.brightCyan) ? createColorErrorMessage('brightCyan', theme.name) : '';
      addErrors(result, error);
    }
  }

  function createColorErrorMessage(colorName: string, themeName: string): string {
    return `Color ${colorName} in theme '${themeName}' is not correct.`;
  }
}

export interface ThemesResult extends ParseResult {
  themes?: Theme[];
}
