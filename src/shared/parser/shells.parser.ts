import {ShellConfig} from '../models/settings';
import * as JSON5 from 'json5';
import {ParseResult} from './settings.parser';
import {addErrors} from './parser';

export namespace ShellConfigParser {

  export function validateShellConfig(shellConfigs: ShellConfig[], enrich = false): ShellConfigResult {
    try {
      const result = {isValid: true, shellConfigs: shellConfigs, hasWarnings: false};
      validate(result, enrich);
      return result;
    } catch (e) {
      return {isValid: false, errors: [e.message], hasWarnings: false};
    }
  }

  export function parseShellConfig(shellsAsJson: string): ShellConfigResult {
    try {
      return validateShellConfig(JSON5.parse(shellsAsJson) as ShellConfig[]);
    } catch (e) {
      return {isValid: false, errors: [e.message], hasWarnings: false};
    }
  }

  function validate(result: ShellConfigResult, enrich: boolean) {
    validateRequiredFields(result, enrich);
  }

  function validateRequiredFields(result: ShellConfigResult, enrich: boolean) {
    if (!result.isValid) {
      return;
    }
    for (const shellConfig of result.shellConfigs) {
      let error = !shellConfig.name ? 'Shell as no \'name\' attribute.' : '';
      addErrors(result, error);
      error = !shellConfig.type ? `Shell '${shellConfig.name}' as no 'type' attribute.` : '';
      addErrors(result, error);
      if (enrich && !shellConfig.id) {shellConfig.id = shellConfig.name.toLowerCase().replace(/\s/g, ''); }
      error = !shellConfig.id ? `Shell '${shellConfig.name}' as no 'id' attribute.` : '';
      if(shellConfig.injectionType === undefined) {
        if(shellConfig.remoteType) {
          shellConfig.injectionType = 'Remote';
        } else if(shellConfig['isAdvancedEnabled'] === undefined || shellConfig['isAdvancedEnabled'] === true) {
          shellConfig.injectionType = 'Auto';
        } else {
          shellConfig.injectionType = 'Manual';
        }
        delete shellConfig['hasSubshell'];
        delete shellConfig['isAdvancedEnabled'];
      }
    }
  }
}

export interface ShellConfigResult extends ParseResult {
  shellConfigs?: ShellConfig[];
}
