
export namespace IpcChannel {
  export const Close = 'close';
  export const Minimize = 'minimize';
  export const Maximize = 'maximize';
  export const Unmaximize = 'unmaximize';
  export const Relaunch = 'relaunch';
  export const WindowStateChanged = 'windowStateChanged';
  export const OpenDevTools = 'openDevTools';
  export const Startup = 'startup';
  export const SettingsLoaded = 'settingsLoaded';
  export const AppDirs = 'appDirs';
  export const Version = 'version';
  export const OnResize = 'onResize';
  export const UpdateInfo = 'updateInfo';
  export const InstallUpdate = 'installUpdate';
  export const Session = 'session';
  export const DarkMode = 'darkMode';
  export const InstallTheme = 'installTheme';
  export const SaveThemeConfig = 'saveThemeConfig';
  export const SaveSettings = 'saveSettings';
  export const SaveGeneralConfig = 'saveGeneralConfig';
  export const SaveAutocompleteConfig = 'saveAutocompleteConfig';
  export const SaveShellsConfig = 'saveShellsConfig';
  export const SaveShortcutConfig = 'saveShortcutConfig';
  export const UninstallTheme = 'uninstallTheme';
  export const GetAvailableShells = 'getAvailableShells';
  export const GetAvailableFonts = 'getAvailableFonts';
  export const GetAvailableAppFonts = 'getAvailableAppFonts';
  export const OpenPath = 'openPath';
  export const OpenExternal = 'openExternal';
  export const OpenFileDialog = 'openFileDialog';
  export const LoadScriptContent = 'loadScriptContent';
  export const Http = 'http';

  export const DownloadFile = 'downloadFile';
  export const OpenNewWindow = 'openNewWindow';
  export const ChangeWindowName = 'changeWindowName';
  export const DB = 'db';
}

