# Cogno
_A terminal with a self learning autocompleter._ 
![alt text](https://cogno.rocks/images/theme4.jpg)

# Download
> Cogno is a alpha-version at the moment. It may contains some bugs and is not production ready.

[Windows-Installer](https://mistral-update.s3.eu-central-1.amazonaws.com/cogno-installer.exe "Latest stable version")

[Windows-Installer (Nightly)](https://mistral-update.s3.eu-central-1.amazonaws.com/cogno-installer-nightly.exe "Latest nightly version")

[MacOs Silicon](https://mistral-update.s3.eu-central-1.amazonaws.com/cogno.dmg "Latest stable version")

[MacOs Silicon (Nightly)](https://mistral-update.s3.eu-central-1.amazonaws.com/cogno-nightly.dmg "Latest nightly version")

[Linux .deb](ttps://mistral-update.s3.eu-central-1.amazonaws.com/cogno.deb "Latest stable version")

[Linux .deb (Nightly)](ttps://mistral-update.s3.eu-central-1.amazonaws.com/cogno.deb-nightly "Latest nightly version")


# Motivation (Why the hell a new terminal?)

I am forgetful and lazy. I want to type as little as possible and think as little as possible. That's why I wanted a terminal with a self learning autocompleter. And here it is.

# Features

**Context aware autocompletion** 

Cogno try to figgure out, what you want to type and offers matching suggestions.

**Easy to style.**

Simply design the terminal with a WYSIWYG theme editor. No settings files. So there is already this settings file, you can use that too, if you want. But an editor is much more convenient.

**Configurable shortcuts**

You can change the existing shortcuts or even better - put a command on a shortcut.

**Different shells**
    
Cogno supports GitBash, Powershell, ZSH and Bash for Windows, MacOS (Apple Silicon), and Ubuntu (Debian). There are a few restrictions. For example Bash version >= 4 and Windows 10 version > 1909, and probably a few more. 🤔


**Paste history**

If you need to paste something again, you will find it in your paste history.

**Tabs, Panes and Workspaces**

Cogno supports tabs and freely rearrangeable split panes which you can also save as a workspace.

# Support
* Give Cogno a star. 
* Talk about and show it to other people.
* Contribute by issue a [merge request](https://gitlab.com/cogno-rockers/cogno/-/merge_requests), see also [contribute section.](https://gitlab.com/biberklatsche/cogno#contribute)
* Do you have more ideas? Open a [feature request](https://gitlab.com/cogno-rockers/cogno/-/issues/new).
* Or [Donate](https://ko-fi.com/cognorocks). 

Every support would be awesome! Thank you!

# Tech/framework used
Cogno is developed in Typescript/HTML/CSS.
I use a lot of other Open Source Projects, but the main frameworks are:

- electron
- angular
- node-pty
- xterm

# FAQ?
## Why javascript for a terminal?
Because I can, it really is that simple. Building something like this takes a lot of time and I have to manage it as best I can. That's why I had to take something that I know and that is well supported. With the above technologies, I have exactly that.

## And the high memory consumption and poor performance?
Well, that plays a subordinate role for me. Features are playing a bigger role at the moment. I have such a hell of a developer machine, there it runs like clockwork. And even on my old notebook it runs quite well. Tauri might be a solution to that problem.

## Do you use it in production?
Yes, every day. Most of my colleagues, too, by the way. It's pretty well tested on Window 10 and 11, with the GitBash. On the Mac (Apple Silicon) I use it with ZSH. Cogno unfortunately only works from Bash 4.

# Contribute
You'll need a recent version of Node installed. (Be aware to select "Tools for Native Modules" during the node install process on windows)

Clone this repository: 
```
git clone git@gitlab.com:biberklatsche/cogno.git

# or

git clone https://gitlab.com/biberklatsche/cogno.git
```

Install the dependencies:
```
npm install
```

Check if you can build the project:
```
npm run build
```

Start Cogno in dev-mode:
```
npm start
```

Run the unittests:
```
npm test
```

If you wan't to build a executable run:
```
npm run build
# you will find the executable in the release folder
```
or 

```
npm run build
# you will find the executable in the release folder
```

or

```
npm run build
# you will find the executable in the release folder
```
