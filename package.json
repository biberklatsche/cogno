{
  "name": "cogno",
  "version": "1.7.0-nightly.6",
  "description": "A Terminal with a self learning autocompleter.",
  "license": "GPL-3.0-or-later",
  "homepage": "https://cogno.rocks",
  "author": {
    "name": "Lars Wolfram",
    "email": "lars@cogno.rocks"
  },
  "keywords": [
    "terminal",
    "emulator",
    "angular",
    "electron",
    "typescript"
  ],
  "main": "src/main/main.js",
  "private": false,
  "scripts": {
    "ng": "ng",
    "postinstall": "electron-builder install-app-deps && npm run patch",
    "patch": "node ./build/patch",
    "start": "concurrently \"npm:ng:serve\" \"npm:electron:serve\"",
    "start:gc": "concurrently \"npm:ng:serve\" \"npm:electron:serve:gc\"",
    "analyze": "ng build --stats-json && webpack-bundle-analyzer dist/stats.json",
    "build:prod": "npm run electron:tsc && ng build -c production",
    "ng:serve": "ng serve --port 4600",
    "electron:tsc": "tsc -p tsconfig.main.json",
    "electron:serve": "wait-on http-get://localhost:4600/ && npm run electron:tsc && electron . --serve --trace-warnings",
    "electron:serve:gc": "wait-on http-get://localhost:4600/ && npm run electron:tsc && electron . --serve --js-flags=\"--expose-gc\"",
    "---Build And Publish Installer---": "",
    "publish": "npm run generate-third-party-license && npm run build:prod && node ./build/build --publish",
    "publish:x64": "npm run generate-third-party-license && npm run build:prod && node ./build/build --publish --x64",
    "publish:rpm": "npm run generate-third-party-license && npm run build:prod && node ./build/build --publish --rpm",
    "---Build Installer---": "",
    "build": "npm run build:prod && node ./build/build",
    "build:x64": "npm run build:prod && node ./build/build --x64",
    "build:rpm": "npm run build:prod && node ./build/build --rpm",
    "---Test---": "",
    "test": "jest",
    "test:coverage": "jest --coverage",
    "---Third Party License---": "",
    "generate-third-party-license": "tplgen ./build/THIRD-PARTY-LICENSES.txt --withDev"
  },
  "build": {
    "productName": "Cogno",
    "npmRebuild": true,
    "directories": {
      "output": "release/"
    },
    "files": [
      {
        "from": "dist/renderer",
        "to": "renderer"
      },
      {
        "from": "dist/src/main",
        "to": "src/main"
      },
      {
        "from": "src/scripts",
        "to": "src/scripts"
      },
      {
        "from": "src/shared",
        "to": "src/shared"
      },
      "package.json"
    ],
    "extraFiles": [
      "build/THIRD-PARTY-LICENSES.txt",
      "src/scripts/bash.sh",
      "src/scripts/zsh.sh",
      "src/scripts/powershell.ps1"
    ],
    "mac": {
      "target": [
        "dmg",
        "zip"
      ],
      "hardenedRuntime": true,
      "gatekeeperAssess": false,
      "entitlements": "build/darwin/entitlements.mac.plist",
      "entitlementsInherit": "build/darwin/entitlements.mac.plist"
    },
    "linux": {
      "category": "Development"
    },
    "win": {
      "target": [
        "nsis"
      ]
    },
    "nsis": {
      "include": "build/win32/installer.nsh",
      "oneClick": true,
      "allowElevation": false,
      "perMachine": false,
      "allowToChangeInstallationDirectory": false,
      "runAfterFinish": true,
      "createStartMenuShortcut": true,
      "deleteAppDataOnUninstall": false
    },
    "publish": [
      {
        "provider": "s3",
        "bucket": "mistral-update"
      }
    ]
  },
  "jest": {
    "preset": "jest-preset-angular",
    "setupFilesAfterEnv": [
      "<rootDir>/setup-jest.ts"
    ],
    "moduleFileExtensions": [
      "ts",
      "json",
      "js",
      "node"
    ],
    "testMatch": [
      "**/*.spec.ts"
    ],
    "rootDir": "",
    "collectCoverageFrom": [
      "<rootDir>/src/renderer/app/**/*.ts",
      "<rootDir>/src/shared/**/*.ts"
    ],
    "coverageDirectory": "coverage"
  },
  "dependencies": {
    "@ctrl/tinycolor": "4.1.0",
    "@seald-io/nedb": "4.0.4",
    "chokidar": "4.0.3",
    "concurrently": "9.1.2",
    "electron-localshortcut": "3.2.1",
    "electron-log": "5.3.0",
    "electron-updater": "6.3.9",
    "fontmanager-redux": "1.1.0",
    "json5": "2.2.3",
    "node-pty": "1.1.0-beta11",
    "ps-tree": "1.2.0",
    "rxjs": "7.8.1",
    "tar": "7.4.3",
    "uuid": "11.0.5"
  },
  "optionalDependencies": {
    "windows-native-registry": "3.2.2"
  },
  "devDependencies": {
    "@angular-builders/custom-webpack": "19.0.0",
    "@angular-slider/ngx-slider": "19.0.0",
    "@angular/animations": "19.1.4",
    "@angular/build": "19.1.5",
    "@angular/cli": "19.1.5",
    "@angular/common": "19.1.4",
    "@angular/compiler": "19.1.4",
    "@angular/compiler-cli": "19.1.4",
    "@angular/core": "19.1.4",
    "@angular/forms": "19.1.4",
    "@angular/platform-browser": "19.1.4",
    "@angular/platform-browser-dynamic": "19.1.4",
    "@electron/notarize": "2.3.2",
    "@mdi/js": "7.4.47",
    "@types/electron-localshortcut": "3.1.3",
    "@types/jest": "29.5.14",
    "@types/luxon": "3.4.2",
    "@types/node": "22.13.0",
    "@types/ps-tree": "1.1.6",
    "@types/tabulator-tables": "6.2.3",
    "@types/uuid": "10.0.0",
    "@xterm/addon-canvas": "0.7.0",
    "@xterm/addon-fit": "0.10.0",
    "@xterm/addon-ligatures": "0.9.0",
    "@xterm/addon-search": "0.15.0",
    "@xterm/addon-unicode11": "0.8.0",
    "@xterm/addon-web-links": "0.11.0",
    "@xterm/addon-webgl": "0.18.0",
    "@xterm/xterm": "5.5.0",
    "angular-eslint": "19.0.2",
    "cross-env": "7.0.3",
    "electron": "35.0.0",
    "electron-builder": "26.0.10",
    "eslint": "9.19.0",
    "jest": "29.7.0",
    "jest-preset-angular": "14.5.1",
    "js-search": "2.0.1",
    "license-checker": "25.0.1",
    "lodash.clonedeep": "4.5.0",
    "lodash.isequal": "4.5.0",
    "luxon": "3.5.0",
    "mixpanel": "0.18.0",
    "ngx-color-picker": "17.0.0",
    "path": "0.12.7",
    "semver": "7.7.0",
    "tabulator-tables": "6.3.1",
    "tplgen": "1.0.0",
    "ts-jest": "29.2.5",
    "ts-node": "10.9.2",
    "typescript": "5.5.4",
    "typescript-eslint": "8.22.0",
    "uglify-js": "3.19.3",
    "wait-on": "8.0.2",
    "zone.js": "~0.15.0"
  },
  "engines": {
    "node": ">=20.18.0"
  }
}
