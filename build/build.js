const packageJson = require('../package.json');
const exec = require('child_process').exec;
const path = require('path');
const os = require('os');
const {uglify} = require("./uglify");

const platform = process.platform;
const arch = process.argv.find(a => a === '--x64') ? 'x64' : 'arm64'
const target = process.argv.find(a =>  a === '--rpm') ? 'rpm' : 'deb'

function isPublishBuild() {
  return process.argv.find(a =>  a === '-p' || a === '--publish');
}

function isNightlyVersion() {
  return packageJson.version.includes('-nightly');
}

function getOsDependencies() {
  const baseName = packageJson.name.toLowerCase();
  const nightlyName = isNightlyVersion() ? '-nightly' : '';
  const iconPath = path.join('build', platform, isNightlyVersion() ? 'icon/nightly/' : 'icon/normal/');
  switch (platform) {
    case 'linux': {
      let version;
      if (target === 'rpm') {
        version = isNightlyVersion() ? packageJson.version.replace('-nightly', '-rpmnightly') : packageJson.version + '-rpm'
      } else {
        version = packageJson.version
      }
      return {artifact: baseName + nightlyName + `.${target}`, icon: path.join(iconPath, 'icon.svg'), version: version};
    }
    case 'darwin': {
      if (arch === 'x64') {
        const version = isNightlyVersion() ? packageJson.version.replace('-nightly', '-x64nightly') : packageJson.version + '-x64'
        return {artifact: baseName + '-x64' + nightlyName + '.dmg', icon: path.join(iconPath, 'icon.png'), version: version};
      } else {
        return {artifact: baseName + nightlyName + '.dmg', icon: path.join(iconPath, 'icon.png'), version: packageJson.version};
      }
    }
    case 'win32': {
      const artifactName = baseName + nightlyName + '.exe';
      const installerName = baseName + '-installer' + nightlyName + '.exe';
      return {artifact: artifactName, installer: installerName, icon: path.join(iconPath, 'icon.ico'), version: packageJson.version};
    }
  }
}

function getCommand() {
  const isPublish = isPublishBuild();
  const buildOrPublish = isPublish ? '--publish always' : 'build';
  const notarize = isPublish ? '--c.afterSign build/darwin/notarize.js' : '';
  const deps = getOsDependencies();
  console.log('start build for', deps);
  switch (platform) {
    case 'linux': {
      return `electron-builder ${buildOrPublish} --linux --config.linux.target=${target} -c.extraMetadata.version=${deps.version} --config.linux.icon=${deps.icon} --config.linux.artifactName=${deps.artifact}`;
    }
    case 'darwin': {
      return `electron-builder ${buildOrPublish} --mac --${arch} --config.dmg.artifactName=${deps.artifact} -c.extraMetadata.version=${deps.version} --config.mac.icon=${deps.icon} ${notarize}`;
    }
    case 'win32': {
      let pathToSecrets = path.join(os.homedir(), '.code-signing', 'secrets.json');
      let secrets;
      try
      {
        secrets = require(pathToSecrets);
      } catch {
        console.warn(`Missing '${pathToSecrets}'. Could not sign app.`);
      }
      if(!!secrets) {
        process.env.CSC_LINK = path.join(os.homedir(), '.code-signing', 'cogno.p12');
        process.env.CSC_KEY_PASSWORD = secrets.password;
      }
      return `electron-builder ${buildOrPublish} --windows -c.extraMetadata.version=${deps.version} --config.win.artifactName=${deps.artifact} --config.nsis.artifactName=${deps.installer} --config.win.icon=${deps.icon} --config.win.signAndEditExecutable=${!!secrets}`;
    }
  }
}

uglify(path.resolve('./src/main'), path.resolve('./dist/src/main'));
uglify(path.resolve('./src/shared'), path.resolve('./dist/src/shared'));
console.log('Uglification completed.');

const command = getCommand();
const commandOutput = exec(command);
console.log('Run command', command);
commandOutput.stdout.on('data', function (data) {
  console.log(data.toString());
});
commandOutput.stderr.on('data', function (data) {
  console.log(data.toString());
});
commandOutput.on('exit', function (code) {
  console.log('child process exited with code ' + code.toString());
});
