const { notarize } = require('@electron/notarize');
const path = require('path');

exports.default = async function notarizing(context) {

  const { electronPlatformName, appOutDir } = context;
  if (electronPlatformName !== 'darwin') {
    return;
  }

  const homedir = require('os').homedir();
  const filePath = path.resolve(homedir, '.apple', 'credentials');
  const data = await require('fs/promises').readFile(filePath);
  const settings = JSON.parse(data.toString());

  console.log(settings);

  const appName = context.packager.appInfo.productFilename;

  console.log("Notarize APP.", appOutDir, appName);

  return await notarize({
    appBundleId: 'rocks.cogno.cogno',
    appPath: `${appOutDir}/${appName}.app`,
    appleId: settings.appleId,
    teamId: settings.teamId,
    appleIdPassword: settings.appleIdPassword
  });
};
