
const fs = require("fs");
const path = require('path');
const UglifyJS = require("uglify-js");
function processFiles(dir, sourceDir, outputDir) {
  const files = fs.readdirSync(dir);

  files.forEach((filename) => {
    const filePath = path.join(dir, filename);

    if (fs.statSync(filePath).isDirectory()) {
      // If it's a directory, recursively process its contents
      processFiles(filePath, sourceDir, outputDir);
    } else if (filename.endsWith('.js')) {
      // If it's a JavaScript file, uglify it
      const code = fs.readFileSync(filePath, 'utf-8');
      const result = UglifyJS.minify(code);

      // Create subdirectories in the output directory if they don't exist
      const relativePath = path.relative(sourceDir, filePath);
      const outputPath = path.join(outputDir, relativePath);

      // Ensure the directory structure exists
      fs.mkdirSync(path.dirname(outputPath), { recursive: true });

      // Write the minified code to the output file
      fs.writeFileSync(outputPath, result.code);
    }
  });
}

function uglify(sourceDir, outputDir){
  processFiles(sourceDir, sourceDir, outputDir);
}

module.exports = { uglify }
